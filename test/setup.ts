// setup.js file
import { config } from "@vue/test-utils";

/* Vue i18n*/
import i18n from "../src/plugins/vue-i18n";

/* Vue Router */
import router from "../src/router";

/* Bootstrap Vue Next */
import createBootstrap from "../src/plugins/bootstrap-vue-next";

/* Stream Polyfill */
import "../src/plugins/stream-poly";

/* vue-dompurify-html */
import VueDOMPurifyHTML, {
  config as dompurifyConfig,
} from "../src/plugins/vue-dompurify-html";

/* vue-select */
import VueSelect from "vue-select";
import "vue-select/dist/vue-select.css";

/* vue-multi-select */
import Multiselect from "../src/plugins/vue-multiselect";

/* vue-observer-visibility */
import VueObserveVisibility from "vue-observe-visibility";

/* @coscine/form-generator */
import FormGenerator from "../src/plugins/form-generator";

/* Corporate Design */
import "../src/assets/scss/_custom.scss";
import "../src/assets/css/_custom.css";

/* Other */
import "windi.css";

config.global.plugins = [
  i18n,
  router,
  [VueDOMPurifyHTML, dompurifyConfig],
  createBootstrap(),
  VueObserveVisibility,
];
config.global.components = {
  VueSelect,
  Multiselect,
  FormGenerator,
};
