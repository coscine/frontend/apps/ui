# Notification Toast Orchestrator Component Documentation

This component is responsible for displaying notification toasts to the user based on a notification queue. It ensures that notifications are shown appropriately, considering the user's session state, and handles internationalization for the toast content.

## Overview

- **Purpose**: To orchestrate the display of notification toasts from a queue, managing their lifecycle and presentation.
- **Key Features**:
  - Retrieves notifications from a centralized store (`notificationStore`).
  - Displays toasts using `bootstrap-vue-next`'s toast system.
  - Prevents notifications from displaying when the user's session has expired.
  - Supports internationalization using `vue-i18n`, including dynamic placeholders within the toast content.

## Component Structure

### Template

- Renders a `<BToastOrchestrator />` component, which manages the placement and stacking of toasts in the UI.
 
  > NOTE: **Only one** such component is needed to globally render bootstrap toasts in the application. This is why other programatically triggered toasts do not require extra definition of this element, as it is already provided by the `NotificationToast.vue` component. For more information, check the [bootstrap-vue-next documentation](https://bootstrap-vue-next.github.io/bootstrap-vue-next/docs/composables/useToastController.html).

### Script

#### Imports

- **Stores**:
  - `useNotificationStore`: Accesses the notification queue.
  - `useLoginStore`: Checks the user's login status.
- **Types**:
  - `NotificationToast`: Defines the structure of a notification toast.
  - `NotificationTranslationProps`: Defines properties for internationalizing the toast content.
- **Libraries**:
  - `Translation` from `vue-i18n`: Handles internationalized content within toasts.
  - `BToast`, `useToast` from `bootstrap-vue-next`: Manages toast components and their display.

#### Setup Function

- Initializes the required stores and the toast controller for displaying toasts.

#### Computed Properties

- **`isSessionExpired`**: Determines if the user's session has expired based on the login status.
- **`currentToast`**: Retrieves the next notification toast from the queue.

#### Watchers

- **`currentToast`**: Monitors changes to the `currentToast` property.
  - When a new toast is available:
    - Checks if the session is active (`!this.isSessionExpired`).
    - If active, displays the toast using `makeToast(toast)`.
    - Removes the toast from the queue after displaying it.

#### Methods

##### `makeToast(toast: NotificationToast)`

- **Purpose**: Constructs and displays a notification toast based on the provided `toast` object.
- **Logic**:
  - Sets a default position (`"bottom-end"`) if none is specified.
  - If the toast includes a component for content (`toast.component`):
    - Extracts internationalization options and placeholders.
    - Builds the toast content using `vue-i18n`'s `Translation` component.
    - Dynamically inserts any placeholders into the content.
  - If `toast.autoHide` is `false`, sets the toast's duration to a large number to keep it visible indefinitely.
  - Displays the toast using the `toastController`.

## Internationalization Support

- Supports internationalization for toast content using `vue-i18n`.
- Toast messages can include dynamic placeholders, replaced with components or values at runtime.
- Placeholders are specified in the `toast.component.placeholders` object.

## Behavior Details

- **Session Awareness**:
  - Checks if the user's session is active before displaying toasts.
  - If the session has expired, no new toasts are displayed.
  - Prevents redundant or confusing notifications when the user is logged out.

- **Notification Queue Management**:
  - Retrieves notifications from `notificationStore.notificationQueue`.
  - Processes one notification at a time (the first in the queue).
  - After displaying a toast, removes it from the queue using `deleteNotification(toast)`.

- **Toast Display Logic**:
  - Utilizes `bootstrap-vue-next`'s toast system for consistent styling and behavior.
  - If `autoHide` is disabled, the toast remains visible indefinitely until manually dismissed.

## Key Points

- **Modular Design**: Separates the toast display logic into `makeToast` for clean code and easy maintenance.
- **Responsive Notifications**: Uses watchers to ensure notifications are promptly displayed as they are added to the queue.
- **Session Handling**: Integrates session checks to prevent unnecessary notifications when the user is not logged in.
- **Dynamic Content**: Supports complex toast content with dynamic components and placeholders, enhancing user experience.

## External Dependencies

- **`bootstrap-vue-next`**: Provides UI components like `BToast` and `BToastOrchestrator` for toast management.
- **`vue-i18n`**: Manages internationalization of toast content.
- **`useNotificationStore`**: Supplies the notification queue and methods to manage notifications.
- **`useLoginStore`**: Provides the user's login status to determine if toasts should be displayed.

## Usage Example

- To enqueue a new notification toast, add it to the `notificationStore.notificationQueue`.
- The component will automatically detect the new toast and display it if the session is active.

## Conclusion

This component efficiently manages the display of notification toasts, ensuring that users receive timely and relevant information. It leverages Vue's reactivity and external libraries to provide a robust notification system that respects the user's session state and supports internationalized content.
