# Session Expiration Component Documentation

This component handles session expiration logic by displaying warnings and notifications to the user as their session approaches expiration or has expired. It ensures that users are aware of their session status and can take appropriate action, such as re-authenticating.

## Overview

- **Purpose**: To monitor the user's session time and provide visual cues (toasts and overlays) as the session nears expiration or has expired.
- **Key Features**:
  - Displays a warning toast with a countdown progress bar two minutes before session expiration.
  - Shows an overlay with an expiration message when the session has expired.
  - Recalculates and updates timers on significant UI events to ensure accuracy.

## Session Time Calculation

The remaining session time is recalculated in the following scenarios:

1. **On Every Route Change**: Whenever the user navigates to a different route, the remaining session time is recalculated. This is handled in `router/index.ts`.

2. **After UI Stops Loading**: Each time the application finishes loading (e.g., after an API call), the remaining session time is recalculated. This leverages the `isLoading` prop in `App.vue`.

## Warning Toast Logic

- **Warning Threshold**: Set to two minutes (`warningThreshold` data variable).
- **Triggering the Warning**:
  - If the remaining session time is more than the warning threshold, a timer (`warningTimeoutId`) is set to display the warning toast when the threshold is reached.
  - If the remaining time is less than the warning threshold, the warning toast is shown immediately.
- **Warning Toast Features**:
  - Contains a progress bar that counts down until the session expires.
  - Automatically adjusts the countdown on page refresh or significant UI events by recalculating `timeToExpiry`.

## Session Expiration Handling

- **Expiration Timeout**: A timer (`expirationTimeoutId`) is set to mark the session as expired when the remaining time reaches zero.
- **Post-Expiration Actions**:
  - The warning toast is hidden.
  - An overlay (`showOverlay`) is displayed with an expiration message.
  - The user is prompted to log in again.

## Component Breakdown

### Template (`<template>`)

- **Overlay (`<b-overlay>`)**:
  - Displays when `showOverlay` is `true`.
  - Contains the session expiration toast.
- **Session Expired Toast (`<b-toast>`)**:
  - Binds to `isSessionExpired`.
  - Displays a message informing the user that the session has ended.
  - Includes a "Login" button that triggers `redirectToLogin`.

### Script (`<script lang="ts">`)

#### Imports

- **Stores and Utilities**:
  - `useLoginStore`: Accesses the login state and session expiration timestamps.
  - `useToast`: Manages toast notifications.
  - `vue-i18n`: Handles internationalization.

#### Data Properties (`data()`)

- **Session State Flags**:
  - `showOverlay`: Controls the visibility of the session expiration overlay.
  - `isSessionExpired`: Indicates whether the session has expired.
- **Warning Toast Control**:
  - `showWarningToast`: Toggles the display of the warning toast.
  - `warningToast`: Reference to the warning toast instance.
- **Timing Variables**:
  - `timeToExpiry`: Stores the remaining session time in milliseconds.
  - `warningThreshold`: Set to 2 minutes (in milliseconds).
  - `warningTimeoutId` & `expirationTimeoutId`: Identifiers for the timers managing warnings and expiration.

#### Computed Properties (`computed`)

- **Session Information**:
  - `isLoggedIn`: Reflects the user's login status from `loginStore`.
  - `tokenExpirationTimestamp` & `cookieExpirationTimestamp`: Retrieve expiration timestamps.

#### Watchers (`watch`)

- **`isLoggedIn`**:
  - On login (`newVal` is `true`):
    - Resets session expiration flags.
    - Sets up new session timers.
  - On logout (`newVal` is `false`):
    - Clears timers.
    - Sets session as expired.
    - Displays overlay if the route requires authentication.

- **`tokenExpirationTimestamp` & `cookieExpirationTimestamp`**:
  - When these timestamps change, session timers are recalculated to adjust for any session time extensions or reductions.

- **`showWarningToast`**:
  - When `true`, the warning toast is created and displayed.
  - When `false`, any existing warning toast is removed.

#### Lifecycle Hooks

- **`mounted`**:
  - Invokes `setupSessionTimers()` to initialize timers when the component is mounted.

- **`unmounted`**:
  - Clears timers and removes any active warning toasts to prevent memory leaks.

#### Methods (`methods`)

##### `setupSessionTimers()`

- **Purpose**: Sets up timers based on the remaining session time.
- **Logic**:
  - Clears existing timers via `clearTimers()`.
  - Calculates `remainingTime` using `loginStore.calculateRemainingSessionTime()`.
  - If `remainingTime` is positive:
    - Determines if the warning should be shown immediately or after a delay.
    - Sets `warningTimeoutId` to display the warning toast when appropriate.
    - Sets `expirationTimeoutId` to mark the session as expired when time runs out.
  - If `remainingTime` is zero or negative:
    - Marks the session as expired immediately.

##### `clearTimers()`

- **Purpose**: Clears any active timers for warnings or expiration.
- **Actions**:
  - Clears `warningTimeoutId` and `expirationTimeoutId` if they are set.
  - Resets `showWarningToast` to `false` to hide any warning toasts.

##### `createAndShowWarningToast()`

- **Purpose**: Creates and displays the warning toast with a countdown.
- **Features**:
  - Uses `BToast` to display the warning.
  - Includes a progress bar indicating time until expiration.
  - Leverages `toastController` for displaying the toast.

##### `removeWarningToast()`

- **Purpose**: Removes the warning toast from the view.
- **Actions**:
  - Checks if `warningToast` is set.
  - Uses `toastController.remove` to remove the toast.
  - Resets `warningToast` to `undefined`.

##### `redirectToLogin()`

- **Purpose**: Redirects the user to the login page.
- **Actions**:
  - Utilizes `$router.push` to navigate to the login route.

---

## Internationalization

- All user-facing text is internationalized using `vue-i18n`'s `$t` method.
- Keys like `toast.sessionEnd.title`, `toast.sessionEnd.message`, and `toast.sessionEnd.link` are used to fetch the appropriate translations.

## External Dependencies

- **`bootstrap-vue-next`**: Provides UI components like `b-overlay` and `b-toast`.
- **`vue-i18n`**: Manages internationalization of strings.
- **`useLoginStore`**: Accesses authentication states and session timing.
- **`useToast`**: Manages toast notifications in the UI.

## Key Points to Note

- **Accurate Timing**: The component ensures that the timing for warnings and expiration is accurate by recalculating timers on significant events like route changes and after loading completes.
- **User Experience**: By providing a countdown and timely notifications, users are given ample warning before being logged out, improving the overall user experience.
- **Resource Management**: Timers and toasts are carefully managed and cleaned up to prevent memory leaks and unintended behavior.
