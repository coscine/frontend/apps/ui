/* Vue i18n*/
import i18n from "@/plugins/vue-i18n";

/* Pinia */
import pinia from "@/plugins/pinia";

/* Vue Router */
import router from "@/router";

/* Bootstrap Vue Next */
import createBootstrap from "@/plugins/bootstrap-vue-next";

/* Stream Polyfill */
import "@/plugins/stream-poly";

/* vue-dompurify-html */
import VueDOMPurifyHTML, { config } from "@/plugins/vue-dompurify-html";

/* vue-select */
import VueSelect from "vue-select";
import "vue-select/dist/vue-select.css";

/* vue-multi-select */
import MultiSelect from "@/plugins/vue-multiselect";

/* vue-observer-visibility */
import VueObserveVisibility from "vue-observe-visibility";

/* @coscine/form-generator */
import FormGenerator from "@/plugins/form-generator";

/* Corporate Design */
import "@/assets/scss/_custom.scss";
import "@/assets/css/_custom.css";

/* Other */
import App from "@/App.vue";
import "windi.css";

/* App Definition */
const app = createApp({
  render: () => h(App),
});

app.component("VSelect", VueSelect);
app.component("Multiselect", MultiSelect);
app.component("FormGenerator", FormGenerator);

app.use(router);
app.use(createBootstrap());
app.use(VueDOMPurifyHTML, config);
app.use(pinia);
app.use(i18n);
app.use(VueObserveVisibility);

// had to take it out from pinia.ts
pinia.use(({ store }) => {
  // add custom plugins
  store.router = markRaw(router);
});

app.mount("#app");
