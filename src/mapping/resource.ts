import { MapperConfiguration, MappingPair } from "@dynamic-mapper/mapper";

import type {
  DisciplineDto,
  DisciplineForResourceManipulationDto,
  GitLabOptionsDto,
  GitlabResourceTypeOptionsForCreationDto,
  GitlabResourceTypeOptionsForUpdateDto,
  LicenseDto,
  LicenseForResourceManipulationDto,
  QuotaDto,
  QuotaForManipulationDto,
  RdsOptionsDto,
  RdsResourceTypeOptionsForManipulationDto,
  RdsS3OptionsDto,
  RdsS3ResourceTypeOptionsForManipulationDto,
  RdsS3WormOptionsDto,
  RdsS3WormResourceTypeOptionsForManipulationDto,
  ResourceDto,
  ResourceForUpdateDto,
  ResourceTypeInformationDto,
  ResourceTypeOptionsDto,
  ResourceTypeOptionsForCreationDto,
  ResourceTypeOptionsForUpdateDto,
  VisibilityDto,
  VisibilityForResourceManipulationDto,
} from "@coscine/api-client/dist/types/Coscine.Api";
import { CoscineResourceTypes } from "@/modules/resource/types";

export const LicenseDto2LicenseForResourceManipulationDto = new MappingPair<
  LicenseDto,
  LicenseForResourceManipulationDto
>();
export const VisibilityDto2VisibilityForResourceManipulationDto =
  new MappingPair<VisibilityDto, VisibilityForResourceManipulationDto>();
export const DisciplineDto2DisciplineForResourceManipulationDto =
  new MappingPair<DisciplineDto, DisciplineForResourceManipulationDto>();

export const ResourceDto2ResourceForUpdateDto = new MappingPair<
  ResourceDto,
  ResourceForUpdateDto
>();
export const ResourceTypeOptionsDto2ResourceTypeOptionsForUpdateDto =
  new MappingPair<ResourceTypeOptionsDto, ResourceTypeOptionsForUpdateDto>();
export const QuotaDto2QuotaForManipulationDto = new MappingPair<
  QuotaDto,
  QuotaForManipulationDto
>();

export const GitLabOptionsDto2GitlabResourceTypeOptionsForUpdateDto =
  new MappingPair<GitLabOptionsDto, GitlabResourceTypeOptionsForUpdateDto>();
export const RdsOptionsDto2RdsResourceTypeOptionsForManipulationDto =
  new MappingPair<RdsOptionsDto, RdsResourceTypeOptionsForManipulationDto>();
export const RdsS3OptionsDto2RdsS3ResourceTypeOptionsForManipulationDto =
  new MappingPair<
    RdsS3OptionsDto,
    RdsS3ResourceTypeOptionsForManipulationDto
  >();
export const RdsS3WormOptionsDto2RdsS3WormResourceTypeOptionsForManipulationDto =
  new MappingPair<
    RdsS3WormOptionsDto,
    RdsS3WormResourceTypeOptionsForManipulationDto
  >();

export const ResourceTypeInformationDto2ResourceTypeOptionsForCreationDto =
  new MappingPair<
    ResourceTypeInformationDto,
    ResourceTypeOptionsForCreationDto
  >();

const configuration = new MapperConfiguration((cfg) => {
  cfg.createAutoMap(ResourceDto2ResourceForUpdateDto, {
    usageRights: (opt) => opt.mapFrom((e) => e.usageRights),
    license: (opt) =>
      opt
        .preCondition((e) => e.license?.id !== undefined)
        .mapFromUsing(
          (e) => e.license,
          LicenseDto2LicenseForResourceManipulationDto,
        ),
    visibility: (opt) =>
      opt.mapFromUsing(
        (e) => e.visibility,
        VisibilityDto2VisibilityForResourceManipulationDto,
      ),
    disciplines: (opt) =>
      opt.mapFromUsing(
        (e) => e.disciplines ?? [],
        DisciplineDto2DisciplineForResourceManipulationDto,
      ),
    resourceTypeOptions: (opt) =>
      opt
        .preCondition((e) => e.type?.options !== undefined)
        .mapFromUsing(
          (e) => e.type?.options,
          ResourceTypeOptionsDto2ResourceTypeOptionsForUpdateDto,
        ),
  });

  // Resource type options maps
  cfg.createAutoMap(ResourceTypeOptionsDto2ResourceTypeOptionsForUpdateDto, {
    linkedResourceTypeOptions: (opt) => opt.ignore(),
    gitlabResourceTypeOptions: (opt) =>
      opt
        .mapFromUsing(
          (e) => e.gitLab,
          GitLabOptionsDto2GitlabResourceTypeOptionsForUpdateDto,
        )
        .condition((con) => con.gitLab !== undefined),
    rdsResourceTypeOptions: (opt) =>
      opt
        .mapFromUsing(
          (e) => e.rds,
          RdsOptionsDto2RdsResourceTypeOptionsForManipulationDto,
        )
        .condition((con) => con.rds !== undefined),
    rdsS3ResourceTypeOptions: (opt) =>
      opt
        .mapFromUsing(
          (e) => e.rdsS3,
          RdsS3OptionsDto2RdsS3ResourceTypeOptionsForManipulationDto,
        )
        .condition((con) => con.rdsS3 !== undefined),
    rdsS3WormResourceTypeOptions: (opt) =>
      opt
        .mapFromUsing(
          (e) => e.rdsS3Worm,
          RdsS3WormOptionsDto2RdsS3WormResourceTypeOptionsForManipulationDto,
        )
        .condition((con) => con.rdsS3Worm !== undefined),
  });
  cfg.createMap(ResourceTypeInformationDto2ResourceTypeOptionsForCreationDto, {
    linkedResourceTypeOptions: (opt) =>
      opt
        .preCondition(
          (rti) => rti.generalType === CoscineResourceTypes.LinkedData.General,
        )
        .mapFrom((rti) =>
          rti.generalType === CoscineResourceTypes.LinkedData.General
            ? {}
            : undefined,
        ),
    gitlabResourceTypeOptions: (opt) =>
      opt
        .preCondition(
          (rti) => rti.generalType === CoscineResourceTypes.Gitlab.General,
        )
        .mapFrom((rti) =>
          rti.generalType === CoscineResourceTypes.Gitlab.General
            ? ({} as GitlabResourceTypeOptionsForCreationDto)
            : undefined,
        ),
    rdsResourceTypeOptions: (opt) =>
      opt
        .preCondition(
          (rti) => rti.generalType === CoscineResourceTypes.Rds.General,
        )
        .mapFrom((rti) =>
          rti.generalType === CoscineResourceTypes.Rds.General ? {} : undefined,
        ),
    rdsS3ResourceTypeOptions: (opt) =>
      opt
        .preCondition(
          (rti) => rti.generalType === CoscineResourceTypes.RdsS3.General,
        )
        .mapFrom((rti) =>
          rti.generalType === CoscineResourceTypes.RdsS3.General
            ? {}
            : undefined,
        ),
    rdsS3WormResourceTypeOptions: (opt) =>
      opt
        .preCondition(
          (rti) => rti.generalType === CoscineResourceTypes.RdsS3Worm.General,
        )
        .mapFrom((rti) =>
          rti.generalType === CoscineResourceTypes.RdsS3Worm.General
            ? {}
            : undefined,
        ),
  });
  cfg.createAutoMap(GitLabOptionsDto2GitlabResourceTypeOptionsForUpdateDto, {});
  cfg.createAutoMap(RdsOptionsDto2RdsResourceTypeOptionsForManipulationDto, {
    quota: (opt) =>
      opt.mapFromUsing((e) => e.size, QuotaDto2QuotaForManipulationDto),
  });
  cfg.createAutoMap(
    RdsS3OptionsDto2RdsS3ResourceTypeOptionsForManipulationDto,
    {
      quota: (opt) =>
        opt.mapFromUsing((e) => e.size, QuotaDto2QuotaForManipulationDto),
    },
  );
  cfg.createAutoMap(
    RdsS3WormOptionsDto2RdsS3WormResourceTypeOptionsForManipulationDto,
    {
      quota: (opt) =>
        opt.mapFromUsing((e) => e.size, QuotaDto2QuotaForManipulationDto),
    },
  );

  // Other maps
  cfg.createAutoMap(LicenseDto2LicenseForResourceManipulationDto, {
    id: (opt) => opt.mapFrom((e) => e.id ?? ""),
  });
  cfg
    .createAutoMap(VisibilityDto2VisibilityForResourceManipulationDto, {})
    .forSourceMember("displayName", (opt) => opt.ignore());
  cfg.createAutoMap(DisciplineDto2DisciplineForResourceManipulationDto, {});
  cfg.createAutoMap(QuotaDto2QuotaForManipulationDto, {});
});

export const resourceMapper = configuration.createMapper();
