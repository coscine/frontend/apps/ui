import { MapperConfiguration, MappingPair } from "@dynamic-mapper/mapper";
import moment from "moment";

import type {
  DisciplineDto,
  DisciplineForProjectManipulationDto,
  OrganizationDto,
  OrganizationForProjectManipulationDto,
  ProjectDto,
  ProjectForUpdateDto,
  VisibilityDto,
  VisibilityForProjectManipulationDto,
} from "@coscine/api-client/dist/types/Coscine.Api";

export const ProjectDto2ProjectForUpdateDto = new MappingPair<
  ProjectDto,
  ProjectForUpdateDto
>();
export const VisibilityDto2VisibilityForProjectManipulationDto =
  new MappingPair<VisibilityDto, VisibilityForProjectManipulationDto>();
export const DisciplineDto2DisciplineForProjectManipulationDto =
  new MappingPair<DisciplineDto, DisciplineForProjectManipulationDto>();
export const OrganizationDto2OrganizationForProjectManipulationDto =
  new MappingPair<OrganizationDto, OrganizationForProjectManipulationDto>();

const configuration = new MapperConfiguration((cfg) => {
  cfg.createAutoMap(ProjectDto2ProjectForUpdateDto, {
    name: (opt) => opt.mapFrom((e) => e.name ?? ""),
    disciplines: (opt) =>
      opt.mapFromUsing(
        (e) => e.disciplines,
        DisciplineDto2DisciplineForProjectManipulationDto,
      ),
    visibility: (opt) =>
      opt.mapFromUsing(
        (e) => e.visibility,
        VisibilityDto2VisibilityForProjectManipulationDto,
      ),
    organizations: (opt) =>
      opt.mapFromUsing(
        (e) => e.organizations,
        OrganizationDto2OrganizationForProjectManipulationDto,
      ),
    startDate: (opt) =>
      opt.mapFrom((e) => moment(e.startDate).format("YYYY-MM-DD")),
    endDate: (opt) =>
      opt.mapFrom((e) => moment(e.endDate).format("YYYY-MM-DD")),
  });
  // Other maps
  cfg
    .createAutoMap(VisibilityDto2VisibilityForProjectManipulationDto, {})
    .forSourceMember("displayName", (opt) => opt.ignore());
  cfg.createAutoMap(DisciplineDto2DisciplineForProjectManipulationDto, {});
  cfg.createAutoMap(OrganizationDto2OrganizationForProjectManipulationDto, {
    uri: (opt) => opt.mapFrom((e) => e.uri ?? ""),
  });
});

export const projectMapper = configuration.createMapper();
