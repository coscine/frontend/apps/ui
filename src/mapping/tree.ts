import { MapperConfiguration, MappingPair } from "@dynamic-mapper/mapper";
import { v4 as uuidv4 } from "uuid";

import type { FileTreeDto } from "@coscine/api-client/dist/types/Coscine.Api";
import type {
  FileInformation,
  FolderInformation,
} from "@/modules/resource/types";
import factory from "rdf-ext";
import type { Dataset } from "@rdfjs/types";

export const TreeDto2FileInformation = new MappingPair<
  FileTreeDto,
  FileInformation
>();

export const TreeDto2FolderInformation = new MappingPair<
  FileTreeDto,
  FolderInformation
>();

const configuration = new MapperConfiguration((cfg) => {
  const version = +new Date();
  cfg.createMap(TreeDto2FileInformation, {
    id: (opt) => opt.mapFrom((_) => uuidv4()),
    name: (opt) => opt.mapFrom((dto) => dto.name ?? ""),
    parentDirectory: (opt) => opt.mapFrom((dto) => dto.directory ?? ""),
    path: (opt) => opt.mapFrom((dto) => dto.path ?? ""),
    type: (opt) => opt.mapFrom((_) => "Leaf"),
    createdAt: (opt) => opt.mapFrom((dto) => dto.creationDate ?? undefined),
    lastModified: (opt) => opt.mapFrom((dto) => dto.changeDate),
    size: (opt) => opt.mapFrom((dto) => dto.size ?? 0),
    latestVersion: (opt) => opt.mapFrom((_) => version),
    currentVersion: (opt) => opt.mapFrom((_) => version),
    metadata: (opt) =>
      opt.mapFrom((_) => {
        return { [version]: factory.dataset() as unknown as Dataset };
      }), // Set outside of the mapper
    versions: (opt) => opt.mapFrom((_) => [version]),
    actions: (opt) => opt.mapFrom((dto) => dto.actions),
  });

  cfg.createMap(TreeDto2FolderInformation, {
    id: (opt) => opt.mapFrom((_) => uuidv4()),
    name: (opt) => opt.mapFrom((dto) => dto.name ?? ""),
    parentDirectory: (opt) => opt.mapFrom((dto) => dto.directory ?? ""),
    path: (opt) => opt.mapFrom((dto) => dto.path ?? ""),
    type: (opt) => opt.mapFrom((_) => "Tree"),
    createdAt: (opt) => opt.mapFrom((dto) => dto.creationDate ?? undefined),
    lastModified: (opt) => opt.mapFrom((dto) => dto.changeDate),
    latestVersion: (opt) => opt.mapFrom((_) => version),
    currentVersion: (opt) => opt.mapFrom((_) => version),
    metadata: (opt) =>
      opt.mapFrom((_) => {
        return { [version]: factory.dataset() as unknown as Dataset };
      }), // Set outside of the mapper
    versions: (opt) => opt.mapFrom((_) => [version]),
  });
});

export const treeMapper = configuration.createMapper();
