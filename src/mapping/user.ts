import { MapperConfiguration, MappingPair } from "@dynamic-mapper/mapper";

import type {
  DisciplineDto,
  DisciplineForUserManipulationDto,
  LanguageDto,
  LanguageForUserManipulationDto,
  TitleDto,
  TitleForUserManipulationDto,
  UserDto,
  UserForUpdateDto,
} from "@coscine/api-client/dist/types/Coscine.Api";

export const UserDto2UserForUpdateDto = new MappingPair<
  UserDto,
  UserForUpdateDto
>();
export const TitleDto2TitleForUserManipulationDto = new MappingPair<
  TitleDto,
  TitleForUserManipulationDto
>();
export const DisciplineDto2DisciplineForUserManipulationDto = new MappingPair<
  DisciplineDto,
  DisciplineForUserManipulationDto
>();
export const LanguageDto2LanguageForUserManipulationDto = new MappingPair<
  LanguageDto,
  LanguageForUserManipulationDto
>();

const configuration = new MapperConfiguration((cfg) => {
  cfg.createMap(UserDto2UserForUpdateDto, {
    givenName: (opt) => opt.mapFrom((dto) => dto.givenName ?? ""),
    familyName: (opt) => opt.mapFrom((dto) => dto.familyName ?? ""),
    email: (opt) =>
      opt.mapFrom((dto) => dto.emails?.find((e) => e.isPrimary)?.email ?? ""),
    title: (opt) =>
      opt
        .condition((dto) => undefined !== dto.title)
        .mapFromUsing((dto) => dto.title, TitleDto2TitleForUserManipulationDto),
    disciplines: (opt) =>
      opt.mapFromUsing(
        (dto) => dto.disciplines,
        DisciplineDto2DisciplineForUserManipulationDto,
      ),
    language: (opt) =>
      opt.mapFromUsing(
        (dto) => dto.language,
        LanguageDto2LanguageForUserManipulationDto,
      ),
    organization: (opt) =>
      opt.mapFrom((dto) =>
        dto.organizations?.length ? dto.organizations[0].displayName : null,
      ),
  });
  cfg.createAutoMap(TitleDto2TitleForUserManipulationDto, {});
  cfg.createAutoMap(DisciplineDto2DisciplineForUserManipulationDto, {});
  cfg.createAutoMap(LanguageDto2LanguageForUserManipulationDto, {});
});

export const userMapper = configuration.createMapper();
