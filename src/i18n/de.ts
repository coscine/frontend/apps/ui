export default {
  /*
    --------------------------------------------------------------------------------------
    GERMAN STRINGS
    --------------------------------------------------------------------------------------
  */
  nav: {
    search: "Suche",
    lang: "Sprache",
    langEN: "English",
    langDE: "Deutsch",
    extrasHelp: "Hilfe",
    extrasImprint: "Impressum",
    extrasAccessibility: "Barrierefreiheit",
    extrasContact: "Kontakt",
    user: "{displayName}",
    userProfile: "Nutzendenprofil",
    userLogIn: "Einloggen",
    userLogOut: "Ausloggen",
    coscine: "Coscine",
    privacyPolicy: "Datenschutzerklärung",
    mission: "Mission Statement",

    theme: {
      toggle: "Farbschema wählen",

      light: "Hell",
      dark: "Dunkel (Beta)",
    },

    url: {
      extrasHelp: "https://docs.coscine.de/de/",
      privacyPolicy: "https://about.coscine.de/privacypolicy/",
      extrasImprint: "https://www.coscine.de/imprint/",
      extrasAccessibility:
        "https://about.coscine.de/coscine-accessibility-declaration/",
      coscine: "https://www.coscine.de/",
      mission: "https://about.coscine.de/about/mission-statement/",
    },
  },

  sidebar: {
    home: "Übersicht",
    project: "Projekt | Projekte",
    resource: "Ressource | Ressourcen",
    subProject: "Unterprojekt | Unterprojekte",
    settings: "Einstellungen",

    archived: "Archiviert",

    configurationMetadata: "Konfiguration & Metadaten",
    users: "Mitglieder",
    quota: "Quota",
    projectDataPublication: "Anfrage Datenveröffentlichung",
  },

  buttons: {
    addUser: "Mitglied hinzufügen",
    archive: "@:{'default.archive'}",
    back: "Zurück",
    cancel: "Abbrechen",
    close: "Schließen",
    confirm: "Bestätigen",
    connect: "Verbinden",
    connected: "Verbunden",
    copyMetadata: "Metadaten kopieren",
    create: "Erstellen",
    delete: "Löschen",
    disabled: "Deaktiviert",
    disableLocalMetadataCopy: "Lokale Metadatenkopie deaktivieren",
    download: "Herunterladen",
    enableLocalMetadataCopy: "Lokale Metadatenkopie aktivieren",
    next: "Weiter",
    reset: "Zurücksetzen",
    import: "Importieren",
    invite: "Einladen",
    leave: "Verlassen",
    remove: "Entfernen",
    resend: "Erneut senden",
    revoke: "Widerrufen",
    save: "Speichern",
    submit: "Abschicken",
    tokenCreate: "Zugriffstoken erstellen",
    unarchive: "Archivieren rückgängig",
    verify: "Verifizieren",
  },

  default: {
    applicationProfile: "Metadatenprofil",
    archive: "Archivieren",
    archived: "Archiviert",
    maintenance: "In Wartung",
    deleted: "Gelöscht",
    help: "Hilfe",
    loading: "Laden...",
    or: "ODER",
    none: "k. A.",
    gb: "{number} GB",
  },

  title: {
    default: "Coscine",
    modified: "{title} | @:{'title.default'}",
    error: {
      notFound: "404 - Not Found | @:{'title.default'}",
    },
  },

  toast: {
    quotaLimitExceeded: {
      title: "Upload fehlgeschlagen: Quota überschritten",
      message:
        "Der Upload kann nicht initiiert werden, da er Ihre verfügbare Quota überschreitet. Bitte überprüfen Sie Ihre aktuelle Nutzung oder beantragen Sie mehr Speicherplatz.",
    },
    contactChange: {
      success: {
        title: "Änderung der Kontaktdaten",
        message: "E-Mail Adresse wurde erfolgreich bestätigt.",
      },
      failure: {
        title: "@:{'toast.contactChange.success.title'}",
        message:
          "Es ist ein Fehler bei der Bestätigung der E-Mail Adresse aufgetreten. Das angegebene Token ist ungültig oder wurde bereits verwendet.",
      },
    },
    sessionWarning: {
      title: "Sitzung läuft bald ab",
      message:
        "Ihre Sitzung läuft bald ab. Bleiben Sie aktiv, um weiterzuarbeiten.",
    },
    sessionEnd: {
      title: "Ihre Sitzung ist abgelaufen",
      message: "Um weiterzuarbeiten, loggen Sie sich erneut ein.",
      link: "@:{'nav.userLogIn'}",
    },
    onSave: {
      success: {
        title: "Änderungen erfolgreich",
        message: "Die Daten wurden erfolgreich gespeichert.",
      },
      failure: {
        title: "Fehler beim Speichern",
        message:
          "Es ist ein Fehler aufgetreten. Bitte versuchen Sie es erneut. Falls der Fehler weiterhin auftritt, wenden Sie sich bitte an Ihre Organisation.",
      },
    },
    onDelete: {
      success: {
        title: "Löschung erfolgreich",
        message: "Die Daten wurden erfolgreich gelöscht.",
      },
      failure: {
        title: "Fehler beim Löschen",
        message:
          "Es ist ein Fehler aufgetreten. Bitte versuchen Sie es erneut. Falls der Fehler weiterhin auftritt, wenden Sie sich bitte an Ihre Organisation.",
      },
    },
    apiError: {
      general: {
        title: "Ein Fehler ist aufgetreten",
        body: "Es ist ein Fehler aufgetreten. Bitte versuchen Sie es erneut. Falls der Fehler weiterhin auftritt, wenden Sie sich bitte an Ihre Organisation.",
      },
      specific: {
        title: "Ein Fehler ist aufgetreten",
        body: "Es ist ein Fehler aufgetreten {error}. Bitte versuchen Sie es erneut. Falls der Fehler weiterhin auftritt, wenden Sie sich bitte an Ihre Organisation und geben Sie die folgende Fehler-ID an: {traceId}.",
      },
    },
  },

  breadcrumbs: {
    admin: "Admin",
    home: "Home",
    tos: "Nutzungsbedingungen",
    login: "Einloggen",
    pid: "PID",
    search: "Suche",
    error: {
      notFound: "Nicht Gefunden",
    },
    project: {
      page: "{projectName}",
      create: "Projekt erstellen",
      configMetadata: "Projektkonfiguration & -metadaten",
      dataPublication: "Anfrage Datenveröffentlichung",
      quota: "Projektquota",
      members: "Projektmitglieder",
    },
    resource: {
      page: "{resourceName}",
      create: "Ressource erstellen",
      settings: "Ressource bearbeiten",
    },
    user: {
      profile: "Nutzendenprofil",
    },
  },

  resourceTypes: {
    resource: "Ressource",
    rdsrwth: {
      displayName: "RWTH-RDS-Web",
      fullName: "Research Data Storage (RDS)",
      description:
        "Research Data Storage (RDS) ist objektbasierter Speicher für Forschungsdaten. Sie können RDS-Ressourcen mit Speicherplatz für Ihre Forschungsdaten erstellen solange das Projekt ausreichend Speicherquota hat. Projektbesitzende können zusätzliche Speicherquota beantragen.",
    },
    rdsude: {
      displayName: "UDE-RDS-Web",
      fullName: "Research Data Storage (RDS)",
      description:
        "Research Data Storage (RDS) Universität Duisburg-Essen ist objektbasierter Speicher für Forschungsdaten. Sie können RDS-Ressourcen mit Speicherplatz für Ihre Forschungsdaten erstellen solange das Projekt ausreichend Speicherquota hat. Projektbesitzende können zusätzliche Speicherquota beantragen.",
    },
    rdstudo: {
      displayName: "TUDO-RDS-Web",
      fullName: "Research Data Storage (RDS)",
      description:
        "Research Data Storage (RDS) Technische Universität Dortmund ist objektbasierter Speicher für Forschungsdaten. Sie können RDS-Ressourcen mit Speicherplatz für Ihre Forschungsdaten erstellen solange das Projekt ausreichend Speicherquota hat. Projektbesitzende können zusätzliche Speicherquota beantragen.",
    },
    rdsrub: {
      displayName: "RUB-RDS-Web",
      fullName: "Research Data Storage (RDS)",
      description:
        "Research Data Storage (RDS) Ruhr-Universität Bochum ist objektbasierter Speicher für Forschungsdaten. Sie können RDS-Ressourcen mit Speicherplatz für Ihre Forschungsdaten erstellen solange das Projekt ausreichend Speicherquota hat. Projektbesitzende können zusätzliche Speicherquota beantragen.",
    },
    rdsnrw: {
      displayName: "NRW-RDS-Web",
      fullName: "Research Data Storage (RDS)",
      description:
        "Research Data Storage (RDS) Nordrhein-Westfalen ist objektbasierter Speicher für Forschungsdaten. Sie können RDS-Ressourcen mit Speicherplatz für Ihre Forschungsdaten erstellen solange das Projekt ausreichend Speicherquota hat. Projektbesitzer können zusätzliche Speicherquota beantragen.",
    },
    s3: {
      displayName: "S3 Bucket",
      fullName: "S3 Bucket (S3)",
      description:
        "S3 Buckets (S3) sind objektbasierte Speichereinheiten für Forschungsdaten. Sie basieren auf der gleichen Technik wie der Research Data Storage (RDS), die Verwaltung der Metadaten liegt jedoch beim Nutzenden. Daher ist ein gesondertes Antragsverfahren und ein Datenmanagementplan (DMP) notwendig, um sicherzustellen, dass die Daten die in S3 Buckets gespeichert werden mit Metadaten beschrieben werden um langfristig auffindbar und nachnutzbar zu sein.",
    },
    rdss3rwth: {
      displayName: "RWTH-RDS-S3",
      fullName: "RDS-S3-Ressource (S3)",
      description:
        "RDS-S3 sind objektbasierte Speichereinheiten für Forschungsdaten. Sie basieren auf der gleichen Technik wie der Research Data Storage (RDS), die Verwaltung der Metadaten liegt jedoch beim Nutzenden. Daher ist ein Antragsverfahren notwendig, um sicherzustellen, dass die Daten die in RDS-S3 gespeichert werden mit Metadaten beschrieben werden um langfristig auffindbar und nachnutzbar zu sein.",
    },
    rdss3ude: {
      displayName: "UDE-RDS-S3",
      fullName: "RDS-S3-Ressource (S3)",
      description:
        "RDS-S3 Universität Duisburg-Essen sind objektbasierte Speichereinheiten für Forschungsdaten. Sie basieren auf der gleichen Technik wie der Research Data Storage (RDS), die Verwaltung der Metadaten liegt jedoch beim Nutzenden. Daher ist ein Antragsverfahren notwendig, um sicherzustellen, dass die Daten die in RDS-S3 gespeichert werden mit Metadaten beschrieben werden um langfristig auffindbar und nachnutzbar zu sein.",
    },
    rdss3tudo: {
      displayName: "TUDO-RDS-S3",
      fullName: "RDS-S3-Ressource (S3)",
      description:
        "RDS-S3 Technische Universität Dortmund sind objektbasierte Speichereinheiten für Forschungsdaten. Sie basieren auf der gleichen Technik wie der Research Data Storage (RDS), die Verwaltung der Metadaten liegt jedoch beim Nutzenden. Daher ist ein Antragsverfahren notwendig, um sicherzustellen, dass die Daten die in RDS-S3 gespeichert werden mit Metadaten beschrieben werden um langfristig auffindbar und nachnutzbar zu sein.",
    },
    rdss3rub: {
      displayName: "RUB-RDS-S3",
      fullName: "RDS-S3-Ressource (S3)",
      description:
        "RDS-S3 Ruhr-Universität Bochum sind objektbasierte Speichereinheiten für Forschungsdaten. Sie basieren auf der gleichen Technik wie der Research Data Storage (RDS), die Verwaltung der Metadaten liegt jedoch beim Nutzenden. Daher ist ein Antragsverfahren notwendig, um sicherzustellen, dass die Daten die in RDS-S3 gespeichert werden mit Metadaten beschrieben werden um langfristig auffindbar und nachnutzbar zu sein.",
    },
    rdss3nrw: {
      displayName: "NRW-RDS-S3",
      fullName: "RDS-S3-Ressource (S3)",
      description:
        "RDS-S3 Nordrhein-Westfalen sind objektbasierte Speichereinheiten für Forschungsdaten. Sie basieren auf der gleichen Technik wie der Research Data Storage (RDS), die Verwaltung der Metadaten liegt jedoch beim Nutzenden. Daher ist ein Antragsverfahren notwendig, um sicherzustellen, dass die Daten die in RDS-S3 gespeichert werden mit Metadaten beschrieben werden um langfristig auffindbar und nachnutzbar zu sein.",
    },
    gitlab: {
      displayName: "GitLab",
      fullName: "GitLab (Git)",
      description:
        "GitLab (Git) ist eine Webanwendung zur Versionsverwaltung für Softwareprojekte auf Git-Basis. Sie können Git-Repositorien in Ihren Coscine Projekten hinterlegen und mit Metadaten beschreiben. Im Arbeitsalltag können Sie Git wie gewohnt nutzen und haben in Coscine die Übersicht über verschiedene Repositorien und die Möglichkeit zur  Metadatenverwaltung.",
    },
    linked: {
      displayName: "Linked Data",
      fullName: "Linked Data (Linked)",
      description:
        "Linked Data Ressourcen erlauben das Metadatenmanagement für Daten, die in externen Speichersystemen liegen und auf die nicht direkt mit Coscine zugegriffen werden kann oder soll. So können z.B. Daten auf lokalen Datenspeichern im Institut oder externen Datenspeichern, die nicht mit Coscine kompatibel sind mit Metadaten beschrieben werden. Die Handhabung der Metadaten erfolgt weitgehend analog zu anderen Ressourcentypen, statt dem Upload- und Download von Daten werden Platzhalter angelegt die mit den Metadaten beschrieben werden. Durch ein zusätzliches Feld kann eine Referenz auf den Ort der Datei angegeben werden, um eine eindeutige Zuordnung auf Dateilevel zu erlauben, z. B. Institutsfileserver/Projekt X/Messreihe B/11-12-2020_data.csv.",
    },
    rdss3wormrwth: {
      displayName: "RWTH-RDS-WORM",
      fullName: "RDS-WORM-Ressource (WORM)",
      description:
        'RDS-WORM sind objektbasierte Speichereinheiten mit der Funktion "write once, read many (WORM)" für Forschungsdaten. Dieser Ressourcentyp ist besonders geeignet für Rohdaten oder andere Daten, die nicht mehr verändert werden dürfen. Achtung! Einmal zugewiesener Speicherplatz ist nicht mehr wiederverwendbar, auch nicht durch den Projektbesitzenden. RDS-WORM basiert auf der gleichen Technologie wie RDS-S3, so dass die Verwaltung der Metadaten dem Nutzenden überlassen bleibt. Metadaten, die wie die zugehörigen Daten nicht mehr veränderbar sein sollen, müssen über zusätzliche Dateien (z. B. README-Dateien) direkt im S3-Bucket gespeichert werden. Metadaten, die über die Coscine-Weboberfläche gepflegt werden, bleiben editierbar. Es ist ein Antragsverfahren notwendig, um sicherzustellen, dass die Daten die in RDS-S3 gespeichert werden mit Metadaten beschrieben werden um langfristig auffindbar und nachnutzbar zu sein. Darüber hinaus muss sichergestellt werden, dass die Daten und Metadaten innerhalb des WORM S3-Buckets in dem Bewusstsein abgespeichert werden, dass sie nachträglich nicht mehr verändert werden können.',
    },
  },

  banner: {
    noc: {
      text: {
        Disturbance:
          "Derzeit gibt es ein Problem, das Ihre Nutzung beeinträchtigen könnte. Unser Team arbeitet daran. Weitere Informationen finden Sie im {nocPortal}.",
        PartialDisturbance:
          "Derzeit gibt es ein Problem, das Ihre Nutzung beeinträchtigen könnte. Unser Team arbeitet daran. Weitere Informationen finden Sie im {nocPortal}.",
        Interruption:
          "Derzeit gibt es ein Problem, das Ihre Nutzung beeinträchtigen könnte. Unser Team arbeitet daran. Weitere Informationen finden Sie im {nocPortal}.",
        Maintenance:
          "Geplante Wartungsarbeiten finden derzeit statt, um unseren Service zu verbessern. Wir bemühen uns, Störungen zu minimieren. Vielen Dank für Ihr Verständnis. Weitere Details finden Sie im {nocPortal}.",
        PartialMaintenance:
          "Geplante Wartungsarbeiten finden derzeit statt, um unseren Service zu verbessern. Wir bemühen uns, Störungen zu minimieren. Vielen Dank für Ihr Verständnis. Weitere Details finden Sie im {nocPortal}.",
        LimitedOperation:
          "Wir arbeiten derzeit mit eingeschränkter Funktionalität. Unser Team bemüht sich, den vollen Service so schnell wie möglich wiederherzustellen. Wir danken für Ihr Verständnis. Weitere Informationen finden Sie im {nocPortal}.",
        Change:
          "Eine kürzliche Änderung wurde in unserem System vorgenommen. Wir erwarten, dass dies Ihre Erfahrung verbessert. Weitere Details finden Sie im {nocPortal}.",
        Hint: "Wir haben Neuigkeiten, die für Sie interessant sein könnten. Bitte nehmen Sie sich einen Moment Zeit, um sie zu lesen. Weitere Details finden Sie im {nocPortal}.",
        Information:
          "Wir haben Neuigkeiten, die für Sie interessant sein könnten. Bitte nehmen Sie sich einen Moment Zeit, um sie zu lesen. Weitere Details finden Sie im {nocPortal}.",
        Warning:
          "Es wurde eine Systemwarnung ausgegeben. Bitte überprüfen Sie die neuesten Informationen im {nocPortal}.",
      },
      nocPortal: "Statusmeldungsportal",
    },
  },

  email: {
    serviceDeskEmail: "servicedesk@rwth-aachen.de",
    serviceDeskMailTo: "mailto:servicedesk@rwth-aachen.de",
    serviceDeskName: "IT-ServiceDesk",
  },
};
