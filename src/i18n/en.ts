export default {
  /*  
    --------------------------------------------------------------------------------------
    ENGLISH STRINGS
    --------------------------------------------------------------------------------------
  */
  nav: {
    search: "Search",
    lang: "Language",
    langEN: "English",
    langDE: "Deutsch",
    extrasHelp: "Help",
    extrasImprint: "Imprint",
    extrasAccessibility: "Accessibility",
    extrasContact: "Contact",
    user: "{displayName}",
    userProfile: "User Profile",
    userLogIn: "Log in",
    userLogOut: "Log out",
    coscine: "Coscine",
    privacyPolicy: "Privacy Policy",
    mission: "Mission Statement",

    theme: {
      toggle: "Toggle theme",

      light: "Light",
      dark: "Dark (Beta)",
    },

    url: {
      extrasHelp: "https://docs.coscine.de/en/",
      privacyPolicy: "https://about.coscine.de/en/privacypolicy/",
      extrasImprint: "https://www.coscine.de/en/imprint/",
      extrasAccessibility:
        "https://about.coscine.de/en/coscine-accessibility-declaration/",
      coscine: "https://www.coscine.de/en/",
      mission: "https://about.coscine.de/en/about/mission-statement/",
    },
  },

  sidebar: {
    home: "Home",
    project: "Project | Projects",
    resource: "Resource | Resources",
    subProject: "Sub-Project | Sub-Projects",
    settings: "Settings",

    configurationMetadata: "Configuration & Metadata",
    users: "Members",
    quota: "Quota",
    projectDataPublication: "Data Publication Request",
  },

  buttons: {
    addUser: "Add User",
    archive: "@:{'default.archive'}",
    back: "Back",
    cancel: "Cancel",
    close: "Close",
    confirm: "Confirm",
    connect: "Connect",
    connected: "Connected",
    copyMetadata: "Copy Metadata",
    create: "Create",
    delete: "Delete",
    disabled: "Disabled",
    disableLocalMetadataCopy: "Disable Local Metadata Copy",
    download: "Download",
    enableLocalMetadataCopy: "Enable Local Metadata Copy",
    next: "Next",
    reset: "Reset",
    import: "Import",
    invite: "Invite",
    leave: "Leave",
    remove: "Remove",
    resend: "Resend",
    revoke: "Revoke",
    save: "Save",
    submit: "Submit",
    tokenCreate: "Create Access Token",
    unarchive: "Unarchive",
    verify: "Verify",
  },

  default: {
    applicationProfile: "Metadata Profile",
    archive: "Archive",
    archived: "Archived",
    maintenance: "Under Maintenance",
    deleted: "Deleted",
    help: "Help",
    loading: "Loading...",
    or: "OR",
    none: "n/a",
    gb: "{number} GB",
  },

  title: {
    default: "Coscine",
    modified: "{title} | @:{'title.default'}",
    error: {
      notFound: "404 - Not Found | @:{'title.default'}",
    },
  },

  toast: {
    quotaLimitExceeded: {
      title: "Upload Failed: Quota Exceeded",
      message:
        "The upload cannot be initiated because the file exceeds your available storage quota. Please check your current usage or consider applying for additional space.",
    },
    contactChange: {
      success: {
        title: "Change contact information",
        message: "Email address has been successfully confirmed.",
      },
      failure: {
        title: "@:{'toast.contactChange.success.title'}",
        message:
          "Email address confirmation failed. The supplied token is invalid or was already used.",
      },
    },
    sessionWarning: {
      title: "Session Expiring Soon",
      message: "Your session is about to expire. Stay active to keep working.",
    },
    sessionEnd: {
      title: "Your session has expired",
      message: "To continue working, log back in.",
      link: "@:{'nav.userLogIn'}",
    },
    onSave: {
      success: {
        title: "Saved successfully",
        message: "The data has been saved successfully.",
      },
      failure: {
        title: "Error on saving",
        message:
          "An error occurred. Please try again. If the error persists, please contact your organization.",
      },
    },
    onDelete: {
      success: {
        title: "Deleted successfully",
        message: "The data has been deleted successfully.",
      },
      failure: {
        title: "Error on deletion",
        message:
          "An error occurred. Please try again. If the error persists, please contact your organization.",
      },
    },
    apiError: {
      general: {
        title: "An error occurred",
        body: "An error occurred. Please try again. If the error persists, please contact your organization.",
      },
      specific: {
        title: "An error occurred",
        body: "Something went wrong {error}. Please try again. If the issue persists, contact your organization and provide the following Trace ID: {traceId}.",
      },
    },
  },

  breadcrumbs: {
    admin: "Admin",
    home: "Home",
    tos: "Terms of Use",
    login: "Login",
    pid: "PID",
    search: "Search",
    error: {
      notFound: "Not Found",
    },
    project: {
      page: "{projectName}",
      create: "Create Project",
      configMetadata: "Project Configuration & Metadata",
      dataPublication: "Data Publication Request",
      quota: "Project Quota",
      members: "Project Members",
    },
    resource: {
      page: "{resourceName}",
      create: "Create Resource",
      settings: "Resource Settings",
    },
    user: {
      profile: "User Profile",
    },
  },

  resourceTypes: {
    resource: "Resource",
    rdsrwth: {
      displayName: "RWTH-RDS-Web",
      fullName: "Research Data Storage (RDS)",
      description:
        "Research Data Storage (RDS) is object-based storage for research data. You can create RDS resources with storage space for your research data as long as the project has sufficient storage quota. Project owners can request additional storage quota.",
    },
    rdsude: {
      displayName: "UDE-RDS-Web",
      fullName: "Research Data Storage (RDS)",
      description:
        "Research Data Storage (RDS) University of Duisburg-Essen is object-based storage for research data. You can create RDS resources with storage space for your research data as long as the project has sufficient storage quota. Project owners can request additional storage quota.",
    },
    rdstudo: {
      displayName: "TUDO-RDS-Web",
      fullName: "Research Data Storage (RDS)",
      description:
        "Research Data Storage (RDS) TU Dortmund University is object-based storage for research data. You can create RDS resources with storage space for your research data as long as the project has sufficient storage quota. Project owners can request additional storage quota.",
    },
    rdsrub: {
      displayName: "RUB-RDS-Web",
      fullName: "Research Data Storage (RDS)",
      description:
        "Research Data Storage (RDS) Ruhr-Universität Bochum is object-based storage for research data. You can create RDS resources with storage space for your research data as long as the project has sufficient storage quota. Project owners can request additional storage quota.",
    },
    rdsnrw: {
      displayName: "NRW-RDS-Web",
      fullName: "Research Data Storage (RDS)",
      description:
        "Research Data Storage (RDS) North Rhine-Westphalia is object-based storage for research data. You can create RDS resources with storage space for your research data as long as the project has sufficient storage quota. Project owners can request additional storage quota.",
    },
    s3: {
      displayName: "S3 Bucket",
      fullName: "S3 Bucket (S3)",
      description:
        "S3 Buckets (S3) are object-based storage units for research data They are based on the same technology as the research data storage (RDS), but the administration of the metadata lies with the user. Therefore, a separate application procedure and a data management plan (DMP) is necessary to ensure that the data stored in S3 Buckets is described with metadata in order to be retrievable and reusable in the long term.",
    },
    rdss3rwth: {
      displayName: "RWTH-RDS-S3",
      fullName: "RDS-S3-Resource (S3)",
      description:
        "RDS-S3 are object-based storage units for research data. They are based on the same technology as the research data storage (RDS), but the administration of the metadata lies with the user. Therefore, an application procedure is necessary to ensure that the data stored in S3 Buckets is described with metadata in order to be retrievable and reusable in the long term.",
    },
    rdss3ude: {
      displayName: "UDE-RDS-S3",
      fullName: "RDS-S3-Resource (S3)",
      description:
        "RDS-S3 University of Duisburg-Essen are object-based storage units for research data. They are based on the same technology as the research data storage (RDS), but the administration of the metadata lies with the user. Therefore, an application procedure is necessary to ensure that the data stored in S3 Buckets is described with metadata in order to be retrievable and reusable in the long term.",
    },
    rdss3tudo: {
      displayName: "TUDO-RDS-S3",
      fullName: "RDS-S3-Resource (S3)",
      description:
        "RDS-S3 TU Dortmund University are object-based storage units for research data. They are based on the same technology as the research data storage (RDS), but the administration of the metadata lies with the user. Therefore, an application procedure is necessary to ensure that the data stored in S3 Buckets is described with metadata in order to be retrievable and reusable in the long term.",
    },
    rdss3rub: {
      displayName: "RUB-RDS-S3",
      fullName: "RDS-S3-Resource (S3)",
      description:
        "RDS-S3 Ruhr-Universität Bochum are object-based storage units for research data. They are based on the same technology as the research data storage (RDS), but the administration of the metadata lies with the user. Therefore, an application procedure is necessary to ensure that the data stored in S3 Buckets is described with metadata in order to be retrievable and reusable in the long term.",
    },
    rdss3nrw: {
      displayName: "NRW-RDS-S3",
      fullName: "RDS-S3-Resource (S3)",
      description:
        "RDS-S3 North Rhine-Westphalia are object-based storage units for research data. They are based on the same technology as the research data storage (RDS), but the administration of the metadata lies with the user. Therefore, an application procedure is necessary to ensure that the data stored in S3 Buckets is described with metadata in order to be retrievable and reusable in the long term.",
    },
    gitlab: {
      displayName: "GitLab",
      fullName: "GitLab (Git)",
      description:
        "GitLab (Git) is a web application for version management of software projects based on Git. You can store Git repositories in your Coscine projects and describe them with metadata. In your daily work you can use Git as usual and have an overview of different repositories and the possibility to manage metadata in Coscine.",
    },
    linked: {
      displayName: "Linked Data",
      fullName: "Linked Data (Linked)",
      description:
        "Linked Data Resources allow metadata management for data stored in external storage systems that cannot or should not be directly accessed via Coscine. For example, data on local data storage systems in the institute or external data storage systems that are not compatible with Coscine can be described with metadata. The handling of metadata is largely analogous to other resource types. Instead of uploading and downloading data, placeholders are created which are described with the metadata. An additional field can be used to specify a reference to the location of the file in order to allow a unique assignment at file level, e.g. Institute file server/Project X/Measurement B/11-12-2020_data.csv.",
    },
    rdss3wormrwth: {
      displayName: "RWTH-RDS-WORM",
      fullName: "RDS-WORM-Resource (WORM)",
      description:
        'RDS-WORM are object-based storage units with the function "write once, read many (WORM)" for research data. This resource type is especially suitable for raw data or other data that may no longer be changed. Attention! Once allocated storage space is no longer reusable, not even by the project owner.  RDS-WORM is based on the same technology as RDS-S3, so the management of metadata is up to the user. Metadata that should no longer be editable must be stored directly in the S3 bucket via additional files (e.g. README files). Metadata that is maintained via the Coscine web interface remains editable. An application process is required to ensure that the data stored in S3 buckets is described with metadata so that it can be retrieved and reused in the long term. In addition, it must be ensured that the data and metadata within the WORM S3-Buckets is stored with the awareness that it cannot be changed afterwards.',
    },
  },

  banner: {
    noc: {
      text: {
        Disturbance:
          "We are currently facing an issue that may affect your experience. Our team is on it. For more information, please visit the {nocPortal}.",
        PartialDisturbance:
          "We are currently facing an issue that may affect your experience. Our team is on it. For more information, please visit the {nocPortal}.",
        Interruption:
          "We are currently facing an issue that may affect your experience. Our team is on it. For more information, please visit the {nocPortal}.",
        Maintenance:
          "Scheduled maintenance is in progress to enhance our service. We're working to minimize any disruption. Thank you for your understanding. Visit the {nocPortal} for further details.",
        PartialMaintenance:
          "Scheduled maintenance is in progress to enhance our service. We're working to minimize any disruption. Thank you for your understanding. Visit the {nocPortal} for further details.",
        LimitedOperation:
          "We're currently operating with limited functionality. Our team is working to restore full service as soon as possible. We appreciate your understanding. For more details, visit the {nocPortal}.",
        Change:
          "A recent change has been applied to our system. We expect this to enhance your experience. For more details, please visit the {nocPortal}.",
        Hint: "We have some news that may interest you. Please take a moment to read it. Further details can be found on the {nocPortal}.",
        Information:
          "We have some news that may interest you. Please take a moment to read it. Further details can be found on the {nocPortal}.",
        Warning:
          "A system warning has been issued. Please review the latest information on the {nocPortal}.",
      },
      nocPortal: "Status Updates Website",
    },
  },

  email: {
    serviceDeskEmail: "servicedesk@rwth-aachen.de",
    serviceDeskMailTo: "mailto:servicedesk@rwth-aachen.de",
    serviceDeskName: "IT-ServiceDesk",
  },
};
