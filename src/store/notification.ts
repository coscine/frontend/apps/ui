import { defineStore } from "pinia";
import type {
  CoscineErrorResponse,
  NotificationState,
  NotificationToast,
} from "./types";
import i18n from "@/plugins/vue-i18n";
import { getReasonPhrase } from "http-status-codes";
import type { AxiosError } from "axios";

/*
  Store variable name is "this.<id>Store"
    id: "notification" --> this.notificationStore
  Important! The id must be unique for every store.
*/
export const useNotificationStore = defineStore({
  id: "notification",
  /*  
    --------------------------------------------------------------------------------------
    STATES
    --------------------------------------------------------------------------------------
  */
  state: (): NotificationState => ({
    notificationQueue: [] as NotificationToast[],
  }),

  /*  
    --------------------------------------------------------------------------------------
    GETTERS
    --------------------------------------------------------------------------------------
    Synchronous code only.
    
    In a component use as e.g.:
      :label = "this.notificationStore.<getter_name>;
  */
  getters: {},
  /*  
    --------------------------------------------------------------------------------------
    ACTIONS
    --------------------------------------------------------------------------------------
    Asynchronous & Synchronous code comes here (e.g. API calls and VueX mutations).
    To change a state use an action.

    In a component use as e.g.:
      @click = "this.notificationStore.<action_name>();
  */
  actions: {
    postApiErrorNotification(error: AxiosError) {
      let notification: NotificationToast;
      if (error.response) {
        const coscineErrorResponse = error.response
          .data as CoscineErrorResponse;

        // The request was made and the server responded with a status code that falls out of the range of 2xx
        notification = {
          title: i18n.global.t("toast.apiError.specific.title").toString(),
          component: {
            keypath: "toast.apiError.specific.body",
            tag: "span",
            placeholders: {
              error: {
                el: "span",
                class: "",
                value: `(${error.response.status}: ${getReasonPhrase(
                  error.response.status,
                )})`,
              },
              traceId: {
                el: "span",
                class: "text-monospace",
                value: coscineErrorResponse.traceId ?? error.response.status,
              },
            },
          },
          variant: error.response.status >= 500 ? "danger" : "warning",
          autoHide: false,
        };
      } else {
        // Something happened in setting up the request that triggered an Error
        notification = {
          title: i18n.global.t("toast.apiError.general.title").toString(),
          body: i18n.global.t("toast.apiError.general.body").toString(),
          variant: "danger",
        };
      }
      this.postNotification(notification);
    },

    postGeneralApiWarningNotification(body: string) {
      // Something happened in setting up the request that triggered an Error
      const notification = {
        title: i18n.global.t("toast.apiError.general.title").toString(),
        body: i18n.global
          .t("toast.apiError.specific.body", { error: body })
          .toString(),
        variant: "warning",
      } as NotificationToast;
      this.postNotification(notification);
    },

    postNotification(toast: NotificationToast) {
      this.notificationQueue.push(toast); // TODO: Address this warning
    },

    deleteNotification(toast: NotificationToast) {
      const index = this.notificationQueue.indexOf(toast);
      this.notificationQueue.splice(index);
    },
  },
});

export default useNotificationStore;
