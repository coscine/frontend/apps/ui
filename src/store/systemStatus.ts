import { useLocalStorage } from "@vueuse/core";
import { defineStore } from "pinia";
import type { SystemStatusState } from "./types";
import moment from "moment";

import useNotificationStore from "./notification";

import type { AxiosError } from "axios";
import { SystemStatusApi } from "@coscine/api-client";
import type { MessageDto } from "@coscine/api-client/dist/types/Coscine.Api";

/*
  Store variable name is "this.<id>Store"
    id: "notification" --> this.notificationStore
  Important! The id must be unique for every store.
*/
export const useSystemStatusStore = defineStore({
  id: "systemStatus",
  /*  
    --------------------------------------------------------------------------------------
    STATES
    --------------------------------------------------------------------------------------
  */
  state: (): SystemStatusState => ({
    banner: {
      noc: {
        messages: [],
        lastFetched: null,
      },
      internal: {
        messages: [],
        lastFetched: null,
      },
      hidden: useLocalStorage("coscine.system.status.banner.hidden", []),
    },
  }),

  /*  
    --------------------------------------------------------------------------------------
    GETTERS
    --------------------------------------------------------------------------------------
    Synchronous code only.
    
    In a component use as e.g.:
      :label = "this.notificationStore.<getter_name>;
  */
  getters: {
    /**
     * Returns an array of visible NOC (Network Operations Center) messages.
     * This filters out any messages that have been marked as hidden by the user.
     *
     * @returns {MessageDto[]} An array of NOC messages that are not hidden.
     */
    visibleNocMessages(): MessageDto[] {
      return this.banner.noc.messages.filter(
        (m) => m.id && !this.banner.hidden.includes(m.id),
      );
    },

    /**
     * Returns an array of visible internal messages.
     * This filters out any messages that have been marked as hidden by the user.
     *
     * @returns {MessageDto[]} An array of internal messages that are not hidden.
     */
    visibleInternalMessages(): MessageDto[] {
      return this.banner.internal.messages.filter(
        (m) => m.id && !this.banner.hidden.includes(m.id),
      );
    },
  },
  /*  
    --------------------------------------------------------------------------------------
    ACTIONS
    --------------------------------------------------------------------------------------
    Asynchronous & Synchronous code comes here (e.g. API calls and VueX mutations).
    To change a state use an action.

    In a component use as e.g.:
      @click = "this.notificationStore.<action_name>();
  */
  actions: {
    /**
     * Retroeves the current NOC (Network Operations Center) messages.
     *
     * @param {boolean} [silent=false] - If true, suppresses error notifications.
     * @returns {Promise<MessageDto[]>} - A promise that resolves to an array of MessageDto objects.
     * @throws {AxiosError} - Throws an error if the API call fails and silent is false.
     */
    async retrieveCurrentNocMessages(silent: boolean = false): Promise<void> {
      const notificationStore = useNotificationStore();
      const now = new Date().toISOString();
      try {
        const apiResponse = await SystemStatusApi.getNocMessages({
          startDateBefore: now,
          endDateAfter: now,
        });
        this.banner.noc.messages = apiResponse.data.data ?? [];
        // Update last fetched time to avoid fetching messages too often
        this.banner.noc.lastFetched = new Date();
      } catch (error) {
        if (!silent) {
          // Handle other Status Codes
          notificationStore.postApiErrorNotification(error as AxiosError);
        }
      }
    },

    /**
     * Retrieves the current internal messages.
     *
     * @param {boolean} [silent=false] - If true, suppresses error notifications.
     * @returns {Promise<MessageDto[]>} - A promise that resolves to an array of MessageDto objects.
     * @throws {AxiosError} - Throws an error if the API call fails and silent is false.
     */
    async retrieveCurrentInternalMessages(
      silent: boolean = false,
    ): Promise<void> {
      const notificationStore = useNotificationStore();
      const now = new Date().toISOString();
      try {
        const apiResponse = await SystemStatusApi.getInternalMessages({
          startDateBefore: now,
          endDateAfter: now,
        });
        this.banner.internal.messages = apiResponse.data.data ?? [];
        // Update last fetched time to avoid fetching messages too often
        this.banner.internal.lastFetched = new Date();
      } catch (error) {
        if (!silent) {
          // Handle other Status Codes
          notificationStore.postApiErrorNotification(error as AxiosError);
        }
      }
    },

    /**
     * Helper function to determine if messages should be fetched.
     *
     * @param {Date | null} lastFetched - The last fetched time.
     * @returns {boolean} - True if messages should be fetched, false otherwise.
     */
    shouldFetchMessages(lastFetched: Date | null): boolean {
      const now = new Date();
      const cacheTime = moment.duration(1, "minutes").asMilliseconds();
      // Fetch if messages are never fetched or last fetch is older than 5 minutes
      return (
        !lastFetched ||
        moment
          .duration(now.getTime() - lastFetched.getTime())
          .asMilliseconds() > cacheTime
      );
    },

    /**
     * Hide a banner message by its ID.
     * @param id - The ID of the message to hide.
     */
    hideMessage(id: string) {
      if (!this.banner.hidden.includes(id)) {
        this.banner.hidden.push(id);
      }
    },
  },
});

export default useSystemStatusStore;
