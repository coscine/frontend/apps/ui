import { useLocalStorage } from "@vueuse/core";
import { defineStore } from "pinia";
import type { Theme, MainState } from "./types";
import { v4 as uuidv4 } from "uuid";

/*  
  Store variable name is "this.<id>Store"
    id: "main" --> this.mainStore
  Important! The id must be unique for every store.
*/
export const useMainStore = defineStore({
  id: "main",
  /*  
    --------------------------------------------------------------------------------------
    STATES
    --------------------------------------------------------------------------------------
  */
  state: (): MainState => ({
    coscine: {
      clientcorrelation: {
        id: useLocalStorage("coscine.clientcorrelation.id", uuidv4()),
      },
      loading: {
        counter: 0,
      },
      locale: useLocalStorage("coscine.locale", "en"),
      theme: useLocalStorage<Theme>("coscine.theme", "light"),
    },
    sidebarActive: useLocalStorage("coscine.sidebar.active", true),
  }),

  /*  
    --------------------------------------------------------------------------------------
    GETTERS
    --------------------------------------------------------------------------------------
    Synchronous code only.
    
    In a component use as e.g.:
      :label = "this.mainStore.<getter_name>;
  */
  getters: {
    currentTheme(): Theme {
      return this.coscine.theme;
    },
    isLoading(): boolean {
      return this.coscine.loading.counter > 0;
    },
  },
  /*  
    --------------------------------------------------------------------------------------
    ACTIONS
    --------------------------------------------------------------------------------------
    Asynchronous & Synchronous code comes here (e.g. API calls and VueX mutations).
    To change a state use an action.

    In a component use as e.g.:
      @click = "this.mainStore.<action_name>();
  */
  actions: {
    setTheme(theme: Theme) {
      this.coscine.theme = theme;
    },

    decreasePageLoadingContent() {
      this.coscine.loading.counter--;
    },

    increasePageLoadingContent() {
      this.coscine.loading.counter++;
    },
  },
});

export default useMainStore;
