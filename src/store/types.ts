import type { RemovableRef } from "@vueuse/core";
import type { MessageDto } from "@coscine/api-client/dist/types/Coscine.Api";
import type { OrchestratedToast } from "bootstrap-vue-next";

export type NotificationTranslationProps = {
  keypath: string;
  tag?: string;
  placeholders: Record<string, { el: string; class: string; value: string }>;
};

export type NotificationToast = OrchestratedToast & {
  variant?: "danger" | "warning" | "success" | "info" | "primary" | "secondary";
  component?: NotificationTranslationProps;
  autoHide?: boolean;
};

export type Theme = "light" | "dark";

export interface MainState {
  /*  
    --------------------------------------------------------------------------------------
    STATE TYPE DEFINITION
    --------------------------------------------------------------------------------------
  */
  coscine: {
    clientcorrelation: {
      id: RemovableRef<string>;
    };
    loading: {
      counter: number;
    };
    locale: RemovableRef<string>;
    theme: RemovableRef<Theme>;
  };
  sidebarActive: RemovableRef<boolean>;
}

export interface NotificationState {
  /*  
    --------------------------------------------------------------------------------------
    STATE TYPE DEFINITION
    --------------------------------------------------------------------------------------
  */
  notificationQueue: NotificationToast[];
}

export interface SystemStatusState {
  /*  
    --------------------------------------------------------------------------------------
    STATE TYPE DEFINITION
    --------------------------------------------------------------------------------------
  */
  banner: {
    noc: {
      messages: MessageDto[];
      lastFetched: Date | null;
    };
    internal: {
      messages: MessageDto[];
      lastFetched: Date | null;
    };
    hidden: RemovableRef<string[]>;
  };
}

export interface CoscineErrorResponse {
  data: {
    status: number;
    instance: string;
  };
  traceId: string;
  statusCode: number;
  isSuccess: boolean;
}
