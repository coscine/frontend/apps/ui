import type { TermsOfServiceDto } from "@coscine/api-client/dist/types/Coscine.Api";
import { LoginUrls } from "@coscine/api-client/dist/types/Coscine.Api.STS";
import { RemovableRef } from "@vueuse/core";
import { SupportedLanguages } from "../project/i18n";

export interface LoginState {
  /*  
    --------------------------------------------------------------------------------------
    STATE TYPE DEFINITION
    --------------------------------------------------------------------------------------
  */
  authorization: {
    bearer: RemovableRef<string>;
    expirationTimestamp: {
      token: Date | null;
      cookie: Date | null;
    };
  };
  currentTosVersion: TermsOfServiceDto | null | undefined;
  loginStoredData: RemovableRef<string>;
  loginUrls: LoginUrls | null;
}

export interface DFNAAIInstitution {
  entityId: string;
  displayName: string;
  logo?: string;
}

export interface DFNAAIData {
  entityId: string;
  displayName: {
    [locale: string]: string;
  };
  logo?: string;
}
export interface Partner {
  key: string;
  name: string;
  logo: Record<SupportedLanguages, string>;
  url: string;
  type: string;
}
