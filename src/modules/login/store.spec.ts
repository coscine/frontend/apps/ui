// stores/counter.spec.ts
import { setActivePinia, createPinia } from "pinia";
import useLoginStore from "./store";

describe("Login Store", () => {
  beforeEach(() => {
    // creates a fresh pinia and make it active so it's automatically picked
    // up by any useStore() call without having to pass it to it:
    // `useStore(pinia)`
    setActivePinia(createPinia());
  });

  test("logout", () => {
    const loginStore = useLoginStore();

    /* Make window.location.href writable for the logout method */
    Object.defineProperty(window, "location", {
      value: {
        href: "/",
      },
      writable: true, // possibility to override
    });

    /* Fill the local storage */
    localStorage.setItem(
      "coscine.clientcorrelation.id",
      "f7aef8bc-77f4-4679-9f5d-5724fbdf4513",
    );
    localStorage.setItem(
      "coscine.authorization.bearer",
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9...",
    );
    localStorage.setItem("coscine.locale", "en");
    localStorage.setItem("coscine.login.storedData", "");
    localStorage.setItem("coscine.sidebar.active", "true");
    localStorage.setItem("coscine.system.status.banner.hidden", "[]");
    expect(localStorage.length).toBe(6);

    loginStore.logout();

    /* Check if prerequisites for logout are fulfilled */
    expect(localStorage.length).toBeLessThan(8);
    expect(localStorage.getItem("coscine.authorization.bearer")).toBeNull();
    expect(sessionStorage.length).toBe(0);
    expect(window.location.href).toBe(
      "/coscine/api/Coscine.Api.STS/account/logout",
    );
  });
});
