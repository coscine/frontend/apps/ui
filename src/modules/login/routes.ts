import type { RouteRecordRaw } from "vue-router";

const LoginModule = () => import("./LoginModule.vue");
const Login = () => import("./pages/Login.vue");
const ToS = () => import("./pages/ToS.vue");

import { LoginI18nMessages } from "@/modules/login/i18n/index";

export const LoginRoutes: RouteRecordRaw[] = [
  {
    path: "/login",
    component: LoginModule,
    meta: {
      i18n: LoginI18nMessages,
      breadCrumb: "login",
    },
    children: [
      {
        path: "",
        name: "login",
        component: Login,
        meta: {
          requiresAuth: false,
          breadCrumb: "login",
        },
      },
      {
        path: "tos",
        name: "tos",
        component: ToS,
        meta: {
          requiresAuth: false,
          breadCrumb: "tos",
        },
      },
    ],
  },
];
