import { defineStore } from "pinia";

import Institutes from "./data/dfnaai.json";

import type { DFNAAIData, DFNAAIInstitution, LoginState } from "./types";

// import the main store
import useNotificationStore from "@/store/notification";
import { useLocalStorage } from "@vueuse/core";
import { AccountApi, SelfApi, TosApi } from "@coscine/api-client";
import axios from "axios";
import type { UserTermsOfServiceAcceptDto } from "@coscine/api-client/dist/types/Coscine.Api";
import type { AxiosError } from "axios";
import type { Router, RouteLocationNormalized } from "vue-router";
import { removeQueryParameterFromUrl } from "@/router";
import * as jose from "jose";
import moment from "moment";

export function getCookieValue(name: string): string | null {
  const cookies = document.cookie.split(";");
  for (const cookie of cookies) {
    const [cookieName, cookieValue] = cookie.trim().split("=");
    if (cookieName === name) {
      return decodeURIComponent(cookieValue);
    }
  }
  return null;
}

/*  
  Store variable name is "this.<id>Store"
    id: "login" --> this.loginStore
  Important! The id must be unique for every store.
*/
export const useLoginStore = defineStore({
  id: "login",
  /*  
    --------------------------------------------------------------------------------------
    STATES
    --------------------------------------------------------------------------------------
  */
  state: (): LoginState => ({
    authorization: {
      bearer: useLocalStorage("coscine.authorization.bearer", ""),
      expirationTimestamp: {
        token: null,
        cookie: null,
      },
    },
    currentTosVersion: null,
    loginStoredData: useLocalStorage("coscine.login.storedData", ""), // TODO: Use Type DFNAAIInstitution from March 2023
    loginUrls: null,
  }),

  /*  
    --------------------------------------------------------------------------------------
    GETTERS
    --------------------------------------------------------------------------------------
    Synchronous code only.
    
    In a component use as e.g.:
      :label = "this.loginStore.<getter_name>;
  */
  getters: {
    /**
     * Determines if the user is currently logged in.
     *
     * This getter checks whether the user has a valid bearer token or a valid cookie login session.
     *
     * @returns {boolean} `true` if the user is logged in via bearer token or cookie, otherwise `false`.
     */
    isLoggedIn(): boolean {
      return this.hasValidBearerToken || this.hasValidCookieLogin;
    },

    /**
     * Checks if the bearer token is valid and not expired.
     *
     * This getter decodes the JWT bearer token and verifies its expiration time.
     * It returns `true` if the token exists and has not expired, otherwise `false`.
     *
     * @returns {boolean} `true` if the bearer token is valid and not expired, `false` otherwise.
     */
    hasValidBearerToken(state): boolean {
      const expiresAt = state.authorization.expirationTimestamp.token;
      // If the token has no expiration time, it is invalid
      if (!expiresAt) return false;

      const nowUTC = moment.utc(moment.now()).toDate();
      const remainingTime = expiresAt.getTime() - nowUTC.getTime();
      return remainingTime > 0;
    },

    /**
     * Checks if there is a valid cookie login session.
     *
     * This getter retrieves the expiration time of the cookie login session.
     * It returns `true` if the session has not expired and the expiration time is greater than 0.
     *
     * @returns {boolean} `true` if the cookie login session is valid and not expired, `false` otherwise.
     */
    hasValidCookieLogin(state): boolean {
      const expiresAt = state.authorization.expirationTimestamp.cookie;
      // If the cookie has no expiration time, it is invalid
      if (!expiresAt) return false;

      const nowUTC = moment.utc(moment.now()).toDate();
      const remainingTime = expiresAt.getTime() - nowUTC.getTime();
      return remainingTime > 0;
    },

    institutions(): DFNAAIData[] {
      return Institutes as DFNAAIData[];
    },
    selectedInstitution(): DFNAAIInstitution | null {
      const storedData = this.loginStoredData;
      if (storedData && storedData.trim() !== "") {
        return JSON.parse(this.loginStoredData) as DFNAAIInstitution;
      } else return null;
    },
    // Id for the orcid external provider
    orcidId(): string {
      return "f383f83c-26cb-47eb-80c8-639ed05bd0de";
    },
    // Id for the NFDI4Ing AAI external provider
    nfdi4IngAaiId(): string {
      return "3a1e7185-0328-47ed-afd7-a4ffd7f03a79";
    },
    isCookieLogin(): boolean {
      return getCookieValue("CoscineLoginExpiration") !== null;
    },
  },
  /*  
    --------------------------------------------------------------------------------------
    ACTIONS
    --------------------------------------------------------------------------------------
    Asynchronous & Synchronous code comes here (e.g. API calls and VueX mutations).
    To change a state use an action.
    
    In a component use as e.g.:
      @click = "this.loginStore.<action_name>();
  */
  actions: {
    setBearerTokenExpiration(): void {
      const token = this.authorization.bearer;

      try {
        const jwt = jose.decodeJwt(token);
        if (jwt.exp) {
          const tokenExpiresAt = moment.unix(jwt.exp).utc();
          // Store the expiration time for later use
          this.authorization.expirationTimestamp.token =
            tokenExpiresAt.toDate();
        }
      } catch {
        return;
      }
    },
    setCookieExpiration(): void {
      const cookieValue = getCookieValue("CoscineLoginExpiration");
      if (cookieValue) {
        const cookieExpiresAt = new Date(cookieValue);
        // Store the expiration time for later use
        this.authorization.expirationTimestamp.cookie = cookieExpiresAt;
      }
    },

    /**
     * Calculates the remaining time in milliseconds until the session expires.

    * This getter compares the remaining time of the bearer token and the cookie login session.
    * It returns the minimum remaining time of both sessions, or `null` if no session is found.
    */
    calculateRemainingSessionTime(): number | null {
      const now = moment.utc().toDate();
      // Calculate the remaining time for the bearer token and the cookie session;
      // Should the token or cookie not exist, the remaining time must be set to null
      const bearerTokenExpiration = this.authorization.expirationTimestamp.token
        ? this.authorization.expirationTimestamp.token.getTime() - now.getTime()
        : null;
      const cookieExpiration = this.authorization.expirationTimestamp.cookie
        ? this.authorization.expirationTimestamp.cookie.getTime() -
          now.getTime()
        : null;

      const times: number[] = [];
      if (bearerTokenExpiration !== null) {
        times.push(bearerTokenExpiration);
      }
      if (cookieExpiration !== null) {
        times.push(cookieExpiration);
      }

      if (times.length > 0) {
        const minRemainingTime = Math.min(...times);
        return minRemainingTime;
      } else {
        return null;
      }
    },

    logout() {
      // Clear the local storage
      const storedKeys = Object.keys(
        localStorage._store ? localStorage._store : localStorage,
      );
      storedKeys.forEach((key) => {
        if (
          !key.startsWith("coscine.login.storedData") &&
          !key.startsWith("coscine.system.status.banner.hidden") &&
          !key.startsWith("coscine.theme")
        ) {
          localStorage.removeItem(key);
        }
      });

      // Clear the session storage
      sessionStorage.clear();

      // The logout has to be done by the API.
      // For this we have to do a form post out of a function.
      // There is no other way to do it.
      // Don't try or dare to refactor this.
      // I'm serious, don't touch this.
      if (this.isCookieLogin) {
        const form = document.createElement("form");

        form.method = "post";
        form.action = "/coscine/api/v2/self/session/logout";

        // Append the form to the body
        document.body.appendChild(form);

        // Submit the form
        form.submit();
      } else {
        window.location.href = "/coscine/api/Coscine.Api.STS/account/logout";
      }
    },

    logoutFromShibboleth(institution: DFNAAIInstitution) {
      /*
        This approach was adopted from the previous login app. 
        The URL works for most of the providers, but does not work for RWTH.

        Example: 
          - RWTH Test entityId: https://login-test.rz.rwth-aachen.de/shibboleth  (adding `idp/profile/Logout` DOES NOT work)
          - Actual Test URL: https://sso-test.rwth-aachen.de/  (adding `idp/profile/Logout` works)

        TODO: Get the proper logout URL for the entityId
      */
      window.location.href = institution.entityId + "/idp/profile/Logout";
    },

    setAccessTokenFromRoute(router: Router, route: RouteLocationNormalized) {
      // TODO: Check if this is still needed and if it even works
      // .../?accesstoken=<token>
      const acccessTokenName = "accesstoken";
      if (route.query[acccessTokenName]) {
        this.authorization.bearer = route.query[acccessTokenName].toString();

        if (router.currentRoute.value.name === "login") {
          router.push({ name: "home" });
        }
        // Remove accesstoken from the URL
        removeQueryParameterFromUrl(route, acccessTokenName);
      }
    },

    async getOrcidLoginStatus(logUserOut = false): Promise<boolean> {
      const notificationStore = useNotificationStore();
      try {
        /*
          This does not seem to work. The GET request always delivers a "false", but running it in the browser, "true" is returned when logged in.

          TODO: Figure this out.
        */
        await axios
          .get(
            `https://orcid.org/userStatus.json${
              logUserOut ? "?logUserOut=true" : ""
            }`,
            { withCredentials: true },
          )
          .then((result) => {
            return result.data["loggedIn"] as boolean;
          });
        return false;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return false;
      }
    },

    async retrieveCurrentTosVersion() {
      const notificationStore = useNotificationStore();
      try {
        const apiResponse = await TosApi.getTos();
        this.currentTosVersion = apiResponse.data.data;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async retrieveLoginUrls() {
      const notificationStore = useNotificationStore();
      try {
        const apiResponse = await AccountApi.accountGetLoginUrls();
        this.loginUrls = apiResponse.data;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    storeInstitutionSelection(institution: DFNAAIInstitution) {
      this.loginStoredData = JSON.stringify(institution);
    },

    async acceptToS(): Promise<boolean> {
      const notificationStore = useNotificationStore();
      try {
        // Retrieve the current TOS version if missing
        if (!this.currentTosVersion?.version) {
          await this.retrieveCurrentTosVersion();
        }
        // Build the DTO and send it
        const userTermsOfServiceAcceptDto: UserTermsOfServiceAcceptDto = {
          version: this.currentTosVersion?.version ?? "",
        };
        await SelfApi.acceptCurrentTos({
          userTermsOfServiceAcceptDto: userTermsOfServiceAcceptDto,
        });
        return true;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return false;
      }
    },
  },
});

export default useLoginStore;
