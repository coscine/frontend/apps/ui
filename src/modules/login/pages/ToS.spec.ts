/* Testing imports */
import { mount } from "@vue/test-utils";
import { createTestingPinia } from "@pinia/testing";

/* Vue i18n */
import i18n, { def } from "@/plugins/vue-i18n";
import { LoginI18nMessages } from "@/modules/login/i18n/index";
i18n.global.availableLocales.forEach((locale) => {
  i18n.global.setLocaleMessage(locale, def[locale]); // default locale messages
  i18n.global.mergeLocaleMessage(locale, LoginI18nMessages[locale]); // append the locale messages for the component
});

/* Tested Component */
import ToS from "./ToS.vue";

import type { LoginState } from "../types";

describe("ToS.vue", () => {
  /* Testing ToS accepted*/
  test("ToSAccepted", async () => {
    const href = "https://example.org/tos";

    /* Mount the Component */
    const wrapper = mount(ToS, {
      pinia: createTestingPinia({
        createSpy: vitest.fn,
        initialState: {
          login: {
            expiredSession: undefined,
            currentTosVersion: {
              href: href,
            },
            loginStoredData: ref(""),
            loginUrls: null,
          } satisfies LoginState,
        },
      }),
    });

    // wait for created in ToS.vue
    await wrapper.vm.$nextTick();

    // initial state, Confirm Button is disabled
    expect(wrapper.get("#confirmBtn").attributes()["disabled"]).toBeDefined();

    // check href
    expect(wrapper.find("#tosLink").attributes().href).toEqual(href);

    // check TosAccepted
    const toSFound = wrapper.find("#TosAccepted");
    await toSFound.setValue(true);

    // Confirm button should be active
    expect(
      wrapper.get("#confirmBtn").attributes()["disabled"],
    ).not.toBeDefined();
  });
});
