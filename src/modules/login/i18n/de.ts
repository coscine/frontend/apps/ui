export default {
  /*
    --------------------------------------------------------------------------------------
    GERMAN STRINGS
    --------------------------------------------------------------------------------------
  */
  page: {
    headline: "Coscine - Ihre Plattform für Forschungsdatenmanagement",
    releaseNewsTitle: "Coscine Release News",
    releaseNewsBody:
      "Wir entwickeln Coscine mit Hilfe von Nutzendenfeedback kontinuierlich weiter und liefern regelmäßig Updates und neue Funktionen. Sie möchten regelmäßig über unsere neuen Funktionen und Weiterentwicklungen informiert werden? Dann tragen Sie sich in unsere {mailingList} ein.",
    mailingList: "Coscine-Mailingliste",
    mailingListUrl:
      "https://lists.rwth-aachen.de/postorius/lists/coscine.lists.rwth-aachen.de/",

    partners: {
      coscineNrw: {
        title: "Coscine.nrw",
        url: "https://www.dh.nrw/kooperationen/Coscine.nrw-100",
        type: "Partnerprojekt",
      },
      nhr4ces: {
        title: "NHR4CES",
        url: "https://www.nhr4ces.de/",
        type: "Partnerprojekt",
      },
      iam4nfdi: {
        title: "IAM4NFDI",
        url: "https://base4nfdi.de/projects/iam4nfdi",
        type: "Partnerprojekt",
      },
      eoscFairImpact: {
        title: "FAIR-IMPACT",
        url: "https://fair-impact.eu/",
        type: "Partnerprojekt",
      },
      aims: {
        title: "AIMS",
        url: "https://www.aims-projekt.de/",
        type: "Partnerprojekt",
      },
      nfdi4ing: {
        title: "NFDI4Ing",
        url: "https://nfdi4ing.de/",
        type: "Partnerprojekt",
      },
      nfdiMatwerk: {
        title: "NFDI-MatWerk",
        url: "https://nfdi-matwerk.de/",
        type: "Partnerprojekt",
      },
      fdmNrw: {
        title: "fdm.nrw",
        url: "https://fdm-nrw.coscine.de/#/",
        type: "Netzwerk",
      },
      nhr: {
        title: "NHR-Verein e.V.",
        url: "https://www.nhr-verein.de/",
        type: "Netzwerk",
      },
      itc: {
        title: "IT Center - RWTH Aachen University",
        url: "https://www.itc.rwth-aachen.de/",
        type: "Betrieb",
      },
      dfg: {
        title: "Deutsche Forschungsgemeinschaft",
        url: "https://www.dfg.de/index.jsp",
        type: "Förderung",
      },
      mkwNrw: {
        title:
          "Ministerium für Kultur und Wissenschaft des Landes Nordrhein-Westfalen",
        url: "https://www.mkw.nrw/",
        type: "Förderung",
      },
      dhNrw: {
        title: "Digitale Hochschule NRW",
        url: "https://www.dh.nrw/",
        type: "Förderung",
      },
    },

    institution: {
      placeholder: "Institution auswählen",
    },

    login: {
      title: "Einloggen",
      button_orcid: "Anmelden mit ORCID®",
      button_nfdi4IngAai: "NFDI Login",
      button_other_institution: "Anderes institutionelles Konto",
      button_institution: "Institutionelles Konto",
    },

    logout: {
      title: "Ausloggen",
      success: "Sie wurden erfolgreich ausgeloggt",
      log_in: "Wiederholt einloggen",
    },

    tos: {
      title: "Nutzungsbedingungen",
      tos_body:
        "Um Coscine nutzen zu können, müssen Sie unseren {tos} zustimmen.",
      tos: "Nutzungsbedingungen",
      checkbox: {
        tos: "Ich habe die Nutzungsbedingungen von Coscine gelesen und akzeptiere sie",
      },
    },
  },
};
