export default {
  /*
    --------------------------------------------------------------------------------------
    ENGLISH STRINGS
    --------------------------------------------------------------------------------------
  */
  page: {
    headline: "Coscine - Your Platform for Research Data Management ",
    releaseNewsTitle: "Coscine Release News",
    releaseNewsBody:
      "We continuously develop Coscine with the help of user feedback and deliver regular updates and new features. You would like to be informed about our new features and further developments on a regular basis? Then sign up for our {mailingList}.",
    mailingList: "Coscine mailing list",
    mailingListUrl:
      "https://lists.rwth-aachen.de/postorius/lists/coscine.lists.rwth-aachen.de/",

    partners: {
      coscineNrw: {
        title: "Coscine.nrw",
        url: "https://www.dh.nrw/kooperationen/Coscine.nrw-100",
        type: "Partner Project",
      },
      nhr4ces: {
        title: "NHR4CES",
        url: "https://www.nhr4ces.de/",
        type: "Partner Project",
      },
      iam4nfdi: {
        title: "IAM4NFDI",
        url: "https://base4nfdi.de/projects/iam4nfdi",
        type: "Partner Project",
      },
      eoscFairImpact: {
        title: "FAIR-IMPACT",
        url: "https://fair-impact.eu/",
        type: "Partner Project",
      },
      aims: {
        title: "AIMS",
        url: "https://www.aims-projekt.de/",
        type: "Partner Project",
      },
      nfdi4ing: {
        title: "NFDI4Ing",
        url: "https://nfdi4ing.de/",
        type: "Partner Project",
      },
      nfdiMatwerk: {
        title: "NFDI-MatWerk",
        url: "https://nfdi-matwerk.de/",
        type: "Partner Project",
      },
      fdmNrw: {
        title: "fdm.nrw",
        url: "https://fdm-nrw.coscine.de/#/",
        type: "Network",
      },
      nhr: {
        title: "NHR-Verein e.V.",
        url: "https://www.nhr-verein.de/",
        type: "Network",
      },
      itc: {
        title: "IT Center - RWTH Aachen University",
        url: "https://www.itc.rwth-aachen.de/",
        type: "Maintainer",
      },
      dfg: {
        title: "Deutsche Forschungsgemeinschaft",
        url: "https://www.dfg.de/index.jsp",
        type: "Funding",
      },
      mkwNrw: {
        title:
          "Ministerium für Kultur und Wissenschaft des Landes Nordrhein-Westfalen",
        url: "https://www.mkw.nrw/",
        type: "Funding",
      },
      dhNrw: {
        title: "Digitale Hochschule NRW",
        url: "https://www.dh.nrw/",
        type: "Funding",
      },
    },

    institution: {
      placeholder: "Select Institution",
    },

    login: {
      title: "Login",
      button_orcid: "Sign in with ORCID®",
      button_nfdi4IngAai: "NFDI Login",
      button_other_institution: "Other Institutional Account",
      button_institution: "Institutional Account",
    },

    logout: {
      title: "Logout",
      success: "You have been successfully logged out",
      log_in: "Log back in",
    },

    tos: {
      title: "Terms of Use",
      tos_body: "In order to access Coscine you need to accept our {tos}.",
      tos: "Terms of Use",
      checkbox: {
        tos: "I have read and accept the Terms of Use of Coscine",
      },
    },
  },
};
