import { defineStore } from "pinia";
import type { ErrorState } from "./types";

/*  
  Store variable name is "this.<id>Store"
    id: "error" --> this.errorStore
  Important! The id must be unique for every store.
*/
export const useErrorStore = defineStore({
  id: "error",
  /*  
    --------------------------------------------------------------------------------------
    STATES
    --------------------------------------------------------------------------------------
  */
  state: (): ErrorState => ({}),

  /*  
    --------------------------------------------------------------------------------------
    GETTERS
    --------------------------------------------------------------------------------------
    Synchronous code only.
    
    In a component use as e.g.:
      :label = "this.errorStore.<getter_name>;
  */
  getters: {},
  /*  
    --------------------------------------------------------------------------------------
    ACTIONS
    --------------------------------------------------------------------------------------
    Asynchronous & Synchronous code comes here (e.g. API calls and VueX mutations).
    To change a state use an action.

    In a component use as e.g.:
      @click = "this.errorStore.<action_name>();
  */
  actions: {},
});

export default useErrorStore;
