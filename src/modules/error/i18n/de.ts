export default {
  /*
    --------------------------------------------------------------------------------------
    GERMAN STRINGS
    --------------------------------------------------------------------------------------
  */
  page: {
    notFound: {
      statusCode: "404",
      subtitle: "Seite nicht gefunden",
      description:
        "Die von Ihnen gesuchte Seite konnte nicht gefunden werden. Möglicherweise wurde sie entfernt, umbenannt oder existierte nie.",
      back_home: "Zurück zur Startseite",
    },
  },
};
