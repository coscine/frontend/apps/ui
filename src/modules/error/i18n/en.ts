export default {
  /*
    --------------------------------------------------------------------------------------
    ENGLISH STRINGS
    --------------------------------------------------------------------------------------
  */
  page: {
    notFound: {
      statusCode: "404",
      subtitle: "Page not found",
      description:
        "The page you were looking for could not be found. It might have been removed, renamed or did not exist in the first place.",
      back_home: "Go back home",
    },
  },
};
