import type { RouteRecordRaw } from "vue-router";

const ErrorModule = () => import("./ErrorModule.vue");
const NotFound = () => import("./pages/NotFound.vue");

import { ErrorI18nMessages } from "@/modules/error/i18n/index";

export const ErrorRoutes: RouteRecordRaw[] = [
  // Not Found (404) Page
  {
    path: "/:path(.*)",
    component: ErrorModule,
    meta: {
      i18n: ErrorI18nMessages,
    },
    children: [
      {
        path: "",
        name: "not-found",
        component: NotFound,
        meta: {
          breadCrumb: "error.notFound",
          requiresAuth: false,
        },
      },
    ],
  },
];
