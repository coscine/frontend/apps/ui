import { defineStore } from "pinia";
import type { AdminState } from "./types";

import useNotificationStore from "@/store/notification";
import { ProjectApi, ProjectQuotaApi } from "@coscine/api-client";
import type { AxiosError } from "axios";
import type {
  ProjectQuotaForUpdateDto,
  QuotaDto,
} from "@coscine/api-client/dist/types/Coscine.Api";
import { wrapListRequest } from "@/util/wrapListRequest";

/*  
  Store variable name is "this.<id>Store"
    id: "admin" --> this.adminStore
  Important! The id must be unique for every store.
*/
export const useAdminStore = defineStore({
  id: "admin",
  /*  
    --------------------------------------------------------------------------------------
    STATES
    --------------------------------------------------------------------------------------
  */
  state: (): AdminState => ({
    project: null,
    projectQuotas: null,
  }),
  /*  
    --------------------------------------------------------------------------------------
    GETTERS
    --------------------------------------------------------------------------------------
    Synchronous code only.

    In a component use as e.g.:
      :label = "this.projectStore.<getter_name>;"
  */
  getters: {},
  /*  
    --------------------------------------------------------------------------------------
    ACTIONS
    --------------------------------------------------------------------------------------
    Asynchronous & Synchronous code comes here (e.g. API calls and VueX mutations).
    To change a state use an action.

    In a component use as e.g.:
      @click = "this.userStore.<action_name>();
  */
  actions: {
    async retrieveProjectAndQuotas(projectId: string) {
      const notificationStore = useNotificationStore();
      try {
        const apiProjectResponse = await ProjectApi.getProject({
          projectId,
          includeSubProjects: false,
        });
        const projectQuotas = await wrapListRequest((pageNumber: number) =>
          ProjectQuotaApi.getProjectQuotas({
            projectId,
            pageNumber,
            pageSize: 50,
          }),
        );
        this.project = apiProjectResponse.data.data;
        this.projectQuotas = projectQuotas;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },
    async updateProjectQuota(
      projectId: string,
      resourceTypeId: string,
      quota: QuotaDto,
    ): Promise<boolean> {
      const notificationStore = useNotificationStore();
      try {
        const projectQuotaForUpdateDto: ProjectQuotaForUpdateDto = {
          allocated: quota,
          maximum: quota,
        };
        await ProjectQuotaApi.updateProjectQuota({
          projectId,
          resourceTypeId,
          projectQuotaForUpdateDto,
        });
        return true;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return false;
      }
    },
  },
});

export default useAdminStore;
