import VueI18n from "vue-i18n";
import de from "./de";
import en from "./en";

export const AdminI18nMessages = {
  de: de,
  en: en,
} as VueI18n.LocaleMessages;
