export default {
  page: {
    admin: {
      headline: "Quota Admin Panel",

      projectInputPlaceholder: "Enter a project's Slug or Id",

      projectFound: "Project found",
      projectNotSelected: "No project selected",

      form: {
        labelSymbol: ":",

        projectName: "Project Name",
        projectNameLabel:
          "@:{'page.admin.form.projectName'}@:{'page.admin.form.labelSymbol'}",

        displayName: "Display Name",
        displayNameLabel:
          "@:{'page.admin.form.displayName'}@:{'page.admin.form.labelSymbol'}",

        projectId: "Project Id",
        projectIdLabel:
          "@:{'page.admin.form.projectId'}@:{'page.admin.form.labelSymbol'}",
      },
      projectQuotaHeadline: "Quotas",
      displayHiddenResources: "Display enabled Resource Types only",

      headers: {
        resourceType: "Resource Type",
        projectQuota: "Current Project Quota",
        resourceQuota: "Current Resource Quota",

        maximumQuota: "Maximum Quota",
        maximumQuotaHint:
          "Maximum project quota available for resources of a resource type.",
        allocatedQuota: "Allocated Quota",
        allocatedQuotaHint:
          "Current allocated quota for resources of a resource type.",
        freeQuota: "Free Quota",
        freeQuotaHint:
          "Current free quota for resources of a resource type. The value is the @:{'page.admin.headers.allocatedQuota'} subtracted from the @:{'page.admin.headers.maximumQuota'}.",
        totalUsedQuota: "Total Used Quota",
        totalUsedQuotaHint:
          "The total space used by all uploaded files in all resources of a resource type.",
        totalReservedQuota: "Total Reserved Quota",
        totalReservedQuotaHint:
          "Total space reserved by all existing resources of a resource type.",

        newQuota: "New Project Quota",
        newQuotaHint:
          "Assign a new \"@:{'page.admin.headers.maximumQuota'}\". Will also set the \"@:{'page.admin.headers.allocatedQuota'}\" to this value.",
        action: "Action",
      },
      newQuotaInputPlaceHolder: "Enter Quota in GB",
      gb: "{number} GB",
      toast: {
        success: {
          title: "Quota successfully changed",
          body: "{resourceType} maximum and allocated quotas for project {projectName} set to {newQuota} GB",
        },
        fail: {
          title: "Quota update failed",
          body: "Updating quota for the selected resource type failed.",
        },
      },
    },
  },
};
