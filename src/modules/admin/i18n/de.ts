export default {
  page: {
    admin: {
      headline: "Adminseite",

      projectInputPlaceholder: "Projekt Id oder -Slug eingeben",

      projectFound: "Projekt gefunden",
      projectNotSelected: "Kein Projekt ausgewählt",

      form: {
        labelSymbol: ":",

        projectName: "Projektname",
        projectNameLabel:
          "@:{'page.admin.form.projectName'}@:{'page.admin.form.labelSymbol'}",

        displayName: "Anzeigename",
        displayNameLabel:
          "@:{'page.admin.form.displayName'}@:{'page.admin.form.labelSymbol'}",

        projectId: "Projekt Id",
        projectIdLabel:
          "@:{'page.admin.form.projectId'}@:{'page.admin.form.labelSymbol'}",
      },
      projectQuotaHeadline: "Quota",
      displayHiddenResources: "Nur aktivierte Ressourcentypen anzeigen",

      headers: {
        resourceType: "Ressourcentyp",
        projectQuota: "Aktuelle Projektquota",
        resourceQuota: "Aktuelle Ressourcenquota",

        maximumQuota: "Maximale Quota",
        maximumQuotaHint:
          "Maximal verfügbare Projektquota für Ressourcen eines Ressourcentyps.",
        allocatedQuota: "Zugeteilte Quota",
        allocatedQuotaHint:
          "Aktuell zugeteilte Quota für Ressourcen eines Ressourcentyps.",
        freeQuota: "Freie Quota",
        freeQuotaHint:
          "Aktuell freie Quota für Ressourcen eines Ressourcentyps. Der Wert ist die Differenz zwischen \"@:{'page.admin.headers.maximumQuota'}\" und \"@:{'page.admin.headers.allocatedQuota'}\".",
        totalUsedQuota: "Gesamte genutzte Quota",
        totalUsedQuotaHint:
          "Der gesamte Speicherplatz, der von allen hochgeladenen Dateien in allen Ressourcen eines Ressourcentyps belegt wird.",
        totalReservedQuota: "Gesamte reservierte Quota",
        totalReservedQuotaHint:
          "Gesamtspeicherplatz, der von allen vorhandenen Ressourcen eines Ressourcentyps reserviert wird.",

        newQuota: "Neue Projektquota",
        newQuotaHint:
          "Weist eine neue \"@:{'page.admin.headers.maximumQuota'}\" zu. Setzt auch die \"@:{'page.admin.headers.allocatedQuota'}\" auf diesen Wert.",
        action: "Aktion",
      },
      newQuotaInputPlaceHolder: "Quota in GB angeben",
      gb: "{number} GB",
      toast: {
        success: {
          title: "Quota erfolgreich geändert",
          body: "{resourceType} maximale und zugeteilte Quota für Projekt {projectName} gesetzt auf {newQuota} GB",
        },
        fail: {
          title: "Aktualisierung fehlgeschlagen",
          body: "Aktualisierung der Quota fehlgeschlagen.",
        },
      },
    },
  },
};
