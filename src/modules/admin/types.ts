import type {
  ProjectDto,
  ProjectQuotaDto,
  QuotaDto,
} from "@coscine/api-client/dist/types/Coscine.Api";

export interface ExtendedProjectQuotaDto extends ProjectQuotaDto {
  iDisplayName?: string;
  isQuotaAvailable?: boolean;
  isEnabled?: boolean;
  free?: QuotaDto | null;
}

export interface AdminState {
  project: ProjectDto | null | undefined;
  projectQuotas: ProjectQuotaDto[] | null | undefined;
}
