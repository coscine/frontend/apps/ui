import type { RouteRecordRaw } from "vue-router";

const AdminModule = () => import("./AdminModule.vue");
const Admin = () => import("./pages/Admin.vue");

import { AdminI18nMessages } from "@/modules/admin/i18n/index";

export const AdminRoutes: RouteRecordRaw[] = [
  {
    path: "/admin",
    component: AdminModule,
    meta: {
      i18n: AdminI18nMessages,
    },
    children: [
      {
        path: "",
        name: "admin",
        component: Admin,
        // only authenticated users can access admin
        meta: {
          breadCrumb: "admin",
          // TODO: Implement Admin Check
          requiresAdmin: true,
          requiresAuth: true,
        },
      },
    ],
  },
];
