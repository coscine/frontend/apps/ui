/* Testing imports */
import { mount } from "@vue/test-utils";
import { createTestingPinia } from "@pinia/testing";

/* Vue i18n */
import i18n, { def } from "@/plugins/vue-i18n";
import { AdminI18nMessages } from "@/modules/admin/i18n/index";
i18n.global.availableLocales.forEach((locale) => {
  i18n.global.setLocaleMessage(locale, def[locale]); // default locale messages
  i18n.global.mergeLocaleMessage(locale, AdminI18nMessages[locale]); // append the locale messages for the component
});

/* Tested Component */
import Admin from "./Admin.vue";

/* Import of relevant mockup data */
import { testAdminState, testResourceState } from "@/data/mockup/testAdmin";
import QuotaTable from "../components/QuotaTable.vue";
import type { BFormInput } from "bootstrap-vue-next";

import useAdminStore from "../store";
import useResourceStore from "@/modules/resource/store";

describe("Admin", () => {
  /* Describe Pre-initialization steps */

  /* Description of the test */
  test("metadataResults", async () => {
    /* Mount the Component */
    const wrapper = mount(Admin, {
      pinia: createTestingPinia({
        createSpy: vitest.fn,
        initialState: {
          admin: testAdminState,
          resource: testResourceState,
        },
      }),
    });

    const adminStore = useAdminStore();

    const coscineFormGroupInputs = wrapper
      .findAllComponents({ name: "CoscineFormGroup" })
      .map((formGroup) => {
        return formGroup.findComponent<typeof BFormInput>("input");
      });
    // Ensure the project names are set correctly inside the fields and that every one of those is readonly
    expect(coscineFormGroupInputs.flatMap((e) => e.vm.modelValue)).toEqual([
      adminStore.$state.project?.name,
      adminStore.$state.project?.displayName,
      adminStore.$state.project?.id,
    ]);
    coscineFormGroupInputs.every((e) => e.vm.readonly);

    const resourceStore = useResourceStore();

    const quotaTable = wrapper.findComponent(QuotaTable);
    const rowsOfEnabledOnlyResTypes = quotaTable.find("tbody").findAll("tr");
    // Resource types that are "active" are displayed and their count is equal to the count in the store
    expect(rowsOfEnabledOnlyResTypes.length).toEqual(
      resourceStore.$state.resourceTypes?.filter((e) => e.isEnabled).length,
    );

    quotaTable.vm.showOnlyEnabledResources = false;
    await quotaTable.vm.$nextTick();
    const rowsOfEnabledAndHiddenResTypes = quotaTable
      .find("tbody")
      .findAll("tr");
    // Resource types both "active" and "hidden" are displayed and their count is equal to the count in the store
    expect(rowsOfEnabledAndHiddenResTypes.length).toBeGreaterThanOrEqual(
      resourceStore.$state.resourceTypes?.length ?? 0,
    );

    // Ensure that input fields and buttons are rendered only on entries where isQuotaAvailable === true.
    const quotaTableRows = quotaTable.findAll("tbody tr");
    for (let index = 0; index < quotaTableRows.length; index++) {
      const trElement = quotaTableRows.at(index);
      if (trElement) {
        const inputs = trElement.findAll("td input");
        const buttons = trElement.findAll("td button");

        const isInputPresent = inputs.length === 1;
        const isButtonPresent = buttons.length === 1;

        // If a button is present, it should be disabled when the input is empty
        if (isButtonPresent) {
          expect(
            (buttons.at(0)?.element as HTMLButtonElement).disabled,
          ).toBeDefined();
        }

        // If isQuotaAvailable is true, then we test the input and button behavior
        if (isInputPresent && isButtonPresent) {
          const input = inputs.at(0);
          const button = buttons.at(0);

          // Check that the button is disabled, when the input has no entered value
          await input?.setValue("");
          await quotaTable.vm.$nextTick();
          expect((button?.element as HTMLButtonElement).disabled).toBeDefined();

          // Check that the button is disabled, when the input has a character value
          await input?.setValue("Abc17");
          await quotaTable.vm.$nextTick();
          expect((button?.element as HTMLButtonElement).disabled).toBeDefined();

          // Check that the button is disabled, when the input has a negative value
          await input?.setValue(-5);
          await quotaTable.vm.$nextTick();
          expect((button?.element as HTMLButtonElement).disabled).toBeDefined();

          // Check that the button is enabled, when the input has a value greater than 0
          await input?.setValue(5);
          await quotaTable.vm.$nextTick();
          expect((button?.element as HTMLButtonElement).disabled).to.equal(
            false,
          );

          // Check that only one input field has the value "5"
          const inputWithValueFive = inputs.filter(
            (input) => (input.element as HTMLInputElement).value === "5",
          );
          expect(inputWithValueFive.length).toBe(1);

          // Check that only one button is enabled
          const enabledButtons = buttons.filter(
            (button) => !(button.element as HTMLButtonElement).disabled,
          );
          expect(enabledButtons.length).toBe(1);
        }
      }
    }
  });
});
