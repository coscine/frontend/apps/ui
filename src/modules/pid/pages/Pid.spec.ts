/* Testing imports */
import { mount } from "@vue/test-utils";
import { createTestingPinia } from "@pinia/testing";

/* Vue Router */
import router from "@/router";

/* Vue i18n */
import i18n, { def } from "@/plugins/vue-i18n";
import { PidI18nMessages } from "@/modules/pid/i18n/index";
i18n.global.availableLocales.forEach((locale) => {
  i18n.global.setLocaleMessage(locale, def[locale]); // default locale messages
  i18n.global.mergeLocaleMessage(locale, PidI18nMessages[locale]); // append the locale messages for the component
});

/* Tested Component */
import Pid from "./Pid.vue";

describe("Pid.vue", () => {
  const createWrapper = () => {
    return mount(Pid, {
      global: {
        plugins: [
          createTestingPinia({
            createSpy: vitest.fn,
          }),
          router,
        ],
      },
    });
  };

  let wrapper: ReturnType<typeof createWrapper>;
  beforeEach(() => {
    wrapper = createWrapper();
  });

  /* Testing the computed property 'pid' */
  test("Computed property 'pid' should be set correctly from raw pid", async () => {
    // After this line, router is ready
    await router.isReady();

    // Simulate navigation to a specific route with a query parameter 'pid'
    await router.push({
      name: "pid-page",
      query: { pid: "11148/ee1572f5-b5ef-41b0-8144-9c3c41db77d9" },
    });

    // Make sure changes in the mocked route are reflected in the component
    await wrapper.vm.$nextTick();

    // Now check the computed property 'pid'
    expect(wrapper.vm.pid).toBe("11148/ee1572f5-b5ef-41b0-8144-9c3c41db77d9");
  });

  /* Testing the computed property 'pid' */
  test("Computed property 'pid' should be set correctly from 'handle.net' pid", async () => {
    // After this line, router is ready
    await router.isReady();

    // Simulate navigation to a specific route with a query parameter 'pid'
    await router.push({
      name: "pid-page",
      query: {
        pid: "http://hdl.handle.net/11148/ee1572f5-b5ef-41b0-8144-9c3c41db77d9",
      },
    });

    // Make sure changes in the mocked route are reflected in the component
    await wrapper.vm.$nextTick();

    // Now check the computed property 'pid'
    expect(wrapper.vm.pid).toBe("11148/ee1572f5-b5ef-41b0-8144-9c3c41db77d9");
  });

  test("disables send email form when PID is invalid and enables it when PID is valid", async () => {
    // Verify that the PID field is disabled
    const persistentId = wrapper.get("#PersistentId");
    expect(persistentId.attributes()["readonly"]).toBeDefined();

    // Simulate an invalid PID
    wrapper.vm.isPidValid = false;
    await wrapper.vm.$nextTick();

    // Verify that the send email form is disabled
    let nameField = wrapper.get("#YourName");
    expect(Boolean(nameField.attributes()["disabled"])).toBeDefined();
    let emailField = wrapper.get("#YourEmail");
    expect(Boolean(emailField.attributes()["disabled"])).toBeDefined();
    let messageField = wrapper.get("#YourMessage");
    expect(Boolean(messageField.attributes()["disabled"])).toBeDefined();

    // Simulate a valid PID
    wrapper.vm.isPidValid = true;
    await wrapper.vm.$nextTick();

    // Verify that the send email form is NOT disabled
    nameField = wrapper.get("#YourName");
    expect(nameField.attributes()["disabled"]).not.toBeDefined();
    emailField = wrapper.get("#YourEmail");
    expect(emailField.attributes()["disabled"]).not.toBeDefined();
    messageField = wrapper.get("#YourMessage");
    expect(messageField.attributes()["disabled"]).not.toBeDefined();
  });
});
