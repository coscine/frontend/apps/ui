import type { RouteRecordRaw } from "vue-router";

const PidModule = () => import("./PidModule.vue");
const Pid = () => import("./pages/Pid.vue");

import { PidI18nMessages } from "@/modules/pid/i18n/index";

export const PidRoutes: RouteRecordRaw[] = [
  {
    path: "/pid",
    component: PidModule,
    meta: {
      i18n: PidI18nMessages,
    },
    children: [
      {
        path: "",
        name: "pid-page",
        component: Pid,
        meta: {
          requiresAuth: false,
          breadCrumb: "pid",
        },
      },
    ],
  },
];
