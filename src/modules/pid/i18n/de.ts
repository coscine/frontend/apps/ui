export default {
  /*
    --------------------------------------------------------------------------------------
    GERMAN STRINGS
    --------------------------------------------------------------------------------------
  */
  page: {
    pid: {
      title: "PID Informationen",
      description:
        "Diese PID referenziert auf Daten aus einem Forschungsprojekt in Coscine.",

      noPid: "Die aktuelle URL führt nicht zu einer gültigen PID",

      contactPidOwnerTitle: "Kontakt zur Projektleitung aufnehmen",
      contactPidOwnerDescription:
        "In den meisten Fällen sind die referenzierten Forschungsdaten nicht öffentlich zugänglich. Um Zugang zu erhalten, können Sie sich über das Formular an die zuständige Projektleitung wenden. Diese wird dann von Coscine kontaktiert. Die von Ihnen im Formular angegebenen Informationen werden an die Projektleitung weitergeleitet.",

      toClipboard: "PID wurde in die Zwischenablage kopiert",
      toLocation: "Speicherort wurde in die Zwischenablage kopiert",
      openUrl: "Navigieren Sie zur Objekt-URL in einem neuen Tab",

      form: {
        labelSymbol: ":",

        persistentId: "Persistent Identifier (PID)",
        persistentIdLabel:
          "@:{'page.pid.form.persistentId'}@:{'page.pid.form.labelSymbol'}",
        persistentIdPopover:
          "Für weitere Informationen zum @:{'page.pid.form.persistentId'} siehe",
        persistentIdPopoverUrl: "https://docs.coscine.de/de/resources/pid/",
        objectLocation: "Speicherort",
        objectLocationLabel:
          "@:{'page.pid.form.objectLocation'}@:{'page.pid.form.labelSymbol'}",

        yourName: "Ihr Name",
        yourNameLabel:
          "@:{'page.pid.form.yourName'}@:{'page.pid.form.labelSymbol'}",

        yourEmailAddress: "Ihre E-Mail Adresse",
        yourEmailAddressLabel:
          "@:{'page.pid.form.yourEmailAddress'}@:{'page.pid.form.labelSymbol'}",

        yourMessage: "Ihre Nachricht",
        yourMessageLabel:
          "@:{'page.pid.form.yourMessage'}@:{'page.pid.form.labelSymbol'}",

        sendConfirmationEmail: "Bestätigungsmail an mich senden",
      },

      toastEmailSent: {
        title: "Nachricht wurde versendet",
        body: "Ihre Nachricht wurde an den Ressourcenbesitzer gesendet. Wenn Sie \"@:{'page.pid.form.sendConfirmationEmail'}\" ausgewählt haben, empfangen Sie ebenfalls eine Kopie der Nachricht.",
      },
    },
  },
};
