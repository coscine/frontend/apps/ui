export default {
  /*
    --------------------------------------------------------------------------------------
    ENGLISH STRINGS
    --------------------------------------------------------------------------------------
  */
  page: {
    pid: {
      title: "PID Information",
      description:
        "This PID references data from a research project in Coscine.",

      noPid: "The current URL does not lead to a valid PID",

      contactPidOwnerTitle: "Contact Project Management",
      contactPidOwnerDescription:
        "In most cases, the referenced research data is not publicly accessible. To obtain access, you can contact the responsible project management using the form. They will then be contacted by Coscine. The information you provide in the form will be forwarded to the project management.",

      toClipboard: "PID has been copied to clipboard",
      toLocation: "Object location has been copied to clipboard",
      openUrl: "Navigate to the object location in a new tab",

      form: {
        labelSymbol: ":",

        persistentId: "Persistent Identifier (PID)",
        persistentIdLabel:
          "@:{'page.pid.form.persistentId'}@:{'page.pid.form.labelSymbol'}",
        persistentIdPopover:
          "For more information on @:{'page.pid.form.persistentId'} see",
        persistentIdPopoverUrl: "https://docs.coscine.de/en/resources/pid/",
        objectLocation: "Object Location",
        objectLocationLabel:
          "@:{'page.pid.form.objectLocation'}@:{'page.pid.form.labelSymbol'}",

        yourName: "Your Name",
        yourNameLabel:
          "@:{'page.pid.form.yourName'}@:{'page.pid.form.labelSymbol'}",

        yourEmailAddress: "Your Email Address",
        yourEmailAddressLabel:
          "@:{'page.pid.form.yourEmailAddress'}@:{'page.pid.form.labelSymbol'}",

        yourMessage: "Your Message",
        yourMessageLabel:
          "@:{'page.pid.form.yourMessage'}@:{'page.pid.form.labelSymbol'}",

        sendConfirmationEmail: "Email me a confirmation",
      },

      toastEmailSent: {
        title: "Message was sent successfully",
        body: "Your message was sent to the resource owner. If you have selected \"@:{'page.pid.form.sendConfirmationEmail'}\", you will also receive the request via e-mail.",
      },
    },
  },
};
