// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface PidState {
  /*  
    --------------------------------------------------------------------------------------
    STATE TYPE DEFINITION
    --------------------------------------------------------------------------------------
  */
}

/**
 * Provides a collection of constant identifiers relevant to the Coscine PID Record.
 * These identifiers represent entries in the Data Type Registry (DTR) and are used to identify various
 * types of information within the PID system.
 */
export interface PidHandles {
  /** The general identifier for the Kernel Information Profile type. */
  kernelInformationProfile: string;

  /** The identifier for the Coscine-specific Kernel Information Profile type. */
  coscineKernelInformationProfile: string;

  /** The identifier representing the date the PID record was created. */
  dateCreated: string;

  /** The identifier for identifying the digital object's location. */
  digitalObjectLocation: string;

  /** The identifier for the digital object type. */
  digitalObjectType: string;

  /** The identifier for the digital object value corresponding to a resource. */
  digitalObjectTypeResource: string;

  /** The identifier for the digital object value corresponding to a project. */
  digitalObjectTypeProject: string;

  /** The identifier representing the type of license associated with the digital object. */
  license: string;

  /** The identifier for the contact type, representing contact information within the PID record. */
  contact: string;

  /** The identifier for the topic type, used to categorize or tag the digital object with specific topics. */
  topic: string;
}
