import { PidHandles } from "../types";

export const pidHandles: PidHandles = {
  kernelInformationProfile: "21.T11148/076759916209e5d62bd5",
  coscineKernelInformationProfile: "21.T11148/8882327b7c25331e3cdd",
  dateCreated: "21.T11148/aafd5fb4c7222e2d950a",
  digitalObjectLocation: "21.T11148/b8457812905b83046284",
  digitalObjectType: "21.T11148/1c699a5d1b4ad3ba4956",
  digitalObjectTypeResource: "21.T11148/12aad485b74d04f584c1",
  digitalObjectTypeProject: "21.T11148/0f13b0a83bd926fe269f",
  license: "21.T11148/2f314c8fe5fb6a0063a8",
  contact: "21.T11148/1a73af9e7ae00182733b",
  topic: "21.T11148/b415e16fbe4ca40f2270",
};

export const handleUrl = "http://hdl.handle.net/";
