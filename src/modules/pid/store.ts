import { defineStore } from "pinia";
import type { PidState } from "./types";

import useNotificationStore from "@/store/notification";
import { PidApi } from "@coscine/api-client";
import { HandleApi } from "@coscine/api-client";
import { AxiosError } from "axios";
import { StatusCodes } from "http-status-codes";
import type {
  HandleDto,
  PidRequestDto,
} from "@coscine/api-client/dist/types/Coscine.Api";

/*  
  Store variable name is "this.<id>Store"
    id: "pid" --> this.pidStore
  Important! The id must be unique for every store.
*/
export const usePidStore = defineStore({
  id: "pid",
  /*  
    --------------------------------------------------------------------------------------
    STATES
    --------------------------------------------------------------------------------------
  */
  state: (): PidState => ({}),

  /*  
    --------------------------------------------------------------------------------------
    GETTERS
    --------------------------------------------------------------------------------------
    Synchronous code only.
    
    In a component use as e.g.:
      :label = "this.pidStore.<getter_name>;
  */
  getters: {},
  /*  
    --------------------------------------------------------------------------------------
    ACTIONS
    --------------------------------------------------------------------------------------
    Asynchronous & Synchronous code comes here (e.g. API calls and VueX mutations).
    To change a state use an action.

    In a component use as e.g.:
      @click = "this.pidStore.<action_name>();
  */
  actions: {
    /**
     * Validates the PID (Prefix ID) using the provided PID prefix and ID.
     * @param {string} prefix - The PID prefix value.
     * @param {string} suffix - The ID value.
     * @returns {Promise<boolean | null>} A promise that resolves to a boolean indicating whether the PID is valid, or null if there was an error.
     */
    async validatePid(prefix: string, suffix: string): Promise<boolean | null> {
      const notificationStore = useNotificationStore();
      try {
        const apiResponse = await PidApi.getPid({
          prefix,
          suffix,
        });
        return (
          (apiResponse.data.isSuccess ?? false) &&
          (apiResponse.data.data?.isEntityValid ?? false)
        );
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return null;
      }
    },
    /**
     * Retrieves the PID handles using the provided PID prefix and ID.
     * @param {string} prefix - The PID prefix value.
     * @param {string} suffix - The ID value.
     * @returns {Promise<HandleDto | undefined>} A promise that resolves to the handle data if successful, or undefined if there was an error.
     */
    async getHandle(
      prefix: string,
      suffix: string,
    ): Promise<HandleDto | undefined> {
      const notificationStore = useNotificationStore();
      try {
        const apiResponse = await HandleApi.getHandle({ prefix, suffix });
        return apiResponse.data.data;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },
    /**
     * Contacts the PID owner using the provided PID prefix, ID, and PID enquiry data.
     * @param {string} prefix - The PID prefix value.
     * @param {string} suffix - The ID value.
     * @param {PidRequestDto} pidRequestDto - The PID request data.
     * @returns {Promise<boolean | null>} A promise that resolves to a boolean indicating whether the contact operation was successful, or null if there was an error.
     */
    async contactPidOwner(
      prefix: string,
      suffix: string,
      pidRequestDto: PidRequestDto,
    ): Promise<boolean | null> {
      const notificationStore = useNotificationStore();
      try {
        const apiResponse = await PidApi.sendRequestToOwner({
          suffix,
          prefix,
          pidRequestDto,
        });
        // Note: Beware that only 204 (No Content) is considered a success in this implementation.
        return apiResponse.status === StatusCodes.NO_CONTENT ? true : false;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return null;
      }
    },
  },
});

export default usePidStore;
