import { defineStore } from "pinia";
import { SearchApi } from "@coscine/api-client";
import useNotificationStore from "@/store/notification";

import type { Category, SearchState } from "./types";
import type { AxiosError } from "axios";

import type {
  ProjectDto,
  SearchCategoryType,
} from "@coscine/api-client/dist/types/Coscine.Api";

/*  
  Store variable name is "this.<id>Store"
    id: "search" --> this.searchStore
  Important! The id must be unique for every store.
*/
export const useSearchStore = defineStore({
  id: "search",
  /*  
    --------------------------------------------------------------------------------------
    STATES
    --------------------------------------------------------------------------------------
  */
  state: (): SearchState => ({
    searchResults: null,
    pagination: null,
    categories: null,
  }),

  /*  
    --------------------------------------------------------------------------------------
    GETTERS
    --------------------------------------------------------------------------------------
    Synchronous code only.
    
    In a component use as e.g.:
      :label = "this.searchStore.<getter_name>;
  */
  getters: {},
  /*  
    --------------------------------------------------------------------------------------
    ACTIONS
    --------------------------------------------------------------------------------------
    Asynchronous & Synchronous code comes here (e.g. API calls and VueX mutations).
    To change a state use an action.

    In a component use as e.g.:
      @click = "this.searchStore.<action_name>();
  */
  actions: {
    async retrieveSearchResults(
      query: string,
      pageNumber: number,
      pageSize: number,
      projectDto?: ProjectDto | null,
      categoryFilter?: SearchCategoryType,
      orderBy?: string,
    ): Promise<boolean> {
      const notificationStore = useNotificationStore();
      try {
        let adaptedQuery = `${query}`;
        if (projectDto && projectDto.id) {
          // TODO: This is legacy advanced syntax, figure out how to use it properly with APIv2
          adaptedQuery = `(${adaptedQuery}) + (belongsToProject: "https://purl.org/coscine/projects/${projectDto.id}")`;
        }
        const response = await SearchApi.getSearchResults({
          query: adaptedQuery,
          category: categoryFilter,
          orderBy,
          pageNumber,
          pageSize,
        });

        if (response.headers["x-pagination"]) {
          this.pagination = JSON.parse(response.headers["x-pagination"]);
        }
        // TODO: Hardcoded category names, figure out how to get them from the API
        const categoryCountResult: Category[] = [
          {
            Name: "None",
            Count: JSON.parse(response.headers["x-category-count-none"]),
          },
          {
            Name: "Resource",
            Count: JSON.parse(response.headers["x-category-count-resource"]),
          },
          {
            Name: "Project",
            Count: JSON.parse(response.headers["x-category-count-project"]),
          },
          {
            Name: "Metadata",
            Count: JSON.parse(response.headers["x-category-count-metadata"]),
          },
        ];
        this.categories = categoryCountResult;

        this.searchResults = response.data.data;
        return true;
      } catch (error) {
        this.searchResults = null;
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return false;
      }
    },
  },
});

export default useSearchStore;
