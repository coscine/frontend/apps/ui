/* Testing imports */
import { mount } from "@vue/test-utils";
import { createTestingPinia } from "@pinia/testing";

/* Vue i18n */
import i18n, { def } from "@/plugins/vue-i18n";
import { SearchI18nMessages } from "@/modules/search/i18n/index";
i18n.global.availableLocales.forEach((locale) => {
  i18n.global.setLocaleMessage(locale, def[locale]); // default locale messages
  i18n.global.mergeLocaleMessage(locale, SearchI18nMessages[locale]); // append the locale messages for the component
});

/* Tested Component */
import Search from "./Search.vue";

/* Import of relevant mockup data */
import { testSearchState } from "@/data/mockup/testSearch";
import { SearchResultCategories, SearchResultDirections } from "../types";
import { BDropdownItem } from "bootstrap-vue-next";

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

describe("Search", () => {
  /* Describe Pre-initialization steps */

  /* Description of the test */
  test("metadataResults", async () => {
    /* Test Pre-initialization steps */

    /* Mount the Component */
    const wrapper = mount(Search, {
      pinia: createTestingPinia({
        createSpy: vitest.fn,
        initialState: {
          search: testSearchState,
        },
      }),
    });

    // Set the loading state to false, so that the table is rendered
    wrapper.vm.resultsViewLoading = false;

    await wrapper.vm.$nextTick();

    await sleep(1000);

    // Test how many result cards are shown
    const results = wrapper.findAllComponents({ name: "MetadataResult" });
    expect(results.length).toEqual(7); // Rendered 7 results
  });
  /* Description of the test */
  test("setOrderBy", async () => {
    /* Test Pre-initialization steps */
    /* Mount the Component */
    const wrapper = mount(Search, {
      pinia: createTestingPinia({
        createSpy: vitest.fn,
        initialState: {
          search: testSearchState,
        },
      }),
    });

    // Test if setting the category for sort search result is working
    wrapper.findAllComponents(BDropdownItem).at(1)?.trigger("click");
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.sortingDefinition.category).toBe(
      SearchResultCategories.Score,
    );
    wrapper.vm.sortingDefinition.category = "Date";
    expect(wrapper.vm.sortingDefinition.category).toBe(
      SearchResultCategories.Date,
    );

    // Test if setting the direction for sort search result is working
    expect(wrapper.vm.sortingDefinition.direction).toBe(
      SearchResultDirections.Desc,
    );
  });
});
