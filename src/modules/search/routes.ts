import type { RouteRecordRaw } from "vue-router";

const SearchModule = () => import("./SearchModule.vue");
const Search = () => import("./pages/Search.vue");

import { SearchI18nMessages } from "@/modules/search/i18n/index";

export const SearchRoutes: RouteRecordRaw[] = [
  {
    path: "/search",
    component: SearchModule,
    meta: {
      i18n: SearchI18nMessages,
    },
    children: [
      {
        path: "",
        name: "search",
        component: Search,
        meta: {
          breadCrumb: "search",
        },
      },
    ],
  },
];
