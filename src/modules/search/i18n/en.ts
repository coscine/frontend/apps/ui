export default {
  /*
    --------------------------------------------------------------------------------------
    ENGLISH STRINGS
    --------------------------------------------------------------------------------------
  */
  page: {
    search: {
      title: "Search Page",
      search: "Search",

      buttonSearch: {
        Item1: "Item1",
        Item2: "Item2",
      },

      none: "All Entries",
      metadata: "Files",
      resource: "Resources",
      project: "Projects",

      emptySearch: "No item is found for the given search criteria",
      endSearchResults: "Showing all results",

      allProjects: "All Projects",
      allResources: "All Resources",

      score: "Relevance",
      date: "Date",
      name: "Name",
    },
  },

  results: {
    labels: {
      result: "Result",
      metadata: "File",
      resource: "Resource",
      project: "Project",
      isPublic: "Public",
      isPrivate: "Private",
      title: "Title",
      alternative: "Alternative Title",
      alternative_title: "Display Name",
      graphName: "Graph Name",
      fileName: "File Name",
      absolutefilename: "Absolute File Name",
      conformsto: "Metadata Profile",
      conforms_to: "Metadata Profile",
      belongsToProject: "Belongs To Project",
      id: "ID",
      projectName: "Project Name",
      displayName: "Display Name",
      description: "Description",
      slug: "Project Slug",
      rightsholder: "Principal Investigators (PIs)",
      rights_holder: "Principal Investigators (PIs)",
      startdate: "Project Start",
      enddate: "Project End",
      disciplines: "Discipline",
      organizations: "Participating Organizations",
      subject: "Keywords",
      visibility: "Visibility",
      funding: "Grant ID",
      parentId: "Parent Project",
      resourceName: "Resource Name",
      resourceType: "Resource Type",
      pid: "Persistent ID",
      rights: "Internal Rules for Reuse",
      license: "License",
      creator: "Creator",
      archived: "Archived",
      homepage: "Homepage",
    },
  },
};
