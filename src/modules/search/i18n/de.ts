export default {
  /*
    --------------------------------------------------------------------------------------
    GERMAN STRINGS
    --------------------------------------------------------------------------------------
  */
  page: {
    search: {
      title: "Suchseite",
      search: "Suchen",

      buttonSearch: {
        Item1: "Eintrag 1",
        Item2: "Eintrag 2",
      },

      none: "Alle Einträge",
      metadata: "Dateien",
      resource: "Ressourcen",
      project: "Projekte",

      emptySearch: "Es wurden keine Treffer gefunden",
      endSearchResults: "Alle Ergebnisse werden angezeigt",

      allProjects: "Alle Projekte",
      allResources: "Alle Ressourcen",

      score: "Relevanz",
      date: "Datum",
      name: "Name",
    },
  },

  results: {
    labels: {
      result: "Ergebnis",
      metadata: "Datei",
      resource: "Ressource",
      project: "Projekt",
      isPublic: "Öffentlich",
      isPrivate: "Privat",
      date_created: "Erstelldatum",
      date_created_year: "Erstellungsjahr",
      date_created_month: "Erstellungsmonat",
      date_created_day: "Erstellungstag",
      title: "Titel",
      alternative: "Alternativer Titel",
      alternative_title: "Anzeigename",
      graphName: "Graphname",
      fileName: "Dateiname",
      absolutefilename: "Absoluter Dateiname",
      conformsto: "Metadatenprofil",
      conforms_to: "Metadatenprofil",
      belongsToProject: "Gehört zu Projekt",
      id: "ID",
      projectName: "Projektname",
      displayName: "Anzeigename",
      description: "Beschreibung",
      slug: "Projekt-Slug",
      rightsholder: "Principal Investigators (PIs)",
      rights_holder: "Principal Investigators (PIs)",
      startdate: "Projektstart",
      enddate: "Projektende",
      disciplines: "Disziplin",
      organizations: "Teilnehmende Organisationen",
      subject: "Schlagwörter",
      visibility: "Sichtbarkeit",
      funding: "Grant ID",
      parentId: "Übergeordnetes Projekt",
      resourceName: "Ressourcenname",
      resourceType: "Ressourcentyp",
      pid: "Persistente ID",
      rights: "Interne Regeln zur Nachnutzung",
      license: "Lizenz",
      creator: "Ersteller",
      archived: "Archiviert",
      homepage: "Hauptseite",
    },
  },
};
