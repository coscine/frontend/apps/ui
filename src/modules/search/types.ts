import type {
  SearchCategoryType,
  SearchResultDto,
} from "@coscine/api-client/dist/types/Coscine.Api";

export interface Category {
  Name: string;
  Count: number;
}

export interface Pagination {
  TotalCount: number;
  PageSize: number;
  CurrentPage: number;
  TotalPages: number;
  HasNext: boolean;
  HasPrevious: boolean;
}
export const SearchResultCategories = {
  ["Score"]: "Score",
  ["Date"]: "Date",
  ["Name"]: "Name",
};
export const SearchResultDirections = {
  ["Asc"]: "Asc",
  ["Desc"]: "Desc",
};
export interface SearchResultSorting {
  category: keyof typeof SearchResultCategories;
  direction: keyof typeof SearchResultDirections;
}
// Unfortunately that is a workaround, since we can't directly use the CategoryFilter Enum
export const CategoryFilters = {
  ["None"]: "None" as SearchCategoryType.None,
  ["Metadata"]:
    "Metadata" as SearchCategoryType.HttpsPurlOrgCoscineTermsStructureMetadata,
  ["Project"]:
    "Project" as SearchCategoryType.HttpsPurlOrgCoscineTermsStructureProject,
  ["Resource"]:
    "Resource" as SearchCategoryType.HttpsPurlOrgCoscineTermsStructureResource,
};

export interface SearchState {
  /*  
    --------------------------------------------------------------------------------------
    STATE TYPE DEFINITION
    --------------------------------------------------------------------------------------
  */
  pagination: Pagination | null;
  searchResults: SearchResultDto[] | null | undefined;
  categories: Category[] | null;
}
