import type { RouteRecordRaw } from "vue-router";

const CreateProject = () => import("./pages/CreateProject.vue");
const RootProjectModule = () => import("./RootProjectModule.vue");
const ProjectModule = () => import("./ProjectModule.vue");
const ListProjects = () => import("./pages/ListProjects.vue");
const ProjectPage = () => import("./pages/ProjectPage.vue");
const CreatePublicationRequest = () =>
  import("./pages/CreatePublicationRequest.vue");
const Quota = () => import("./pages/Quota.vue");
const Members = () => import("./pages/Members.vue");
const ConfigurationMetadata = () => import("./pages/ConfigurationMetadata.vue");

import { ResourceRoutes } from "@/modules/resource/routes";
import { ProjectI18nMessages } from "@/modules/project/i18n/index";

export const ProjectRoutes: RouteRecordRaw[] = [
  {
    path: "/",
    component: RootProjectModule,
    meta: {
      i18n: ProjectI18nMessages,
    },
    children: [
      {
        path: "",
        name: "home",
        component: ListProjects,
        meta: {
          breadCrumb: "home",
          requiresAuth: true,
        },
      },
      {
        path: "create-project",
        name: "create-project",
        component: CreateProject,
        meta: {
          requiresAuth: true,
          breadCrumb: "project.create",
        },
      },
    ],
  },
  {
    path: "/p/:slug?",
    component: ProjectModule,
    meta: {
      breadCrumb: "project.page",
      default: "project-page",
      i18n: ProjectI18nMessages,
    },
    children: [
      {
        path: "",
        name: "project-page",
        component: ProjectPage,
        meta: {
          breadCrumb: "project.page",
          requiresAuth: true,
        },
      },
      {
        path: "create-project",
        name: "create-sub-project",
        component: CreateProject,
        meta: {
          breadCrumb: "project.create",
          requiresAuth: true,
        },
      },
      {
        path: "config-metadata",
        name: "project-config-metadata",
        component: ConfigurationMetadata,
        meta: {
          requiresAuth: true,
          breadCrumb: "project.configMetadata",
        },
      },
      {
        path: "project-data-publication",
        name: "project-data-publication",
        component: CreatePublicationRequest,
        meta: {
          requiresAuth: true,
          breadCrumb: "project.dataPublication",
        },
      },
      {
        path: "quota",
        name: "project-quota",
        component: Quota,
        meta: {
          requiresAuth: true,
          breadCrumb: "project.quota",
        },
      },
      {
        path: "members",
        name: "project-members",
        component: Members,
        meta: {
          requiresAuth: true,
          breadCrumb: "project.members",
        },
      },
      ...ResourceRoutes,
    ],
  },
];
