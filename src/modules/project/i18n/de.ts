export default {
  /*
    --------------------------------------------------------------------------------------
    GERMAN STRINGS
    --------------------------------------------------------------------------------------
  */
  page: {
    // ListProjects.vue
    listProjects: {
      title: "Projekt | Projekte",
      addProject: "Projekt hinzufügen",
    },

    // CreateProject.vue
    createProject: {
      title: "Projekt erstellen",

      affiliationMessage:
        "Als Mitglied der RWTH Aachen wird Ihr Projekt mit einem Speicherplatz von 25 GB für RDS-Web-Speicher ausgestattet.",
      loadingSpinnerProjectCreation:
        "Die Projekterstellung kann derzeit bis zu einer Minute dauern. Vielen Dank für Ihre Geduld.",

      copyOwnersToSubprojectLabel: "Owner kopieren:",
      copyOwnersToSubprojectPopover:
        "Alle aktuellen Owner in das neue untergeordnete Projekt kopieren? Zukünftige Änderungen werden nicht automatisch aktualisiert.",

      form: "@:(form.project.form)",
    },

    // Members.vue
    members: {
      title: "Projektmitglieder",
      membersTabTitle: "Projektmitglieder",
      externalUsersTabTitle: "Eingeladene Nutzende",
      pendingStatus: "Wartet",
      expiredStatus: "Abgelaufen",
      changeRole: "Rolle ändern",
      noUserOptions: "Keine registrierte Person gefunden",
      searchNotEnoughCharacters: "Bitte mehr als 3 Zeichen eingeben",
      user: "Mitglied",
      role: "Rolle",
      typeToSearch: "Projektmitglied suchen...",
      perPage: "Pro Seite",
      searchUserPlaceholder:
        "Nach registrierter Person oder E-Mail Adresse suchen...",
      searchEmailInvite:
        "Sie können eine Einladung an diese E-Mail Adresse senden",
      alreadyGotRole: "(bereits Mitglied)",
      selectRolePlaceholder: "Rolle auswählen",
      givenName: "Vorname",
      familyName: "Nachname",
      email: "E-Mail",
      actions: "Aktionen",
      status: "Status",
      userManagement: "@:{'page.members.title'}",
      pleaseTypeSomething:
        "Bitte geben Sie einen Namen oder eine E-Mail Adresse ein",
      removeSelectedInvitation:
        "Sind Sie sicher, dass Sie die Einladung von Nutzenden {user} zum Projekt {projectName} zurückziehen möchten?",
      removeSelectedUser:
        "Sind Sie sicher, dass Sie den Nutzenden {user} aus dem Projekt {projectName} entfernen möchten?",
      deleteInvitationTitle: "Einladung Zurückziehen",
      deleteUserTitle: "Nutzenden entfernen",
      inviteUser: "Einladung abschicken",
      reInviteUser: "Erneut senden",
      inviteUserText:
        "Sind Sie sicher, dass Sie den Nutzenden {email} mit einer Rolle als {role} zum Projekt {projectName} einladen wollen?",
      inviteUserTitle: "Nutzenden einladen",
      inviteUserCaption: "Wählen Sie {displayName} aus",
      invitedUserText:
        "Eine Einladung wurde an {email} mit einer Rolle als {role} für das Projekt {projectName} verschickt.",
      invitationPendingText:
        "Es wurde bereits eine Einladung an {email} gesendet. {br}Wenn Sie eine neue Einladungsemail an diese Person schicken möchten, müssen Sie die vorherige Einladung über den Tab Eingeladene Personen löschen.",
      invitedUserError:
        "Ein Fehler ist beim Einladen von {email} aufgetreten. Eingaben sind fehlerhaft oder der Nutzende wurde bereits eingeladen.",
      deleteExternalUserError:
        "Beim Löschen von {email} ist ein Fehler aufgetreten.",
      emptyTableText: "Keine Nutzenden gefunden",
      emptyFilterText:
        "Keine Nutzenden gefunden, die mit Ihrer Anfrage übereinstimmen.",
      addedUser: "{user} als {role} zu {project} hinzugefügt",
      removedUser: "{email} von {project} gelöscht",
      changedRole: "{user} ist nun {role} in {project}",
      importUserTitle: "Mitglieder importieren",
      emptyImportTableText: "Kein Mitglied zum Importieren",
      emptyImportTableFilterText:
        "Es wurde kein passendes Projektmitglied gefunden",
      removeUser: "Entfernen",
      searchProjectPlaceholder: "Wählen Sie ein Projekt aus...",
      existingEmailInvitation:
        "Es wurde bereits eine Einladung an {email} versendet. Möchten Sie die Einladung erneut versenden?",
    },

    // ProjectPage.vue
    project: {
      toast: {
        merge: {
          success: {
            title: "Verbinden der Accounts war erfolgreich",
            body: "Das Verbinden ihres Accounts mit {externalName} war erfolgreich.",
          },
          failure: {
            title: "Verbinden der Accounts fehlgeschlagen",
            body: "Das Verbinden ihres Accounts mit der ID {externalId} ist fehlgeschalgen.",
          },
        },
      },
      title: "Projektseite",
      resource: "Ressource | Ressourcen",
      subProject: "Unterprojekt | Unterprojekte",
      member: "Mitglied | Mitglieder",
      addResource: "Ressource hinzufügen",
      tooltipDisabled:
        "Fügen Sie bitte {linkToUserProfile} Ihre E-Mail Adresse hinzu und verfizieren Sie diese, um eine neue Ressource anlegen zu können.",
      tooltipHere: "hier",
      members: {
        toProjectMembers: "Mitgliederverwaltung...",

        modal: {
          // Leave project modal
          title: 'Projekt "{name}" verlassen',
          body: 'Sie verlassen das Projekt "{name}". Damit verlieren Sie Zugriff auf das Projekt und die Ressourcen des Projekts. Möchten Sie das Projekt verlassen?',
        },
        tooltip: "Das Projekt verlassen",
        tooltipDisabled:
          "Sie sind der einzige Projekt Owner. Fügen Sie einen weiteren Projekt Owner hinzu, um das Projekt verlassen zu können.",
      },
    },

    // DataPublication.vue
    dataPublication: {
      continue: "Weiter",
      description1:
        "Um Sie bei der Veröffentlichung Ihrer Daten zu unterstützen, stehen Ihnen Publikationsservices zur Verfügung. Coscine bietet einen Service an, um den Datenveröffentlichungsdienst einer Partnerhochschule zu kontaktieren und gleichzeitig erste Informationen über Ihr Projekt, Ihre Ressourcen und die Daten, die Sie veröffentlichen möchten, weiterzuleiten.",
      description2:
        "Nach erfolgreichem Übermitteln der Informationen, werden alle Project Owner benachrichtigt und der entsprechende Publikationsservice wird sich mit Ihnen in Verbindung setzen, um die weiteren Schritte mit Ihnen zu besprechen. Bitte beachten Sie, dass es innerhalb von Coscine keine Möglichkeit zur Nachverfolgung des Status gibt.",
      title: "Anfrage Datenveröffentlichung",
      tab1: "Informationen",
      tab2: "Anfrage Datenveröffentlichung",
      tab3: "Zusammenfassung",
    },
    // DataPublicationProjectData.vue
    dataPublicationForm: {
      description1:
        "Um mit dem von Ihnen bevorzugten Publikationsservice Kontakt aufzunehmen, verwenden Sie bitte das nachstehende Formular. Wenn Sie unsicher sind oder Ihren gewünschten Dienst nicht in der Liste sehen, zögern Sie nicht, sich an das FDM-Team Ihrer Organisation per {email} zu wenden.",
      description2:
        "Bitte wählen Sie unten die entsprechenden Ressourcen aus. Wenn die Projektinformationen geändert werden müssen, gehen Sie bitte auf die Seite mit den Projekteinstellungen.",
      description3:
        "Fügen Sie bitte eine personalisierte Nachricht oder spezielle Wünsche für die Publikationsberatung hinzu.",
      service: "Publikationsservice",
      labelSymbol: ":",
      requesterName: "Antragstellende Person",
      email: "E-Mail",
      projectName: "Projektname",
      pid: "PID",
      discipline: "Projektdisziplin",
      message: "Ihre Nachricht",
      resource: "Ressourcen",
      resourceLabelPopover:
        "Für eine Ressource werden folgende Informationen übermittelt: Ressourcenname, Beschreibung, Disziplin, Persistent Identifier (PID), Metadatenprofil.",
      messageLabelPopover:
        "Geben Sie weitere Informationen für die Publikationsberatung an, z.B eine zeitkritische Veröffentlichungsanfrage oder der Wunsch nach einem bestimmten Veröffentlichungstermin.",
      labels: {
        serviceLabel:
          "@:{'page.dataPublicationForm.service'}@:{'page.dataPublicationForm.labelSymbol'}",
        requesterNameLabel:
          "@:{'page.dataPublicationForm.requesterName'}@:{'page.dataPublicationForm.labelSymbol'}",
        emailLabel:
          "@:{'page.dataPublicationForm.email'}@:{'page.dataPublicationForm.labelSymbol'}",
        projectNameLabel:
          "@:{'page.dataPublicationForm.projectName'}@:{'page.dataPublicationForm.labelSymbol'}",
        pidLabel:
          "@:{'page.dataPublicationForm.pid'}@:{'page.dataPublicationForm.labelSymbol'}",
        disciplineLabel:
          "@:{'page.dataPublicationForm.discipline'}@:{'page.dataPublicationForm.labelSymbol'}",
        messageLabel:
          "@:{'page.dataPublicationForm.message'}@:{'page.dataPublicationForm.labelSymbol'}",
        resourceLabel:
          "@:{'page.dataPublicationForm.resource'}@:{'page.dataPublicationForm.labelSymbol'}",
      },
    },
    // DataPublicationSummary.vue
    dataPublicationSummary: {
      description:
        "Die folgenden Informationen werden an den Publikationsservice und die Projekt-Besitzer übermittelt. Zusätzlich werden die Projektbeschreibung und für die ausgewählten Ressourcen die Metadatenprofile, PIDs, Ressourcenbeschreibung und Disziplin sowie der Gesamtdatenumfang abgeschickt.",
      toastTitle: "Antrag versendet",
      toastBody: "Die Anfrage zur Datenveröffentlichung wurde versendet.",
    },

    // Quota.vue
    quota: {
      headline: "Projektquota",
      resources: "Ressourcen",
      rangeText: "{allocated} GB zugeteilt",
      gb: "{number} GB",

      projectLabel: "Projekt:",
      resourceTypeLabel: "Ressourcentyp:",

      projectQuotaSliderLabel: "Zugeteilte Projektquota:",
      projectQuotaMinimumPopover:
        "Minimale Projektquota. Basierend auf der gesamten reservierten Quota aller vorhandenen {resourceType} Ressourcen.",
      projectQuotaAllocatedPopover:
        "Aktuell zugeteilte Quota für {resourceType} Ressourcen.",
      projectQuotaMaximumPopover:
        "Maximal verfügbare Projektquota für {resourceType} Ressourcen.",

      resourceQuotaMinimumPopover:
        "Der gesamte Speicherplatz, der von allen hochgeladenen Dateien in dieser Ressource belegt wird, berechnet in ganzen Gigabytes.",
      resourceQuotaMaximumPopover:
        "Maximale Quota, auf die diese Ressource erweitert werden kann, basierend auf der verfügbaren zugeteilten Projektquota.",

      emptyProjectSelect: "Bitte wählen Sie ein Projekt aus.",
      emptyResourceTypeSelect: "Bitte wählen Sie einen Ressourcentyp aus.",
      noResourceTypeChoosen: "Es wurde kein Ressourcentyp ausgewählt.",

      moreHelp: "Mehr anfordern",
      moreHelpLink: "https://docs.coscine.de/de/projects/storage/",

      resourceName: "Ressourcenname",
      reserved: "Reserviert",
      used: "Belegt",
      adjustQuota: "Quota anpassen",
      formatUsed: "{quota} sind von Dateien belegt.",

      emptyTableText: "Für diesen Ressourcentyp existieren keine Ressourcen.",

      connectionErrorTitle: "Fehler bei der Abfrage der Quotas",
      connectionErrorBody:
        "Bei der Abfrage der Quotas ist ein Fehler aufgetreten. Bitte versuchen Sie es erneut. Wenn der Fehler weiter auftritt, wenden Sie sich bitte an Ihre Organisation.",

      resourceTypeQuotaChangedSuccessTitle: "Quota erfolgreich verändert",
      resourceTypeQuotaChangedSuccessBody:
        "Die Quota im Projekt {ProjectName} wurde auf {AmountInGB} GB gesetzt.",
      resourceTypeQuotaChangedFailureTitle: "Fehler beim Ändern der Quota",
      resourceTypeQuotaChangedFailureBody:
        "Beim Ändern der Quota ist ein Fehler aufgetreten. Bitte versuchen Sie es erneut. Wenn der Fehler weiter auftritt, wenden Sie sich bitte an Ihre Organisation.",

      resourceQuotaChangedSuccessTitle: "Quota erfolgreich verändert",
      resourceQuotaChangedSuccessBody:
        "Die Quota in Ressource {ResourceName} wurde auf {AmountInGB} GB gesetzt.",
      resourceQuotaChangedFailureTitle: "Fehler beim Ändern der Quota",
      resourceQuotaChangedFailureBody:
        "Beim Ändern der Quota ist ein Fehler aufgetreten. Bitte versuchen Sie es erneut. Wenn der Fehler weiter auftritt, wenden Sie sich bitte an Ihre Organisation.",
    },

    // ConfigurationMetadata.vue
    configMetadata: {
      headline: "Projektkonfiguration & -metadaten",

      modal: {
        deleteModalHeadline: "Projekt wirklich entfernen?",
        deleteModalDescription:
          "Wenn Sie sicher sind, dass Sie dieses Projekt entfernen möchten, wiederholen Sie bitte den Projektnamen:",
        deleteModalHelp:
          "Der angegebene Name stimmt nicht mit dem Projektnamen überein.",
      },
    },
  },

  form: {
    project: {
      labelSymbol: ":",

      projectName: "Projektname",
      projectNameHelp:
        "Dieses Feld ist erforderlich und besitzt eine Maximallänge von {maxLength} Zeichen.",
      projectNameLabel:
        "@:{'form.project.projectName'}@:{'form.project.labelSymbol'}",

      displayName: "Anzeigename",
      displayNameHelp:
        "Dieses Feld ist erforderlich und besitzt eine Maximallänge von {maxLength} Zeichen.",
      displayNameLabel:
        "@:{'form.project.displayName'}@:{'form.project.labelSymbol'}",

      projectDescription: "Projektbeschreibung",
      projectDescriptionHelp:
        "Dieses Feld ist erforderlich und besitzt eine Maximallänge von {maxLength} Zeichen.",
      projectDescriptionLabel:
        "@:{'form.project.projectDescription'}@:{'form.project.labelSymbol'}",

      projectSlug: "Slug",
      projectSlugLabel:
        "@:{'form.project.projectSlug'}@:{'form.project.labelSymbol'}",

      projectPersistentId: "Persistent Identifier (PID)",
      projectPersistentIdLabel:
        "@:{'form.project.projectPersistentId'}@:{'form.project.labelSymbol'}",
      projectPersistentIdPopover:
        "Für weitere Informationen zum @:{'form.project.projectPersistentId'} siehe",
      projectPersistentIdPopoverUrl:
        "https://docs.coscine.de/de/resources/pid/",

      activatedImportFromParent: "Projekt-Metadaten",

      copyMetadataLabel:
        "Kopiere Metadaten aus {project}@:{'form.project.labelSymbol'}",

      projectPrincipleInvestigators: "Principal Investigators (PIs)",
      projectPrincipleInvestigatorsHelp:
        "Dieses Feld ist erforderlich und besitzt eine Maximallänge von {maxLength} Zeichen.",
      projectPrincipleInvestigatorsLabel:
        "@:{'form.project.projectPrincipleInvestigators'}@:{'form.project.labelSymbol'}",

      projectStart: "Projektstart",
      projectStartLabel:
        "@:{'form.project.projectStart'}@:{'form.project.labelSymbol'}",

      projectEnd: "Projektende",
      projectEndLabel:
        "@:{'form.project.projectEnd'}@:{'form.project.labelSymbol'}",

      projectDiscipline: "Disziplin",
      projectDisciplineLabel:
        "@:{'form.project.projectDiscipline'}@:{'form.project.labelSymbol'}",

      projectResponsibleOrganization: "Verantwortliche Organisation",
      projectAdditionalOrganization: "Zusätzliche Organisationen",
      projectResponsibleOrganizationLabel:
        "@:{'form.project.projectResponsibleOrganization'}@:{'form.project.labelSymbol'}",
      projectAdditionalOrganizationLabel:
        "@:{'form.project.projectAdditionalOrganization'}@:{'form.project.labelSymbol'}",
      projectOrganizationHint:
        "Wenn Ihre Organisation nicht in der Liste erscheint, geben Sie bitte ihren Namen in das Feld ein, um nach ihr zu suchen.",
      projectOrganizationLabelPopover:
        "Für weitere Informationen zu verantwortlichen und zustätzlichen Organisationen siehe",
      projectOrganizationLabelPopoverUrl:
        "https://docs.coscine.de/de/projects/create/",
      projectOrganizationNoOptions: "Suche nach Organisationen",
      projectOrganizationNoResult:
        "Keine Organisationen gefunden, die der Suche entsprechen",

      projectKeywords: "Projektschlagwörter",
      projectKeywordsPlaceholder:
        'Tippen und drücken Sie "Enter", um ein Schlagwort einzufügen.',
      projectKeywordsHelp:
        "Dieses Feld besitzt eine Maximallänge von {maxLength} Zeichen.",
      projectKeywordsEmpty: "Die Liste ist leer.",
      projectKeywordsLabel:
        "@:{'form.project.projectKeywords'}@:{'form.project.labelSymbol'}",
      tagPlaceholder: "Sie können diesen Tag hinzufügen",

      projectMetadataVisibility: "Sichtbarkeit der Metadaten",
      projectMetadataVisibilityLabel:
        "@:{'form.project.projectMetadataVisibility'}@:{'form.project.labelSymbol'}",
      projectMetadataVisibilityLabelPopover:
        "Für weitere Informationen zur Sichtbarkeit siehe",
      projectMetadataVisibilityLabelPopoverUrl:
        "https://docs.coscine.de/de/projects/create/",

      projectGrantId: "Grant ID",
      projectGrantIdHelp:
        "Dieses Feld besitzt eine Maximallänge von {maxLength} Zeichen.",
      projectGrantIdLabel:
        "@:{'form.project.projectGrantId'}@:{'form.project.labelSymbol'}",

      toClipboard: "{projectOption} wurde in die Zwischenablage kopiert",
    },
  },
};
