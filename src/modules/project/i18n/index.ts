import de from "./de";
import en from "./en";

export const ProjectI18nMessages = {
  en: en,
  de: de,
};

export type SupportedLanguages = keyof typeof ProjectI18nMessages;
