export default {
  /*
    --------------------------------------------------------------------------------------
    ENGLISH STRINGS
    --------------------------------------------------------------------------------------
  */
  page: {
    // ListProjects.vue
    listProjects: {
      title: "Project | Projects",
      addProject: "Add Project",
    },

    // CreateProject.vue
    createProject: {
      title: "Create Project",

      affiliationMessage:
        "As a member of RWTH Aachen your project will be provided with 25 GB quota for RDS-Web storage.",
      loadingSpinnerProjectCreation:
        "Creating projects currently takes up to a minute. Thank you for your patience.",

      copyOwnersToSubprojectLabel: "Copy Owners:",
      copyOwnersToSubprojectPopover:
        "Copy all current owners to the new sub-project? Future changes will not be automatically updated.",
    },

    // Members.vue
    members: {
      title: "Project Members",
      membersTabTitle: "Project Members",
      externalUsersTabTitle: "Invited Users",
      pendingStatus: "Pending",
      expiredStatus: "Expired",
      changeRole: "Change role",
      noUserOptions: "No user found",
      searchNotEnoughCharacters: "Please type more than 3 characters",
      user: "User",
      role: "Role",
      typeToSearch: "Search within project members...",
      perPage: "Per page",
      searchUserPlaceholder: "Search for a user or an email address to add...",
      searchEmailInvite: "You may send an invitation to this email address",
      alreadyGotRole: "(Already added)",
      selectRolePlaceholder: "Select Role",
      givenName: "Given Name",
      familyName: "Family Name",
      email: "Email",
      actions: "Actions",
      status: "Status",
      userManagement: "@:{'page.members.title'}",
      pleaseTypeSomething: "Please enter a name or an email address",
      removeSelectedInvitation:
        "Are you sure you want to revoke the invitation for {user} to the project {projectName}?",
      removeSelectedUser:
        "Are you sure you want to remove {user} from the project {projectName}?",
      deleteInvitationTitle: "Revoke Invitation",
      deleteUserTitle: "Remove User",
      inviteUser: "Send Invitation",
      reInviteUser: "Resend Invitation",
      inviteUserText:
        "Are you sure you want to invite {email} with a role as {role} to the project {projectName}?",
      inviteUserTitle: "Invite User",
      inviteUserCaption: "Choose to invite {displayName}",
      invitedUserText:
        "An invitation has been sent to {email} with a role as {role} for project {projectName}.",
      invitationPendingText:
        "An invitation has already been sent to {email}. {br}If you would like to resend an invitation email to this user, you have to cancel the previous invitation via the Invited Users tab.",
      invitedUserError:
        "An error ocurred while trying to invite {email}. Invalid input was provided or the user has already been invited.",
      deleteExternalUserError:
        "An error ocurred while trying to remove {email}.",
      emptyTableText: "No user found",
      emptyFilterText: "No user found matching your request",
      addedUser: "Added {user} to {project} as {role}",
      removedUser: "Removed {email} from {project}",
      changedRole: "Changed {user} in {project} to {role}",
      importUserTitle: "Import Members",
      emptyImportTableText: "No member to import",
      emptyImportTableFilterText:
        "No project member found matching your request",
      removeUser: "Remove",
      searchProjectPlaceholder: "Select a project...",
      existingEmailInvitation:
        "An invitation has already been sent to {email}. Do you want to send the invitation again?",
    },

    // ProjectPage.vue
    project: {
      toast: {
        merge: {
          success: {
            title: "Merging the accounts was successfull",
            body: "Your account was succcessfully merged with {externalName}.",
          },
          failure: {
            title: "Merging the accounts has failed",
            body: "Merging your account with the ID {externalId} has failed.",
          },
        },
      },
      title: "Project Page",
      resource: "Resource | Resources",
      subProject: "Sub-Project | Sub-Projects",
      member: "Member | Members",
      addResource: "Add Resource",
      tooltipDisabled:
        "Please add and verify your email address {linkToUserProfile} to create a new resource.",
      tooltipHere: "here",
      members: {
        toProjectMembers: "Manage Members....",

        modal: {
          // Leave project modal
          title: "Leave project {name}",
          body: "You are about to leave project {name}. You will also loose access to the project and its resources. Would you like to leave?",
        },
        tooltip: "Leave the project",
        tooltipDisabled:
          "You are the last project owner, add another project owner to leave this project.",
      },
    },

    // DataPublication.vue
    dataPublication: {
      continue: "Continue",
      description1:
        "Publication advisory services are designed to assist you with your data publication needs. Coscine facilitates this by connecting you to a data publication service of a partner university and simultaneously forwarding first details about your project, resources and the data you wish to publish.",
      description2:
        "Once your request has been successfully submitted, all project owners will be notified and the relevant publication advisory service will contact you to discuss the subsequent stages. Please note that Coscine does not offer a way to track the status of your submitted request.",
      title: "Data Publication Request",
      tab1: "Information",
      tab2: "Data Publication Request",
      tab3: "Summary",
    },
    // DataPublicationProjectData.vue
    dataPublicationForm: {
      description1:
        "To initiate contact with your preferred publication advisory service, please utilize the form below. If you are unsure or do not see your desired service in the list, do not hesitate to reach out to the RDM team of your organization via {email}.",
      description2:
        "Please select the relevant resources below. If the project information has to be changed, please go to the project settings page.",
      description3:
        "Feel free to add a personalized message or any specific requests for the publication advisory service.",
      service: "Data Publication Service",
      labelSymbol: ":",
      requesterName: "Requester Name",
      email: "Email",
      projectName: "Project Name",
      pid: "PID",
      discipline: "Project Discipline",
      message: "Your message",
      resource: "Resources",
      resourceLabelPopover:
        "For a resource, the following information will be submitted: Resource name, description, discipline, persistent identifier (PID), Metadata Profile.",
      messageLabelPopover:
        "Provide further information for the publication advisory service like for example a time-critical publication request or the desire for a specific publication date. ",
      labels: {
        serviceLabel:
          "@:{'page.dataPublicationForm.service'}@:{'page.dataPublicationForm.labelSymbol'}",
        requesterNameLabel:
          "@:{'page.dataPublicationForm.requesterName'}@:{'page.dataPublicationForm.labelSymbol'}",
        emailLabel:
          "@:{'page.dataPublicationForm.email'}@:{'page.dataPublicationForm.labelSymbol'}",
        projectNameLabel:
          "@:{'page.dataPublicationForm.projectName'}@:{'page.dataPublicationForm.labelSymbol'}",
        pidLabel:
          "@:{'page.dataPublicationForm.pid'}@:{'page.dataPublicationForm.labelSymbol'}",
        disciplineLabel:
          "@:{'page.dataPublicationForm.discipline'}@:{'page.dataPublicationForm.labelSymbol'}",
        messageLabel:
          "@:{'page.dataPublicationForm.message'}@:{'page.dataPublicationForm.labelSymbol'}",
        resourceLabel:
          "@:{'page.dataPublicationForm.resource'}@:{'page.dataPublicationForm.labelSymbol'}",
      },
    },
    // DataPublicationSummary.vue
    dataPublicationSummary: {
      description:
        "The following information will be submitted to the Data Publication Service and the project owners. In addition the project description and for the selected resources the Metadata Profiles, PIDs, resource description and discipline as well as the total data size will be submitted.",
      toastTitle: "Publication Request sent",
      toastBody: "The Publication Request was sent.",
    },

    // Quota.vue
    quota: {
      headline: "Project Quota",
      resources: "Resources",
      rangeText: "{allocated} GB allocated",
      gb: "{number} GB",

      projectLabel: "Project:",
      resourceTypeLabel: "Resource Type:",

      projectQuotaSliderLabel: "Project allocated quota:",
      projectQuotaMinimumPopover:
        "Minimum project quota. Based on the total reserved quota by all existing {resourceType} resources.",
      projectQuotaAllocatedPopover:
        "Current allocated quota for {resourceType} resources.",
      projectQuotaMaximumPopover:
        "Maximum project quota available for {resourceType} resources.",

      resourceQuotaMinimumPopover:
        "The total space used by all uploaded files in this resource, calculated in whole Gigabytes.",
      resourceQuotaMaximumPopover:
        "Maximum quota this resource can be extended to, based on your available project allocated quota.",

      emptyProjectSelect: "Please select a project.",
      emptyResourceTypeSelect: "Please select a resource type.",
      noResourceTypeChoosen: "No resource type selected.",

      moreHelp: "Request more",
      moreHelpLink: "https://docs.coscine.de/en/projects/storage/",

      resourceName: "Resource Name",
      reserved: "Reserved",
      used: "Used",
      adjustQuota: "Adjust Quota",
      formatUsed: "{quota} are in use by files.",

      emptyTableText: "No resources exist for this resource type.",

      connectionErrorTitle: "Resource quota retrieval not successful",
      connectionErrorBody:
        "An error occurred while retrieving the resource type quota. Please try again. If the error persists, contact your organization.",

      resourceTypeQuotaChangedSuccessTitle: "Quota extended successfully",
      resourceTypeQuotaChangedSuccessBody:
        "The quota for project {ProjectName} was successfully changed to {AmountInGB} GB.",
      resourceTypeQuotaChangedFailureTitle:
        "Resource quota extension not successful",
      resourceTypeQuotaChangedFailureBody:
        "An error occurred while extending the resource type quota. Please try again. If the error persists, contact your organization.",

      resourceQuotaChangedSuccessTitle: "Quota extended successfully",
      resourceQuotaChangedSuccessBody:
        "The quota for resource {ResourceName} was successfully changed to {AmountInGB} GB.",
      resourceQuotaChangedFailureTitle: "Quota extension not successful",
      resourceQuotaChangedFailureBody:
        "An error occurred while extending the quota. Please try again. If the error persists, contact your organization.",
    },

    // ConfigurationMetadata.vue
    configMetadata: {
      headline: "Project Configuration & Metadata",

      modal: {
        deleteModalHeadline: "Do you really want to delete this project?",
        deleteModalDescription:
          "If you are sure you really want to delete this project, please repeat the project name:",
        deleteModalHelp: "The entered name does not match the project name.",
      },
    },
  },

  form: {
    project: {
      labelSymbol: ":",

      projectName: "Project Name",
      projectNameHelp:
        "This is a required field and can only contain up to {maxLength} characters.",
      projectNameLabel:
        "@:{'form.project.projectName'}@:{'form.project.labelSymbol'}",

      displayName: "Display Name",
      displayNameHelp:
        "This is a required field and can only contain up to {maxLength} characters.",
      displayNameLabel:
        "@:{'form.project.displayName'}@:{'form.project.labelSymbol'}",

      projectDescription: "Project Description",
      projectDescriptionHelp:
        "This is a required field and can only contain up to {maxLength} characters.",
      projectDescriptionLabel:
        "@:{'form.project.projectDescription'}@:{'form.project.labelSymbol'}",

      projectSlug: "Slug",
      projectSlugLabel:
        "@:{'form.project.projectSlug'}@:{'form.project.labelSymbol'}",

      projectPersistentId: "Persistent Identifier (PID)",
      projectPersistentIdLabel:
        "@:{'form.project.projectPersistentId'}@:{'form.project.labelSymbol'}",
      projectPersistentIdPopover:
        "For more information on @:{'form.project.projectPersistentId'} see",
      projectPersistentIdPopoverUrl:
        "https://docs.coscine.de/en/resources/pid/",

      activatedImportFromParent: "Project Metadata",

      copyMetadataLabel:
        "Copy metadata from {project}@:{'form.project.labelSymbol'}",

      projectPrincipleInvestigators: "Principal Investigators (PIs)",
      projectPrincipleInvestigatorsHelp:
        "This is a required field and can only contain up to {maxLength} characters.",
      projectPrincipleInvestigatorsLabel:
        "@:{'form.project.projectPrincipleInvestigators'}@:{'form.project.labelSymbol'}",

      projectStart: "Project Start",
      projectStartLabel:
        "@:{'form.project.projectStart'}@:{'form.project.labelSymbol'}",

      projectEnd: "Project End",
      projectEndLabel:
        "@:{'form.project.projectEnd'}@:{'form.project.labelSymbol'}",

      projectDiscipline: "Discipline",
      projectDisciplineLabel:
        "@:{'form.project.projectDiscipline'}@:{'form.project.labelSymbol'}",

      projectResponsibleOrganization: "Responsible Organization",
      projectAdditionalOrganization: "Additional Organizations",
      projectResponsibleOrganizationLabel:
        "@:{'form.project.projectResponsibleOrganization'}@:{'form.project.labelSymbol'}",
      projectAdditionalOrganizationLabel:
        "@:{'form.project.projectAdditionalOrganization'}@:{'form.project.labelSymbol'}",
      projectOrganizationHint:
        "If your organization does not appear in the list, please enter its name in the field to search for it.",
      projectOrganizationLabelPopover:
        "For more information on responsible and additional Organizations see",
      projectOrganizationLabelPopoverUrl:
        "https://docs.coscine.de/en/projects/create/",
      projectOrganizationNoOptions: "Search for organizations",
      projectOrganizationNoResult: "No organizations found matching the search",

      projectKeywords: "Project Keywords",
      projectKeywordsPlaceholder:
        'Type, then press "Enter" to insert a Keyword.',
      projectKeywordsHelp:
        "This field can only contain up to {maxLength} characters.",
      projectKeywordsEmpty: "The list of keywords is empty.",
      projectKeywordsLabel:
        "@:{'form.project.projectKeywords'}@:{'form.project.labelSymbol'}",
      tagPlaceholder: "You can add this tag",

      projectMetadataVisibility: "Metadata Visibility",
      projectMetadataVisibilityLabel:
        "@:{'form.project.projectMetadataVisibility'}@:{'form.project.labelSymbol'}",
      projectMetadataVisibilityLabelPopover:
        "For more information on visibility see",
      projectMetadataVisibilityLabelPopoverUrl:
        "https://docs.coscine.de/en/projects/create/",

      projectGrantId: "Grant ID",
      projectGrantIdHelp:
        "This field can only contain up to {maxLength} characters.",
      projectGrantIdLabel:
        "@:{'form.project.projectGrantId'}@:{'form.project.labelSymbol'}",

      toClipboard: "{projectOption} has been copied to clipboard",
    },
  },
};
