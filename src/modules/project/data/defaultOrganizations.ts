import type { OrganizationDto } from "@coscine/api-client/dist/types/Coscine.Api";

export const defaultOrganizations: OrganizationDto[] = [
  {
    displayName: "FH Aachen",
    uri: "https://ror.org/04tqgg260",
  },
  {
    displayName: "Karlsruhe Institute of Technology",
    uri: "https://ror.org/04t3en479",
  },
  {
    displayName: "RWTH Aachen University",
    uri: "https://ror.org/04xfq0f34",
  },
  {
    displayName: "Technical University of Munich",
    uri: "https://ror.org/02kkvpp62",
  },
  {
    displayName: "TU Darmstadt",
    uri: "https://ror.org/05n911h24",
  },
  {
    displayName: "TU Dortmund University",
    uri: "https://ror.org/01k97gp34",
  },
  {
    displayName: "Universitätsklinikum Aachen",
    uri: "https://ror.org/02gm5zw39",
  },
  {
    displayName: "University of Cologne",
    uri: "https://ror.org/00rcxh774",
  },
  {
    displayName: "University of Duisburg-Essen",
    uri: "https://ror.org/04mz5ra38",
  },
  {
    displayName: "University of Münster",
    uri: "https://ror.org/00pd74e08",
  },
];
