import type {
  DisciplineDto,
  LicenseDto,
  OrganizationDto,
  ProjectDto,
  ProjectInvitationDto,
  ProjectInvitationForProjectManipulationDto,
  ProjectQuotaDto,
  ProjectRoleDto,
  ResourceDto,
  RoleDto,
  VisibilityDto,
} from "@coscine/api-client/dist/types/Coscine.Api";
import type { CancelTokenSource } from "axios";

export interface VisitedProjectDto extends ProjectDto {
  invitations: ProjectInvitationDto[] | null;
  resources: ResourceDto[] | null;
  roles: ProjectRoleDto[] | null;
  quotas: ProjectQuotaDto[] | null;
}

export interface ExtendedProjectInvitationDto
  extends ProjectInvitationForProjectManipulationDto {
  expired: boolean;
  invited: boolean;
  projectId: string;
}

export interface PublicationRequestTab {
  title: string;
  active: boolean;
  step: string;
  hidden: boolean;
}

export enum AcceptedLanguages {
  De = "de",
  En = "en",
}
export interface ProjectState {
  /*  
    --------------------------------------------------------------------------------------
    STATE TYPE DEFINITION
    --------------------------------------------------------------------------------------
  */
  allProjects: ProjectDto[] | null;
  currentSlug: string | null;
  visitedProjects: { [slug: string]: VisitedProjectDto };
  topLevelProjects: ProjectDto[] | null;

  disciplines: DisciplineDto[] | null;
  licenses: LicenseDto[] | null;
  publicationAdvisoryServices: OrganizationDto[] | null;
  organizations: OrganizationDto[] | null;
  organizationsMeta: {
    cancelToken: CancelTokenSource | null;
    searchTerm: string;
    pagination: {
      currentPage: number;
      hasNext: boolean;
    };
  };
  roles: RoleDto[] | null;
  visibilities: VisibilityDto[] | null;
}
