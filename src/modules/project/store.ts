import { defineStore } from "pinia";
import {
  AcceptedLanguages,
  type ProjectState,
  type VisitedProjectDto,
} from "./types";
import { removeQueryParameterFromUrl } from "@/router";
import { defaultOrganizations } from "./data/defaultOrganizations";

import {
  DisciplineApi,
  OrganizationApi,
  ProjectApi,
  ProjectResourceApi,
  ProjectInvitationApi,
  ProjectMemberApi,
  ProjectPublicationRequestApi,
  RoleApi,
  VisibilityApi,
  LicenseApi,
  ProjectQuotaApi,
  SelfApi,
} from "@coscine/api-client";
import type { RouteLocationNormalized } from "vue-router";

import useNotificationStore from "@/store/notification";
import useUserStore from "../user/store";
import axios, { AxiosError } from "axios";
import type {
  AcceptedLanguage,
  OrganizationDto,
  ProjectDto,
  ProjectForCreationDto,
  ProjectForUpdateDto,
  ProjectInvitationDto,
  ProjectInvitationForProjectManipulationDto,
  ProjectInvitationResolveDto,
  ProjectQuotaDto,
  ProjectQuotaForUpdateDto,
  ProjectRoleDto,
  ProjectRoleForProjectCreationDto,
  ProjectRoleForProjectManipulationDto,
  PublicationRequestForCreationDto,
  QuotaForManipulationDto,
  ResourceDto,
  ResourceTypeInformationDto,
  ResourceTypeOptionsForUpdateDto,
  RoleDto,
} from "@coscine/api-client/dist/types/Coscine.Api";
import {
  resourceMapper,
  ResourceDto2ResourceForUpdateDto,
} from "@/mapping/resource";
import { CoscineResourceTypes } from "../resource/types";
import { wrapListRequest } from "@/util/wrapListRequest";

/*  
  Store variable name is "this.<id>Store"
    id: "project" --> this.projectStore
  Important! The id must be unique for every store.
*/
export const useProjectStore = defineStore({
  id: "project",
  /*  
    --------------------------------------------------------------------------------------
    STATES
    --------------------------------------------------------------------------------------
  */
  state: (): ProjectState => ({
    allProjects: null,
    currentSlug: null,
    visitedProjects: {},
    topLevelProjects: null,
    disciplines: null,
    licenses: null,
    publicationAdvisoryServices: null,
    organizations: null,
    organizationsMeta: {
      cancelToken: null,
      searchTerm: "",
      pagination: {
        currentPage: 0,
        hasNext: false,
      },
    },
    roles: null,
    visibilities: null,
  }),

  /*  
    --------------------------------------------------------------------------------------
    GETTERS
    --------------------------------------------------------------------------------------
    Synchronous code only.
    
    In a component use as e.g.:
      :label = "this.projectStore.<getter_name>;
  */
  getters: {
    currentProject(): VisitedProjectDto | null {
      if (this.currentSlug && this.visitedProjects[this.currentSlug]) {
        return this.visitedProjects[this.currentSlug];
      } else {
        return null;
      }
    },
    currentResources(): ResourceDto[] | null {
      if (this.currentSlug && this.visitedProjects[this.currentSlug]) {
        return this.visitedProjects[this.currentSlug].resources;
      } else {
        return null;
      }
    },
    currentSubProjects(): ProjectDto[] | null {
      if (this.currentSlug && this.visitedProjects[this.currentSlug]) {
        return this.visitedProjects[this.currentSlug].subProjects ?? [];
      } else {
        return null;
      }
    },
    currentResourceTypesQuotas(): ProjectQuotaDto[] | null {
      if (this.currentSlug && this.visitedProjects[this.currentSlug]) {
        return this.visitedProjects[this.currentSlug].quotas;
      } else {
        return null;
      }
    },
    currentInvitations(): ProjectInvitationDto[] | null {
      if (this.currentSlug && this.visitedProjects[this.currentSlug]) {
        return this.visitedProjects[this.currentSlug].invitations;
      } else {
        return null;
      }
    },
    currentParentProjects(): ProjectDto[] | null {
      if (
        this.currentSlug &&
        this.visitedProjects[this.currentSlug] &&
        this.allProjects
      ) {
        const parentProjects = [] as ProjectDto[];
        let currentParentProjectId =
          this.visitedProjects[this.currentSlug].parent?.id;
        while (
          currentParentProjectId &&
          currentParentProjectId !== "00000000-0000-0000-0000-000000000000"
        ) {
          const parentProject = this.allProjects.find(
            (project) => project.id === currentParentProjectId,
          );
          if (parentProject) {
            if (parentProjects.includes(parentProject)) {
              break;
            }
            parentProjects.push(parentProject);
            currentParentProjectId = parentProject.parent?.id;
          } else {
            break;
          }
        }
        return parentProjects;
      } else {
        return null;
      }
    },
    currentProjectRoles(): ProjectRoleDto[] | null {
      if (this.currentSlug && this.visitedProjects[this.currentSlug]) {
        return this.visitedProjects[this.currentSlug].roles;
      } else {
        return null;
      }
    },
    currentProjectRolesSorted(): ProjectRoleDto[] | null {
      const sorted = this.currentProjectRoles;
      if (sorted) {
        sorted.sort((a, b) => {
          if (a.user && b.user && a.user.displayName && b.user.displayName) {
            const valueA = a.user.displayName.toUpperCase();
            const valueB = b.user.displayName.toUpperCase();
            return valueA < valueB ? -1 : valueA > valueB ? 1 : 0;
          } else {
            return 0;
          }
        });
        return sorted;
      } else {
        return null;
      }
    },
    currentUserRole(): RoleDto | undefined | null {
      const currentRoles = this.currentProjectRoles;
      const userStore = useUserStore();
      const currentUser = userStore.user;
      if (currentRoles && currentUser) {
        const userProjectRole = currentRoles.find(
          (projectRole) =>
            projectRole.user && projectRole.user.id === currentUser.id,
        );
        return userProjectRole ? userProjectRole.role : null;
      } else {
        return null;
      }
    },
    defaultOrganizations(): OrganizationDto[] {
      return defaultOrganizations;
    },
    currentUserRoleIsOwner(): boolean | undefined {
      if (this.currentUserRole) {
        return this.currentUserRole.displayName === "Owner";
      } else {
        return undefined;
      }
    },
    currentUserRoleIsMember(): boolean | undefined {
      if (this.currentUserRole) {
        return this.currentUserRole.displayName === "Member";
      } else {
        return undefined;
      }
    },
    currentUserRoleIsGuest(): boolean | undefined {
      if (this.currentUserRole) {
        return this.currentUserRole.displayName === "Guest";
      } else {
        return undefined;
      }
    },
  },
  /*  
    --------------------------------------------------------------------------------------
    ACTIONS
    --------------------------------------------------------------------------------------
    Asynchronous & Synchronous code comes here (e.g. API calls and VueX mutations).
    To change a state use an action.

    In a component use as e.g.:
      @click = "this.projectStore.<action_name>();
  */
  actions: {
    async retrieveAllProjects() {
      const notificationStore = useNotificationStore();
      try {
        this.allProjects = await wrapListRequest((pageNumber: number) =>
          ProjectApi.getProjects({
            topLevel: false,
            pageNumber,
            pageSize: 50,
          }),
        );
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async retrieveTopLevelProjects() {
      const notificationStore = useNotificationStore();
      try {
        this.topLevelProjects = await wrapListRequest((pageNumber: number) =>
          ProjectApi.getProjects({
            topLevel: true,
            pageNumber,
            pageSize: 50,
          }),
        );
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async getProjectById(
      projectId: string,
      silent: boolean = false,
    ): Promise<ProjectDto | null> {
      const notificationStore = useNotificationStore();
      try {
        const apiResponse = await ProjectApi.getProject({ projectId });
        return apiResponse.data.data as ProjectDto;
      } catch (error) {
        if (!silent) {
          // Handle other Status Codes
          notificationStore.postApiErrorNotification(error as AxiosError);
        }
        return null;
      }
    },

    async retrieveProjectBySlug(slug: string) {
      const notificationStore = useNotificationStore();
      try {
        const apiResponse = await ProjectApi.getProject({
          projectId: slug,
          includeSubProjects: true,
        });
        const project: ProjectDto = apiResponse.data.data as ProjectDto;
        this.setProjectAsVisited(project);
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async retrieveInvitations(project: ProjectDto | null) {
      const notificationStore = useNotificationStore();
      try {
        if (project?.id && project?.slug) {
          const projectId = project.id;
          const invitations = await wrapListRequest((pageNumber: number) =>
            ProjectInvitationApi.getProjectInvitations({
              projectId,
              pageNumber,
              pageSize: 50,
            }),
          );
          this.visitedProjects[project.slug].invitations = invitations;
        } else {
          if (!project) {
            console.error("Selected project is null.");
          }
          if (project && !project.id) {
            console.error("Selected project's ID is missing.");
          }
          if (project && !project.slug) {
            console.error("Selected project's slug is missing.");
          }
        }
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async getResourcesForProject(
      project: ProjectDto | null,
    ): Promise<ResourceDto[]> {
      const notificationStore = useNotificationStore();
      try {
        if (project?.id && project.id) {
          const projectId = project.id;
          const resources = await wrapListRequest((pageNumber) =>
            ProjectResourceApi.getResourcesForProject({
              projectId,
              pageNumber,
              pageSize: 50,
            }),
          );
          return resources;
        } else {
          if (!project) {
            console.error("Selected project is null.");
          }
          if (project && !project.id) {
            console.error("Selected project's ID is missing.");
          }
        }
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
      return [];
    },

    /**
     * Retrieves resources for a project asynchronously.
     * @param {ProjectDto | null} project - The project object or null if no project is selected.
     */
    async retrieveResources(project: ProjectDto | null) {
      if (project && project.slug && project.id) {
        // Retrieve the first page
        const projectId = project.id;
        const resources = await wrapListRequest((pageNumber) =>
          ProjectResourceApi.getResourcesForProject({
            projectId,
            pageNumber,
            pageSize: 50,
          }),
        );
        this.visitedProjects[project.slug].resources = resources;
      } else {
        console.error("Selected project is null or its ID is undefined.");
      }
    },

    /**
     * Retrieves the resource type quotas for a specific project.
     * @param {ProjectDto | null} project - The project for which to retrieve the resource type quotas.
     * @returns {Promise<void>} - A promise that resolves once the resource type quotas are retrieved.
     */
    async retrieveResourceTypeQuotas(
      project: ProjectDto | null,
    ): Promise<void> {
      const notificationStore = useNotificationStore();
      try {
        if (project?.id && project?.slug) {
          const projectId = project.id;
          const projectQuotas = await wrapListRequest((pageNumber: number) =>
            ProjectQuotaApi.getProjectQuotas({
              projectId,
              pageNumber,
              pageSize: 50,
            }),
          );
          this.visitedProjects[project.slug].quotas = projectQuotas ?? [];
        } else {
          if (!project) {
            console.error("Selected project is null.");
          }
          if (project && !project.id) {
            console.error("Selected project's ID is missing.");
          }
          if (project && !project.slug) {
            console.error("Selected project's slug is missing.");
          }
        }
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    /**
     * Updates the project's quota for the specified resource type.
     * @param {ProjectDto} project - The project for which to update the resource type quota.
     * @param {ResourceTypeInformationDto} resourceType - The resource type for which to update the quota.
     * @param {ProjectQuotaForUpdateDto} quota - The updated quota information.
     * @returns {Promise<boolean>} - True if the operation is successful, false otherwise.
     */
    async updateResourceTypeProjectQuota(
      project: ProjectDto,
      resourceType: ResourceTypeInformationDto,
      quota: ProjectQuotaForUpdateDto,
    ): Promise<boolean> {
      const notificationStore = useNotificationStore();
      try {
        if (project.id && resourceType.id) {
          await ProjectQuotaApi.updateProjectQuota({
            projectId: project.id,
            resourceTypeId: resourceType.id,
            projectQuotaForUpdateDto: quota,
          });
          return true;
        } else {
          console.error(
            "Selected project's or the selected resource type's ID is undefined.",
          );
          return false;
        }
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return false;
      }
    },

    /**
     * Updates the desired reserved quota for a resource in a project.
     * @param {string} projectId - The ID of the project.
     * @param {string} resourceId - The ID of the resource to update.
     * @param {QuotaForManipulationDto} desiredReservedQuota - The desired reserved quota for the resource.
     * @returns {boolean} - True if the operation is successful, false otherwise.
     */
    async updateResourceQuota(
      projectId: string,
      resourceId: string,
      desiredReservedQuota: QuotaForManipulationDto,
    ): Promise<boolean> {
      const notificationStore = useNotificationStore();
      try {
        const resource = this.currentProject?.resources?.find(
          (r) => resourceId === r.id,
        );
        if (!resource)
          throw new Error(
            `Resource with id: ${resourceId} not found in the current project`,
          );

        const resourceForUpdate = resourceMapper.map(
          ResourceDto2ResourceForUpdateDto,
          resource,
        );

        /**
         * The resource type options for manipulation.
         * @type {ResourceTypeOptionsForUpdateDto}
         */
        const resourceTypeOptionsForManipulationDto: ResourceTypeOptionsForUpdateDto =
          {};

        // Evaluate general resource type - RDS
        if (resource.type?.generalType === CoscineResourceTypes.Rds.General) {
          resourceTypeOptionsForManipulationDto.rdsResourceTypeOptions = {
            quota: desiredReservedQuota,
          };
        }
        // Evaluate general resource type - RDS S3
        else if (
          resource.type?.generalType === CoscineResourceTypes.RdsS3.General
        ) {
          resourceTypeOptionsForManipulationDto.rdsS3ResourceTypeOptions = {
            quota: desiredReservedQuota,
          };
        }
        // Evaluate general resource type - RDS S3 WORM
        else if (
          resource.type?.generalType === CoscineResourceTypes.RdsS3Worm.General
        ) {
          resourceTypeOptionsForManipulationDto.rdsS3WormResourceTypeOptions = {
            quota: desiredReservedQuota,
          };
        }
        // Assign the correct resource type options
        resourceForUpdate.resourceTypeOptions =
          resourceTypeOptionsForManipulationDto;

        // Trigger resource update
        await ProjectResourceApi.updateResourceForProject({
          projectId,
          resourceId,
          resourceForUpdateDto: resourceForUpdate,
        });
        return true;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return false;
      }
    },

    async getOrganizationByURL(
      url: string,
    ): Promise<OrganizationDto | undefined | null> {
      const notificationStore = useNotificationStore();
      try {
        const apiResponse = await OrganizationApi.getOrganization({
          organizationRorUri: url,
        });
        return apiResponse.data.data;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return null;
      }
    },

    /**
     * Retrieves more organizations based on the current search term and language.
     * If the search term has changed, it resets the organization list and pagination.
     * If no more pages are available and the search term hasn't changed, the function exits early.
     *
     * @param {string | boolean} searchTerm - The search term used to filter the organizations.
     * If it's a boolean, the function will use the existing filter.
     * @param {AcceptedLanguage | undefined} language - The language in which to retrieve the organization labels.
     *
     * @returns {Promise<void>} - A promise that resolves when the organizations are successfully retrieved.
     */
    async retrieveMoreOrganizations(
      searchTerm: string | boolean,
      language: AcceptedLanguage | undefined,
    ): Promise<void> {
      const notificationStore = useNotificationStore();

      // Ensure that searchTerm is a string (use existing filter if it's a boolean)
      if (typeof searchTerm === "boolean") {
        searchTerm = this.organizationsMeta?.searchTerm || "";
      }

      // Exit early if organizations are fully loaded and the search term hasn't changed
      if (
        this.organizationsMeta?.pagination?.hasNext === false &&
        this.organizationsMeta?.searchTerm === searchTerm
      ) {
        return;
      }

      // If search term has changed, reset organizations and pagination
      if (this.organizationsMeta?.searchTerm !== searchTerm) {
        this.organizations = [];
        this.organizationsMeta = {
          ...this.organizationsMeta,
          searchTerm,
          pagination: { currentPage: 0, hasNext: true },
        };

        // Cancel the previous request if it exists
        if (this.organizationsMeta.cancelToken) {
          this.organizationsMeta.cancelToken.cancel(
            "Operation canceled due to new request.",
          );
        }

        // Create a new cancel token for the new request
        this.organizationsMeta.cancelToken = axios.CancelToken.source();
      }

      try {
        // Calculate next page number
        const nextPage = this.organizationsMeta.pagination.currentPage + 1;

        // Fetch organizations from the API
        const organizationResponse = await OrganizationApi.getOrganizations(
          {
            searchTerm,
            language: language,
            pageSize: 10,
            pageNumber: nextPage,
          },
          { cancelToken: this.organizationsMeta.cancelToken?.token },
        );

        // Initialize organizations if it's null after a successful API call
        if (this.organizations === null) {
          this.organizations = [];
        }

        // If data exists, update the organizations and pagination
        if (organizationResponse.data.data) {
          this.organizations.push(...organizationResponse.data.data);
          const pagination = organizationResponse.data.pagination;
          this.organizationsMeta.searchTerm = searchTerm;
          this.organizationsMeta.pagination = {
            currentPage: pagination?.currentPage ?? 0,
            hasNext: pagination?.hasNext ?? false,
          };
        }
      } catch (error) {
        if (!axios.isCancel(error)) {
          // Handle API errors via the notification store, except for canceled requests
          notificationStore.postApiErrorNotification(error as AxiosError);
        }
      }
    },

    async retrievePublicationOrganizations(
      language: string | undefined,
      filter = "",
    ) {
      const notificationStore = useNotificationStore();
      try {
        const organizationDtos = await wrapListRequest((pageNumber: number) =>
          OrganizationApi.getOrganizations({
            searchTerm: filter,
            language:
              language == "de" ? AcceptedLanguages.De : AcceptedLanguages.En,
            filterByPublicationService: true,
            pageNumber,
            pageSize: 50,
          }),
        );
        this.publicationAdvisoryServices = organizationDtos;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async createPublicationRequest(
      project: ProjectDto,
      publicationRequest: PublicationRequestForCreationDto,
    ): Promise<boolean> {
      const notificationStore = useNotificationStore();
      try {
        if (project.id) {
          await ProjectPublicationRequestApi.createPublicationRequest({
            projectId: project.id,
            publicationRequestForCreationDto: publicationRequest,
          });
          return true;
        } else {
          console.error("Selected project's ID is undefined.");
          return false;
        }
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return false;
      }
    },

    async retrieveVisibilities() {
      const notificationStore = useNotificationStore();
      try {
        const apiResponse = await VisibilityApi.getVisibilities();
        this.visibilities = apiResponse.data.data ?? null;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async retrieveLicenses() {
      const notificationStore = useNotificationStore();
      try {
        const licenses = await wrapListRequest((pageNumber: number) =>
          LicenseApi.getLicenses({
            orderBy: "displayname asc",
            pageNumber,
            pageSize: 50,
          }),
        );
        this.licenses = licenses;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async retrieveDisciplines() {
      const notificationStore = useNotificationStore();
      try {
        const disciplineDtos = await wrapListRequest((pageNumber: number) =>
          DisciplineApi.getDisciplines({
            orderBy: "displayNameDe asc",
            pageNumber,
            pageSize: 50,
          }),
        );
        this.disciplines = disciplineDtos ?? null;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async retrieveRoles() {
      const notificationStore = useNotificationStore();
      try {
        const apiResponse = await RoleApi.getRoles();
        this.roles = apiResponse.data.data ?? null;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async resolveProjectInvitation() {
      const notificationStore = useNotificationStore();

      // .../?invitationToken=<token>
      const invitationTokenQueryParameter = "invitationToken";

      try {
        const invitationToken =
          this.router.currentRoute.value.query[invitationTokenQueryParameter];
        if (invitationToken) {
          const projectInvitationResolveDto: ProjectInvitationResolveDto = {
            token: invitationToken.toString(),
          };
          await SelfApi.resolveProjectInvitation({
            projectInvitationResolveDto,
          });
          this.refreshProjectInformation();
          removeQueryParameterFromUrl(
            this.router.currentRoute.value,
            invitationTokenQueryParameter,
          );
        }
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        removeQueryParameterFromUrl(
          this.router.currentRoute.value,
          invitationTokenQueryParameter,
        );
      }
    },

    async retrieveUnsetProject(
      project: ProjectDto | null,
      route: RouteLocationNormalized,
    ) {
      const routeParams = route.params;
      if (!routeParams) {
        return;
      }
      if (
        // Case: Project null or undefined, Slug from URL
        (!project && routeParams.slug) ||
        // Case: Project NOT null, Slug from URL not equal Project Slug
        (project !== null &&
          project.slug &&
          routeParams.slug &&
          project.slug !== routeParams.slug)
      ) {
        // Try getting project from visitedProjects before fetching from API
        const slug = routeParams.slug as string;
        if (!this.visitedProjects[slug]) {
          await this.retrieveProjectBySlug(slug);
        }
        // Handle slug and id
        this.currentSlug = this.findSlug(slug);
      }
    },

    async storeProject(
      project: ProjectForCreationDto,
    ): Promise<ProjectDto | null> {
      const notificationStore = useNotificationStore();
      try {
        const apiResponse = await ProjectApi.createProject({
          projectForCreationDto: project,
        });
        return apiResponse.data.data as ProjectDto;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return null;
      }
    },

    async updateProject(
      projectId: string,
      project: ProjectForUpdateDto,
    ): Promise<boolean> {
      const notificationStore = useNotificationStore();
      try {
        if (projectId) {
          await ProjectApi.updateProject({
            projectId,
            projectForUpdateDto: project,
          });
          return true;
        } else {
          console.error("Selected project's ID is undefined.");
          return false;
        }
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return false;
      }
    },

    async deleteProject(project: ProjectDto): Promise<boolean> {
      const notificationStore = useNotificationStore();
      try {
        if (project.id) {
          await ProjectApi.deleteProject({
            projectId: project.id,
          });
          return true;
        } else {
          console.error("Selected project's ID is undefined.");
          return false;
        }
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return false;
      }
    },

    async refreshProjectInformation(parentProject: ProjectDto | null = null) {
      await Promise.all([
        this.retrieveAllProjects(),
        this.retrieveTopLevelProjects(),
      ]);
      if (parentProject?.slug) {
        await this.retrieveProjectBySlug(parentProject.slug);
        await Promise.all([this.retrieveResources(parentProject)]);
      }
    },

    setProjectAsVisited(project: ProjectDto | null) {
      if (project && project.slug) {
        if (!this.visitedProjects[project.slug]) {
          // Important! Keep object assignment reactive()
          const visitedProject: VisitedProjectDto = reactive({
            ...project,
            invitations: null,
            resources: null,
            roles: null,
            quotas: null,
          });
          this.visitedProjects = {
            ...this.visitedProjects,
            [project.slug]: visitedProject,
          };
        } else {
          Object.assign(this.visitedProjects[project.slug], project);
        }
      }
    },

    async getProjectRoles(projectId: string): Promise<ProjectRoleDto[]> {
      const notificationStore = useNotificationStore();
      try {
        const projectRoles = await wrapListRequest((pageNumber: number) =>
          ProjectMemberApi.getMemberships({
            projectId,
            pageNumber,
            pageSize: 50,
          }),
        );
        return projectRoles;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
      return [];
    },

    async retrieveProjectRoles(project: ProjectDto | null) {
      const notificationStore = useNotificationStore();
      try {
        if (project?.slug && project?.id) {
          const projectId = project.id;
          const projectRoles = await wrapListRequest((pageNumber: number) =>
            ProjectMemberApi.getMemberships({
              projectId,
              pageNumber,
              pageSize: 50,
            }),
          );
          this.visitedProjects[project.slug].roles = projectRoles ?? [];
        } else {
          if (!project) {
            console.error("Selected project is null.");
          }
          if (project && !project.id) {
            console.error("Selected project's ID is missing.");
          }
          if (project && !project.slug) {
            console.error("Selected project's slug is missing.");
          }
        }
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async deleteProjectRole(projectRole: ProjectRoleDto): Promise<boolean> {
      const notificationStore = useNotificationStore();
      try {
        if (projectRole.id && projectRole.project?.id) {
          await ProjectMemberApi.deleteMembership({
            membershipId: projectRole.id,
            projectId: projectRole.project.id,
          });
          return true;
        } else {
          if (!projectRole.id) {
            console.error("ProjectRole's ID is missing.");
          }
          if (!projectRole.project?.id) {
            console.error("ProjectRole's project ID is missing.");
          }
          return false;
        }
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return false;
      }
    },

    async addMembershipToProject(
      projectId: string,
      projectRoleForProjectCreationDto: ProjectRoleForProjectCreationDto,
    ) {
      const notificationStore = useNotificationStore();
      try {
        await ProjectMemberApi.addMembership({
          projectId,
          projectRoleForProjectCreationDto,
        });
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async updateMembershipOfProject(
      projectId: string,
      membershipId: string,
      projectRoleForProjectManipulationDto: ProjectRoleForProjectManipulationDto,
    ) {
      const notificationStore = useNotificationStore();
      try {
        await ProjectMemberApi.updateMembership({
          projectId,
          membershipId,
          projectRoleForProjectManipulationDto,
        });
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async deleteInvitation(projectInvitation: ProjectInvitationDto) {
      const notificationStore = useNotificationStore();
      try {
        if (projectInvitation.project?.id && projectInvitation.id) {
          await ProjectInvitationApi.deleteProjectInvitation({
            projectId: projectInvitation.project.id,
            projectInvitationId: projectInvitation.id,
          });
          return true;
        } else {
          if (!projectInvitation.project?.id) {
            console.error("ProjectInvitation's projectId is missing.");
          }
          if (!projectInvitation.id) {
            console.error("ProjectInvitation's ID is missing.");
          }
        }
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return false;
      }
    },

    async storeInvitation(
      projectId: string,
      invitation: ProjectInvitationForProjectManipulationDto,
    ) {
      const notificationStore = useNotificationStore();
      try {
        await ProjectInvitationApi.createProjectInvitation({
          projectId,
          projectInvitationForProjectManipulationDto: invitation,
        });
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async deleteProjectAssociation(
      project: ProjectDto,
      projectRole: ProjectRoleDto | null | undefined,
    ) {
      const notificationStore = useNotificationStore();
      try {
        if (project?.slug && projectRole?.project?.id && projectRole?.id) {
          await ProjectMemberApi.deleteMembership({
            membershipId: projectRole.id,
            projectId: projectRole.project.id,
          });
          this.visitedProjects[project.slug].roles = null;
          this.refreshProjectInformation();
          this.router.push({ name: "home" });
        } else {
          if (!project?.slug) {
            console.error("Project slug is missing.");
          }
          if (!projectRole?.project?.id) {
            console.error("ProjectRole's project ID is missing.");
          }
          if (!projectRole?.id) {
            console.error("ProjectRole's ID is missing.");
          }
        }
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    findSlug(projectIdOrSlug: string): string | null {
      // Check if the projectIdOrSlug is a slug
      if (this.visitedProjects[projectIdOrSlug]) {
        return projectIdOrSlug;
      } else {
        // Check if the projectIdOrSlug is a projectId
        for (const slug in this.visitedProjects) {
          if (this.visitedProjects[slug].id === projectIdOrSlug) {
            return slug;
          }
        }
      }
      return null;
    },
  },
});

export default useProjectStore;
