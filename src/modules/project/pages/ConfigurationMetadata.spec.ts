/* Testing imports */
import { mount } from "@vue/test-utils";
import { createTestingPinia } from "@pinia/testing";

/* Vue i18n */
import i18n, { def } from "@/plugins/vue-i18n";
import { ProjectI18nMessages } from "@/modules/project/i18n/index";
i18n.global.availableLocales.forEach((locale) => {
  i18n.global.setLocaleMessage(locale, def[locale]); // default locale messages
  i18n.global.mergeLocaleMessage(locale, ProjectI18nMessages[locale]); // append the locale messages for the component
});

/* Additional Dependencies */

/* Tested Component */
import ConfigurationMetadata from "./ConfigurationMetadata.vue";

/* Import of relevant mockup data */
import {
  testProjectState,
  testProjectStateGuest,
} from "@/data/mockup/testProject";
import { getTestShibbolethUserState } from "@/data/mockup/testUser";

describe("ConfigurationMetadata.vue", () => {
  /* Describe Pre-initialization steps */

  /* Description of the test */
  test("Should enable buttons and update project name", async () => {
    /* Test Pre-initialization steps */

    /* Mount the Component */
    const wrapper = mount(ConfigurationMetadata, {
      pinia: createTestingPinia({
        createSpy: vitest.fn,
        initialState: {
          project: testProjectState,
          user: getTestShibbolethUserState(),
        },
      }),
    });
    // Check initial state of buttons
    expect(
      (wrapper.get("#DeleteProjectBtn").element as HTMLButtonElement).disabled,
    ).toBeFalsy(); // Delete button - active
    expect(
      (wrapper.get("#SubmitProjectBtn").element as HTMLButtonElement).disabled,
    ).toBeTruthy(); // Submit button - disabled

    await wrapper.vm.$nextTick();

    // Find element (Project Name)
    const element = wrapper.find("#ProjectName");
    expect(element.exists()).toBeTruthy();

    // Change value of element
    await element.setValue("New Test Project");
    expect(wrapper.vm.projectForUpdate.name).toBe("New Test Project");

    // Buttons should be enabled
    expect(
      (wrapper.get("#DeleteProjectBtn").element as HTMLButtonElement).disabled,
    ).toBeFalsy(); // Delete button - active
    expect(
      (wrapper.get("#SubmitProjectBtn").element as HTMLButtonElement).disabled,
    ).toBeFalsy(); // Submit button - active
  });

  test("Should be read only for guests", async () => {
    /* Mount the Component */
    const wrapper = mount(ConfigurationMetadata, {
      pinia: createTestingPinia({
        createSpy: vitest.fn,
        initialState: {
          project: testProjectStateGuest,
          user: getTestShibbolethUserState(),
        },
      }),
    });
    await wrapper.vm.$nextTick();

    // Find element (ProjectName)
    const projectName = wrapper.find("#ProjectName");
    expect(projectName.exists()).toBeTruthy();
    // Change value of element
    await projectName.setValue("New Test Project");
    expect(wrapper.vm.projectForUpdate.displayName).toBe("Test Project");
    await wrapper.vm.$nextTick();
    // Find element (Description)
    const description = wrapper.find("#Description");
    expect(description.exists()).toBeTruthy();
    await wrapper.vm.$nextTick();
    // Change value of element
    await description.setValue("New Test Project for Guests");
    expect(wrapper.vm.projectForUpdate.description).toBe(
      "Test Project Description",
    );
  });
});
