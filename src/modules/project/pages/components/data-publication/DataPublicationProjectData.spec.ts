/* Testing imports */
import { mount } from "@vue/test-utils";
import { createTestingPinia } from "@pinia/testing";

/* Vue i18n */
import i18n, { def } from "@/plugins/vue-i18n";
import { ProjectI18nMessages } from "@/modules/project/i18n/index";
i18n.global.availableLocales.forEach((locale) => {
  i18n.global.setLocaleMessage(locale, def[locale]); // default locale messages
  i18n.global.mergeLocaleMessage(locale, ProjectI18nMessages[locale]); // append the locale messages for the component
});

/* Tested Component */
import DataPublicationProjectData from "./DataPublicationProjectData.vue";

/* Import of relevant mockup data */
import { testProjectState } from "@/data/mockup/testProject";
import { getTestResourceState } from "@/data/mockup/testResource";
import { getTestShibbolethUserState } from "@/data/mockup/testUser";

describe("DataPublicationProjectData", () => {
  /* Describe Pre-initialization steps */

  /* Description of the test */

  test("Should check button, update publication service and resources and check button again", async () => {
    /* Test Pre-initialization steps */
    const initialPropValue = {
      publicationServiceRorId: "",
      resourceIds: [],
      message: "",
    };

    /* Mount the Component */
    const wrapper = mount(DataPublicationProjectData, {
      global: {
        plugins: [
          createTestingPinia({
            createSpy: vitest.fn,
            initialState: {
              project: testProjectState,
              resource: await getTestResourceState(),
              user: getTestShibbolethUserState(),
            },
          }),
        ],
      },
      propsData: {
        modelValue: initialPropValue,
      },
    });

    // Check initial state of button
    expect(
      (wrapper.get("#SubmitRequestBtn").element as HTMLButtonElement).disabled,
    ).toBeTruthy(); // Submit button - disabled

    // Find elements
    const elementService = wrapper.find("#services");
    expect(elementService.exists()).toBeTruthy();

    const elementResources = wrapper.find("#resources");
    expect(elementResources.exists()).toBeTruthy();

    const elementMessage = wrapper.find("#message");
    expect(elementMessage.exists()).toBeTruthy();

    // Check initial values
    expect(wrapper.vm.modelValue.publicationServiceRorId).toBe("");
    expect(wrapper.vm.modelValue.resourceIds).toStrictEqual([]);
    expect(wrapper.vm.modelValue.message).toBe("");

    // Change value of elements
    wrapper.setProps({
      modelValue: {
        publicationServiceRorId: "PublicationService",
        resourceIds: ["123", "456"],
        message: "This is a message.",
      },
    });

    await wrapper.vm.$nextTick();

    // Check values again
    expect(wrapper.vm.modelValue.publicationServiceRorId).toBe(
      "PublicationService",
    );
    expect(wrapper.vm.modelValue.resourceIds).toContain("123");
    expect(wrapper.vm.modelValue.resourceIds).toContain("456");
    expect(wrapper.vm.modelValue.message).toBe("This is a message.");

    // Button should be enabled
    expect(
      (wrapper.get("#SubmitRequestBtn").element as HTMLButtonElement).disabled,
    ).toBeDefined(); // Submit button - enabled

    // Set message to an empty string
    await elementMessage.setValue("");

    // Button should still be enabled
    expect(
      (wrapper.get("#SubmitRequestBtn").element as HTMLButtonElement).disabled,
    ).toBeDefined(); // Submit button - enabled
  });
});
