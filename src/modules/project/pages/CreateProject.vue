<template>
  <div id="createProject">
    <CoscineHeadline :headline="$t('page.createProject.title')" />
    <b-row>
      <div class="col-sm-2" />
      <div class="col-sm-8">
        <b-form id="edit_form" @submit.stop.prevent="clickSave">
          <!-- Affiliation Message -->
          <b-form-group
            v-if="isRWTHMember"
            label-for="AffiliationMessage"
            label-cols-sm="3"
            class="m-0 mb-2"
          >
            <b-form-text id="AffiliationMessage">
              {{ $t("page.createProject.affiliationMessage") }}
            </b-form-text>
          </b-form-group>

          <FormNaming
            v-model="projectForCreation"
            :is-loading="isLoading"
            @validation="setNamingValidation"
          />

          <FormMetadata
            v-model="projectForCreation"
            :is-loading="isLoading"
            :parent-project="project"
            @validation="setMetadataValidation"
          />

          <!-- Copy Owner Switch -->
          <CoscineFormGroup
            v-if="project"
            :label="$t('page.createProject.copyOwnersToSubprojectLabel')"
            label-for="CopyOwnerCheckbox"
            type="input"
            info
          >
            <template #popover>
              {{ $t("page.createProject.copyOwnersToSubprojectPopover") }}
            </template>

            <b-form-checkbox
              id="CopyOwnerCheckbox"
              v-model="projectForCreation.copyOwnersFromParent"
              switch
            >
            </b-form-checkbox>
          </CoscineFormGroup>

          <b-form-group>
            <!-- Submit Button -->
            <b-button
              type="submit"
              variant="primary"
              class="float-end"
              :disabled="
                formValidations.naming?.$invalid ||
                formValidations.metadata?.$invalid ||
                (!formValidations.naming?.$anyDirty &&
                  !formValidations.metadata?.$anyDirty) ||
                isWaitingForResponse
              "
              @click.prevent="clickSave"
            >
              {{ $t("buttons.submit") }}</b-button
            >
          </b-form-group>
        </b-form>

        <!-- Loading Spinner on Submit -->
        <LoadingSpinner
          :is-waiting-for-response="isWaitingForResponse"
          :text-after="$t('page.createProject.loadingSpinnerProjectCreation')"
        />
      </div>
      <div class="col-sm-2" />
    </b-row>
  </div>
</template>

<script lang="ts">
import moment from "moment";
import FormNaming from "./components/FormNaming.vue";
import FormMetadata from "./components/FormMetadata.vue";

import type { BaseValidation } from "@vuelidate/core";

// import the store for current module
import useProjectStore from "../store";
import useResourceStore from "@/modules/resource/store";
import useUserStore from "@/modules/user/store";
import useNotificationStore from "@/store/notification";
import { navigateToProject } from "@/router";
import type {
  OrganizationDto,
  ProjectDto,
  ProjectForCreationDto,
  VisibilityForProjectManipulationDto,
} from "@coscine/api-client/dist/types/Coscine.Api";
import { ProjectForCreation } from "@/models/ProjectForCreation";

export default defineComponent({
  components: {
    FormNaming,
    FormMetadata,
  },
  setup() {
    const projectStore = useProjectStore();
    const resourceStore = useResourceStore();
    const userStore = useUserStore();
    const notificationStore = useNotificationStore();

    return { projectStore, resourceStore, userStore, notificationStore };
  },

  data() {
    return {
      projectForCreation: {
        description: "",
        displayName: "",
        startDate: moment(new Date()).format("YYYY-MM-DD"),
        endDate: moment(new Date()).format("YYYY-MM-DD"),
        keywords: [] as string[],
        name: "",
        principleInvestigators: "",
        grantId: "",
        disciplines: [],
        organizations: [],
        visibility: {} as VisibilityForProjectManipulationDto,
        parentId: undefined,
        copyOwnersFromParent: undefined,
      } as ProjectForCreation,

      formValidations: {
        naming: undefined as BaseValidation<ProjectForCreationDto> | undefined,
        metadata: undefined as
          | BaseValidation<ProjectForCreationDto>
          | undefined,
      },

      isLoading: false,
      isWaitingForResponse: false,
      isCreated: false,
      projectLocalStorageKey: "coscine.projectForCreation",
    };
  },

  computed: {
    isRWTHMember(): boolean {
      const memberships = this.userStore.user?.organizations;
      if (memberships) {
        return memberships.some(
          (membership) => membership.uri === "https://ror.org/04xfq0f34",
        );
      }
      return false;
    },
    project(): ProjectDto | null {
      return this.projectStore.currentProject;
    },
  },

  async created() {
    this.isLoading = true;
    // Initialize data in projectStore and userStore.
    await this.fetchData();
    // Prefil responsible organization.
    await this.setResponsibleOrganization();
    // Restore any saved data.
    await this.restoreProjectForCreationLocalStorage();
    this.isLoading = false;
    // Save project data if user exits page and this is not detected in beforeunmount.
    window.addEventListener(
      "beforeunload",
      this.saveProjectForCreationLocalStorage,
    );
  },

  async beforeUnmount() {
    // Save form data for later use if the project has not been created.
    if (!this.isCreated) {
      await this.saveProjectForCreationLocalStorage();
    }
  },

  methods: {
    /***
     * Restores the projectForCreation with the saved data from the local storage if possible.
     */
    async restoreProjectForCreationLocalStorage(): Promise<void> {
      const projectJson = localStorage.getItem(this.projectLocalStorageKey);
      if (projectJson != null) {
        const project = JSON.parse(projectJson);
        Object.assign(this.projectForCreation, project); // Use this to only copy the properties
      }
    },
    /***
     * Saves the projectForCreation to the local storage.
     */
    async saveProjectForCreationLocalStorage(): Promise<void> {
      const projectJson = JSON.stringify(this.projectForCreation);
      localStorage.setItem(this.projectLocalStorageKey, projectJson);
    },
    /***
     * Removes the saved projectForCreation from the local storage.
     */
    async clearProjectForCreationLocalStorage(): Promise<void> {
      localStorage.removeItem(this.projectLocalStorageKey);
    },

    async fetchData() {
      // Load Project Visibilities if not present
      if (this.projectStore.visibilities === null) {
        await this.projectStore.retrieveVisibilities();
      }
      // Load Project Disciplines if not present
      if (this.projectStore.disciplines === null) {
        await this.projectStore.retrieveDisciplines();
      }
      // Load User Memberships if not present
      if (this.userStore.user === null) {
        await this.userStore.retrieveUser();
      }
    },

    /**
     * Sets the responsible organization for the project being created.
     * Marks the user's first organization as responsible and adds it to the project
     * if both URI and display name are present.
     *
     * @async
     * @function setResponsibleOrganization
     * @returns {Promise<void>}
     */
    async setResponsibleOrganization(): Promise<void> {
      // Initialize responsibleOrganization as null
      let responsibleOrganization: OrganizationDto | null = null;

      // Check if the user has at least one organization, and use the first one if available
      const userOrganizations = this.userStore.user?.organizations;
      if (userOrganizations?.[0]) {
        // Clone the first organization from user organizations and mark as responsible
        responsibleOrganization = Object.assign({}, userOrganizations[0]);
        // Only add the organization if the URI is present
        if (responsibleOrganization.uri) {
          this.projectForCreation.organizations.push({
            uri: responsibleOrganization.uri,
            responsible: true,
          });
        }
      }
    },

    async clickSave() {
      // Validate form again
      this.formValidations.naming?.$touch();
      this.formValidations.metadata?.$touch();
      // Check if there are errors in each form and if they were changed
      if (
        this.formValidations.naming?.$invalid ||
        this.formValidations.metadata?.$invalid ||
        (!this.formValidations.naming?.$anyDirty &&
          !this.formValidations.metadata?.$anyDirty)
      ) {
        console.error("The form is invalid!");
        return;
      }
      // Trigger API call to create a new project
      this.isWaitingForResponse = true;
      const createdProject = await this.projectStore.storeProject(
        this.projectForCreation,
      );
      if (createdProject) {
        // On Success
        this.formValidations.naming?.$reset();
        this.formValidations.metadata?.$reset();
        // Refresh the project information in the store
        await this.projectStore.refreshProjectInformation(this.project);
        // Remove data from localStorage.
        await this.clearProjectForCreationLocalStorage();
        this.isCreated = true;
        // Navigate inside the newly created Project
        navigateToProject(createdProject);
      } else {
        this.isWaitingForResponse = false;
        // On Failure
        this.notificationStore.postNotification({
          title: this.$t("toast.onSave.failure.title").toString(),
          body: this.$t("toast.onSave.failure.message").toString(),
          variant: "danger",
        });
      }
    },

    setNamingValidation(validation: BaseValidation<ProjectForCreationDto>) {
      // Use any to resolve a weird TypeScript error between Validations
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      this.formValidations.naming = validation as any;
    },
    setMetadataValidation(validation: BaseValidation<ProjectForCreationDto>) {
      // Use any to resolve a weird TypeScript error between Validations
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      this.formValidations.metadata = validation as any;
    },
  },
});
</script>

<style scoped></style>
