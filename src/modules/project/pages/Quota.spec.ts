/* Testing imports */
import { mount } from "@vue/test-utils";
import { createTestingPinia } from "@pinia/testing";

/* Vue i18n */
import i18n, { def } from "@/plugins/vue-i18n";
import { ProjectI18nMessages } from "@/modules/project/i18n/index";
i18n.global.availableLocales.forEach((locale) => {
  i18n.global.setLocaleMessage(locale, def[locale]); // default locale messages
  i18n.global.mergeLocaleMessage(locale, ProjectI18nMessages[locale]); // append the locale messages for the component
});

/* Tested Component */
import Quota from "./Quota.vue";

import { getTestShibbolethUserState } from "@/data/mockup/testUser";
import { getTestResourceState } from "@/data/mockup/testResource";
import { testProjectState } from "@/data/mockup/testProject";
import { useProjectStore } from "../store";
import {
  ProjectQuotaDto,
  ResourceQuotaDto,
} from "@coscine/api-client/dist/types/Coscine.Api";
import { QuotaUnit } from "@/modules/resource/types";

describe("Quota.vue", async () => {
  // Create a mocked pinia instance with initial state
  const testingPinia = createTestingPinia({
    createSpy: vitest.fn,
    initialState: {
      project: testProjectState,
      resource: await getTestResourceState(),
      user: getTestShibbolethUserState(),
    },
  });

  // Mock the API calls
  const projectStore = useProjectStore(testingPinia);
  vi.mocked(projectStore.updateResourceQuota).mockReturnValue(
    Promise.resolve(true),
  );

  const createWrapper = () => {
    return mount(Quota, {
      global: {
        plugins: [testingPinia, i18n],
      },
      computed: {
        project() {
          return projectStore.currentProject;
        },

        resources() {
          return [
            { id: "res1", type: { id: "rt1" }, displayName: "Resource 1" },
          ];
        },
        resourceTypesInformation() {
          return [
            {
              id: "rt1",
              iDisplayName: "Resource Type 1",
              isQuotaAdjustable: true,
              isQuotaAvailable: true,
              isEnabled: true,
            },
            {
              id: "rt2",
              iDisplayName: "Resource Type 2",
              isQuotaAdjustable: true,
              isQuotaAvailable: true,
              isEnabled: true,
            },
          ];
        },
        resourceQuotaTableFields() {
          return [
            {
              key: "name",
              label: "name",
              sortable: true,
              sortByFormatted: true,
            },
            { key: "used", label: "page.quota.used", sortable: true },
            {
              key: "reserved",
              label: "page.quota.reserved",
              sortable: true,
            },
            { key: "adjust" },
          ];
        },
        resourceQuotas() {
          return [
            {
              reserved: { value: 2, unit: QuotaUnit.GibiByte },
              used: { value: 1, unit: QuotaUnit.GibiByte },
              usedPercentage: 0.5,
              resource: { id: "res1" },
            },
          ];
        },
        resourceTypesQuotas() {
          return [
            {
              allocated: { value: 50, unit: QuotaUnit.GibiByte },
              totalReserved: { value: 10, unit: QuotaUnit.GibiByte },
              maximum: { value: 100, unit: QuotaUnit.GibiByte },
              resourceType: { id: "rt1" },
              resourceQuotas: [
                {
                  reserved: { value: 2, unit: QuotaUnit.GibiByte },
                  used: { value: 1, unit: QuotaUnit.GibiByte },
                  usedPercentage: 0.5,
                  resource: { id: "res1" },
                },
              ] as ResourceQuotaDto[],
            },
          ] as ProjectQuotaDto[];
        },
        selectedResourceTypeInformation() {
          return {
            id: "rt1",
            iDisplayName: "Resource Type 1",
            isQuotaAdjustable: true,
            isQuotaAvailable: true,
            isEnabled: true,
          };
        },
      },
    });
  };

  let wrapper: ReturnType<typeof createWrapper>;
  beforeEach(() => {
    // shallowMount does not work here!
    wrapper = createWrapper();
  });

  test("Should populate the resource types dropdown correctly.", async () => {
    await wrapper.vm.$nextTick();

    const options = wrapper
      .findComponent({ ref: "resourceTypeSelect" })
      .findAll("option");
    expect(options.length).toBeGreaterThan(1);
    expect(options.at(1)?.text()).toContain("Resource Type 1");
    expect(options.at(2)?.text()).toContain("Resource Type 2");
  });

  test("Should adjust the project resource type quota", async () => {
    wrapper.vm.selectedQuotas = {
      allocated: { value: 50, unit: QuotaUnit.GibiByte },
      totalReserved: { value: 10, unit: QuotaUnit.GibiByte },
    };

    await wrapper.vm.$nextTick();

    const slider = wrapper.find("#projectQuotaSlider");
    await slider.setValue(20);

    expect(projectStore.updateResourceTypeProjectQuota).toHaveBeenCalledOnce();
  });

  test("Should adjust the resource quota", async () => {
    await wrapper.setData({
      selectedQuotas: {
        allocated: { value: 50, unit: QuotaUnit.GibiByte },
        totalReserved: { value: 10, unit: QuotaUnit.GibiByte },
        maximum: { value: 100, unit: QuotaUnit.GibiByte },
        resourceType: { id: "rt1" },
        totalUsed: { value: 1 },
        projectId: "p1",
        resourceQuotas: [
          {
            reserved: { value: 2, unit: QuotaUnit.GibiByte },
            used: { value: 1, unit: QuotaUnit.GibiByte },
            usedPercentage: 0.5,
            resource: { id: "res1" },
          },
        ],
      } as ProjectQuotaDto | undefined,
    });

    await wrapper.vm.$nextTick();

    const slider = wrapper.find("#resourceQuotaSlider-res1");
    await slider.setValue(15);

    expect(projectStore.updateResourceQuota).toHaveBeenCalledOnce();
  });
});
