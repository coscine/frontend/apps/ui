import {
  LanguageApi,
  OrganizationApi,
  SelfApi,
  SelfApiTokenApi,
  TitleApi,
  UserApi,
} from "@coscine/api-client";
import { defineStore } from "pinia";
import type { UserState } from "./types";

// import the main store
import useMainStore from "@/store/index";
import useNotificationStore from "@/store/notification";
import type { AxiosError } from "axios";
import type { RouteLocationNormalized } from "vue-router";
import i18n from "@/plugins/vue-i18n";
import { removeQueryParameterFromUrl } from "@/router";
import { DisciplineApi } from "@coscine/api-client";
import type {
  ApiTokenForCreationDto,
  IdentityProviders,
  UserForUpdateDto,
  UserMergeDto,
} from "@coscine/api-client/dist/types/Coscine.Api";
import { wrapListRequest } from "@/util/wrapListRequest";
import axios from "axios";
import useLoginStore from "../login/store";

/*  
  Store variable name is "this.<id>Store"
    id: "user" --> this.userStore
  Important! The id must be unique for every store.
*/
export const useUserStore = defineStore({
  id: "user",
  /*  
    --------------------------------------------------------------------------------------
    STATES
    --------------------------------------------------------------------------------------
  */
  state: (): UserState => ({
    userProfile: {
      // TODO: Move to project store
      disciplines: null,
      languages: null,
      organizations: null,
      titles: null,
      tokens: null,
    },
    user: null,
    isRetrievingUser: false,
  }),

  /*  
    --------------------------------------------------------------------------------------
    GETTERS
    --------------------------------------------------------------------------------------
    Synchronous code only.
    
    In a component use as e.g.:
      :label = "this.userStore.<getter_name>;
  */
  getters: {
    /**
     * Checks if the user is loaded and the retrieval process is complete.
     *
     * @param state - The current state of the user store.
     * @returns {boolean} - Returns true if the user data is loaded and not currently being retrieved, otherwise false.
     *
     * @remarks
     * The `!!` syntax is true if `state.user` is truthy (like a non-null object) and false if `state.user` is falsy (like `null` or `undefined`).
     */
    isUserLoaded: (state): boolean => !!state.user && !state.isRetrievingUser,

    /**
     * Retrieves the contact email of the user's primary organization.
     * If no organization email is found, returns the default email address.
     *
     * @returns {string} - The contact email of the primary organization or a default email address if none is found.
     */
    contactEmail(): string {
      if (this.user?.organizations) {
        const emails = this.user.organizations.filter((userMembership) =>
          userMembership.email ? true : false,
        );
        if (emails.length > 0 && emails[0].email) {
          return emails[0].email;
        }
      }
      return "servicedesk@itc.rwth-aachen.de";
    },
  },
  /*  
    --------------------------------------------------------------------------------------
    ACTIONS
    --------------------------------------------------------------------------------------
    Asynchronous & Synchronous code comes here (e.g. API calls and VueX mutations).
    To change a state use an action.

    In a component use as e.g.:
      @click = "this.userStore.<action_name>();
  */
  actions: {
    async searchUsers(searchString: string, projectId: string) {
      const notificationStore = useNotificationStore();
      try {
        const apiResponse = await UserApi.getUsers({
          searchTerm: searchString,
          orderBy: projectId,
        });
        return apiResponse.data.data;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async confirmUserEmail(route: RouteLocationNormalized) {
      // .../?emailtoken=<token>
      const notificationStore = useNotificationStore();
      const tokenKey = "emailtoken";
      try {
        const confirmationToken = route.query[tokenKey]?.toString();
        if (confirmationToken) {
          await SelfApi.confirmUserEmail({
            confirmationToken,
          });
          notificationStore.postNotification({
            title: i18n.global
              .t("toast.contactChange.success.title")
              .toString(),
            body: i18n.global
              .t("toast.contactChange.success.message")
              .toString(),
          });
          // Update user information inside the store
          await this.retrieveUser();
          // Remove token from the URL
          removeQueryParameterFromUrl(route, tokenKey);
        }
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postNotification({
          title: i18n.global.t("toast.contactChange.failure.title").toString(),
          body: i18n.global.t("toast.contactChange.failure.message").toString(),
          variant: "warning",
        });
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async retrieveDisciplines() {
      const notificationStore = useNotificationStore();
      try {
        const disciplines = await wrapListRequest((pageNumber) =>
          DisciplineApi.getDisciplines({
            orderBy: "displaynamede asc",
            pageNumber,
            pageSize: 50,
          }),
        );
        this.userProfile.disciplines = disciplines ?? null;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async retrieveLanguages() {
      const notificationStore = useNotificationStore();
      try {
        const languages = await LanguageApi.getLanguages();
        this.userProfile.languages = languages.data.data;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async retrieveOrganizations(filter = "") {
      const notificationStore = useNotificationStore();
      try {
        // Intentionally only display 10
        const apiResponse = await OrganizationApi.getOrganizations({
          searchTerm: filter,
          pageSize: 10,
        });
        this.userProfile.organizations =
          apiResponse.data.data !== undefined ? apiResponse.data.data : null;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async retrieveTitles() {
      const notificationStore = useNotificationStore();
      try {
        const titles = await TitleApi.getTitles();
        this.userProfile.titles = titles.data.data;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async retrieveTokens() {
      const notificationStore = useNotificationStore();
      try {
        const tokens = await wrapListRequest((pageNumber) =>
          SelfApiTokenApi.getAllApiTokens({
            pageNumber,
            pageSize: 50,
          }),
        );

        this.userProfile.tokens = tokens;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async retrieveUser() {
      if (this.isRetrievingUser) {
        return;
      }
      this.isRetrievingUser = true;
      const notificationStore = useNotificationStore();
      try {
        const apiResponse = await SelfApi.getCurrentUser();
        this.user = apiResponse.data.data;
      } catch (error) {
        if (axios.isAxiosError(error)) {
          if (error.response?.status === 401) {
            // If 401 Unauthorized is returned, force a logout
            const loginStore = useLoginStore();
            loginStore.logout();
          }
          // Handle other Status Codes
          notificationStore.postApiErrorNotification(error as AxiosError);
        }
      }
      this.isRetrievingUser = false;
    },

    async deleteToken(tokenId: string) {
      const notificationStore = useNotificationStore();
      try {
        await SelfApiTokenApi.revokeToken({
          apiTokenId: tokenId,
        });
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async initiateUserMerge(
      provider: IdentityProviders,
    ): Promise<UserMergeDto | null | undefined> {
      const notificationStore = useNotificationStore();
      try {
        const apiResponse = await SelfApi.initiateUserMerge({
          identityProvider: provider,
        });
        return apiResponse.data.data;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return null;
      }
    },

    async createApiToken(tokenForCreation: ApiTokenForCreationDto) {
      const notificationStore = useNotificationStore();
      try {
        const apiResponse = await SelfApiTokenApi.createApiToken({
          apiTokenForCreationDto: tokenForCreation,
        });
        return apiResponse.data.data;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async updateUser(userForUpdateDto: UserForUpdateDto): Promise<boolean> {
      const notificationStore = useNotificationStore();
      try {
        await SelfApi.updateCurrentUser({
          userForUpdateDto,
        });
        return true;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return false;
      }
    },

    setUserLanguagePreference() {
      if (this.user && this.user.language && this.user.language?.abbreviation) {
        const mainStore = useMainStore();
        mainStore.coscine.locale = this.user.language?.abbreviation;
      }
    },
  },
});

export default useUserStore;
