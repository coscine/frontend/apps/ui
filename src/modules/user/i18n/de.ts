export default {
  /*
    --------------------------------------------------------------------------------------
    GERMAN STRINGS
    --------------------------------------------------------------------------------------
  */
  page: {
    userprofile: {
      form: {
        labelSymbol: ":",

        personalInformation: {
          header: "Persönliche Daten",

          title: "Titel",
          givenName: "Vorname",
          familyName: "Nachname",
          email: "E-Mail",
          organization: "Organisation",
          discipline: "Disziplin",

          emailChange: {
            changedTitle: "Änderung der E-Mail-Adresse",
            changedMessage:
              "E-Mail-Adresse wurde geändert. Eine E-Mail zur Bestätigung der Änderung wurde an die neue E-Mail-Adresse gesendet.",
            noAddress: "Keine E-Mail-Adresse angegeben.",
            pendingConfirmation: "E-Mail-Bestätigung ausstehend.",
          },

          multiselect: {
            placeholderText:
              "@:{'page.userprofile.form.personalInformation.organization'}",
            placeholderTitle: "Bitte wählen Sie einen Titel.",
            placeholderDiscipline:
              "@:{'page.userprofile.form.personalInformation.discipline'}",
            selectEnter: "Zum Auswählen Enter drücken.",
            noResults:
              "Keine Ergebnisse verfügbar. Bitte passen Sie die Suchanfrage an.",
            noOptions: "Die Liste ist leer.",
            noOptionsOrganization: "Suche nach Organisationen",
            searchNotEnoughCharacters:
              "Bitte geben Sie mindestens {min} Zeichen ein, um zu suchen.",
          },

          labels: {
            titleLabel:
              "@:{'page.userprofile.form.personalInformation.title'}@:{'page.userprofile.form.labelSymbol'}",
            firstNameLabel:
              "@:{'page.userprofile.form.personalInformation.givenName'}@:{'page.userprofile.form.labelSymbol'}",
            lastNameLabel:
              "@:{'page.userprofile.form.personalInformation.familyName'}@:{'page.userprofile.form.labelSymbol'}",
            emailLabel:
              "@:{'page.userprofile.form.personalInformation.email'}@:{'page.userprofile.form.labelSymbol'}",
            organizationLabel:
              "@:{'page.userprofile.form.personalInformation.organization'}@:{'page.userprofile.form.labelSymbol'}",
            disciplineLabel:
              "@:{'page.userprofile.form.personalInformation.discipline'}@:{'page.userprofile.form.labelSymbol'}",
          },
        },

        accessToken: {
          header: "Zugriffstoken erstellen",
          bodyText:
            "Sie können für jede von Ihnen verwendete Anwendung, die Zugriff auf die Coscine-API benötigt, ein persönliches Zugriffstoken generieren. Weitere Informationen finden Sie in der Dokumentation.",
          tokenExpire: "Verfallsdatum",
          tokenName: "Token Name",

          table: {
            emptyText: "Sie haben kein aktives Zugriffs Token.",
            tokenCreated: "Erstellt",
            tokenExpires: "Verfällt",
            tokenAction: "Aktion",
          },

          modal: {
            createToken: {
              // -- Modal contents for creating access-token
              title: "Ihr neues persönliches Access Token",
              body: "Stellen Sie sicher, dass Sie es speichern - Sie können dann nicht mehr darauf zugreifen.",
              copyToClipboard: "Token wurde in die Zwischenablage kopiert",
            },
            revokeToken: {
              // -- Modal contents for revoking access-token
              title: "Token Widerrufen",
              body: "Sind Sie sicher, dass Sie dieses Token widerrufen wollen? Diese Aktion kann nicht rückgängig gemacht werden.",
            },
          },

          labels: {
            tokenNameLabel:
              "@:{'page.userprofile.form.accessToken.tokenName'}@:{'page.userprofile.form.labelSymbol'}",
            tokenExpireLabel:
              "Verfällt am@:{'page.userprofile.form.labelSymbol'}",
          },
        },

        userPreferences: {
          header: "Nutzendenpräferenzen",
          language: "Sprache",

          labels: {
            languageLabel:
              "@:{'page.userprofile.form.userPreferences.language'}@:{'page.userprofile.form.labelSymbol'}",
          },
        },
        connectedAccounts: {
          header: "Verbundene Accounts",
          nfdi4ingaai: "NFDI Login",
          orcid: "ORCID®",
          shibboleth: "Single Sign-On",
          shibbolethMergeOff:
            "Das Zusammenführen mit Single Sign-On ist derzeit deaktiviert. Um Ihre Konten zusammenzuführen, melden Sie sich bitte zuerst mit Ihrem institutionellen Konto an.",
          labels: {
            nfdi4IngAaiLabel:
              "@:{'page.userprofile.form.connectedAccounts.nfdi4ingaai'}@:{'page.userprofile.form.labelSymbol'}",
            orcidLabel:
              "@:{'page.userprofile.form.connectedAccounts.orcid'}@:{'page.userprofile.form.labelSymbol'}",
            shibbolethLabel:
              "@:{'page.userprofile.form.connectedAccounts.shibboleth'}@:{'page.userprofile.form.labelSymbol'}",
          },
        },
      },
      mergeModal: {
        title: "Mit {provider} Konto zusammenführen",
        body: "Um Ihre Konten zusammenzuführen, müssen Sie sich zuerst {logout} und {login}.",
        logout: "ausloggen",
        login: "dann mit Ihrem {provider} Konto einloggen",
        ready: "Wenn Sie bereit sind, bestätigen Sie es unten.",
      },
      mergeModalV2: {
        title: "Aktuelles Konto mit {provider} Konto zusammenführen",
        body: "Um Ihre Konten zusammenzuführen, müssen Sie sich in {provider} anmelden. Dazu werden Sie auf die entsprechende Loginseite weitergeleitet.{br}{br}Bitte beachten Sie, dass durch das Zusammenführen Ihrer Konten {tokenDisclaimer}, und Sie müssen neue erstellen.",
        tokenDisclaimer:
          "alle zuvor erstellten API-Token aus Ihrem {provider}-Konto ungültig werden",
        ready: "Wenn Sie bereit sind, bestätigen Sie es unten.",
      },
    },
  },
};
