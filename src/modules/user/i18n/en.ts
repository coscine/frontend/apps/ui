export default {
  /*
    --------------------------------------------------------------------------------------
    ENGLISH STRINGS
    --------------------------------------------------------------------------------------
  */
  page: {
    userprofile: {
      form: {
        labelSymbol: ":",

        personalInformation: {
          header: "Personal Information",

          title: "Title",
          givenName: "Given Name",
          familyName: "Family Name",
          email: "Email",
          organization: "Organization",
          discipline: "Discipline",

          emailChange: {
            changedTitle: "Email address changed",
            changedMessage:
              "Email address was changed. A mail for confirmation was send to the new email address.",
            noAddress: "No email address given.",
            pendingConfirmation: "Email confirmation pending.",
          },

          multiselect: {
            placeholderText:
              "@:{'page.userprofile.form.personalInformation.organization'}",
            placeholderTitle: "Please select a title.",
            placeholderDiscipline: "Select Discipline(s)",
            selectEnter: "Press enter to select.",
            noResults: "No elements found. Consider changing the search query.",
            noOptions: "List is empty.",
            noOptionsOrganization: "Search for organizations",
            searchNotEnoughCharacters:
              "Please enter at least {min} characters to search.",
          },

          labels: {
            titleLabel:
              "@:{'page.userprofile.form.personalInformation.title'}@:{'page.userprofile.form.labelSymbol'}",
            firstNameLabel:
              "@:{'page.userprofile.form.personalInformation.givenName'}@:{'page.userprofile.form.labelSymbol'}",
            lastNameLabel:
              "@:{'page.userprofile.form.personalInformation.familyName'}@:{'page.userprofile.form.labelSymbol'}",
            emailLabel:
              "@:{'page.userprofile.form.personalInformation.email'}@:{'page.userprofile.form.labelSymbol'}",
            organizationLabel:
              "@:{'page.userprofile.form.personalInformation.organization'}@:{'page.userprofile.form.labelSymbol'}",
            disciplineLabel:
              "@:{'page.userprofile.form.personalInformation.discipline'}@:{'page.userprofile.form.labelSymbol'}",
          },
        },

        accessToken: {
          header: "Access Token",
          bodyText:
            "You can generate a personal access token for each application you use that needs access to the Coscine API. See the documentation for further information.",
          tokenExpire: "Expiration Date",
          tokenName: "Token Name",

          table: {
            emptyText: "You have no active access token.",
            tokenCreated: "Created",
            tokenExpires: "Expires",
            tokenAction: "Action",
          },

          modal: {
            createToken: {
              // -- Modal contents for creating access-token
              title: "Your new personal access token",
              body: "Make sure you save it - you won't be able to access it again.",
              copyToClipboard: "Token has been copied to clipboard",
            },
            revokeToken: {
              // -- Modal contents for revoking access-token
              title: "Revoke Token",
              body: "Are you sure you want to revoke this token? This action can not be undone.",
            },
          },

          labels: {
            tokenNameLabel:
              "@:{'page.userprofile.form.accessToken.tokenName'}@:{'page.userprofile.form.labelSymbol'}",
            tokenExpireLabel:
              "Expires on@:{'page.userprofile.form.labelSymbol'}",
          },
        },

        userPreferences: {
          header: "User Preferences",
          language: "Language",

          labels: {
            languageLabel:
              "@:{'page.userprofile.form.userPreferences.language'}@:{'page.userprofile.form.labelSymbol'}",
          },
        },
        connectedAccounts: {
          header: "Connected Accounts",
          nfdi4ingaai: "NFDI Login",
          orcid: "ORCID®",
          shibboleth: "Single Sign-On",
          shibbolethMergeOff:
            "Merging with Single Sign-On is currently disabled. To merge your accounts, please log in using your institutional account first.",

          labels: {
            nfdi4IngAaiLabel:
              "@:{'page.userprofile.form.connectedAccounts.nfdi4ingaai'}@:{'page.userprofile.form.labelSymbol'}",
            orcidLabel:
              "@:{'page.userprofile.form.connectedAccounts.orcid'}@:{'page.userprofile.form.labelSymbol'}",
            shibbolethLabel:
              "@:{'page.userprofile.form.connectedAccounts.shibboleth'}@:{'page.userprofile.form.labelSymbol'}",
          },
        },
      },
      mergeModal: {
        title: "Merge {provider} Account",
        body: "To merge your accounts, you must first {logout} and {login}.",
        logout: "log out",
        login: "then log back in with your {provider} account",
        ready: "When ready, proceed by confirming below.",
      },
      mergeModalV2: {
        title: "Merge current account with {provider} account",
        body: "To merge your accounts, you must first log in with {provider}. You will be redirected to the respective login page.{br}{br}Please note that {tokenDisclaimer}, and you will need to generate new ones.",
        tokenDisclaimer:
          "merging your accounts will invalidate all previously created API tokens from the {provider} account",
        ready: "When ready, proceed by confirming below.",
      },
    },
  },
};
