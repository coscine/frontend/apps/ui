import type { RouteRecordRaw } from "vue-router";

const UserModule = () => import("./UserModule.vue");
const UserProfile = () => import("./pages/UserProfile.vue");

import { UserI18nMessages } from "@/modules/user/i18n/index";

export const UserRoutes: RouteRecordRaw[] = [
  {
    path: "/user",
    component: UserModule,
    meta: {
      i18n: UserI18nMessages,
    },
    children: [
      {
        path: "",
        name: "userprofile",
        component: UserProfile,
        meta: {
          breadCrumb: "user.profile",
          requiresAuth: true,
        },
      },
    ],
  },
];
