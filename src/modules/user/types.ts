import type {
  ApiTokenDto,
  DisciplineDto,
  LanguageDto,
  OrganizationDto,
  TitleDto,
  UserDto,
} from "@coscine/api-client/dist/types/Coscine.Api";

export interface UserState {
  /*  
    --------------------------------------------------------------------------------------
    STATE TYPE DEFINITION
    --------------------------------------------------------------------------------------
  */
  userProfile: {
    disciplines: DisciplineDto[] | null;
    languages: LanguageDto[] | null | undefined;
    organizations: OrganizationDto[] | null;
    titles: TitleDto[] | null | undefined;
    tokens: ApiTokenDto[] | null | undefined;
  };
  user: UserDto | null | undefined;
  isRetrievingUser: boolean;
}

export interface TokenValidityBoundDates {
  minDate: Date;
  maxDate: Date;
}

/**
 * Defining the {@link IdentityProviders} enum inside the types, because it is not being exported correctly from the @coscine/api-client library.
 */
export enum IdentityProviders {
  Shibboleth = "Shibboleth",
  OrciD = "ORCiD",
}
