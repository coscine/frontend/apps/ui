/* Testing imports */
import { mount } from "@vue/test-utils";
import { TestingPinia, createTestingPinia } from "@pinia/testing";

/* Vue i18n */
import i18n, { def } from "@/plugins/vue-i18n";
import { UserI18nMessages } from "@/modules/user/i18n/index";
i18n.global.availableLocales.forEach((locale) => {
  i18n.global.setLocaleMessage(locale, def[locale]); // default locale messages
  i18n.global.mergeLocaleMessage(locale, UserI18nMessages[locale]); // append the locale messages for the component
});

/* Tested Component */
import UserProfile from "./UserProfile.vue";
import MergeUserModalV2 from "./modals/MergeUserModalV2.vue";
import {
  testOrganizationFromShibboleth,
  getTestShibbolethUserState,
  getTestOrcidUserState,
  OrcidProvider,
  NFDI4Ing_AAIProvider,
  getTestShibbolethUserWithoutLanguageState,
} from "@/data/mockup/testUser";
import type Multiselect from "vue-multiselect";

describe("UserProfile.vue", () => {
  const createWrapper = (testingPinia: TestingPinia) => {
    return mount(UserProfile, {
      global: {
        plugins: [testingPinia, i18n],
      },
    });
  };

  let wrapper: ReturnType<typeof createWrapper>;

  /* Checks for correct button validation for ORCiD (external) users */
  test("ORCiD (external) User", async () => {
    // Prepare the initial state of the user store
    const testUserState = getTestOrcidUserState();
    if (!testUserState.user) {
      throw new Error("Test user is null or undefined!");
    }
    // Clear the organizations to test the validation
    testUserState.user.organizations = [];
    // Create a mocked pinia instance with initial state
    const testingPinia = createTestingPinia({
      createSpy: vitest.fn,
      initialState: {
        user: testUserState,
      },
    });
    // Mount the component
    wrapper = createWrapper(testingPinia);

    /*
     * NOTE:  This test might fail if any of the other validation property is invalid!
     *        Ensure here that the rest of the properties inside the validation v$ are valid.
     */
    expect(wrapper.vm.v$.userForUpdate.organization.$invalid).toBeTruthy();
    // Save button should be disabled, since the organization is not set
    expect(
      (wrapper.get("#saveBtn").element as HTMLButtonElement).disabled,
    ).toBeTruthy();

    /* Organization */
    const element = wrapper.findComponent<Multiselect>({
      ref: "organization",
    });
    expect(element.exists()).toBeTruthy();
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (element.vm as unknown as Multiselect as unknown as any).select(
      testOrganizationFromShibboleth.displayName,
    );
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.userForUpdate.organization).toBe(
      testOrganizationFromShibboleth.displayName,
    );

    /* Active Save Button since every condition is fulfilled and the entire form is valid */
    expect(wrapper.vm.v$.userForUpdate.organization.$invalid).toBeFalsy();
    expect(
      (wrapper.get("#saveBtn").element as HTMLButtonElement).disabled,
    ).toBeFalsy();
  });

  /* Checks for correct button validation for internal users */
  test("Shibboleth (internal) User email changed", async () => {
    // Prepare the initial state of the user store
    const testUserState = getTestShibbolethUserState();
    if (!testUserState.user) {
      throw new Error("Test user is null or undefined!");
    }
    // Create a mocked pinia instance with initial state
    const testingPinia = createTestingPinia({
      createSpy: vitest.fn,
      initialState: {
        user: testUserState,
      },
    });
    // Mount the component
    wrapper = createWrapper(testingPinia);

    /* Save button disabled, since nothing has changed */
    expect(wrapper.vm.v$.userForUpdate.email.$invalid).toBeFalsy();
    expect(wrapper.vm.v$.userForUpdate.organization.$invalid).toBeFalsy();
    expect(
      (wrapper.get("#saveBtn").element as HTMLButtonElement).disabled,
    ).toBeTruthy();

    /* Organization */
    const element = wrapper.findComponent({
      ref: "organization",
    });
    expect(element.exists()).toBeTruthy();
    expect(
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      (element.vm as unknown as typeof Multiselect as any).disabled,
    ).toBe(true);

    /* Email */
    expect(wrapper.vm.userForUpdate.email).toBe("example@university.com");
    expect(wrapper.vm.v$.userForUpdate.email.$invalid).toBeFalsy();
    expect(wrapper.vm.v$.userForUpdate.email.$anyDirty).toBeFalsy();
    await wrapper.get("#email").setValue("servicedesk@rwth-aachen.de");
    expect(wrapper.vm.userForUpdate.email).toBe("servicedesk@rwth-aachen.de");
    expect(wrapper.vm.v$.userForUpdate.email.$invalid).toBeFalsy();
    expect(wrapper.vm.v$.userForUpdate.email.$anyDirty).toBeTruthy();

    /* Active Save Button since every condition is fulfilled */
    expect(
      (wrapper.get("#saveBtn").element as HTMLButtonElement).disabled,
    ).toBeFalsy();
  });

  /* Checks for correct button validation for new internal users without a language set */
  test("Shibboleth (internal) New user and no language is set", async () => {
    // Prepare the initial state of the user store
    const testUserState = getTestShibbolethUserWithoutLanguageState();
    if (!testUserState.user) {
      throw new Error("Test user is null or undefined!");
    }
    // Create a mocked pinia instance with initial state
    const testingPinia = createTestingPinia({
      createSpy: vitest.fn,
      initialState: {
        user: testUserState,
      },
    });
    // Mount the component
    wrapper = createWrapper(testingPinia);

    await wrapper.vm.$nextTick();

    /* Save button disabled, since nothing has changed */
    expect(wrapper.vm.v$.userForUpdate.email.$invalid).toBeFalsy();
    expect(wrapper.vm.v$.userForUpdate.organization.$invalid).toBeFalsy();

    expect(
      (wrapper.get("#saveBtn").element as HTMLButtonElement).disabled,
    ).toBeTruthy();

    /* Language */
    expect(wrapper.vm.v$.userForUpdate.language).toBeDefined();

    wrapper.vm.userForUpdate.language.id = "";

    wrapper.vm.$forceUpdate();

    // Manually trigger the validation
    wrapper.vm.v$.userForUpdate.language.$touch();

    await wrapper.vm.$nextTick();

    expect(wrapper.vm.v$.userForUpdate.language.$invalid).toBeTruthy();
    expect(wrapper.vm.v$.userForUpdate.language.$anyDirty).toBeTruthy();

    /* Disabled Save Button since every condition is fulfilled */
    expect(
      (wrapper.get("#saveBtn").element as HTMLButtonElement).disabled,
    ).toBeTruthy();

    await wrapper.vm.$nextTick();
  });

  /* Checks for correct ORCID connect button behavior */
  test("ORCID connect button triggers user merge modal", async () => {
    const testUserState = getTestShibbolethUserState();
    if (!testUserState.user) {
      throw new Error("Test user is null or undefined!");
    }
    testUserState.user.organizations = [testOrganizationFromShibboleth];
    /* Assign the test pinia store to a variable to use later */
    const testingPinia = createTestingPinia({
      createSpy: vitest.fn,
      initialState: {
        user: testUserState,
      },
    });
    const wrapper = createWrapper(testingPinia);

    // Identify the ORCID button
    const orcidConnectButton = wrapper.find('button[name="orcidConnect"]');
    expect(
      (orcidConnectButton.element as HTMLButtonElement).disabled,
    ).toBeFalsy();
    await orcidConnectButton.trigger("click.prevent");

    await wrapper.vm.$nextTick();

    // Assert modal appeared
    const modal = wrapper.findComponent(MergeUserModalV2);
    expect(modal.exists()).toBeTruthy();
  });

  /* Checks for correct NFDI4Ing AAI connect button behavior */
  test("NFDI4Ing AAI connect button triggers user merge modal", async () => {
    const testUserState = getTestShibbolethUserState();
    if (!testUserState.user) {
      throw new Error("Test user is null or undefined!");
    }
    testUserState.user.organizations = [testOrganizationFromShibboleth];
    /* Assign the test pinia store to a variable to use later */
    const testingPinia = createTestingPinia({
      createSpy: vitest.fn,
      initialState: {
        user: testUserState,
      },
    });
    const wrapper = createWrapper(testingPinia);

    // Identify the NFDI4Ing AAI button
    const nfdi4IngAaiButton = wrapper.find('button[name="nfdi4IngAaiConnect"]');
    expect(
      (nfdi4IngAaiButton.element as HTMLButtonElement).disabled,
    ).toBeFalsy();
    await nfdi4IngAaiButton.trigger("click.prevent");

    await wrapper.vm.$nextTick();

    // Assert modal appeared
    const modal = wrapper.findComponent(MergeUserModalV2);
    expect(modal.exists()).toBeTruthy();
  });

  /* Checks for correct connect button behavior */
  test("Connect buttons disabled", async () => {
    const testUserState = getTestShibbolethUserState();
    if (!testUserState.user) {
      throw new Error("Test user is null or undefined!");
    }

    testUserState.user.organizations = [testOrganizationFromShibboleth];

    testUserState.user.identities = [OrcidProvider, NFDI4Ing_AAIProvider];

    /* Assign the test pinia store to a variable to use later */
    const testingPinia = createTestingPinia({
      createSpy: vitest.fn,
      initialState: {
        user: testUserState,
      },
    });

    const wrapper = createWrapper(testingPinia);

    // Identify the NFDI4Ing AAI button
    const orcidConnectButton = wrapper.find('button[name="orcidConnect"]');
    expect(
      (orcidConnectButton.element as HTMLButtonElement).disabled,
    ).toBeTruthy();

    const nfdi4IngAaiButton = wrapper.find('button[name="nfdi4IngAaiConnect"]');
    expect(
      (nfdi4IngAaiButton.element as HTMLButtonElement).disabled,
    ).toBeTruthy();
  });
});
