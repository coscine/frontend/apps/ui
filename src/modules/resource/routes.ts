import type { RouteRecordRaw } from "vue-router";

const CreateResource = () => import("./pages/CreateResource.vue");
const ProjectModule = () => import("../project/ProjectModule.vue");
const ResourceModule = () => import("./ResourceModule.vue");
const ResourcePage = () => import("./pages/ResourcePage.vue");
const Settings = () => import("./pages/Settings.vue");

import { ResourceI18nMessages } from "@/modules/resource/i18n/index";

export const ResourceRoutes: RouteRecordRaw[] = [
  {
    // Omit the slash "/" at the start of "path" to not reference "root". Nested under module "projects".
    path: "create-resource",
    component: ProjectModule,
    meta: {
      i18n: ResourceI18nMessages,
      breadCrumb: "resource.create",
    },
    children: [
      {
        path: "",
        alias: "",
        name: "create-resource",
        component: CreateResource,
        meta: {
          requiresAuth: true,
        },
      },
    ],
  },
  {
    path: "r/:guid?",
    component: ResourceModule,
    meta: {
      breadCrumb: "resource.page",
      default: "resource-page",
      i18n: ResourceI18nMessages,
      requiresAuth: true,
    },
    children: [
      {
        path: "",
        redirect: (to) => {
          return {
            name: "resource-page",
            params: {
              slug: to.params.slug,
              guid: to.params.guid,
              dirTrail: "",
            },
            meta: {
              breadCrumb: "resource.page",
              requiresAuth: true,
            },
          };
        },
      },
      {
        path: "-/:dirTrail(.*)*",
        name: "resource-page",
        component: ResourcePage,
        meta: {
          requiresAuth: true,
        },
      },
      {
        path: "settings",
        name: "resource-settings",
        component: Settings,
        meta: {
          breadCrumb: "resource.settings",
          requiresAuth: true,
        },
      },
    ],
  },
];
