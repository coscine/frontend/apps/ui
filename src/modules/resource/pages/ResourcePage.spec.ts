/* Testing imports */
import { mount } from "@vue/test-utils";
import { createTestingPinia } from "@pinia/testing";

/* Vue i18n */
import i18n, { def } from "@/plugins/vue-i18n";
import { ResourceI18nMessages } from "@/modules/resource/i18n/index";
i18n.global.availableLocales.forEach((locale) => {
  i18n.global.setLocaleMessage(locale, def[locale]); // default locale messages
  i18n.global.mergeLocaleMessage(locale, ResourceI18nMessages[locale]); // append the locale messages for the component
});

/* Tested Component */
import ResourcePage from "./ResourcePage.vue";

import { getTestShibbolethUserState } from "@/data/mockup/testUser";
import { getTestResourceState } from "@/data/mockup/testResource";
import { testProjectState } from "@/data/mockup/testProject";
import useResourceStore from "../store";
import {
  getFileTreeResponse,
  getMetadataTreeResponse,
} from "@/data/mockup/responses/getMetadata";
import type { FileInformation } from "../types";

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

/**
 * Generates a dummy file object for testing purposes.
 * This function creates a file with a specified size and name without allocating the actual memory for its contents.
 * @param {number} sizeInByte The desired size of the file in bytes.
 * @param {string} name The name of the file.
 * @returns {File} A File object with the specified name and a mocked size property. The file contains no actual data.
 */
function generateDummyFile(sizeInByte: number, name: string): File {
  const file = new File([""], name, { type: "text/plain" });
  // Mock the size property to simulate a file with a specific size without allocating memory
  Object.defineProperty(file, "size", { value: sizeInByte });
  return file;
}

describe("ResourcePage.vue", async () => {
  // Create a mocked pinia instance with initial state
  const testingPinia = createTestingPinia({
    createSpy: vitest.fn,
    initialState: {
      project: testProjectState,
      resource: await getTestResourceState(),
      user: getTestShibbolethUserState(),
    },
  });

  // Mock the API calls
  const resourceStore = useResourceStore(testingPinia);
  vi.mocked(resourceStore.getMetadataTree).mockReturnValue(
    Promise.resolve(getMetadataTreeResponse),
  );
  vi.mocked(resourceStore.getFileTree).mockReturnValue(
    Promise.resolve(getFileTreeResponse),
  );
  vi.mocked(resourceStore.getVocabularyInstances).mockReturnValue(
    Promise.resolve({ en: [], de: [] }),
  );

  const createWrapper = () => {
    return mount(ResourcePage, {
      pinia: testingPinia,
    });
  };

  let wrapper: ReturnType<typeof createWrapper>;
  beforeEach(() => {
    // shallowMount does not work here!
    wrapper = createWrapper();
  });

  test(
    "Should render form fields and file entries and reflect metadata upon selection (folder with no metadata).",
    {
      // Override the maximum run time for this test (10 sec), due to the sleep() calls
      timeout: 10000,
    },
    async () => {
      await wrapper.vm.$nextTick();

      // Wait for 1 second until everything is set up
      await sleep(1000); // Don't remove!

      // Form-Generator rendered
      // The test resource uses the Base Application profile,
      // which has has 5 fields (2x text, 1x date, 2x combo box)
      let wrapperInputFields = wrapper.findAllComponents({
        name: "WrapperInput",
      });
      expect(wrapperInputFields.length).toBe(5);

      // File-View rendered with as many entries as the test resource has files/folders
      const tableFields = wrapper.findAll(".fileViewEntry");
      expect(tableFields.length).toBeGreaterThanOrEqual(3);

      // Find the first entry in the table and select it
      const firstFound = wrapper.find("td");
      await firstFound.trigger("click");

      // Wait for 1 second until everything is updated
      await sleep(1000); // Don't remove!

      // After selecting an entry in the table, its metadata should be rendered in the metadata manager
      wrapperInputFields = wrapper.findAllComponents({
        name: "InputTextField",
      });
      const textInput = wrapperInputFields[0]; // For the used AP, the first InputTextField is "Title"
      expect((textInput.element as HTMLInputElement).value).toBe("");
    },
  );

  test(
    "Should render form fields and file entries and reflect metadata upon selection",
    {
      // Override the maximum run time for this test (10 sec), due to the sleep() calls
      timeout: 10000,
    },
    async () => {
      await wrapper.vm.$nextTick();

      // Wait for 1 second until everything is set up
      await sleep(1000); // Don't remove!

      // Form-Generator rendered
      // The test resource uses the Base Application profile,
      // which has has 5 fields (2x text, 1x date, 2x combo box)
      let wrapperInputFields = wrapper.findAllComponents({
        name: "WrapperInput",
      });
      expect(wrapperInputFields.length).toBe(5);

      // File-View rendered with as many entries as the test resource has files/folders
      const tableFields = wrapper.findAll(".fileViewEntry");
      expect(tableFields.length).toBeGreaterThanOrEqual(3);

      // Find the first entry in the table and select it
      const foundEntries = wrapper.findAll("tbody tr");
      await foundEntries[2].find("td").trigger("click");

      // Wait for 1 second until everything is updated
      await sleep(1000); // Don't remove!

      // After selecting an entry in the table, its metadata should be rendered in the metadata manager
      wrapperInputFields = wrapper.findAllComponents({
        name: "InputTextField",
      });
      const textInput = wrapperInputFields[0]; // For the used AP, the first InputTextField is "Title"
      expect((textInput.element as HTMLInputElement).value).toBe("Revised");
    },
  );

  test("Should properly handle files on upload", async () => {
    await wrapper.vm.$nextTick();

    // Mock file selection
    const files = [
      generateDummyFile(1024 * 1024 * 500, "normalSizeFile.txt"), // A 500 MiB file
      generateDummyFile(1024 * 1024 * 1024 * 2 - 1, "largeSizeFile.txt"), // A 2 GiB - 1 byte file
      generateDummyFile(1024 * 1024 * 1024 * 2, "oversizedFile1.txt"), // A 2 GiB file
      generateDummyFile(1024 * 1024 * 1024 * 3, "oversizedFile2.txt"), // A 3 GiB file
    ];

    // Starting with a clean state, no files should be present for upload
    let fileListUpload: FileInformation[] = wrapper.vm.fileListUpload;
    expect(fileListUpload.length).toBe(0);

    // Simulate file input
    const fileTrigger = wrapper.findComponent({ ref: "fileTrigger" });
    fileTrigger.vm.$emit("update:modelValue", files);

    await wrapper.vm.$nextTick();

    fileListUpload = wrapper.vm.fileListUpload;
    expect(fileListUpload.length).toBe(4); // The 500 MiB and 2 GiB - 1 byte files should be present for upload
    expect(
      fileListUpload.some((f) => f.name === "normalSizeFile.txt"),
    ).toBeTruthy();
    expect(
      fileListUpload.some((f) => f.name === "largeSizeFile.txt"),
    ).toBeTruthy();
  });
});
