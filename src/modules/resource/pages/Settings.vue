<template>
  <div id="settings">
    <CoscineHeadline :headline="$t('page.settings.title')" />

    <!-- Overview -->
    <Overview />

    <!-- Navigation Tabs -->
    <div v-if="!isLoading && projectStore.currentUserRole" class="w-100 mb-3">
      <b-tabs justified class="my-4">
        <b-tab
          v-for="(tab, index) in tabs.filter((t) => !t.hidden)"
          :key="index"
          :disabled="!tab.active"
          :title="tab.title"
          @click.prevent="toTab(tab)"
        />
      </b-tabs>

      <!-- Configuration -->
      <Configuration
        v-show="currentTab === 'configuration'"
        v-model="resourceForUpdate"
      />

      <!-- Resource Metadata -->
      <ResourceMetadata
        v-show="currentTab === 'resourceMetadata'"
        v-model="resourceForUpdate"
        :is-loading="isLoading"
        :readonly="
          (resource ? resource.archived : false) || !isUserAllowedToEdit
        "
        @validation="setMetadataValidation"
      /><!-- TODO: Fix @validation assignment and typing -->

      <!-- Application Profile -->
      <ApplicationProfile
        v-show="currentTab === 'applicationProfile'"
        v-model="resourceForUpdate"
        :application-profile="applicationProfile"
        :is-loading-form-generator="isLoading"
        :is-user-allowed-to-edit="isUserAllowedToEdit"
        @clickSave="clickSave"
      />

      <!-- Actions -->
      <Actions
        v-if="resource"
        v-show="currentTab === 'action'"
        :resource-for-update="resourceForUpdate"
        :is-user-allowed-to-edit="isUserAllowedToEdit"
        @toggleArchive="toggleArchive"
        @toggleLocalMetadataCopy="toggleLocalMetadataCopy"
        @clickDelete="clickDelete"
      />

      <!-- Button Confirm -->
      <b-form-group>
        <b-button
          v-if="confirmButtonVisibility"
          :disabled="!validResourceForm || resource?.archived == true"
          class="float-end"
          variant="primary"
          @click.prevent="clickSave"
        >
          {{ $t("buttons.confirm") }}
        </b-button>
      </b-form-group>
    </div>
    <b-row v-else align-h="center" class="my-4">
      <b-spinner variant="primary" />
    </b-row>
    <!-- Loading Spinner on Submit -->
    <LoadingSpinner :is-waiting-for-response="isWaitingForResponse" />
  </div>
</template>

<script lang="ts">
// import the store for current module
import useResourceStore from "../store";
import useProjectStore from "@/modules/project/store";
import useUserStore from "@/modules/user/store";
import { navigateToProject } from "@/router";
import ResourceMetadata from "../components/create-resource/ResourceMetadata.vue";
import ApplicationProfile from "../components/settings/ApplicationProfile.vue";
import Actions from "../components/settings/Actions.vue";
import Overview from "../components/settings/Overview.vue";
import Configuration from "../components/settings/Configuration.vue";
import {
  type ApplicationProfileDefinition,
  CoscineResourceTypes,
  type ResourceCreationTab,
} from "../types";
import useNotificationStore from "@/store/notification";
import type { BaseValidation } from "@vuelidate/core";
import type {
  DisciplineForResourceManipulationDto,
  GitLabOptionsDto,
  LicenseForResourceManipulationDto,
  ProjectDto,
  RdsOptionsDto,
  RdsS3OptionsDto,
  RdsS3WormOptionsDto,
  ResourceDto,
  ResourceForCreationDto,
  ResourceForUpdateDto,
  UserDto,
  VisibilityForResourceManipulationDto,
} from "@coscine/api-client/dist/types/Coscine.Api";
import {
  resourceMapper,
  ResourceDto2ResourceForUpdateDto,
} from "@/mapping/resource";

export default defineComponent({
  components: {
    Overview,
    Configuration,
    ResourceMetadata,
    ApplicationProfile,
    Actions,
  },
  setup() {
    const resourceStore = useResourceStore();
    const projectStore = useProjectStore();
    const userStore = useUserStore();
    const notificationStore = useNotificationStore();

    return { resourceStore, projectStore, userStore, notificationStore };
  },

  data() {
    return {
      coscineResourceTypes: CoscineResourceTypes,
      currentTab: "" as string | undefined,
      resourceForUpdate: {
        description: "",
        displayName: "",
        name: "",
        keywords: [] as string[],
        license: undefined as LicenseForResourceManipulationDto | undefined,
        usageRights: "",
        disciplines: [] as DisciplineForResourceManipulationDto[],
        visibility: {} as VisibilityForResourceManipulationDto,
        archived: false,
      } as ResourceForUpdateDto,
      applicationProfile: null as ApplicationProfileDefinition | null,
      validation: {} as BaseValidation<
        ResourceForCreationDto | ResourceForUpdateDto
      >,
      isWaitingForResponse: false,
      isLoading: false,
    };
  },

  computed: {
    project(): ProjectDto | null {
      return this.projectStore.currentProject;
    },
    resource(): ResourceDto | null {
      return this.resourceStore.currentResource;
    },
    resourceTypeOptions():
      | object
      | GitLabOptionsDto
      | RdsOptionsDto
      | RdsS3OptionsDto
      | RdsS3WormOptionsDto
      | undefined {
      switch (this.resource?.type?.generalType) {
        case this.coscineResourceTypes.LinkedData.General:
          return this.resource.type.options?.linkedData;
        case this.coscineResourceTypes.Gitlab.General:
          return this.resource.type.options?.gitLab;
        case this.coscineResourceTypes.Rds.General:
          return this.resource.type.options?.rds;
        case this.coscineResourceTypes.RdsS3.General:
          return this.resource.type.options?.rdsS3;
        case this.coscineResourceTypes.RdsS3Worm.General:
          return this.resource.type.options?.rdsS3Worm;
        default:
          return undefined;
      }
    },
    validResourceForm(): boolean {
      if (this.resource && this.resource.archived) {
        // Limit button to only changes in the "Resource Metadata" tab, when the resource is archived.
        return !this.validation.$invalid && this.validation.$anyDirty;
      } else {
        // Button should always be enabled; Alternatively make the validation check here.
        return true;
      }
    },
    confirmButtonVisibility(): boolean {
      if (this.isUserAllowedToEdit) {
        if (this.resource && this.resource.archived) {
          // Limit button to only be visible in the "Resource Metadata" tab, when the resource is archived.
          return this.currentTab === "resourceMetadata";
        } else if (
          this.resource?.type?.generalType ===
          this.coscineResourceTypes.Gitlab.General
        ) {
          return (
            this.currentTab === "configuration" ||
            this.currentTab === "resourceMetadata" ||
            this.currentTab === "applicationProfile"
          );
        } else {
          return (
            this.currentTab === "resourceMetadata" ||
            this.currentTab === "applicationProfile"
          );
        }
      }
      return false;
    },
    tabs(): ResourceCreationTab[] {
      // Order of the objects does matter
      const tabs: ResourceCreationTab[] = [
        {
          title: this.$t("form.steps.first").toString(),
          active: true,
          step: "configuration",
          hidden: (this.isGuest || this.isMember) ?? false,
        },
        {
          title: this.$t("form.steps.second").toString(),
          active: true,
          step: "resourceMetadata",
          hidden: false,
        },
        {
          title: this.$t("form.steps.third").toString(),
          active: true,
          step: "applicationProfile",
          hidden: false,
        },
        {
          title: this.$t("form.steps.fifth").toString(),
          active: true,
          step: "action",
          hidden: false,
        },
      ];
      // Remove the first (Configuration) tab for linked data resources
      if (
        this.resource?.type?.generalType ===
        this.coscineResourceTypes.LinkedData.General
      ) {
        return tabs.splice(1);
      }
      return tabs;
    },
    isOwner(): boolean | undefined {
      return this.projectStore.currentUserRoleIsOwner;
    },
    isMember(): boolean | undefined {
      return this.projectStore.currentUserRoleIsMember;
    },
    isGuest(): boolean | undefined {
      return this.projectStore.currentUserRoleIsGuest;
    },
    user(): UserDto | null | undefined {
      return this.userStore.user;
    },
    isResourceCreator(): boolean | undefined {
      if (this.resource?.creator && this.user?.id) {
        return this.resource.creator.id === this.user.id;
      } else {
        return undefined;
      }
    },
    isUserAllowedToEdit(): boolean {
      // Convoluted logic that prohibits a Guest&Creator edits, but allows Member&Creator edits. Setting it like so for the time being to manage the deadline.
      // TODO: Proper Roles Matrix
      if (
        (this.isOwner ||
          (this.isResourceCreator && !(this.isGuest || this.isMember))) &&
        this.resourceForUpdate
      ) {
        return true;
      }
      return false;
    },
  },

  watch: {
    resource(oldValue: ResourceDto | null, newValue: ResourceDto | null) {
      if (oldValue?.id !== newValue?.id) {
        this.init();
      }
    },
    "resource.archived"() {
      // Listen only for archived status changes
      this.onResourceLoaded();
    },
    "resource.metadataLocalCopy"() {
      // Listen only for metadata local copy status changes
      this.onResourceLoaded();
    },
  },

  mounted() {
    this.init();
  },

  methods: {
    async init() {
      this.isLoading = true;
      await this.fetchData();
      this.onResourceLoaded();
      this.isLoading = false;
    },
    async fetchData() {
      if (this.project?.id && this.resource?.id) {
        // Load resource
        await this.resourceStore.retrieveResource(
          this.project?.id,
          this.resource?.id,
        );
      }
      // Load Project Visibilities if not present
      if (this.projectStore.visibilities === null) {
        await this.projectStore.retrieveVisibilities();
      }
      // Load Project Disciplines if not present
      if (this.projectStore.disciplines === null) {
        await this.projectStore.retrieveDisciplines();
      }
      // Load Project Licenses if not present
      if (this.projectStore.licenses === null) {
        await this.projectStore.retrieveLicenses();
      }
    },
    onResourceLoaded() {
      if (this.resource) {
        // Fill the form. Note that regular assignment breaks reactivity!
        const mapped = resourceMapper.map(
          ResourceDto2ResourceForUpdateDto,
          this.resource,
        );
        Object.assign(this.resourceForUpdate, mapped);

        this.getApplicationProfile();
        this.toTab(this.tabs.filter((t) => !t.hidden)[0]);
      }
    },

    toTab(tab: ResourceCreationTab) {
      this.currentTab = this.tabs.find((t) => t.step === tab.step)?.step;
    },

    async getApplicationProfile() {
      if (this.resource && this.resource.applicationProfile) {
        this.isLoading = true;
        const applicationProfile =
          await this.resourceStore.getApplicationProfile(
            this.resource.applicationProfile.uri ?? "",
          );
        this.applicationProfile = applicationProfile;
        this.isLoading = false;
      } else {
        this.applicationProfile = null;
      }
    },

    async clickSave() {
      if (this.project?.id && this.resource?.id) {
        this.isWaitingForResponse = true;
        const success = await this.resourceStore.updateResourceForProject(
          this.project?.id,
          this.resource?.id,
          this.resourceForUpdate,
        );
        if (success) {
          // On Success
          // Refresh the project information in the store
          await this.projectStore.refreshProjectInformation(this.project);
          await this.resourceStore.retrieveResource(
            this.project?.id,
            this.resource?.id,
          );
          this.notificationStore.postNotification({
            title: this.$t("toast.onSave.success.title").toString(),
            body: this.$t("toast.onSave.success.message").toString(),
          });
        } else {
          // On Failure
          this.notificationStore.postNotification({
            title: this.$t("toast.onSave.failure.title").toString(),
            body: this.$t("toast.onSave.failure.message").toString(),
            variant: "danger",
          });
        }
        this.isWaitingForResponse = false;
      }
    },

    async toggleArchive() {
      if (this.project?.id && this.resource?.id) {
        this.isWaitingForResponse = true;
        // Re-map the original resource, so that one only sets its archived,
        // otherwise we risk saving the rest of the possible temporary changes
        // to the resourceForUpdate variable as a side-effect
        const resourceForArchiving = resourceMapper.map(
          ResourceDto2ResourceForUpdateDto,
          this.resource,
        );
        resourceForArchiving.archived = !resourceForArchiving.archived;
        const success = await this.resourceStore.updateResourceForProject(
          this.project.id,
          this.resource.id,
          resourceForArchiving,
        );
        if (success) {
          // On Success
          // Refresh the project information in the store
          await this.projectStore.refreshProjectInformation(this.project);
          // TODO: User will lose all resourceForUpdate changes here - consider different archiving UI approach
          await this.resourceStore.retrieveResource(
            this.project?.id,
            this.resource?.id,
          );
          const action = !this.resource.archived ? "unarchive" : "archive";
          this.notificationStore.postNotification({
            title: this.$t(
              `page.settings.actions.${action}.toast.title`,
            ).toString(),
            body: this.$t(
              `page.settings.actions.${action}.toast.body`,
            ).toString(),
          });
        } else {
          // On Failure
          this.notificationStore.postNotification({
            title: this.$t("toast.onSave.failure.title").toString(),
            body: this.$t("toast.onSave.failure.message").toString(),
            variant: "danger",
          });
        }
        this.isWaitingForResponse = false;
      }
    },

    async toggleLocalMetadataCopy() {
      if (this.project?.id && this.resource?.id) {
        this.isWaitingForResponse = true;
        // Re-map the original resource, so that one only sets its metadataLocalCopy property,
        // otherwise we risk saving the rest of the possible temporary changes
        // to the resourceForUpdate variable as a side-effect
        const resourceForLocalMetadataCopy = resourceMapper.map(
          ResourceDto2ResourceForUpdateDto,
          this.resource,
        );
        resourceForLocalMetadataCopy.metadataLocalCopy =
          !resourceForLocalMetadataCopy.metadataLocalCopy;
        const success = await this.resourceStore.updateResourceForProject(
          this.project.id,
          this.resource.id,
          resourceForLocalMetadataCopy,
        );
        if (success) {
          // On Success
          // Refresh the project information in the store
          await this.projectStore.refreshProjectInformation(this.project);
          // TODO: User will lose all resourceForUpdate changes here - consider different archiving UI approach
          await this.resourceStore.retrieveResource(
            this.project?.id,
            this.resource?.id,
          );
          const action = !this.resource.metadataLocalCopy
            ? "disableLocalMetadataCopy"
            : "enableLocalMetadataCopy";
          this.notificationStore.postNotification({
            title: this.$t(
              `page.settings.actions.${action}.toast.title`,
            ).toString(),
            body: this.$t(
              `page.settings.actions.${action}.toast.body`,
            ).toString(),
          });
        } else {
          // On Failure
          this.notificationStore.postNotification({
            title: this.$t("toast.onSave.failure.title").toString(),
            body: this.$t("toast.onSave.failure.message").toString(),
            variant: "danger",
          });
        }
        this.isWaitingForResponse = false;
      }
    },

    async clickDelete() {
      if (this.project?.id && this.resource?.id) {
        this.isWaitingForResponse = true;
        const success = await this.resourceStore.deleteResourceForProject(
          this.project?.id,
          this.resource?.id,
        );
        if (success) {
          // On Success
          const parentProject = this.project;
          // Refresh the project information in the store
          await this.projectStore.refreshProjectInformation(parentProject);
          // Refresh the project quota information
          await this.projectStore.retrieveResourceTypeQuotas(parentProject);
          this.notificationStore.postNotification({
            title: this.$t("toast.onDelete.success.title").toString(),
            body: this.$t("toast.onDelete.success.message").toString(),
          });
          navigateToProject(parentProject);
        } else {
          // On Failure
          this.notificationStore.postNotification({
            title: this.$t("toast.onDelete.failure.title").toString(),
            body: this.$t("toast.onDelete.failure.message").toString(),
            variant: "danger",
          });
        }
        this.isWaitingForResponse = false;
      }
    },

    setMetadataValidation(
      validation: BaseValidation<ResourceForCreationDto | ResourceForUpdateDto>,
    ) {
      // Use any to resolve a weird TypeScript error between Validations
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      this.validation = validation as any;
    },
  },
});
</script>
