import factory from "rdf-ext";
import { Readable } from "stream";
import { rdfParser } from "rdf-parse";
import type { Dataset, Quad } from "@rdfjs/types";
import formats, { mediaTypes } from "@rdfjs-elements/formats-pretty";
import stringifyStream from "stream-to-string";
import { ApplicationProfileApi } from "@coscine/api-client";
import type DatasetExt from "rdf-ext/lib/Dataset";

export async function parseRDFDefinition(
  definition: string,
  contentType = "text/turtle",
  baseIRI = "http://coscine.rwth-aachen.de",
): Promise<Dataset> {
  const input = new Readable({
    read: () => {
      input.push(definition);
      input.push(null);
    },
  });
  const dataset = factory.dataset();
  await new Promise((resolve) => {
    rdfParser
      .parse(input, { contentType: contentType, baseIRI: baseIRI })
      .on("data", (quad: Quad) => dataset.add(quad))
      .on("error", (error: unknown) => console.error(error))
      .on("end", () => resolve(dataset));
  });
  return dataset as unknown as Dataset;
}

/**
 * Serializes an RDF dataset into a specified format.
 *
 * @param {Dataset | null} dataset - The RDF dataset to be serialized.
 * @param {string} [contentType="text/turtle"] - The MIME type for the serialization format.
 * @returns {Promise<string>} A promise that resolves with the serialized dataset as a string.
 *
 * @example
 * const turtleStr = await serializeRDFDefinition(myDataset, "text/turtle");
 */
export async function serializeRDFDefinition(
  dataset: Dataset | null,
  contentType: string = "text/turtle",
): Promise<string> {
  if (!dataset || dataset.size === 0) return "";

  const output = formats.serializers.import(contentType, dataset.toStream());
  if (!output) return "";

  return await stringifyStream(output as NodeJS.ReadableStream);
}

/**
 * Retrieves all relevant RDF content types
 * @returns media types
 */
export async function getRDFContentTypes(): Promise<string[]> {
  return Object.values(mediaTypes);
}

/**
 * Deals with import statements and create a full application profile
 * @param baseUrl Base Url of the starting dataset
 * @param dataset The starting dataset
 * @returns Full Dataset with every imported dataset
 */
export async function resolveImports(
  baseUrl: string,
  dataset: Dataset,
): Promise<Dataset> {
  let fullApplicationProfile = dataset;

  const toScanAPs = [dataset];
  const owlImportsNode = factory.namedNode(
    "http://www.w3.org/2002/07/owl#imports",
  );
  const visitedAPs = [baseUrl];
  while (toScanAPs.length > 0) {
    const dataset = toScanAPs.pop();
    if (dataset) {
      const importedAPs = Array.from(
        dataset.match(null, owlImportsNode, null),
      ).map((quad) => quad.object);
      for (const importedAP of importedAPs) {
        if (
          importedAP.termType === "NamedNode" &&
          !visitedAPs.includes(importedAP.value)
        ) {
          try {
            const importedApiResponse =
              await ApplicationProfileApi.getApplicationProfile({
                profile: importedAP.value,
              });
            const apResponse = importedApiResponse.data.data;
            if (
              apResponse?.definition?.content &&
              apResponse?.definition.type
            ) {
              const importedApplicationProfile = await parseRDFDefinition(
                apResponse.definition.content,
                apResponse.definition.type,
                importedAP.value,
              );
              fullApplicationProfile = (
                fullApplicationProfile as unknown as DatasetExt
              ).merge(
                importedApplicationProfile as unknown as DatasetExt,
              ) as unknown as Dataset;
              toScanAPs.push(importedApplicationProfile);
            } else {
              console.error("Invalid AP Response");
            }
          } catch {
            console.error(
              `The Metadata Profile ${importedAP.value} is not accessible.`,
            );
          } finally {
            visitedAPs.push(importedAP.value);
          }
        }
      }
    }
  }

  return fullApplicationProfile;
}
