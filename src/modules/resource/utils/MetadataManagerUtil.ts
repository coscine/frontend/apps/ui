import useResourceStore from "../store";
import type { Dataset } from "@rdfjs/types";
import factory from "rdf-ext";
import { parseRDFDefinition } from "./linkedData";
import type { FolderContent } from "../types";
import type {
  ProjectDto,
  ResourceDto,
} from "@coscine/api-client/dist/types/Coscine.Api";

export default {
  /**
   * Loads metadata for a specified file. If no metadata is found, it returns an empty dataset.
   *
   * @param {FolderContent} fileInfo - Information about the file for which metadata is to be loaded.
   * @param {ProjectDto | null} project - Parent project of the file.
   * @param {ResourceDto | null} resource - Parent resource of the file.
   * @returns {Promise<Dataset>} A promise that resolves to the metadata for the file or an empty dataset if none is found.
   */
  async loadMetadataForFile(
    fileInfo: FolderContent,
    project: ProjectDto | null,
    resource: ResourceDto | null,
  ): Promise<Dataset | null> {
    // Return an empty dataset if no file info is available
    if (!fileInfo?.path || !project?.id || !resource?.id) {
      return factory.dataset() as unknown as Dataset;
    }

    // Return existing metadata if available
    const currentMetadata = fileInfo.metadata[fileInfo.latestVersion];
    if (currentMetadata && currentMetadata.size > 0) {
      return currentMetadata;
    }

    // Fetch metadata from the API
    const metadataTree = await useResourceStore().getMetadataTree(
      project.id,
      resource.id,
      fileInfo.path,
    );

    // If the metadata exists and has a definition, parse it.
    // Make sure to select the correct metadata set and not just the first.
    // This is a prefix based search!
    const metadata = metadataTree?.find((x) => x.path === fileInfo.path);

    if (metadata?.definition?.content && metadata.definition.type) {
      return await parseRDFDefinition(
        metadata.definition.content,
        metadata.definition.type,
      );
    }

    // Return an empty dataset if no metadata was found
    return factory.dataset() as unknown as Dataset;
  },

  /**
   * Copies metadata from the source to the target.
   *
   * @param {Dataset | null | undefined} source - The source dataset from which to copy metadata.
   * @param {FolderContent | undefined} target - The target where the metadata should be copied to.
   */
  copyMetadata(
    source: Dataset | null | undefined,
    target: FolderContent | undefined,
  ) {
    if (source && target) {
      target.metadata[target.latestVersion] = factory.dataset(
        Array.from(source),
      ) as unknown as Dataset;
    }
  },
};
