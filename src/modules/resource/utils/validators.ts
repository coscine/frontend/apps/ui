import type {
  ResourceTypeOptionsForCreationDto,
  RdsResourceTypeOptionsForManipulationDto,
  RdsS3ResourceTypeOptionsForManipulationDto,
  RdsS3WormResourceTypeOptionsForManipulationDto,
} from "@coscine/api-client/dist/types/Coscine.Api";
import { QuotaUnit } from "../types";

/**
 * Checks if only one resource type option is set.
 *
 * @param {ResourceTypeOptionsForCreationDto} value - The object containing resource type options.
 * @returns {boolean} - Returns true if exactly one resource type option is defined, otherwise false.
 */
export const onlyOneResourceTypeOptionSet = (
  value: ResourceTypeOptionsForCreationDto,
): boolean => {
  // Count how many resourceTypeOptions are defined
  const count = Object.keys(value).reduce(
    (acc, key) =>
      acc + (value[key as keyof ResourceTypeOptionsForCreationDto] ? 1 : 0),
    0,
  );
  // Return true if exactly one is defined
  return count === 1;
};

/**
 * Validates if the resource type options are valid based on various resource types.
 *
 * @param {ResourceTypeOptionsForCreationDto} options - The object containing various resource type options.
 * @returns {boolean} - Returns true if the resource type options are valid, otherwise false.
 */
export const validResourceTypeOptions = (
  options: ResourceTypeOptionsForCreationDto,
): boolean => {
  // Evaluate general resource type - Linked Data
  if (options.linkedResourceTypeOptions) {
    return true;
  }
  // Evaluate general resource type - GitLab
  else if (options.gitlabResourceTypeOptions) {
    return (
      options.gitlabResourceTypeOptions &&
      options.gitlabResourceTypeOptions.repoUrl !== "" &&
      options.gitlabResourceTypeOptions.accessToken !== "" &&
      options.gitlabResourceTypeOptions.branch !== "" &&
      options.gitlabResourceTypeOptions.projectId > 0 &&
      options.gitlabResourceTypeOptions.tosAccepted
    );
  }
  // Evaluate general resource type - RDS
  else if (options.rdsResourceTypeOptions) {
    return hasValidResourceTypeOptionsWithQuota(options.rdsResourceTypeOptions);
  }
  // Evaluate general resource type - RDS S3
  else if (options.rdsS3ResourceTypeOptions) {
    return hasValidResourceTypeOptionsWithQuota(
      options.rdsS3ResourceTypeOptions,
    );
  }
  // Evaluate general resource type - RDS S3 WORM
  else if (options.rdsS3WormResourceTypeOptions) {
    return hasValidResourceTypeOptionsWithQuota(
      options.rdsS3WormResourceTypeOptions,
    );
  }
  return false;
};

/**
 * Validates if the given resource type options have valid quota values.
 *
 * @param {RdsResourceTypeOptionsForManipulationDto | RdsS3ResourceTypeOptionsForManipulationDto | RdsS3WormResourceTypeOptionsForManipulationDto} options - The resource type options to validate.
 * @returns {boolean} - Returns true if the options have valid quota values, otherwise false.
 */
const hasValidResourceTypeOptionsWithQuota = (
  options:
    | RdsResourceTypeOptionsForManipulationDto
    | RdsS3ResourceTypeOptionsForManipulationDto
    | RdsS3WormResourceTypeOptionsForManipulationDto,
): boolean => {
  if (options !== undefined && options.quota) {
    const { value, unit } = options.quota;
    const isValidUnit = Object.values(QuotaUnit).includes(unit as QuotaUnit);
    const isValidValue = Number.isFinite(value) && value > 0;
    return isValidUnit && isValidValue;
  }
  return false;
};

/**
 * Validates a Coscine entry name
 * @param entry
 * @returns True when valid, false if invalid
 */
export const validEntryName = (entry: string): boolean => {
  return !/[\\:?*<>|+#]+/g.test(entry);
};
