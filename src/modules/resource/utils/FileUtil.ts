import type {
  QuotaDto,
  QuotaForManipulationDto,
  QuotaUnit,
} from "@coscine/api-client/dist/types/Coscine.Api";

export interface ExtendedFile extends File {
  /**
   * From Bootstrap-Vue (is the full path)
   */
  $path?: string;
  /**
   * Custom (is the folder path)
   */
  path?: string;
}

export class FileUtil {
  public static formatBytes(bytes: number, decimals = 2) {
    if (bytes === 0) {
      return "0 Bytes";
    }
    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    // Backend will deliver the values in KiB, MiB, GiB, TiB, etc (...1024).
    const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
  }

  public static convertCapacityUnits(
    input: QuotaDto | QuotaForManipulationDto,
    output_unit: QuotaUnit,
  ): number {
    if (input.value === undefined || isNaN(input.value)) {
      return 0;
    }

    const k = 1024;
    const sizes = [
      QuotaUnits.BYTE,
      QuotaUnits.KibiBYTE,
      QuotaUnits.MebiBYTE,
      QuotaUnits.GibiBYTE,
      QuotaUnits.TebiBYTE,
      QuotaUnits.PebiBYTE,
    ];

    const input_exponent = sizes.findIndex((v) => v === input.unit);
    const output_exponent = sizes.findIndex((v) => v === output_unit);

    return input.value * Math.pow(k, input_exponent - output_exponent);
  }

  public static getFilesDataTransferItems(
    dataTransferItems: DataTransferItemList,
  ): Promise<ExtendedFile[]> {
    function traverseFileTreePromise(
      item: FileSystemEntry | null,
      path = "",
      folder: ExtendedFile[],
    ) {
      return new Promise((resolve) => {
        if (item?.isFile) {
          (item as FileSystemFileEntry).file((file) => {
            (file as ExtendedFile).path = path || "" + file.name; //save full path
            (file as ExtendedFile).$path = item.fullPath;
            if ((file as ExtendedFile).$path?.startsWith("/")) {
              (file as ExtendedFile).$path = (
                file as ExtendedFile
              ).$path?.substring(1);
            }
            folder.push(file as ExtendedFile);
            resolve(file);
          });
        } else if (item?.isDirectory) {
          const dirReader = (item as FileSystemDirectoryEntry).createReader();
          dirReader.readEntries((entries) => {
            const entriesPromises = [];
            for (const entr of entries)
              entriesPromises.push(
                traverseFileTreePromise(
                  entr,
                  path || "" + item.name + "/",
                  folder,
                ),
              );
            resolve(Promise.all(entriesPromises));
          });
        }
      });
    }

    const files: ExtendedFile[] = [];
    return new Promise((resolve, _) => {
      const entriesPromises = [];
      for (const it of dataTransferItems)
        entriesPromises.push(
          traverseFileTreePromise(it.webkitGetAsEntry(), "", files),
        );
      Promise.all(entriesPromises).then((_) => {
        resolve(files);
      });
    });
  }
}

/**@deprecated Unfortunately that is a workaround, since we can't directly use the QuotaUnit Enum */
export const QuotaUnits = {
  ["BYTE"]: "https://qudt.org/vocab/unit/BYTE",
  ["KibiBYTE"]: "https://qudt.org/vocab/unit/KibiBYTE",
  ["MebiBYTE"]: "https://qudt.org/vocab/unit/MebiBYTE",
  ["GibiBYTE"]: "https://qudt.org/vocab/unit/GibiBYTE",
  ["TebiBYTE"]: "https://qudt.org/vocab/unit/TebiBYTE",
  ["PebiBYTE"]: "https://qudt.org/vocab/unit/PebiBYTE",
};
