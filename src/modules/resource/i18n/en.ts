export default {
  /*
    --------------------------------------------------------------------------------------
    ENGLISH STRINGS
    --------------------------------------------------------------------------------------
  */
  page: {
    createResource: {
      title: "Add Resource",
      configuration: {
        title: "Step 1: @:{'form.steps.first'}",
        needMore: "Need more?",
        bucketSize: "{size} GB, this equals approximately {files} files.",
        bucketSizeAlt: "Or enter quota here:",
        exceededQuotaWarning:
          "Please enter a valid number in the range of {min} GB and {max} GB.",
        labels: {
          resourceType: "Resource Type:",
          size: "Resource Size:",
          resourceTypePopover: "For more information on resource types see",
          resourceTypePopoverUrl: "https://docs.coscine.de/en/resources/types/",
          hintTextSSO:
            "The resource types you can create are impacted by your affiliation with {organizationName}. Check the dropdown menu to see the options available to you.",
          hintTextORCiD:
            "The resource types you can create are impacted by your affiliation. Merge your organization account under {userProfile} to make more options available to you",
          disclaimerAP: "Disclaimer:",
        },
        popover: {
          title: "Quota not sufficient",
          body: "This project does not have sufficient quota to create more resources. You can apply for more storage space in the quota management.",
        },
        validation: {
          errorMessage:
            "No connection with the resource is possible. Please check the provided data.",
        },
        disclaimerAPText1:
          "In Step 2, you will be prompted to choose a Metadata Profile that you want to use for the chosen resource type. In case you don't know yet which Metadata Profile you would like to choose, you can take a look at the already existing ones here: ",
        disclaimerAPAimsUrl:
          "https://coscine.rwth-aachen.de/coscine/apps/aimsfrontend/",
        disclaimerAPText2:
          "To do this, simply select one from the list on the left and display it in metadata format for a better overview. In case you want to create a new Metadata Profile because none of the already existing ones fit your data, follow the link above and see the description in the documentation as well for help: ",
        disclaimerAPHelpUrl:
          "https://docs.coscine.de/en/metadata/generator/about/",
      },
      applicationProfile: {
        title: "Step 2: @:{'form.steps.third'}",
        createAp: {
          tooltip: "Request for creation of Metadata Profiles",
          title: "Creation of Metadata Profiles:",
          body: "You are going to get redirected to another page to configure and create a customized Metadata Profile. {br}{br}The review process for provisioning submissions usually takes a few days. If required, you will be contacted by our consulting team to support you with the process. {br}{br}Do you want to get redirected to the Metadata Profile creation page?",
        },
        applicationProfileHint:
          "Choose the Metadata Profile carefully, as it cannot be changed once it is assigned to a resource.",
        applicationProfileLabel: "Metadata Profile:",
        selectApplicationProfile: "Please select an Metadata Profile",
        applicationProfilePopover:
          "For more information on Metadata Profiles see",
        applicationProfilePopoverUrl:
          "https://docs.coscine.de/en/metadata/profiles/",
      },
      resourceMetadata: {
        title: "Step 3: @:{'form.steps.second'}",
      },
      overview: {
        title: "Step 4: @:{'form.steps.fourth'}",
        edit: "Edit",
      },
      multiselect: {
        placeholderResourceText: "Please select a resource type.",
        placeholderApplicationProfileText: "Please select a Metadata Profile.",

        selectEnter: "Press enter to select.",
        noResults: "No elements found. Consider changing the search query.",
        noOptions: "List is empty.",
      },
    },
    resource: {
      resources: "Resources",

      upload: "Upload Files",
      download: "Download",

      edit: "Edit Resource Configuration",
      info: "Information",

      canDropFile: "You can drop your file here!",

      loading: "Loading...",
      typeToSearch: "Type to search",
      clear: "Clear",
      perPage: "Per page",
      more: "More",
      fileName: "Name",
      lastModified: "Modified",
      actions: "Actions",

      resourceType: "Resource Type",
      resourceName: "Resource Name",
      displayName: "Display Name",
      description: "Resource Description",
      disciplines: "Discipline",
      keywords: "Resource Keywords",
      visibility: "Metadata Visibility",
      license: "License",
      usageRights: "Usage Rights",

      PID: "Persistent ID",

      quotaPopover:
        "Currently {used} out of {reserved} are in use. It may take 2-3 minutes until this value is updated.",
      occupiedBytes: "Occupied Storage Space",

      noData: "This resource contains no data.",
      emptyTableText: "This resource contains no files.",
      emptyFilterText: "No files found matching your request.",

      metadataManagerBadFileName:
        "Invalid file name. The following characters are not permissible: /:?*<>|+#",

      metadataManagerBtnUpload: "Upload",
      metadataManagerBtnSelectFiles: "Select Files",
      metadataManagerBtnSelectFolders: "Select Folders",

      metadataManagerBtnCantUploadArchived:
        "Upload disabled because the current resource is archived.",
      metadataManagerBtnCantUploadUploading:
        "Upload disabled during ongoing uploading.",
      metadataManagerBtnCantUploadReadonly:
        "Upload disabled, page is read-only.",

      toClipboard: "Link has been copied to clipboard!",
      expirationMessage: "Notice: The link is only valid for 24 hours.",

      metadataManagerBtnDownload: "Download",
      metadataManagerBtnDownloadMetadata: "Download Metadata",
      metadataManagerBtnDownloadExtractedMetadata:
        "Download Extracted Metadata",
      metadataManagerBtnUpdate: "Update",
      metadataManagerBtnSaving: "Saving...",
      metadataManagerBtnCopyLink: "Share",

      metadataViewButton: "Metadata",
      extractedViewButton: "Extracted",
      noExtractedMetadata: "No extracted metadata found...",

      infoFileName: "File name",
      infoFileLastModified: "Last modified",
      infoFileCreated: "Uploaded",
      infoFileSize: "File Size",
      infoFileNoInformation: "No Information",

      metadataManager: "Metadata Manager",
      all: "All",
      allFiles: "All Files",

      validationErrors: "Validation Errors",

      modalSaveDuplicateFilesHeader: "Replace or skip duplicate files",
      modalSaveDuplicateFilesBody:
        "One or more files with the same name already exist. Uploading will result in overwriting the following file(s):",
      modalSaveDuplicatesFilesBodyWorm:
        "One or more files with the same name already exist. The following files cannot be overwritten:",
      modalSaveDuplicateFilesBtnCancel: "CANCEL UPLOAD",
      modalSaveDuplicateFilesBtnSkip: "SKIP DUPLICATE FILES",
      modalSaveDuplicateFilesBtnOverwrite: "OVERWRITE",

      modalDeleteFolderContentsHeader: "Delete files and metadata",
      modalDeleteFolderContentsBody:
        "Are you sure, you want to delete the following files and metadata:",

      modalLeavingPageHeader: "Upload in progress",
      modalLeavingPageBodyTop: "These files are currently uploading:",
      modalLeavingPageBodyBottom:
        "Are you sure you want to cancel the process?",
      modalLeavingPageBtnStay: "STAY ON PAGE",
      modalLeavingPageBtnLeave: "LEAVE CURRENT PAGE",

      toastSavingSuccessfulTitle: "Saving file(s) successful",
      toastSavingSuccessfulBody: "Number of files saved: {number}",

      toastSavingFailedTitle: "Saving file(s) failed",
      toastSavingFailedBodyTop:
        "An error occurred while saving the following files:",
      toastSavingFailedBodyBottom: "Please try again.",

      dataUrl: "Data URL",
      metadataKey: "Entry Name",
      size: "File Size",
      slashHint: "Using a forward slash (/), will create (sub-)folders.",
      urlSafeEntryNameHint:
        "The following characters cannot be used for the Entry Name: \\ : ? * < > | + # ",

      toast: {
        noExtractedMetadata: {
          title: "No extracted metadata found!",
          body: "No extracted metadata could be found for this dataset.",
        },
      },
    },
    settings: {
      title: "@:{'breadcrumbs.resource.settings'}",
      applicationProfile: {
        openTemplateModal: "Open Extraction Template",
      },
      template: {
        loading: "Loading...",
        start: {
          modal: {
            title: "Provide an example dataset",
            body: "Please provide an example dataset from which the template can be filled out.",

            placeholder: "Choose a file or drop it here...",
            dropPlaceholder: "Drop file here...",
          },
        },
        apply: {
          modal: {
            title: "Apply Template",
            body: "You can use the extracted values from your provided dataset to create a template for your metadata fields. Drag them from the left to the right.",

            search: "Search for term or content",

            edit: "Edit",
            preview: "Preview",
          },
        },
      },
      overview: {
        resource: "Resource",
        resourceLabel:
          "@:{'page.settings.overview.resource'}@:{'form.labelSymbol'}",
        resourceSelect: "Please select a resource",

        resourceTypeLabel:
          "@:{'page.createResource.configuration.labels.resourceType'}",

        persistentId: "Persistent Identifier (PID)",
        persistentIdLabel:
          "@:{'page.settings.overview.persistentId'}@:{'form.labelSymbol'}",
        persistentIdPopover:
          "For more information on @:{'page.settings.overview.persistentId'} see",
        persistentIdPopoverUrl: "https://docs.coscine.de/en/resources/pid/",

        quota: "Quota",
        quotaLabel: "@:{'page.settings.overview.quota'}@:{'form.labelSymbol'}",
      },
      configuration: {
        bucketName: "Bucket Name",
        bucketNameLabel:
          "@:{'page.settings.configuration.bucketName'}@:{'form.labelSymbol'}",

        size: "Resource Size",
        sizeLabel:
          "@:{'page.settings.configuration.size'}@:{'form.labelSymbol'}",

        accessKey: "Access Key",
        accessKeyRead: "@:{'page.settings.configuration.accessKey'} (Reading)",
        accessKeyReadLabel:
          "@:{'page.settings.configuration.accessKeyRead'}@:{'form.labelSymbol'}",

        accessKeyWrite: "@:{'page.settings.configuration.accessKey'} (Writing)",
        accessKeyWriteLabel:
          "@:{'page.settings.configuration.accessKeyWrite'}@:{'form.labelSymbol'}",

        secretKey: "Secret Key",
        secretKeyRead: "@:{'page.settings.configuration.secretKey'} (Reading)",
        secretKeyReadLabel:
          "@:{'page.settings.configuration.secretKeyRead'}@:{'form.labelSymbol'}",

        secretKeyWrite: "@:{'page.settings.configuration.secretKey'} (Writing)",
        secretKeyWriteLabel:
          "@:{'page.settings.configuration.secretKeyWrite'}@:{'form.labelSymbol'}",

        endpoint: "Endpoint",
        endpointLabel:
          "@:{'page.settings.configuration.endpoint'}@:{'form.labelSymbol'}",

        resourceResourceUrl: "Resource Url",
        resourceResourceUrlLabel:
          "@:{'page.settings.configuration.resourceResourceUrl'}@:{'form.labelSymbol'}",

        resourceRepositoryNumber: "Repository ID",
        resourceRepositoryNumberLabel:
          "@:{'page.settings.configuration.resourceRepositoryNumber'}@:{'form.labelSymbol'}",

        resourceRepositoryUrl: "Repository URL",
        resourceRepositoryUrlLabel:
          "@:{'page.settings.configuration.resourceRepositoryUrl'}@:{'form.labelSymbol'}",

        toClipboard: "{resourceOption} has been copied to clipboard",
      },
      actions: {
        resourceArchive: "Archive Resource",
        resourceArchiveLabel:
          "@:{'page.settings.actions.resourceArchive'}@:{'form.labelSymbol'}",

        resourceDelete: "Delete Resource",
        resourceDeleteLabel:
          "@:{'page.settings.actions.resourceDelete'}@:{'form.labelSymbol'}",
        archive: {
          modal: {
            title: "Archive resource",
            body: "If the status of a resource is set to archived, data and metadata can not be changed by users any longer. However, reading the data and downloading files is still possible. {br}{br}The status can be unset by project owners if the data has to be extended or updated.",
          },
          toast: {
            title: "Resource archived",
            body: "The resource was successfully set to archived.",
          },
        },
        unarchive: {
          modal: {
            title: "Unset archived status",
            body: "If the archived-status is unset, resources return to normal functionality (e.g. adding new files and updating metadata). {br}{br}The archived status can be set again by project owners to prevent users from editing data.",
          },
          toast: {
            title: "Resource status reset",
            body: "The resource archived status was successfully set to normal.",
          },
        },
        archivePopover: "Sets resource to read-only. For more information see ",
        archivePopoverUrl: "https://docs.coscine.de/en/resources/archiving/",
        dotCoscine: ".coscine",
        localMetadataCopyLabel: "Local Metadata Copy",
        localMetadataCopyHint:
          "Activating local metadata copies will store additional files within your used resources.",
        localMetadataCopyPopover:
          "For more information on local metadata copies see",
        localMetadataCopyPopoverUrl:
          "https://docs.coscine.de/en/resources/metadata/",
        enableLocalMetadataCopy: {
          modal: {
            title: "Enable local metadata copy",
            body: "By opting in for local copies of your metadata, a copy will be saved in your designated S3 bucket under the folder {dotCoscineFolder} on file upload. While this feature enriches your data management capabilities, it's important to note that these copies will consume part of your available storage. The additional storage used will generally be minimal, but it can grow with the volume of files and data you manage. {br}{br}Enabling this feature will only apply to future metadata uploads. Any existing metadata prior to enabling metadata copies will remain secure and unchanged in our system, but will not be copied to your S3 bucket. This ensures your current data management and storage allocation are not retroactively affected. All your metadata continues to be securely stored on our servers. The {dotCoscineFolder} folder will not be displayed by the user interface. {br}{br}Deactivating this feature does not delete existing copies.            ",
          },
          toast: {
            title: "Local metadata copy enabled",
            body: "The local metadata copy was successfully enabled. A local copy of your metadata will be available for future uploads.",
          },
        },
        disableLocalMetadataCopy: {
          modal: {
            title: "Disable local metadata copy",
            body: "Disabling the local metadata copy will not remove any existing local metadata copies.",
          },
          toast: {
            title: "Local metadata copy disabled",
            body: "The local metadata copy was successfully disabled. No local copies will be saved for future uploads. Existing local copies will not be removed.",
          },
        },
        delete: {
          modal: {
            title: "Do you really want to delete this resource?",
            body: "If you are sure you really want to delete this resource, please type its name:",
            help: "The entered name does not match the resource name.",
          },
        },
      },
    },
    module: {
      redirection: {
        maintenance: {
          title: "Resource Temporarily Unavailable",
          body: "We've temporarily disabled all actions on the resource {resourceName} for maintenance. You have been redirected to another page. Please try again later.",
        },
      },
    },
  },

  resourceType: {
    gitlab: {
      domain: "GitLab Domain",
      domainLabel: "Domain:",
      domainInvalidTooltip:
        "The provided domain is not a valid full URL. Note that only the HTTPS protocol is allowed.",

      accessToken: "Project or Group Access Token",
      accessTokenLabel: "@:{'resourceType.gitlab.accessToken'}:",
      accessTokenInvalidTooltip: "This is a required field",
      accessTokenInfoPopover: "For more information on access tokens see",
      accessTokenInfoPopoverUrl:
        "https://docs.coscine.de/en/resources/types/gitlab/",

      projectId: "GitLab Project Id",
      projectIdLabel: "GitLab Project Id:",

      project: "Select a GitLab Project",
      projectLabel: "GitLab Project:",
      projectNotValidated: "Validate your token to preview the project name",

      reference: "Select a GitLab Branch",
      referenceLabel: "GitLab Branch:",

      tos: "By creating the resource, I commit that all members of the project will comply with the terms of use of the underlying system.",

      verifyConnection: "Verify GitLab Connection",
      verificationToast: {
        success: {
          title: "Successful GitLab Connection",
          body: "Successfully established connection with GitLab using the provided access token.",
        },
        failure: {
          title: "Failed GitLab Connection",
          body: "Could not establish connection with GitLab using the provided access token. Ensure the provided information is correct and try again.",
        },
      },

      cantPush:
        'The token you have provided does not have sufficient rights to push on the current branch "{branch}"',
    },
    linked: {
      page: {
        resource: {
          upload: "Save Entries",
          download: "Open",

          noData: "This resource contains no entries.",
          emptyTableText: "This resource contains no entries.",
          emptyFilterText: "No entries found matching your request.",

          metadataManagerBadFileName:
            "Invalid entry name. The following characters are not permissible: /:?*<>|+#",

          metadataManagerBtnDownload: "Open",
          metadataManagerBtnUpload: "Save",
          metadataManagerBtnSelectFiles: "New Entry",
          metadataManagerBtnSelectFolders: "New folder",

          infoFileName: "Entry name",
          infoFileSize: "Entry Size",

          allFiles: "All Entries",

          modalSaveDuplicateFilesHeader: "Replace or skip duplicate entries",
          modalSaveDuplicateFilesBody:
            "One or more entries with the same name already exist. Saving will result in overwriting the following entry(s):",
          modalSaveDuplicateFilesBtnCancel: "CANCEL SAVING",
          modalSaveDuplicateFilesBtnSkip: "SKIP DUPLICATE ENTRIES",

          modalDeleteFolderContentsHeader: "Delete entries and metadata",
          modalDeleteFolderContentsBody:
            "Are you sure, you want to delete the following entries and metadata:",

          modalLeavingPageHeader: "Saving in progress",
          modalLeavingPageBodyTop: "These entries are currently saving:",

          toastSavingSuccessfulTitle: "Saving entries(s) successful",
          toastSavingSuccessfulBody: "Number of entries saved: {number}",

          toastSavingFailedTitle: "Saving entry(s) failed",
          toastSavingFailedBodyTop:
            "An error occurred while saving the following entries:",
        },
      },
    },
  },

  form: {
    labelSymbol: ":",
    project: {
      projectName: "Project Name",
      projectNameLabel: "@:{'form.project.projectName'}@:{'form.labelSymbol'}",
    },
    resource: {
      resourceName: "Resource Name",
      resourceNameHelp:
        "This is a required field and can only contain up to {maxLength} characters.",
      resourceNameLabel:
        "@:{'form.resource.resourceName'}@:{'form.labelSymbol'}",

      displayName: "Display Name",
      displayNameHelp:
        "This is a required field and can only contain up to {maxLength} characters.",
      displayNameLabel: "@:{'form.resource.displayName'}@:{'form.labelSymbol'}",

      resourceDescription: "Resource Description",
      resourceDescriptionHelp:
        "This is a required field and can only contain up to {maxLength} characters.",
      resourceDescriptionLabel:
        "@:{'form.resource.resourceDescription'}@:{'form.labelSymbol'}",

      resourceDiscipline: "Discipline",
      resourceDisciplineLabel:
        "@:{'form.resource.resourceDiscipline'}@:{'form.labelSymbol'}",

      resourceKeywords: "Resource Keywords",
      resourceKeywordsPlaceholder:
        'Type, then press "Enter" to insert a Keyword.',
      resourceKeywordsHelp:
        "This field can only contain up to {maxLength} characters.",
      resourceKeywordsEmpty: "The list of keywords is empty.",
      resourceKeywordsLabel:
        "@:{'form.resource.resourceKeywords'}@:{'form.labelSymbol'}",
      tagPlaceholder: "You can add this tag",

      resourceMetadataVisibility: "Metadata Visibility",
      resourceMetadataVisibilityLabel:
        "@:{'form.resource.resourceMetadataVisibility'}@:{'form.labelSymbol'}",
      resourceMetadataPopover: "For more information on visibility see",
      resourceMetadataPopoverUrl:
        "https://docs.coscine.de/en/resources/create/",

      resourceLicense: "License",
      resourceLicenseLabel:
        "@:{'form.resource.resourceLicense'}@:{'form.labelSymbol'}",
      resourceLicensePopover: "For more information on licenses see ",
      resourceLicensePopoverUrl: "https://docs.coscine.de/en/resources/create/",
      resourceLicenseSelect: "Please select a License",
      resourceLicenseHelp:
        "This field can only contain up to {maxLength} characters.",

      resourceReuse: "Internal Rules for Reuse",
      resourceReuseLabel:
        "@:{'form.resource.resourceReuse'}@:{'form.labelSymbol'}",
      resourceReusePopover:
        "For more information on internal rules for reuse see ",
      resourceReusePopoverUrl: "https://docs.coscine.de/en/resources/create/",
      resourceReuseHelp:
        "This field can only contain up to {maxLength} characters.",
    },
    steps: {
      first: "Resource Configuration",
      second: "Resource Metadata",
      third: "Metadata Profile",
      fourth: "Overview & Confirm",
      fifth: "Actions",
    },
  },
};
