export default {
  /*
    --------------------------------------------------------------------------------------
    GERMAN STRINGS
    --------------------------------------------------------------------------------------
  */
  page: {
    createResource: {
      title: "Ressource hinzufügen",
      configuration: {
        title: "Schritt 1: @:{'form.steps.first'}",
        needMore: "Benötigen Sie mehr Speicher?",
        bucketSize: "{size} GB, dies entspricht circa {files} Dateien.",
        bucketSizeAlt: "Oder geben Sie hier die Quota ein:",
        exceededQuotaWarning:
          "Geben Sie eine gültige Zahl im Bereich von {min} GB und {max} GB ein.",
        labels: {
          resourceType: "Ressourcentyp:",
          size: "Ressourcengröße:",
          resourceTypePopover:
            "Für weitere Informationen zu Ressourcentypen siehe",
          resourceTypePopoverUrl: "https://docs.coscine.de/de/resources/types/",
          hintTextSSO:
            "Die von Ihnen erstellbaren Ressourcentypen werden durch Ihre Zugehörigkeit zu Organisation {organizationName} beeinflusst. Bedienen Sie das Dropdown-Menü, um die für Sie verfügbaren Optionen zu sehen.",
          hintTextORCiD:
            "Die von Ihnen erstellbaren Ressourcentypen werden durch Ihre Zugehörigkeit zu einer Organisation beeinflusst. Verknüpfen Sie Ihr Organisationskonto unter {userProfile}, um mehr Optionen zu sehen",
          disclaimerAP: "Disclaimer:",
        },
        popover: {
          title: "Quota nicht ausreichend",
          body: "Dieses Projekt hat nicht genug Quota um weitere Ressourcen anlegen zu können. Im Quotamanagement können Sie zusätzlichen Speicher beantragen.",
        },
        validation: {
          errorMessage:
            "Es konnte keine Verbindung zur Ressource hergestellt werden. Bitte überprüfen Sie die Eingaben.",
        },
        disclaimerAPText1:
          "In Schritt 2 werden Sie aufgefordert, ein Metadatenprofil auszuwählen, das Sie für den gewählten Ressourcentyp verwenden möchten. Falls Sie noch nicht wissen, welches Metadatenprofil Sie wählen möchten, können Sie sich hier die bereits vorhandenen ansehen: ",
        disclaimerAPAimsUrl:
          "https://coscine.rwth-aachen.de/coscine/apps/aimsfrontend/",
        disclaimerAPText2:
          "Dazu einfach aus der Liste links eins auswählen und zur besseren Übersicht im Metadatenformat anzeigen lassen. Falls Sie ein neues Metadatenprofil erstellen möchten, weil keines der bereits vorhandenen zu Ihren Daten passt, folgen Sie dem obigen Link und lesen Sie auch die Beschreibung in der Dokumentation: ",
        disclaimerAPHelpUrl:
          "https://docs.coscine.de/de/metadata/generator/about/",
      },
      applicationProfile: {
        title: "Schritt 2: @:{'form.steps.third'}",
        createAp: {
          tooltip: "Anfrage zur Erstellung eines Metadatenprofils",
          title: "Erstellung von Metadatenprofilen:",
          body: "Zur Konfiguration und Erstellung eines individuellen Metadatenprofils werden Sie auf eine andere Webseite umgeleitet. {br}{br}Der Review-Prozess für die Bereitstellung des Profils dauert für gewöhnlich einige Tage. Falls nötig, werden Sie von unserem Consulting-Team kontaktiert, um Sie in diesem Prozess zu unterstützen. {br}{br}Möchten Sie zur Webseite zur Erstellung eines Metadatenprofils weiterleitet werden?",
        },
        applicationProfileHint:
          "Wählen Sie das Metadatenprofil sorgfältig aus, da es nicht mehr geändert werden kann, sobald es einer Ressource zugewiesen ist.",
        applicationProfileLabel: "Metadatenprofil:",
        selectApplicationProfile: "Bitte wählen sie ein Metadatenprofil aus",
        applicationProfilePopover:
          "Für weitere Informationen zu Metadatenprofilen siehe",
        applicationProfilePopoverUrl:
          "https://docs.coscine.de/de/metadata/profiles/",
      },
      resourceMetadata: {
        title: "Schritt 3: @:{'form.steps.second'}",
      },
      overview: {
        title: "Schritt 4: @:{'form.steps.fourth'}",
        edit: "Bearbeiten",
      },
      multiselect: {
        placeholderResourceText: "Bitte wählen Sie einen Ressourcentyp aus.",
        placeholderApplicationProfileText:
          "Bitte wählen Sie ein Metadatenprofil aus.",

        selectEnter: "Zum Auswählen Enter drücken.",
        noResults:
          "Keine Ergebnisse verfügbar. Bitte passen Sie die Suchanfrage an.",
        noOptions: "Die Liste ist leer.",
      },
    },
    resource: {
      resources: "Ressourcen",

      upload: "Dateien hochladen",
      download: "Herunterladen",

      edit: "Ressourcenkonfiguration bearbeiten",
      info: "Informationen",

      canDropFile: "Sie können Ihre Datei hier ablegen!",

      loading: "Laden...",
      typeToSearch: "Suchen...",
      clear: "Leeren",
      perPage: "Pro Seite",
      more: "Mehr",
      fileName: "Name",
      lastModified: "Geändert",
      actions: "Aktionen",

      resourceType: "Ressourcentyp",
      resourceName: "Ressourcenname",
      displayName: "Anzeigename",
      description: "Ressourcenbeschreibung",
      disciplines: "Disziplin",
      keywords: "Ressourcenschlagwörter",
      visibility: "Sichtbarkeit der Metadaten",
      license: "Lizenz",
      usageRights: "Verwendungsrechte",

      PID: "Persistente ID",

      quotaPopover:
        "Derzeit sind {used} von {reserved} belegt. Es kann 2-3 Minuten dauern, bis dieser Wert aktualisiert wird.",
      occupiedBytes: "Belegter Speicher",

      noData: "Diese Ressource enthält keine Daten",
      emptyTableText: "Diese Ressource enthält keine Dateien.",
      emptyFilterText:
        "Keine Dateien gefunden die mit Ihrer Anfrage übereinstimmen.",

      metadataManagerBadFileName:
        "Ungültiger Dateiname. Folgende Zeichen sind nicht zulässig: /:?*<>|+#",

      metadataManagerBtnUpload: "Hochladen",
      metadataManagerBtnSelectFiles: "Dateien auswählen",
      metadataManagerBtnSelectFolders: "Ordner auswählen",

      metadataManagerBtnCantUploadArchived:
        "Uploads sind deaktiviert, weil die aktuelle Ressource archiviert ist.",
      metadataManagerBtnCantUploadUploading:
        "Uploads sind während eines laufenden Uploads deaktiviert.",
      metadataManagerBtnCantUploadReadonly:
        "Uploads sind deaktiviert, weil die Seite schreibgeschützt ist.",

      toClipboard: "Link wurde in die Zwischenablage kopiert!",
      expirationMessage: "Hinweis: Der Link ist für 24 Stunden gültig.",
      metadataManagerBtnDownload: "Herunterladen",
      metadataManagerBtnDownloadMetadata: "Metadaten herunterladen",
      metadataManagerBtnDownloadExtractedMetadata:
        "Extrahierte Metadaten herunterladen",
      metadataManagerBtnUpdate: "Aktualisieren",
      metadataManagerBtnSaving: "Speichern...",
      metadataManagerBtnCopyLink: "Teilen",

      metadataViewButton: "Metadaten",
      extractedViewButton: "Extrahiert",
      noExtractedMetadata: "Keine extrahierten Metadaten gefunden...",

      infoFileName: "Dateiname",
      infoFileLastModified: "Zuletzt geändert",
      infoFileCreated: "Hochgeladen",
      infoFileSize: "Dateigröße",
      infoFileNoInformation: "Keine Information",

      metadataManager: "Metadaten Manager",
      all: "Alle",
      allFiles: "Alle Dateien",

      validationErrors: "Validierungsfehler",

      modalSaveDuplicateFilesHeader: "Dateien ersetzen oder überspringen",
      modalSaveDuplicateFilesBody:
        "Es sind bereits Dateien mit dem gleichen Namen vorhanden. Diese Dateien werden überschrieben:",
      modalSaveDuplicatesFilesBodyWorm:
        "Es sind bereits Dateien mit dem gleichen Namen vorhanden. Diese Dateien können nicht überschrieben werden:",
      modalSaveDuplicateFilesBtnCancel: "HOCHLADEN ABBRECHEN",
      modalSaveDuplicateFilesBtnSkip: "DIESE DATEIEN ÜBERSPRINGEN",
      modalSaveDuplicateFilesBtnOverwrite: "ÜBERSCHREIBEN",

      modalDeleteFolderContentsHeader: "Löschen von Dateien und Metadaten",
      modalDeleteFolderContentsBody:
        "Sind Sie sicher, dass Sie die folgenden Dateien und Metadaten löschen wollen:",

      modalLeavingPageHeader: "Dateien werden hochgeladen",
      modalLeavingPageBodyTop: "Diese Dateien werden momentan hochgeladen:",
      modalLeavingPageBodyBottom:
        "Sind Sie sicher, dass Sie den den Prozess abbrechen wollen?",
      modalLeavingPageBtnStay: "AUF SEITE BLEIBEN",
      modalLeavingPageBtnLeave: "SEITE VERLASSEN",

      toastSavingSuccessfulTitle: "Speichern erfolgreich",
      toastSavingSuccessfulBody:
        "Zahl der erfolgreich gespeicherten Dateien: {number}",

      toastSavingFailedTitle: "Speichern fehlgeschlagen",
      toastSavingFailedBodyTop:
        "Es ist ein Fehler beim Speichern der folgenden Dateien aufgetreten:",
      toastSavingFailedBodyBottom: "Bitte versuchen Sie es erneut.",

      dataUrl: "Daten URL",
      metadataKey: "Eintragsname",
      size: "Dateigröße",

      slashHint:
        "Die Verwendung eines Schrägstrichs (/) erstellt (Unter-)Ordner.",
      urlSafeEntryNameHint:
        "Die folgenden Zeichen können nicht für den Eintragsnamen verwendet werden: \\ : ? * < > | + #",

      toast: {
        noExtractedMetadata: {
          title: "Keine extrahierten Metadaten gefunden!",
          body: "Es konnten keine extrahierten Metadaten zu dem ausgewählten Datensatz gefunden werden.",
        },
      },
    },
    settings: {
      title: "@:{'breadcrumbs.resource.settings'}",
      applicationProfile: {
        openTemplateModal: "Extrahierungs-Template Öffnen",
      },
      template: {
        loading: "Lädt...",
        start: {
          modal: {
            title: "Stellen Sie einen Beispieldatensatz bereit",
            body: "Bitte stellen Sie einen Beispieldatensatz bereit, mit dem die möglichen Template-Werte extrahiert werden können.",

            placeholder:
              "Wählen Sie eine Datei aus oder ziehen Sie in dieses Feld...",
            dropPlaceholder: "Ziehen Sie Datei hierhin...",
          },
        },
        apply: {
          modal: {
            title: "Template Anwenden",
            body: "Sie können die extrahierten Werte aus dem Beispieldatensatz verwenden, um ein Template für ihre Metadatenfelder zu erstellen. Ziehen Sie die Werte von links nach rechts.",

            search: "Nach Termen oder Inhalt suchen",

            edit: "Editieren",
            preview: "Vorschau",
          },
        },
      },
      overview: {
        resource: "Ressource",
        resourceLabel:
          "@:{'page.settings.overview.resource'}@:{'form.labelSymbol'}",
        resourceSelect: "Bitte wählen Sie eine Ressource aus",

        resourceTypeLabel:
          "@:{'page.createResource.configuration.labels.resourceType'}",

        persistentId: "Persistent Identifier (PID)",
        persistentIdLabel:
          "@:{'page.settings.overview.persistentId'}@:{'form.labelSymbol'}",
        persistentIdPopover:
          "Für weitere Informationen zum @:{'page.settings.overview.persistentId'} siehe",
        persistentIdPopoverUrl: "https://docs.coscine.de/de/resources/pid/",

        quota: "Quota",
        quotaLabel: "@:{'page.settings.overview.quota'}@:{'form.labelSymbol'}",
      },
      configuration: {
        bucketName: "Bucket-Name",
        bucketNameLabel:
          "@:{'page.settings.configuration.bucketName'}@:{'form.labelSymbol'}",

        size: "Ressourcengröße",
        sizeLabel:
          "@:{'page.settings.configuration.size'}@:{'form.labelSymbol'}",

        accessKey: "Access Key",
        accessKeyRead: "@:{'page.settings.configuration.accessKey'} (Lesen)",
        accessKeyReadLabel:
          "@:{'page.settings.configuration.accessKeyRead'}@:{'form.labelSymbol'}",

        accessKeyWrite:
          "@:{'page.settings.configuration.accessKey'} (Schreiben)",
        accessKeyWriteLabel:
          "@:{'page.settings.configuration.accessKeyWrite'}@:{'form.labelSymbol'}",

        secretKey: "Secret Key",
        secretKeyRead: "@:{'page.settings.configuration.secretKey'} (Lesen)",
        secretKeyReadLabel:
          "@:{'page.settings.configuration.secretKeyRead'}@:{'form.labelSymbol'}",

        secretKeyWrite:
          "@:{'page.settings.configuration.secretKey'} (Schreiben)",
        secretKeyWriteLabel:
          "@:{'page.settings.configuration.secretKeyWrite'}@:{'form.labelSymbol'}",

        endpoint: "Endpoint",
        endpointLabel:
          "@:{'page.settings.configuration.endpoint'}@:{'form.labelSymbol'}",

        resourceResourceUrl: "Ressourcen-URL",
        resourceResourceUrlLabel:
          "@:{'page.settings.configuration.resourceResourceUrl'}@:{'form.labelSymbol'}",

        resourceRepositoryNumber: "Repository ID",
        resourceRepositoryNumberLabel:
          "@:{'page.settings.configuration.resourceRepositoryNumber'}@:{'form.labelSymbol'}",

        resourceRepositoryUrl: "Repository URL",
        resourceRepositoryUrlLabel:
          "@:{'page.settings.configuration.resourceRepositoryUrl'}@:{'form.labelSymbol'}",

        toClipboard: "{resourceOption} wurde in die Zwischenablage kopiert",
      },
      actions: {
        resourceArchive: "Ressource archivieren",
        resourceArchiveLabel:
          "@:{'page.settings.actions.resourceArchive'}@:{'form.labelSymbol'}",

        resourceDelete: "Ressource löschen",
        resourceDeleteLabel:
          "@:{'page.settings.actions.resourceDelete'}@:{'form.labelSymbol'}",
        archive: {
          modal: {
            title: "Ressource archivieren",
            body: "Wenn der Status einer Ressource auf archiviert gesetzt wird, können Daten und Metadaten von Benutzern nicht mehr geändert werden. Das Lesen der Daten und das Herunterladen von Dateien ist jedoch weiterhin möglich. {br}{br}Der Status kann von Projektbesitzenden zurückgesetzt werden, wenn die Daten ergänzt oder aktualisiert werden sollen.",
          },
          toast: {
            title: "Ressource archiviert",
            body: "Die Ressource wurde erfolgreich archiviert.",
          },
        },
        unarchive: {
          modal: {
            title: "Aufheben des archivierten Status",
            body: "Wenn der archivierte Status nicht mehr gesetzt ist, kehren die Ressourcen zur normalen Funktionalität zurück (z.B. Hinzufügen neuer Dateien und Aktualisieren von Metadaten). {br}{br}Der Status kann von Projektbesitzenden wieder gesetzt werden, um zu verhindern, dass Nutzende Daten bearbeiten.",
          },
          toast: {
            title: "Ressourcenstatus zurückgesetzt",
            body: "Der Archivierungsstatus der Ressource wurde erfolgreich zurückgesetzt.",
          },
        },
        archivePopover:
          "Setzt die Ressource auf read-only. Für weitere Informationen siehe ",
        archivePopoverUrl: "https://docs.coscine.de/de/resources/archiving/",
        dotCoscine: ".coscine",
        localMetadataCopyLabel: "Lokale Metadatenkopie",
        localMetadataCopyHint:
          "Die Aktivierung lokaler Metadatenkopien wird zusätzliche Dateien in Ihren genutzten Ressourcen speichern.",
        localMetadataCopyPopover:
          "Für weitere Informationen zur lokalen Metadatenkopie siehe ",
        localMetadataCopyPopoverUrl:
          "https://docs.coscine.de/de/resources/metadata/",
        enableLocalMetadataCopy: {
          modal: {
            title: "Lokale Metadatenkopie aktivieren",
            body: "Wenn Sie sich für lokale Kopien Ihrer Metadaten entscheiden, wird bei jedem Dateiupload eine Kopie in dem dafür vorgesehenen S3-Bucket unter dem Ordner {dotCoscineFolder} gespeichert. Obwohl diese Funktion Ihre Datenverwaltungsfähigkeiten erweitert, ist es wichtig zu beachten, dass diese Kopien einen Teil Ihres verfügbaren Speicherplatzes verbrauchen werden. Der zusätzlich genutzte Speicherplatz ist in der Regel minimal, kann jedoch mit dem Volumen der verwalteten Dateien und Daten anwachsen. {br}{br}Die Aktivierung dieser Funktion gilt nur für zukünftige Metadaten-Uploads. Bereits vorhandene Metadaten vor der Aktivierung von Metadatenkopien bleiben sicher und unverändert in unserem System gespeichert, werden jedoch nicht in Ihren S3-Bucket kopiert. Dies stellt sicher, dass Ihre aktuelle Datenverwaltung und Speicherzuweisung nicht rückwirkend beeinflusst werden. Alle Ihre Metadaten werden weiterhin sicher auf unseren Servern gespeichert. Der Ordner {dotCoscineFolder} wird von der Nutzendenoberfläche nicht angezeigt. {br}{br}Das Deaktivieren der Funktion führt nicht zur Löschung bestehender Kopien.",
          },
          toast: {
            title: "Lokale Metadatenkopie aktiviert",
            body: "Die lokale Metadatenkopie wurde erfolgreich aktiviert. Die Metadaten werden nun zusätzlich lokal in Ihrem S3-Bucket gespeichert.",
          },
        },
        disableLocalMetadataCopy: {
          modal: {
            title: "Lokale Metadatenkopie deaktivieren",
            body: "Die Deaktivierung der lokalen Metadatenkopie führt dazu, dass keine zusätzlichen Metadatenkopien mehr in Ihrem S3-Bucket gespeichert werden. Bestehende Kopien bleiben unverändert.",
          },
          toast: {
            title: "Lokale Metadatenkopie deaktiviert",
            body: "Die lokale Metadatenkopie wurde erfolgreich deaktiviert. Die Metadaten werden nun nicht mehr zusätzlich lokal in Ihrem S3-Bucket gespeichert. Bestehende Kopien bleiben unverändert.",
          },
        },
        delete: {
          modal: {
            title: "Ressource wirklich entfernen?",
            body: "Wenn Sie sicher sind, dass Sie diese Ressource entfernen möchten, wiederholen Sie bitte den Ressourcennamen:",
            help: "Der angegebene Name stimmt nicht mit dem Ressourcenname überein.",
          },
        },
      },
    },
    module: {
      redirection: {
        maintenance: {
          title: "Ressource Vorübergehend Nicht Verfügbar",
          body: "Wir haben alle Aktionen an der Ressource {resourceName} vorübergehend für Wartungsarbeiten deaktiviert. Sie wurden auf eine andere Seite weitergeleitet. Bitte versuchen Sie es später erneut.",
        },
      },
    },
  },

  resourceType: {
    gitlab: {
      domain: "GitLab Domain",
      domainLabel: "Domain:",
      domainInvalidTooltip:
        "Die angegebene Domain ist keine gültige vollständige URL. Bitte beachten, dass nur das HTTPS-Protokoll zugelassen ist.",

      accessToken: "Projekt- oder Gruppenzugangstoken",
      accessTokenLabel: "@:{'resourceType.gitlab.accessToken'}:",
      accessTokenInvalidTooltip: "Dieses Feld ist erforderlich",
      accessTokenInfoPopover:
        "Für weitere Informationen zu Zugangstokens siehe",
      accessTokenInfoPopoverUrl:
        "https://docs.coscine.de/de/resources/types/gitlab/",

      projectId: "GitLab Projekt Id",
      projectIdLabel: "GitLab Projekt Id:",

      project: "GitLab Projekt auswählen",
      projectLabel: "GitLab Projekt:",
      projectNotValidated:
        "Validieren Sie Ihr Zugangstoken, um den Projektnamen anzuzeigen",

      reference: "GitLab Branch auswählen",
      referenceLabel: "GitLab Branch:",

      tos: "Mit Erstellung der Ressource versichere ich, dass alle Mitglieder des Projekts die Nutzungsbedingungen des zugrundeliegenden Systems befolgen.",

      verifyConnection: "Überprüfen der GitLab-Verbindung",
      verificationToast: {
        success: {
          title: "Erfolgreiche GitLab-Verbindung",
          body: "Verbindung mit GitLab unter Verwendung des angegebenen Zugangstokens erfolgreich hergestellt.",
        },
        failure: {
          title: "GitLab-Verbindung fehlgeschlagen",
          body: "Es konnte keine Verbindung mit GitLab unter Verwendung des angegebenen Zugangstokens hergestellt werden. Stellen Sie sicher, dass die angegebenen Informationen korrekt sind und versuchen Sie es erneut.",
        },
      },

      cantPush:
        'Das von Ihnen angegebene Token verfügt nicht über ausreichende Rechte, um auf dem gewählten Branch "{branch}" zu pushen.',
    },
    linked: {
      page: {
        resource: {
          upload: "Einträge speichern",
          download: "Öffnen",

          noData: "Diese Ressource enthält keine Einträge",
          emptyTableText: "Diese Ressource enthält keine Einträge.",
          emptyFilterText:
            "Keine Einträge gefunden die mit Ihrer Anfrage übereinstimmen.",

          metadataManagerBadFileName:
            "Ungültiger Eintrag. Folgende Zeichen sind nicht zulässig: /:?*<>|+#",

          metadataManagerBtnDownload: "Öffnen",
          metadataManagerBtnUpload: "Speichern",
          metadataManagerBtnSelectFiles: "Neuer Eintrag",
          metadataManagerBtnSelectFolders: "Neuer Ordner",

          infoFileName: "Eintragname",
          infoFileSize: "Eintraggröße",

          allFiles: "Alle Einträge",

          modalSaveDuplicateFilesHeader: "Einträge ersetzen oder überspringen",
          modalSaveDuplicateFilesBody:
            "Es sind bereits Einträge mit dem gleichen Namen vorhanden. Diese Einträge werden überschrieben:",
          modalSaveDuplicateFilesBtnCancel: "SPEICHERN ABBRECHEN",
          modalSaveDuplicateFilesBtnSkip: "DIESE EINTRÄGE ÜBERSPRINGEN",

          modalDeleteFolderContentsHeader:
            "Löschen von Einträgen und Metadaten",
          modalDeleteFolderContentsBody:
            "Sind Sie sicher, dass Sie die folgenden Einträge und Metadaten löschen wollen:",

          modalLeavingPageHeader: "Einträge werden hochgeladen",
          modalLeavingPageBodyTop:
            "Diese Einträge werden momentan hochgeladen:",

          toastSavingSuccessfulTitle: "Speichern erfolgreich",
          toastSavingSuccessfulBody:
            "Zahl der erfolgreich gespeicherten Einträge: {number}",

          toastSavingFailedTitle: "Speichern fehlgeschlagen",
          toastSavingFailedBodyTop:
            "Es ist ein Fehler beim Speichern der folgenden Einträge aufgetreten:",
        },
      },
    },
  },

  form: {
    labelSymbol: ":",
    project: {
      projectName: "Projektname",
      projectNameLabel: "@:{'form.project.projectName'}@:{'form.labelSymbol'}",
    },
    resource: {
      resourceName: "Ressourcenname",
      resourceNameHelp:
        "Dieses Feld ist erforderlich und besitzt eine Maximallänge von {maxLength} Zeichen.",
      resourceNameLabel:
        "@:{'form.resource.resourceName'}@:{'form.labelSymbol'}",

      displayName: "Anzeigename",
      displayNameHelp:
        "Dieses Feld ist erforderlich und besitzt eine Maximallänge von {maxLength} Zeichen.",
      displayNameLabel: "@:{'form.resource.displayName'}@:{'form.labelSymbol'}",

      resourceDescription: "Ressourcenbeschreibung",
      resourceDescriptionHelp:
        "Dieses Feld ist erforderlich und besitzt eine Maximallänge von {maxLength} Zeichen.",
      resourceDescriptionLabel:
        "@:{'form.resource.resourceDescription'}@:{'form.labelSymbol'}",

      resourceDiscipline: "Disziplin",
      resourceDisciplineLabel:
        "@:{'form.resource.resourceDiscipline'}@:{'form.labelSymbol'}",

      resourceKeywords: "Ressourcenschlagwörter",
      resourceKeywordsPlaceholder:
        'Tippen und drücken Sie "Enter", um ein Schlagwort einzufügen.',
      resourceKeywordsHelp:
        "Dieses Feld besitzt eine Maximallänge von {maxLength} Zeichen.",
      resourceKeywordsEmpty: "Die Liste ist leer.",
      resourceKeywordsLabel:
        "@:{'form.resource.resourceKeywords'}@:{'form.labelSymbol'}",
      tagPlaceholder: "Sie können diesen Tag hinzufügen",

      resourceMetadataVisibility: "Sichtbarkeit der Metadaten",
      resourceMetadataVisibilityLabel:
        "@:{'form.resource.resourceMetadataVisibility'}@:{'form.labelSymbol'}",
      resourceMetadataPopover:
        "Für weitere Informationen zur Sichtbarkeit siehe",
      resourceMetadataPopoverUrl:
        "https://docs.coscine.de/de/resources/create/",

      resourceLicense: "Lizenz",
      resourceLicenseLabel:
        "@:{'form.resource.resourceLicense'}@:{'form.labelSymbol'}",
      resourceLicensePopover: "Für weitere Informationen zu Lizenzen siehe ",
      resourceLicensePopoverUrl: "https://docs.coscine.de/de/resources/create/",
      resourceLicenseSelect: "Bitte wählen Sie eine Lizenz aus",
      resourceLicenseHelp:
        "Dieses Feld besitzt eine Maximallänge von {maxLength} Zeichen.",

      resourceReuse: "Interne Regeln zur Nachnutzung",
      resourceReuseLabel:
        "@:{'form.resource.resourceReuse'}@:{'form.labelSymbol'}",
      resourceReusePopover:
        "Für weitere Informationen zu internen Regeln zur Nachnutzung siehe ",
      resourceReusePopoverUrl: "https://docs.coscine.de/de/resources/create/",
      resourceReuseHelp:
        "Dieses Feld besitzt eine Maximallänge von {maxLength} Zeichen.",
    },
    steps: {
      first: "Ressourcen-Konfiguration",
      second: "Ressource-Metadaten",
      third: "Metadatenprofil",
      fourth: "Übersicht & Bestätigung",
      fifth: "Aktionen",
    },
  },
};
