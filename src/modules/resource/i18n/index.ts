import de from "./de";
import en from "./en";

export const ResourceI18nMessages = {
  de: de,
  en: en,
};
