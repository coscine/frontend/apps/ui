/* Testing imports */
import { mount } from "@vue/test-utils";
import { createTestingPinia } from "@pinia/testing";

/* Vue i18n */
import i18n, { def } from "@/plugins/vue-i18n";
import { ResourceI18nMessages } from "@/modules/resource/i18n/index";
i18n.global.availableLocales.forEach((locale) => {
  i18n.global.setLocaleMessage(locale, def[locale]); // default locale messages
  i18n.global.mergeLocaleMessage(locale, ResourceI18nMessages[locale]); // append the locale messages for the component
});

/* Tested Component */
import FilesView from "./FilesView.vue";

import { getTestShibbolethUserState } from "@/data/mockup/testUser";
import { getTestResourceState } from "@/data/mockup/testResource";
import { testProjectState } from "@/data/mockup/testProject";
import useResourceStore from "../../store";
import {
  getFileTreeResponse,
  getMetadataTreeResponse,
} from "@/data/mockup/responses/getMetadata";

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

describe("FilesView.vue", async () => {
  // Create a mocked pinia instance with initial state
  const testingPinia = createTestingPinia({
    createSpy: vitest.fn,
    initialState: {
      project: testProjectState,
      resource: await getTestResourceState(),
      user: getTestShibbolethUserState(),
    },
  });

  // Mock the API calls
  const resourceStore = useResourceStore(testingPinia);
  vi.mocked(resourceStore.getMetadataTree).mockReturnValue(
    Promise.resolve(getMetadataTreeResponse),
  );
  vi.mocked(resourceStore.getFileTree).mockReturnValue(
    Promise.resolve(getFileTreeResponse),
  );
  vi.mocked(resourceStore.getVocabularyInstances).mockReturnValue(
    Promise.resolve({ en: [], de: [] }),
  );

  const createWrapper = () => {
    return mount(FilesView, {
      global: {
        plugins: [testingPinia, i18n],
      },
      propsData: {
        dirTrail: "/",
        dirCrumbs: [],
        folderContents: [],
      },
    });
  };

  let wrapper: ReturnType<typeof createWrapper>;

  beforeEach(() => {
    // shallowMount does not work here!
    wrapper = createWrapper();
  });

  test(
    "Should toggle column visibility and persist to local storage",
    {
      // Override the maximum run time for this test (10 sec), due to the sleep() calls
      timeout: 10000,
    },
    async () => {
      await wrapper.vm.$nextTick();

      // Wait for 1 second until everything is set up
      await sleep(1000); // Don't remove!

      let headers = wrapper.findAll(".b-table-sortable-column");
      expect(headers.length).toBe(3);

      const selectButton = wrapper.find("#addColumnDropDown");
      await selectButton.trigger("click");

      const checkBox = wrapper.find(
        'ul[aria-labelledby="addColumnDropDown"] .form-check-input',
      );
      await checkBox.trigger("click");

      // The previous clicks should have done that, workaround for now
      wrapper.vm.columns[0].active = true;

      await wrapper.vm.$nextTick();

      // Wait for 1 second until everything is set up
      await sleep(1000); // Don't remove!

      headers = wrapper.findAll(".b-table-sortable-column");
      expect(headers.length).toBe(4);

      expect(resourceStore.setStoredColumns).toBeCalledTimes(1);
    },
  );

  test(
    "Trigger download uses download store method",
    {
      // Override the maximum run time for this test (10 sec), due to the sleep() calls
      timeout: 10000,
    },
    async () => {
      const folderContents = await wrapper.vm.constructFolderContents(
        getFileTreeResponse,
        getMetadataTreeResponse,
      );

      await wrapper.setProps({
        dirTrail: "/",
        dirCrumbs: [],
        folderContents,
      });

      await wrapper.vm.$nextTick();

      // Wait for 1 second until everything is set up
      await sleep(1000); // Don't remove!

      const entry = wrapper.find("#fileViewEntry2");
      await entry.trigger("click");

      // Wait for 1 second until everything is set up
      await sleep(1000); // Don't remove!

      expect(resourceStore.download).toBeCalledTimes(1);
    },
  );
  test(
    "Trigger share uses getdownloadUrl store method",
    {
      // Override the maximum run time for this test (10 sec), due to the sleep() calls
      timeout: 10000,
    },
    async () => {
      const folderContents = await wrapper.vm.constructFolderContents(
        getFileTreeResponse,
        getMetadataTreeResponse,
      );

      await wrapper.setProps({
        dirTrail: "/",
        dirCrumbs: [],
        folderContents,
      });

      await wrapper.vm.$nextTick();

      // Wait for 1 second until everything is set up
      await sleep(1000); // Don't remove!

      // Find three point menu
      const dropdown = wrapper.find(".dropdown.dotMenu");
      await dropdown.trigger("click");

      // Wait for the dropdown menu to appear
      await wrapper.vm.$nextTick();

      // Find share button
      const shareButton = wrapper.find("#dropDownItemShare");
      await shareButton.trigger("click");

      // Wait for the action to complete
      await wrapper.vm.$nextTick();

      // Verify that the store method was called
      expect(resourceStore.getDownloadUrl).toBeCalledTimes(1);
    },
  );
});
