import { mount } from "@vue/test-utils";

import MetadataManagerViewSwitch from "./MetadataManagerViewSwitch.vue"; // Update this path to the actual location

/* Vue i18n */
import i18n, { def } from "@/plugins/vue-i18n";
import { ResourceI18nMessages } from "@/modules/resource/i18n/index";
i18n.global.availableLocales.forEach((locale) => {
  i18n.global.setLocaleMessage(locale, def[locale]); // default locale messages
  i18n.global.mergeLocaleMessage(locale, ResourceI18nMessages[locale]); // append the locale messages for the component
});

import pinia from "@/plugins/mockupPinia";

describe("MetadataManagerViewSwitch", () => {
  it('renders correctly with initial "Metadata" view', () => {
    const wrapper = mount(MetadataManagerViewSwitch, {
      global: {
        plugins: [pinia],
      },
      propsData: {
        currentView: "Metadata",
      },
    });

    // Check that the first button has the "primary" variant when "Metadata" is the current view
    const metadataButton = wrapper.findAll("button").at(0);
    expect(metadataButton?.attributes("class")).toContain("btn-primary");

    // Check that the second button has the "secondary" variant when "Metadata" is the current view
    const extractedButton = wrapper.findAll("button").at(1);
    expect(extractedButton?.attributes("class")).toContain("btn-secondary");
  });

  it("switches view and emits correct event when button is clicked", async () => {
    const wrapper = mount(MetadataManagerViewSwitch, {
      global: {
        plugins: [pinia],
      },
      propsData: {
        currentView: "Metadata",
      },
    });

    const button = wrapper.findAll("button").at(0);
    await button?.trigger("click");
    const switchView = wrapper.emitted().switchView;
    expect(switchView).toBeTruthy();
    expect(switchView && switchView[0] ? switchView[0] : null).toEqual([
      "Extracted",
    ]); // Check emitted event argument
  });
});
