import { mount } from "@vue/test-utils";

import MetadataManagerExtractedView from "./MetadataManagerExtractedView.vue"; // Update this path to the actual location

import { radarMetadata } from "@/data/mockup/metadata/metadata";
import { parseRDFDefinition } from "@/modules/resource/utils/linkedData";
import pinia from "@/plugins/mockupPinia";

/* Vue i18n */
import i18n, { def } from "@/plugins/vue-i18n";
import { ResourceI18nMessages } from "@/modules/resource/i18n/index";
i18n.global.availableLocales.forEach((locale) => {
  i18n.global.setLocaleMessage(locale, def[locale]); // default locale messages
  i18n.global.mergeLocaleMessage(locale, ResourceI18nMessages[locale]); // append the locale messages for the component
});

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

describe("MetadataManagerExtractedView", () => {
  it("renders the extracted metadata correctly", async () => {
    const dataset = await parseRDFDefinition(radarMetadata, "text/turtle");

    const wrapper = mount(MetadataManagerExtractedView, {
      global: {
        plugins: [pinia],
      },
      propsData: {
        loadingExtractedMetadata: false,
        metadata: dataset,
      },
    });

    await wrapper.vm.$nextTick();

    // Wait for 1 second until everything is set up
    await sleep(1000); // Don't remove!

    // Check that the textarea includes the RDF
    const textarea = wrapper.findAll("textarea").at(0);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect(textarea?.element.value).toContain(
      "<https://hdl.handle.net/21.11102/eeb8d803-46a1-49ba-a47c-81cd4f49cd65@path=%2FTest>",
    );

    // Check if mime type changing works
    wrapper.vm.mimeType = "application/ld+json";

    await wrapper.vm.$nextTick();

    // Wait for 1 second until everything is set up
    await sleep(1000); // Don't remove!

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect(textarea?.element.value).toContain(
      '"@id":"https://hdl.handle.net/21.11102/eeb8d803-46a1-49ba-a47c-81cd4f49cd65@path=%2FTest"',
    );
  });
});
