<template>
  <div class="SetupPageRds">
    <CoscineFormGroup
      :mandatory="true"
      :label="$t('page.createResource.configuration.labels.size')"
    >
      <div class="d-flex align-items-center">
        <b-form-input
          id="quotaInput"
          v-model="v$.sliderValue.$model"
          class="w-40"
          type="number"
          :min="min"
          :max="max"
          :step="1"
          :state="v$.sliderValue.$anyDirty && !v$.sliderValue.$error"
          oninput="if(Number(this.value) > Number(this.max)) this.value = this.max;"
        />
        <span class="ml-2">GB of {{ max }} GB</span>
      </div>
      <div v-if="v$.sliderValue.$error" class="text-danger">
        <p>
          {{
            $t("page.createResource.configuration.exceededQuotaWarning", {
              min: min,
              max: max,
            })
          }}
        </p>
      </div>
      <div>
        <!-- Router Link -->
        <router-link
          v-if="isOwner"
          :to="{ name: 'project-quota' }"
          class="text-primary"
        >
          {{ $t("page.createResource.configuration.needMore") }}
        </router-link>
      </div>
    </CoscineFormGroup>
  </div>
</template>

<script lang="ts" setup>
import useProjectStore from "@/modules/project/store";
import { isNumber } from "lodash";
import { FileUtil } from "../../utils/FileUtil";
import type {
  QuotaDto,
  ResourceForCreationDto,
  ResourceTypeInformationDto,
  ResourceTypeOptionsForCreationDto,
} from "@coscine/api-client/dist/types/Coscine.Api";
import { CoscineResourceTypes, QuotaUnit } from "../../types";
import useVuelidate from "@vuelidate/core";
import { required, numeric, minValue, maxValue } from "@vuelidate/validators";

const resourceForCreation = defineModel<ResourceForCreationDto>({
  required: true,
});

const props = defineProps({
  selectedResourceTypeInformation: {
    type: Object as PropType<ResourceTypeInformationDto>,
    required: true,
  },
});

const projectStore = useProjectStore();

const coscineResourceTypes = CoscineResourceTypes;

const min = ref(1);
const max = ref(1);
const sliderValue = ref(min.value); // used to set the Resource Size

// Define validations
const rules = reactive(() => ({
  sliderValue: {
    required,
    numeric,
    minValue: minValue(min.value),
    maxValue: maxValue(max.value),
  },
}));

const v$ = useVuelidate(rules, reactive({ sliderValue }));

const isOwner = computed(() => {
  return projectStore.currentUserRoleIsOwner;
});

watch(
  () => resourceForCreation.value.resourceTypeOptions,
  () => {
    initTabContent();
    retrieveResourceTypeQuota();
    // Timeout deals with otherwise incorrect updates
    setTimeout(() => setResourceSize(sliderValue.value), 1);
  },
  { deep: true },
);

watch(sliderValue, () => {
  setResourceSize(sliderValue.value);
});

/**
 * Retrieve the quota for the resource type and update the slider's maximum and value
 */
const retrieveResourceTypeQuota = () => {
  if (projectStore.currentResourceTypesQuotas) {
    const resourceTypeId = resourceForCreation.value.resourceTypeId;
    const quota = projectStore.currentResourceTypesQuotas.find(
      (q) => q.resourceType?.id === resourceTypeId,
    );
    if (quota) {
      // Calculate slider maximum from Reserved and Used
      if (
        quota.allocated &&
        isNumber(quota.allocated.value) &&
        quota.totalReserved &&
        isNumber(quota.totalReserved.value)
      ) {
        max.value = toGiB(quota.allocated) - toGiB(quota.totalReserved);
        // Prevent negative maximum values
        if (max.value < 0) {
          max.value = 0;
        }
        // Prevent above maximum values
        if (sliderValue.value > max.value) {
          sliderValue.value = max.value;
        }
        // Set slider to 1 when maximum is available
        if (max.value > 0 && sliderValue.value === 0) {
          sliderValue.value = 1;
        }
      }
    }
  }
};

/**
 * Initialize the tab content based on the resource type options
 */
const initTabContent = () => {
  if (sliderValue.value <= 0) {
    // Handle general resource type - RDS
    if (
      resourceForCreation.value.resourceTypeOptions?.rdsResourceTypeOptions
        ?.quota?.value !== undefined
    ) {
      sliderValue.value =
        resourceForCreation.value.resourceTypeOptions?.rdsResourceTypeOptions
          ?.quota?.value;
    }
    // Handle general resource type - RDS S3
    else if (
      resourceForCreation.value.resourceTypeOptions?.rdsS3ResourceTypeOptions
        ?.quota?.value !== undefined
    ) {
      sliderValue.value =
        resourceForCreation.value.resourceTypeOptions?.rdsS3ResourceTypeOptions
          ?.quota?.value;
    }
    // Handle general resource type - RDS S3 WORM
    else if (
      resourceForCreation.value.resourceTypeOptions
        ?.rdsS3WormResourceTypeOptions?.quota?.value !== undefined
    ) {
      sliderValue.value =
        resourceForCreation.value.resourceTypeOptions
          ?.rdsS3WormResourceTypeOptions?.quota?.value;
    }
  }
};

/**
 * Update the size of the resource based on the selected resource type
 *
 * @param {number} size - The new size for the resource
 */
const setResourceSize = (size: number) => {
  const resourceTypeOptionsForManipulationDto: ResourceTypeOptionsForCreationDto =
    {};

  // Evaluate general resource type - RDS
  if (
    props.selectedResourceTypeInformation.generalType ===
    coscineResourceTypes.Rds.General
  ) {
    resourceTypeOptionsForManipulationDto.rdsResourceTypeOptions = {
      quota: { value: size, unit: QuotaUnit.GibiByte },
    };
  }
  // Evaluate general resource type - RDS S3
  else if (
    props.selectedResourceTypeInformation.generalType ===
    coscineResourceTypes.RdsS3.General
  ) {
    resourceTypeOptionsForManipulationDto.rdsS3ResourceTypeOptions = {
      quota: { value: size, unit: QuotaUnit.GibiByte },
    };
  }
  // Evaluate general resource type - RDS S3 WORM
  else if (
    props.selectedResourceTypeInformation.generalType ===
    coscineResourceTypes.RdsS3Worm.General
  ) {
    resourceTypeOptionsForManipulationDto.rdsS3WormResourceTypeOptions = {
      quota: { value: size, unit: QuotaUnit.GibiByte },
    };
  }
  resourceForCreation.value.resourceTypeOptions =
    resourceTypeOptionsForManipulationDto;
};

/**
 * Converts a quota value to GiB (Gibibytes).
 * @param quota - The quota to be converted.
 * @returns {number} - The quota value in GiB.
 */
const toGiB = (quota: QuotaDto): number => {
  // Almost all displayed values inside the html must be shown in GiB, making this method necessary to ensure that's always the case.
  return FileUtil.convertCapacityUnits(quota, QuotaUnit.GibiByte);
};

initTabContent();
retrieveResourceTypeQuota();
setResourceSize(sliderValue.value);
v$.value.$touch();
</script>
