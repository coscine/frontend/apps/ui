/* Testing imports */
import { mount } from "@vue/test-utils";
import { createTestingPinia } from "@pinia/testing";

/* Vue i18n */
import i18n, { def } from "@/plugins/vue-i18n";
import { ResourceI18nMessages } from "@/modules/resource/i18n/index";
i18n.global.availableLocales.forEach((locale) => {
  i18n.global.setLocaleMessage(locale, def[locale]); // default locale messages
  i18n.global.mergeLocaleMessage(locale, ResourceI18nMessages[locale]); // append the locale messages for the component
});

/* Tested Component */
import TemplateModals from "./TemplateModals.vue";

import TemplateStartModal from "./TemplateStartModal.vue";
import TemplateApplyModal from "./TemplateApplyModal.vue";

import { parseRDFDefinition } from "@/modules/resource/utils/linkedData";

import { getTestShibbolethUserState } from "@/data/mockup/testUser";
import { getTestResourceState } from "@/data/mockup/testResource";
import { testProjectState } from "@/data/mockup/testProject";
import { extractedMetadata } from "@/data/mockup/metadata/metadata";
import useResourceStore from "../../../../store";
import useMetadataExtractionStore from "./store";
import type { ResourceForUpdateDto } from "@coscine/api-client/dist/types/Coscine.Api";
import { BFormFile } from "bootstrap-vue-next";

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

describe("TemplateModals.vue", async () => {
  // Create a mocked pinia instance with initial state
  const testResourceState = await getTestResourceState();
  const testingPinia = createTestingPinia({
    createSpy: vitest.fn,
    initialState: {
      project: testProjectState,
      resource: testResourceState,
      user: getTestShibbolethUserState(),
    },
  });

  // Mock the API calls
  const resourceStore = useResourceStore(testingPinia);
  vi.mocked(resourceStore.getVocabularyInstances).mockReturnValue(
    Promise.resolve({ en: [], de: [] }),
  );

  const metadataExtractionStore = useMetadataExtractionStore(testingPinia);
  vi.mocked(metadataExtractionStore.extractMetadata).mockReturnValue(
    parseRDFDefinition(
      extractedMetadata,
      "text/turtle",
      "https://purl.org/coscine/resources/activity_plot.png",
    ),
  );

  const resourceForm = {
    name: "",
    archived: false,
    description: "",
    disciplines: [],
    visibility: { id: "" },
    fixedValues: null as ResourceForUpdateDto["fixedValues"],
  };

  const wrapper = mount(TemplateModals, {
    pinia: testingPinia,
    propsData: {
      applicationProfile: {
        definition:
          testResourceState.visitedResources[testResourceState.currentId ?? "0"]
            .rawApplicationProfile!,
        displayName: "",
      },
      open: false,
      resourceForm: resourceForm,
    },
  });

  test(
    "Template Modals Flow.",
    {
      // Override the maximum run time for this test (10 sec), due to the sleep() calls
      timeout: 10000,
    },
    async () => {
      await wrapper.vm.$nextTick();

      resourceForm.fixedValues = {};
      // Open modal
      await wrapper.setProps({
        applicationProfile: {
          definition:
            testResourceState.visitedResources[
              testResourceState.currentId ?? "0"
            ].rawApplicationProfile!,
          displayName: "",
        },
        open: true,
        resourceForm: resourceForm,
      });

      // Wait for 1 second until everything is set up
      await sleep(1000); // Don't remove!

      // Start Modal Visible
      let startModal = wrapper.findComponent(TemplateStartModal);
      expect(startModal.vm.open).toBe(true);
      let applyModal = wrapper.findComponent(TemplateApplyModal);
      expect(applyModal.vm.open).toBe(false);

      // Find File Input and set it
      const inputFile = startModal.findComponent(BFormFile).find("input");
      const fileMock = new File(["(file content)"], "test-file.txt", {
        type: "text/plain",
      });

      expect(inputFile.exists()).toBe(true);

      Object.defineProperty(inputFile.element, "files", {
        value: [fileMock],
        writable: false,
      });

      await inputFile.trigger("change");
      await wrapper.vm.$nextTick();

      const nextButton = startModal.findComponent({ ref: "nextFromStart" });

      expect((nextButton.element as HTMLButtonElement).disabled).toBe(false);
      await nextButton.trigger("click");
      await wrapper.vm.$nextTick();

      // Wait for 1 second until everything is updated
      await sleep(1000); // Don't remove!

      // Apply Modal Visible
      startModal = wrapper.findComponent(TemplateStartModal);
      expect(startModal.vm.open).toBe(false);
      applyModal = wrapper.findComponent(TemplateApplyModal);
      expect(applyModal.vm.open).toBe(true);

      // Filter and check if it filters
      const applySearchInput = applyModal
        .findComponent({ ref: "applySearch" })
        .find("input");
      await applySearchInput.setValue("created");
      await wrapper.vm.$nextTick();
      const extractedValueListItems = applyModal
        .findComponent({ ref: "extractedValueList" })
        .findAll(".draggable");
      expect(extractedValueListItems.length).toBe(1);

      // Drag & Drop
      await extractedValueListItems[0].element.dispatchEvent(
        new DragEvent("dragStart"),
      );
      let applyEditFormGeneratorFirstInput = applyModal
        .findComponent({ ref: "applyEditFormGenerator" })
        .find("input");
      // TODO: Figure out how to replicate drop event
      await applyEditFormGeneratorFirstInput.trigger("drop");
      const keyInput = `{{${
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        (applyModal.vm as any).extractedValueList[0].keyFull
      }}}`;
      await applyEditFormGeneratorFirstInput.setValue(keyInput);
      await applyEditFormGeneratorFirstInput.trigger("change");

      await wrapper.vm.$nextTick();
      expect(
        (applyEditFormGeneratorFirstInput.element as HTMLInputElement).value,
      ).toBe(keyInput);
      // Check if after switching to preview the extracted value gets shown
      const applyPreviewButton = applyModal.findComponent({
        ref: "applyPreviewButton",
      });
      await applyPreviewButton.trigger("click");
      await wrapper.vm.$nextTick();
      // Wait for 1 second until everything is set up
      await sleep(1000); // Don't remove!

      applyEditFormGeneratorFirstInput = applyModal
        .findComponent({ ref: "applyViewFormGenerator" })
        .find("input");
      expect(
        (applyEditFormGeneratorFirstInput.element as HTMLInputElement).value,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
      ).toBe((applyModal.vm as any).extractedValueList[0].valueFull);

      // Save
      const applyNextButton = applyModal.findComponent({
        ref: "applyNextButton",
      });

      expect((applyNextButton.element as HTMLButtonElement).disabled).toBe(
        false,
      );
      await applyNextButton.trigger("click");
      await wrapper.vm.$nextTick();

      // Has emitted input and close
      expect(wrapper.emitted("update:modelValue")?.length).toBe(1);
      expect(wrapper.emitted("close")?.length).toBe(1);
    },
  );
});
