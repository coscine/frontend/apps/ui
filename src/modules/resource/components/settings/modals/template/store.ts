import { defineStore } from "pinia";
import type { MetadataExtractionState } from "./types";
import type { Dataset } from "@rdfjs/types";

import { parseRDFDefinition } from "../../../../utils/linkedData";

import axios, { type AxiosError } from "axios";
import useNotificationStore from "@/store/notification";
import { useLocalStorage } from "@vueuse/core";

/*  
  Store variable name is "this.<id>Store"
    id: "resource" --> this.resourceStore
  Important! The id must be unique for every store.
*/
export const useMetadataExtractionStore = defineStore({
  id: "metadataExtraction",
  /*  
    --------------------------------------------------------------------------------------
    STATES
    --------------------------------------------------------------------------------------
  */
  state: (): MetadataExtractionState => ({
    defaultMetadataExtractionEndpoint:
      "https://metadataextractor.otc.coscine.dev/",
    metadataExtractionEndpoint: useLocalStorage<string | null>(
      "coscine.metadataExtraction.endpoint",
      null,
    ),
  }),

  /*  
    --------------------------------------------------------------------------------------
    GETTERS
    --------------------------------------------------------------------------------------
    Synchronous code only.
    
    In a component use as e.g.:
      :label = "this.resourceStore.<getter_name>;
  */
  getters: {
    endpoint(): string {
      return (
        this.metadataExtractionEndpoint ??
        this.defaultMetadataExtractionEndpoint
      );
    },
  },

  /*  
    --------------------------------------------------------------------------------------
    ACTIONS
    --------------------------------------------------------------------------------------
    Asynchronous & Synchronous code comes here (e.g. API calls and VueX mutations).
    To change a state use an action.

    In a component use as e.g.:
      @click = "this.resourceStore.<action_name>();
  */
  actions: {
    async extractMetadata(file: File): Promise<Dataset | null> {
      const notificationStore = useNotificationStore();
      try {
        const formData = new FormData();
        formData.append("file", file);
        const acceptMimeType = "text/turtle";
        const response = await axios.post(this.endpoint, formData, {
          headers: {
            Accept: acceptMimeType,
            "Content-Type": "multipart/form-data",
          },
        });
        return await parseRDFDefinition(response.data, acceptMimeType);
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return null;
      }
    },
  },
});

export default useMetadataExtractionStore;
