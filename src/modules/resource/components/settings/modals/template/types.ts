import type { RemovableRef } from "@vueuse/core";

export type MetadataExtractionState = {
  defaultMetadataExtractionEndpoint: string;
  metadataExtractionEndpoint: RemovableRef<string | null>;
};
