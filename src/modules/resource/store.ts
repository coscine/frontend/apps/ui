import { defineStore } from "pinia";
import type {
  ApplicationProfileDefinition,
  ApplicationProfilesPaged,
  BilingualLabel,
  BilingualLabels,
  ResourceState,
  StoredColumnValues,
  VisitedResourceObject,
} from "./types";
import type { Dataset } from "@rdfjs/types";

import {
  ApplicationProfileApi,
  BlobApi,
  ProjectResourceApi,
  ProjectResourceQuotaApi,
  ProjectResourceTypeApi,
  ResourceApi,
  ResourceTypeApi,
  TreeApi,
  VocabularyApi,
} from "@coscine/api-client";
import type { RouteLocationNormalized } from "vue-router";
import axios, { type AxiosRequestConfig, type AxiosError } from "axios";
import useProjectStore from "../project/store";
import useNotificationStore from "@/store/notification";
import { useLocalStorage } from "@vueuse/core";
import {
  parseRDFDefinition,
  resolveImports,
  serializeRDFDefinition,
} from "./utils/linkedData";
import factory from "rdf-ext";
import fileSaver from "file-saver";
import type {
  FileActionsDto,
  RdfFormat,
  GitlabBranchDto,
  GitlabProjectDto,
  MetadataTreeForCreationDto,
  MetadataTreeForUpdateDto,
  ProjectDto,
  ResourceDto,
  ResourceForCreationDto,
  ResourceForUpdateDto,
  ResourceQuotaDto,
  FileTreeDto,
  MetadataTreeDto,
} from "@coscine/api-client/dist/types/Coscine.Api";
import { wrapListRequest } from "@/util/wrapListRequest";
import i18n from "@/plugins/vue-i18n";

/*  
  Store variable name is "this.<id>Store"
    id: "resource" --> this.resourceStore
  Important! The id must be unique for every store.
*/
export const useResourceStore = defineStore({
  id: "resource",
  /*  
    --------------------------------------------------------------------------------------
    STATES
    --------------------------------------------------------------------------------------
  */
  state: (): ResourceState => ({
    allResources: null,
    classes: {},
    currentId: null,
    resourceTypes: null,
    enabledResourceTypes: null,
    visitedResources: {},
  }),

  /*  
    --------------------------------------------------------------------------------------
    GETTERS
    --------------------------------------------------------------------------------------
    Synchronous code only.
    
    In a component use as e.g.:
      :label = "this.resourceStore.<getter_name>;
  */
  getters: {
    currentFullApplicationProfile(): Dataset | null {
      if (this.currentId) {
        return this.visitedResources[this.currentId].fullApplicationProfile;
      } else {
        return null;
      }
    },

    currentResource(): VisitedResourceObject | null {
      if (this.currentId) {
        return this.visitedResources[this.currentId];
      } else {
        return null;
      }
    },

    currentResourceQuota(): ResourceQuotaDto | null | undefined {
      if (this.currentId) {
        return this.visitedResources[this.currentId].quota;
      } else {
        return null;
      }
    },

    currentStoredColumns(): StoredColumnValues | null {
      if (this.currentId && this.currentResource) {
        if (this.currentResource.storedColumns) {
          const storedColumnValues = JSON.parse(
            this.currentResource.storedColumns,
          ) as StoredColumnValues;
          // Deal with legacy values
          if (typeof storedColumnValues.sortBy === "string") {
            storedColumnValues.sortBy = [
              { key: storedColumnValues.sortBy, order: "desc" },
            ];
          }
          return storedColumnValues;
        }
      }
      return null;
    },
  },
  /*  
    --------------------------------------------------------------------------------------
    ACTIONS
    --------------------------------------------------------------------------------------
    Asynchronous & Synchronous code comes here (e.g. API calls and VueX mutations).
    To change a state use an action.

    In a component use as e.g.:
      @click = "this.resourceStore.<action_name>();
  */
  actions: {
    /**
     * Currently not in use!
     * Retrieves all resources in the context of the Search module
     */
    async retrieveAllResources() {
      const projectStore = useProjectStore();
      const notificationStore = useNotificationStore();

      // Retrieve all projects if unset
      if (!projectStore.allProjects || projectStore.allProjects?.length === 0) {
        await projectStore.retrieveAllProjects();
      }
      const allProjects: ProjectDto[] = projectStore.allProjects || [];

      // Iterate over all existing projects
      try {
        this.allResources = [];
        allProjects.forEach(async (project) => {
          if (project.id) {
            const projectId = project.id;
            const resourceDtos = await wrapListRequest((pageNumber) =>
              ProjectResourceApi.getResourcesForProject({
                projectId,
                pageNumber,
                pageSize: 50,
              }),
            );
            this.allResources = this.allResources?.concat(resourceDtos) ?? [];
          } else {
            console.error("Selected project's ID is undefined.");
            return false;
          }
        });
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async retrieveResource(projectId: string, resourceId: string) {
      const notificationStore = useNotificationStore();
      try {
        const apiResponse = await ProjectResourceApi.getResourceForProject({
          projectId,
          resourceId,
        });
        this.setResourceAsVisited(apiResponse.data.data);
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async getResourceById(
      resourceId: string,
      silent: boolean = false,
    ): Promise<ResourceDto | null> {
      const notificationStore = useNotificationStore();
      try {
        const apiResponse = await ResourceApi.getResource({
          resourceId,
        });
        return apiResponse.data.data as ResourceDto;
      } catch (error) {
        if (!silent) {
          // Handle other Status Codes
          notificationStore.postApiErrorNotification(error as AxiosError);
        }
        return null;
      }
    },

    async retrieveApplicationProfile(resource: VisitedResourceObject) {
      const notificationStore = useNotificationStore();
      try {
        if (resource.applicationProfile?.uri) {
          const apiResponse = await ApplicationProfileApi.getApplicationProfile(
            {
              profile: resource.applicationProfile.uri,
            },
          );
          const returnedData = apiResponse.data.data;
          if (
            returnedData?.definition?.content &&
            returnedData?.definition?.type &&
            returnedData?.uri
          ) {
            resource.rawApplicationProfile = await parseRDFDefinition(
              returnedData.definition.content,
              returnedData.definition.type,
              returnedData.uri,
            );
            resource.fullApplicationProfile = await resolveImports(
              returnedData.uri,
              resource.rawApplicationProfile,
            );
          } else {
            console.error("Incorrect return from API.");
          }
        } else {
          console.error("Resource's application profile may be undefined.");
        }
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async getApplicationProfile(
      applicationProfile: string,
      doResolveImports = true,
    ): Promise<ApplicationProfileDefinition> {
      const notificationStore = useNotificationStore();
      try {
        const apiResponse = await ApplicationProfileApi.getApplicationProfile({
          profile: applicationProfile,
        });
        const returnedData = apiResponse.data.data;
        if (
          returnedData?.definition?.content &&
          returnedData?.definition?.type &&
          returnedData?.uri
        ) {
          let returnApplicationProfile = await parseRDFDefinition(
            returnedData.definition.content,
            returnedData.definition.type,
            returnedData.uri,
          );
          if (doResolveImports) {
            returnApplicationProfile = await resolveImports(
              returnedData.uri,
              returnApplicationProfile,
            );
          }
          return {
            definition: returnApplicationProfile,
            displayName: returnedData.displayName ?? applicationProfile,
          };
        } else {
          console.error("Incorrect return from API.");
        }
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
      return {
        definition: factory.dataset() as unknown as Dataset,
        displayName: applicationProfile,
      };
    },

    async getApplicationProfiles(
      orderBy: string,
      searchTerm: string,
      pageNumber: number,
      pageSize: number = 10,
    ): Promise<ApplicationProfilesPaged | undefined> {
      const notificationStore = useNotificationStore();
      try {
        const response = await ApplicationProfileApi.getApplicationProfiles({
          searchTerm,
          modules: false,
          orderBy,
          pageNumber,
          pageSize,
        });
        return {
          applicationProfiles: response.data.data ?? [],
          allResultsLoaded: !(response.data.pagination?.hasNext ?? false),
        };
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async retrieveResourceQuota(
      projectId: string,
      resource: VisitedResourceObject,
    ) {
      const notificationStore = useNotificationStore();
      try {
        if (resource.id) {
          const apiResponse =
            await ProjectResourceQuotaApi.getQuotaForResourceForProject({
              projectId,
              resourceId: resource.id,
            });
          resource.quota = apiResponse.data.data;
        } else {
          console.error("Selected resource's ID is undefined.");
        }
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async retrieveAvailableResourceTypesInformationForProject(
      project: ProjectDto | null,
    ) {
      const notificationStore = useNotificationStore();
      try {
        if (project && project.id) {
          const apiResponse =
            await ProjectResourceTypeApi.getAvailableResourceTypesInformationForProject(
              {
                projectId: project.id,
              },
            );
          this.enabledResourceTypes = apiResponse.data.data;
        } else {
          console.error("Selected project is null or its ID is undefined.");
        }
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async retrieveAllResourceTypesInformation() {
      const notificationStore = useNotificationStore();
      try {
        const apiResponse =
          await ResourceTypeApi.getAllResourceTypesInformation();
        this.resourceTypes = apiResponse.data.data;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
      }
    },

    async createResourceForProject(
      project: ProjectDto,
      resource: ResourceForCreationDto,
    ): Promise<ResourceDto | null | undefined> {
      const notificationStore = useNotificationStore();
      try {
        if (project.id) {
          const apiResponse = await ProjectResourceApi.createResourceForProject(
            {
              projectId: project.id,
              resourceForCreationDto: resource,
            },
          );
          return apiResponse.data.data;
        } else {
          console.error("Selected project's ID is undefined.");
          return null;
        }
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return null;
      }
    },

    async updateResourceForProject(
      projectId: string,
      resourceId: string,
      resourceForUpdateDto: ResourceForUpdateDto,
    ): Promise<boolean> {
      const notificationStore = useNotificationStore();
      resourceForUpdateDto.resourceTypeOptions = undefined;
      try {
        await ProjectResourceApi.updateResourceForProject({
          projectId,
          resourceId,
          resourceForUpdateDto,
        });
        return true;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return false;
      }
    },

    async deleteResourceForProject(
      projectId: string,
      resourceId: string,
    ): Promise<boolean> {
      const notificationStore = useNotificationStore();
      try {
        await ProjectResourceApi.deleteResourceForProject({
          projectId,
          resourceId,
        });
        return true;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return false;
      }
    },

    async retrieveUnsetResource(
      projectId: string,
      resource: ResourceDto | null,
      route: RouteLocationNormalized,
    ) {
      const routeParams = route.params;
      if (
        // Case: Resource null, ID from URL
        (resource === null && routeParams.guid !== undefined) ||
        // Case: Project NOT null, ID from URL not equal Project ID
        (resource !== null &&
          resource.id !== null &&
          resource.id !== undefined &&
          routeParams.guid !== undefined &&
          resource.id !== routeParams.guid)
      ) {
        // Try getting project from visitedProjects before fetching from API
        const guid = routeParams.guid as string;
        if (this.visitedResources[guid] === undefined) {
          await this.retrieveResource(projectId, guid);
        }
        this.currentId = guid;
      }
    },

    setResourceAsVisited(resource: ResourceDto | undefined) {
      if (resource && resource.id) {
        if (this.visitedResources[resource.id]) {
          delete this.visitedResources[resource.id];
        }
        // Important! Keep object assignment reactive()
        const visitedResource: VisitedResourceObject = reactive({
          ...resource,
          fullApplicationProfile: null,
          quota: null,
          rawApplicationProfile: null,
          storedColumns: useLocalStorage<string>(
            `coscine.rcv.storedColumns.${resource.id}`,
            JSON.stringify({
              columns: [],
              filter: "",
              sortBy: [],
            } satisfies StoredColumnValues),
          ),
        });
        this.visitedResources[resource.id] = visitedResource;
      }
    },

    async setStoredColumns(storedColumnValues: StoredColumnValues) {
      if (this.currentResource && this.currentResource.storedColumns) {
        // Use Ticks since otherwise, the current data might not be used
        await nextTick();
        this.currentResource.storedColumns = JSON.stringify(storedColumnValues);
        await nextTick();
      }
    },

    async getVocabularyInstance(instanceUrl: string): Promise<BilingualLabel> {
      const notificationStore = useNotificationStore();
      try {
        const enInstance = await VocabularyApi.getVocabularyInstance({
          acceptLanguage: "en",
          instance: instanceUrl,
        });
        const deInstance = await VocabularyApi.getVocabularyInstance({
          acceptLanguage: "de",
          instance: instanceUrl,
        });
        const bilingualLabel: BilingualLabel = {
          de: {
            name: deInstance.data.data?.displayName,
            value: deInstance.data.data?.instanceUri,
          },
          en: {
            name: enInstance.data.data?.displayName,
            value: enInstance.data.data?.instanceUri,
          },
        };
        return bilingualLabel;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return {};
      }
    },

    async getVocabularyInstances(
      className: string,
      pageNumber: number,
      pageSize: number,
      searchTerm?: string | undefined,
    ): Promise<BilingualLabels> {
      const notificationStore = useNotificationStore();
      const classIdentifier = className + pageNumber + searchTerm;
      try {
        if (!this.classes[classIdentifier]) {
          const enInstances = await VocabularyApi.getVocabularyInstances({
            _class: className,
            language: "en",
            pageNumber,
            pageSize,
            searchTerm,
          });
          const deInstances = await VocabularyApi.getVocabularyInstances({
            _class: className,
            language: "de",
            pageNumber,
            pageSize,
            searchTerm,
          });
          const bilingualLabels: BilingualLabels = {
            dePagination: deInstances.data.pagination,
            de: deInstances.data.data?.map((instance) => {
              return {
                name: instance.displayName,
                value: instance.instanceUri,
              };
            }),
            enPagination: enInstances.data.pagination,
            en: enInstances.data.data?.map((instance) => {
              return {
                name: instance.displayName,
                value: instance.instanceUri,
              };
            }),
          };

          this.classes[classIdentifier] = bilingualLabels;
        }
        return this.classes[classIdentifier];
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return {};
      }
    },

    async createMetadataTree(
      projectId: string,
      resourceId: string,
      metadataTreeForCreationDto: MetadataTreeForCreationDto,
    ): Promise<boolean> {
      const notificationStore = useNotificationStore();
      try {
        await TreeApi.createMetadataTree({
          projectId,
          resourceId,
          metadataTreeForCreationDto,
        });
        return true;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return false;
      }
    },

    async updateMetadataTree(
      projectId: string,
      resourceId: string,
      metadataTreeForUpdateDto: MetadataTreeForUpdateDto,
    ): Promise<boolean> {
      const notificationStore = useNotificationStore();
      try {
        await TreeApi.updateMetadataTree({
          projectId,
          resourceId,
          metadataTreeForUpdateDto,
        });
        return true;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return false;
      }
    },

    /**
     * Attempts to add a new metadata tree. If a conflict (409) error occurs, it tries to update the existing metadata tree.
     *
     * @param {string} projectId - The ID of the project.
     * @param {string} resourceId - The ID of the resource.
     * @param {MetadataTreeForCreationDto | MetadataTreeForUpdateDto} metadataTreeDto - The data transfer object containing metadata tree details. Can be for creation or update.
     * @returns {Promise<boolean>} Returns true if the operation is successful, false otherwise.
     * @throws {AxiosError} Throws an AxiosError if the API call fails.
     */
    async addOrUpdateMetadataTree(
      projectId: string,
      resourceId: string,
      metadataTreeDto: MetadataTreeForCreationDto | MetadataTreeForUpdateDto,
    ): Promise<boolean> {
      const notificationStore = useNotificationStore();

      try {
        // Attempt to create metadata tree
        await TreeApi.createMetadataTree({
          projectId,
          resourceId,
          metadataTreeForCreationDto: metadataTreeDto,
        });
        return true;
      } catch (error: unknown) {
        if (axios.isAxiosError(error)) {
          if (error.response?.status === 409) {
            // Metadata tree already exists, attempt to update
            try {
              await TreeApi.updateMetadataTree({
                projectId,
                resourceId,
                metadataTreeForUpdateDto: metadataTreeDto, // NOTE: Potential issue here, as the DTO might not be the same for creation and update if signature changes
              });
              return true;
            } catch (updateError) {
              // Handle other Status Codes
              notificationStore.postApiErrorNotification(
                updateError as AxiosError,
              );
              return false;
            }
          }
        }
      }
      // Handle notification externally
      return false;
    },

    /**
     * Attempts to update a new metadata tree. If a not found (404) error occurs, it tries to create a new metadata tree.
     *
     * @param {string} projectId - The ID of the project.
     * @param {string} resourceId - The ID of the resource.
     * @param {MetadataTreeForCreationDto | MetadataTreeForUpdateDto} metadataTreeDto - The data transfer object containing metadata tree details. Can be for creation or update.
     * @returns {Promise<boolean>} Returns true if the operation is successful, false otherwise.
     * @throws {AxiosError} Throws an AxiosError if the API call fails.
     */
    async updateOrAddMetadataTree(
      projectId: string,
      resourceId: string,
      metadataTreeDto: MetadataTreeForCreationDto | MetadataTreeForUpdateDto,
    ): Promise<boolean> {
      const notificationStore = useNotificationStore();

      try {
        // Attempt to update metadata tree
        await TreeApi.updateMetadataTree({
          projectId,
          resourceId,
          metadataTreeForUpdateDto: metadataTreeDto, // NOTE: Potential issue here, as the DTO might not be the same for creation and update if signature changes
        });
        return true;
      } catch (error: unknown) {
        if (axios.isAxiosError(error)) {
          if (error.response?.status === 404) {
            // Metadata tree already exists, attempt to update
            try {
              await TreeApi.createMetadataTree({
                projectId,
                resourceId,
                metadataTreeForCreationDto: metadataTreeDto, // NOTE: Potential issue here, as the DTO might not be the same for creation and update if signature changes
              });
              return true;
            } catch (updateError) {
              // Handle other Status Codes
              notificationStore.postApiErrorNotification(
                updateError as AxiosError,
              );
              return false;
            }
          }
        }
      }
      // Handle notification externally
      return false;
    },

    async getMetadataTree(
      projectId: string,
      resourceId: string,
      filePath: string,
      format: RdfFormat = "JsonLd" as RdfFormat,
    ): Promise<MetadataTreeDto[] | null | undefined> {
      const notificationStore = useNotificationStore();
      try {
        const response = await wrapListRequest((pageNumber: number) =>
          TreeApi.getMetadataTree({
            projectId,
            resourceId,
            path: filePath,
            format,
            pageNumber,
            pageSize: 50,
          }),
        );
        return response;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return null;
      }
    },

    async getFileTree(
      projectId: string,
      resourceId: string,
      filePath: string,
    ): Promise<FileTreeDto[] | null | undefined> {
      const notificationStore = useNotificationStore();
      try {
        const response = await wrapListRequest((pageNumber: number) =>
          TreeApi.getFileTree({
            projectId,
            resourceId,
            path: filePath,
            pageNumber,
            pageSize: 50,
          }),
        );
        return response;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return null;
      }
    },

    async getCurrentMetadataVersion(
      projectId: string,
      resourceId: string,
      filePath: string,
    ): Promise<
      { response: MetadataTreeDto; dataset: Dataset } | null | undefined
    > {
      const format: RdfFormat = "JsonLd" as RdfFormat;
      const notificationStore = useNotificationStore();
      try {
        const response = await TreeApi.getSpecificMetadataTree({
          projectId,
          resourceId,
          path: filePath,
          format,
        });
        const metadataTreeDto = response.data.data;
        if (metadataTreeDto) {
          const content = metadataTreeDto.definition?.content;
          const type = metadataTreeDto.definition?.type;
          if (content && type) {
            return {
              response: metadataTreeDto,
              dataset: await parseRDFDefinition(content, type),
            };
          }
        }
        return undefined;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return null;
      }
    },

    async getMetadataVersion(
      projectId: string,
      resourceId: string,
      filePath: string,
      version?: number,
    ): Promise<Dataset | null | undefined> {
      const format: RdfFormat = "JsonLd" as RdfFormat;
      const notificationStore = useNotificationStore();
      try {
        const response = await TreeApi.getSpecificMetadataTree({
          projectId,
          resourceId,
          path: filePath,
          format,
          version,
        });
        const metadata = response.data.data;
        if (metadata) {
          const content = metadata?.definition?.content;
          const type = metadata?.definition?.type;
          if (content && type) {
            return await parseRDFDefinition(content, type);
          }
        }
        return undefined;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return null;
      }
    },

    async getExtractedMetadata(
      projectId: string,
      resourceId: string,
      filePath: string,
      version: number,
    ): Promise<Dataset | null | undefined> {
      const format: RdfFormat = "JsonLd" as RdfFormat;
      const notificationStore = useNotificationStore();
      try {
        const response = await TreeApi.getSpecificMetadataTree({
          projectId,
          resourceId,
          path: filePath,
          format,
          version,
          includeExtractedMetadata: true,
        });
        const extracted = response.data.data?.extracted;
        if (extracted) {
          const content = extracted?.definition?.content;
          const type = extracted?.definition?.type;
          if (content && type) {
            return await parseRDFDefinition(
              content,
              type,
              extracted.metadataId,
            );
          }
        }
        return undefined;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return null;
      }
    },

    async download(
      projectId: string,
      resourceId: string,
      path: string,
      name: string,
      actions?: FileActionsDto,
    ) {
      const notificationStore = useNotificationStore();
      try {
        // Easiest case: Use the provided download URL
        if (actions?.download?.url) {
          fileSaver.saveAs(actions.download.url, name);
        } else {
          const response = await this.getBlob(
            projectId,
            resourceId,
            path,
            true,
          );
          if (response !== null) {
            // Trigger file download
            fileSaver.saveAs(
              new Blob([response.data], {
                type: response.headers["content-type"],
              }),
              name,
            );
          } else {
            // Handle other Status Codes
            notificationStore.postGeneralApiWarningNotification(
              "No Download Response",
            );
            return null;
          }
        }
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return null;
      }
    },

    async getDownloadUrl(
      projectId: string,
      resourceId: string,
      path: string,
      name: string,
      actions?: FileActionsDto,
      editableDataUrl?: boolean,
    ): Promise<string | null> {
      const notificationStore = useNotificationStore();
      try {
        // Easiest case: Use the provided download URL
        if (actions?.download?.url) {
          return actions.download.url;
        }
        // Check for editableDataUrl and prevent GitLab
        if (editableDataUrl) {
          const response = await this.getBlob(
            projectId,
            resourceId,
            path,
            true,
          );
          if (response !== null) {
            // Create a URL from the Blob
            const url = URL.createObjectURL(
              new Blob([response.data], {
                type: response.headers["content-type"],
              }),
            );
            return url;
          } else {
            // Handle other Status Codes
            notificationStore.postGeneralApiWarningNotification(
              "No Download Response",
            );
            return null;
          }
        }
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return null;
      }
      return null;
    },
    async triggerMetadataDownload(
      metadata: Dataset,
      fileName: string = "metadata.ttl",
    ) {
      const parsedMetadata = await serializeRDFDefinition(
        metadata,
        "text/turtle",
      );
      // Trigger file download
      fileSaver.saveAs(
        new Blob([parsedMetadata], {
          type: "text/turtle",
        }),
        fileName,
      );
    },

    async getBlob(
      projectId: string,
      resourceId: string,
      absoluteFilePath: string,
      asBlob = false,
    ) {
      const notificationStore = useNotificationStore();
      try {
        const response = await BlobApi.getBlob(
          {
            projectId,
            resourceId,
            key: absoluteFilePath,
          },
          /* the following is an axios option */
          asBlob
            ? {
                responseType: "blob",
              }
            : undefined,
        );
        return response;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return null;
      }
    },

    async createBlob(
      projectId: string,
      resourceId: string,
      absoluteFilePath: string,
      file: File,
      options?: AxiosRequestConfig,
    ) {
      const notificationStore = useNotificationStore();
      try {
        const response = await BlobApi.createBlob(
          {
            projectId,
            resourceId,
            key: absoluteFilePath,
            file: file,
          },
          options,
        );
        return response.status >= 200 && response.status < 300;
      } catch (error) {
        if (axios.isAxiosError(error)) {
          // Handle Quota Limit Exceeded
          if (
            error.response?.data.data.errorId ===
            "QuotaLimitExceededBadRequestException"
          ) {
            notificationStore.postNotification({
              title: i18n.global.t("toast.quotaLimitExceeded.title").toString(),
              body: i18n.global
                .t("toast.quotaLimitExceeded.message")
                .toString(),
              variant: "warning",
              autoHide: false,
            });
          }
        } else {
          // Handle other Status Codes
          notificationStore.postApiErrorNotification(error as AxiosError);
        }
      }
      return false;
    },

    async updateBlob(
      projectId: string,
      resourceId: string,
      absoluteFilePath: string,
      file: File,
      options?: AxiosRequestConfig,
    ) {
      const notificationStore = useNotificationStore();
      try {
        const response = await BlobApi.updateBlob(
          {
            projectId,
            resourceId,
            key: absoluteFilePath,
            file: file,
          },
          options,
        );
        return response.status >= 200 && response.status < 300;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return false;
      }
    },

    async deleteBlob(
      projectId: string,
      resourceId: string,
      absoluteFilePath: string,
    ) {
      const notificationStore = useNotificationStore();
      try {
        await BlobApi.deleteBlob({
          projectId,
          resourceId,
          key: absoluteFilePath,
        });
        return true;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return false;
      }
    },

    async getGitlabAllProjects(
      domain: string,
      accessToken: string,
    ): Promise<GitlabProjectDto[] | null | undefined> {
      try {
        const response = await ResourceTypeApi.getAllGitlabProjects({
          accessToken,
          domain,
        });
        return response.data.data;
      } catch (error) {
        return null;
      }
    },

    async getGitlabProject(
      gitlabProjectId: number,
      domain: string,
      accessToken: string,
    ): Promise<GitlabProjectDto | null | undefined> {
      const notificationStore = useNotificationStore();
      try {
        const response = await ResourceTypeApi.getGitlabProject({
          accessToken,
          domain,
          gitlabProjectId,
        });
        return response.data.data;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return null;
      }
    },

    async getGitlabBranchesForProject(
      gitlabProjectId: number,
      domain: string,
      accessToken: string,
    ): Promise<GitlabBranchDto[] | null | undefined> {
      const notificationStore = useNotificationStore();
      try {
        const response = await ResourceTypeApi.getAllGitlabBranchesForProject({
          accessToken,
          domain,
          gitlabProjectId,
        });
        return response.data.data;
      } catch (error) {
        // Handle other Status Codes
        notificationStore.postApiErrorNotification(error as AxiosError);
        return null;
      }
    },
  },
});

export default useResourceStore;
