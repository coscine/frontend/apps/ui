import {
  FixedValueForResourceManipulationDto,
  ResourceQuotaDto,
} from "@coscine/api-client/dist/types/Coscine.Api";
import type {
  ApplicationProfileDto,
  FileActionsDto,
  GitlabBranchDto,
  GitlabProjectDto,
  Pagination,
  ResourceDto,
  ResourceTypeInformationDto,
  TreeDataType,
} from "@coscine/api-client/dist/types/Coscine.Api";

import type { Dataset } from "@rdfjs/types";
import type { BTable, BTableSortBy, TableField } from "bootstrap-vue-next";

export interface VisitedResourceObject extends ResourceDto {
  rawApplicationProfile: Dataset | null;
  fullApplicationProfile: Dataset | null;
  quota: ResourceQuotaDto | null | undefined;
  storedColumns: string | null;
}

export interface Metadata {
  [key: string | number]: Array<Metadata> | Metadata | string;
}

export interface ExtendedResourceTypeInformationDto
  extends ResourceTypeInformationDto {
  iDisplayName: string;
  iFullName?: string;
  iDescription?: string;
}

export interface ResourceCreationTab {
  title: string;
  active: boolean;
  step: string;
  hidden: boolean;
}

export interface GroupedApplicationProfile {
  uri: string;
  profiles: ApplicationProfileDto[];
}

export interface GitlabInformation {
  domain: string;
  accessToken: string;
  project: GitlabProjectDto | null;
  branch: GitlabBranchDto | null;
}

export interface CustomTableField extends TableField<FolderContent> {
  key: string;
  active: boolean;
}

export interface StoredColumnValues {
  columns: CustomTableField[];
  filter: string;
  sortBy?: BTableSortBy[];
}

export interface FixedValuesForResource {
  [key: string]: { [key: string]: FixedValueForResourceManipulationDto[] };
}

export interface ResourceState {
  /*  
    --------------------------------------------------------------------------------------
    STATE TYPE DEFINITION
    --------------------------------------------------------------------------------------
  */
  allResources: ResourceDto[] | null;
  classes: { [className: string]: BilingualLabels };
  currentId: string | null;
  resourceTypes: ResourceTypeInformationDto[] | null | undefined;
  enabledResourceTypes: ResourceTypeInformationDto[] | null | undefined;
  visitedResources: { [slug: string]: VisitedResourceObject };
}

/**
 * Represents a map of specific resource type names to their string identifiers.
 * Each property is optional as not all "Specific" objects have all properties.
 */
export interface CoscineSpecificResourceType {
  RWTH?: string;
  NRW?: string;
  UDE?: string;
  TUDO?: string;
  RUB?: string;
}

/**
 * Represents a generic structure for resource types,
 * containing a general name for the resource type and a map of specific names ({@link CoscineSpecificResourceType}) to their string identifiers.
 */
export interface CoscineResourceType {
  General: string;
  Specific: CoscineSpecificResourceType;
}

/**
 * Represents the structure of the resource types in Coscine.
 * Each resource type is represented as a key with a value that follows the {@link CoscineResourceType} structure.
 */
export interface CoscineResourceTypesDefinition {
  LinkedData: CoscineResourceType;
  Gitlab: CoscineResourceType;
  Rds: CoscineResourceType;
  RdsS3: CoscineResourceType;
  RdsS3Worm: CoscineResourceType;
}

/**
 * ResourceTypes constant is an object that adheres to the CoscineResourceTypes interface.
 * It defines specific and general identifiers for the resource types in Coscine.
 */
export const CoscineResourceTypes: CoscineResourceTypesDefinition = {
  LinkedData: {
    General: "linked",
    Specific: {},
  },
  Gitlab: {
    General: "gitlab",
    Specific: {},
  },
  Rds: {
    General: "rds",
    Specific: {
      RWTH: "rdsrwth",
      NRW: "rdsnrw",
      UDE: "rdsude",
      TUDO: "rdstudo",
      RUB: "rdsrub",
    },
  },
  RdsS3: {
    General: "rdss3",
    Specific: {
      RWTH: "rdss3rwth",
      NRW: "rdss3nrw",
      UDE: "rdss3ude",
      TUDO: "rdss3tudo",
      RUB: "rdss3rub",
    },
  },
  RdsS3Worm: {
    General: "rdss3worm",
    Specific: {
      RWTH: "rdss3wormrwth",
    },
  },
};

/**
 * Defining the {@link QuotaUnit} enum inside the types, because it is not being exported correctly from the @coscine/api-client library.
 */
export enum QuotaUnit {
  Byte = "https://qudt.org/vocab/unit/BYTE",
  KibiByte = "https://qudt.org/vocab/unit/KibiBYTE",
  MebiByte = "https://qudt.org/vocab/unit/MebiBYTE",
  GibiByte = "https://qudt.org/vocab/unit/GibiBYTE",
  TebiByte = "https://qudt.org/vocab/unit/TebiBYTE",
  PebiByte = "https://qudt.org/vocab/unit/PebiBYTE",
}

/**
 * Defining the {@link ResourceTypeStatus} enum inside the types, because it is not being exported correctly from the @coscine/api-client library.
 */
export enum ResourceTypeStatus {
  Hidden = "hidden",
  Active = "active",
}

/**
 * Bilingual labels
 * @export
 * @interface BilingualLabel
 */
export interface BilingualLabel {
  /**
   * English labels
   * @type {Label}
   * @memberof BilingualLabel
   */
  en?: Label | null;
  /**
   * German labels
   * @type {Label}
   * @memberof BilingualLabel
   */
  de?: Label | null;
}

/**
 * This interface is inherited from the logic of API v1.
 */
export interface BilingualLabels {
  enPagination?: Pagination;
  en?: Array<Label> | null;
  dePagination?: Pagination;
  de?: Array<Label> | null;
}
/**
 *This interface is inherited from the logic of API v1.
 */
export interface Label {
  name?: string | null;
  value?: string | null;
}

export interface GeneralInformation {
  id: string;
  name: string;
  path: string;
  parentDirectory: string;
  type: TreeDataType;
  versions: number[];
  latestVersion: number;
  currentVersion: number;
  actions?: FileActionsDto;
  lastModified?: string | null;
  readOnly?: boolean;
  createdAt?: string;
  metadata: Record<number, Dataset | null>;
  extractedMetadata?: Record<number, Dataset | null>;
}

export interface FileInformation extends GeneralInformation {
  type: "Leaf";
  size: number;
  dataUrl?: string;
  info?: File;
  uploading?: boolean;
  requesting?: boolean;
}

export interface FolderInformation extends GeneralInformation {
  type: "Tree";
}

export interface ReadOnlyFolderInformation extends FolderInformation {
  readOnly: true;
}

export type FolderContent = FileInformation | FolderInformation;

export interface ApplicationProfileDefinition {
  definition: Dataset;
  displayName: string;
}

export type ApplicationProfilesPaged = {
  applicationProfiles: ApplicationProfileDto[];
  allResultsLoaded: boolean;
};

export type MetadataManagerViews = "Metadata" | "Extracted";

export type AdaptTable = Parameters<
  NonNullable<ReturnType<typeof BTable<FolderContent>>["__ctx"]>["expose"]
>[0];
