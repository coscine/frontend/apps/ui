import type {
  FileTreeDto,
  MetadataTreeDto,
} from "@coscine/api-client/dist/types/Coscine.Api";

export const getMetadataTreeResponse: MetadataTreeDto[] = [
  {
    version: "1693212042",
    availableVersions: ["1693212042"],
    definition: {
      content:
        '@base <https://purl.org/coscine/resources/4103cbea-ffa3-40a5-9e5c-b99cc16f0007/folder_1/folder_2/A.txt/@type=metadata&version=1693212042>.\r\n\r\n@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.\r\n@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.\r\n@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.\r\n@prefix ns2: <http://purl.org/dc/terms/>.\r\n\r\n_:b8477997 ns2:created "2023-08-15"^^xsd:date;\r\n ns2:creator "Petar Hristov";\r\n ns2:title "Title inside Form Generator";\r\n a <https://purl.org/coscine/ap/base/>.\r\n',
      type: "text/turtle",
    },
    path: "folder_1/folder_2/A.txt",
    type: "Leaf",
  },
  {
    version: "1692777419",
    availableVersions: ["1692777419"],
    definition: {
      content:
        '@base <https://purl.org/coscine/resources/4103cbea-ffa3-40a5-9e5c-b99cc16f0007/file_1.txt/@type=metadata&version=1692777419>.\r\n\r\n@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.\r\n@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.\r\n@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.\r\n@prefix ns2: <http://purl.org/dc/terms/>.\r\n\r\n_:b8425359 ns2:created "2023-08-18"^^xsd:date;\r\n ns2:creator "Petar Hristov";\r\n ns2:title "file_1";\r\n a <https://purl.org/coscine/ap/base/>.\r\n',
      type: "text/turtle",
    },
    path: "file_1.txt",
    type: "Leaf",
  },
  {
    version: "1692779210",
    availableVersions: [
      "1692340745",
      "1692340906",
      "1692340959",
      "1692341031",
      "1692779210",
    ],
    definition: {
      content:
        '@base <https://purl.org/coscine/resources/4103cbea-ffa3-40a5-9e5c-b99cc16f0007/file_0.txt/@type=metadata&version=1692779210>.\r\n\r\n@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.\r\n@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.\r\n@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.\r\n@prefix ns2: <http://purl.org/dc/terms/>.\r\n\r\n_:b8425361 ns2:created "2023-08-18"^^xsd:date;\r\n ns2:creator "Petar Hristov";\r\n ns2:title "Revised";\r\n a <https://purl.org/coscine/ap/base/>.\r\n',
      type: "text/turtle",
    },
    path: "file_0.txt",
    type: "Leaf",
  },
  {
    version: "1693209938",
    availableVersions: ["1693209938"],
    definition: {
      content:
        '@base <https://purl.org/coscine/resources/4103cbea-ffa3-40a5-9e5c-b99cc16f0007/my_folder/file_of_folder.txt/@type=metadata&version=1693209938>.\r\n\r\n@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.\r\n@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.\r\n@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.\r\n@prefix ns2: <http://purl.org/dc/terms/>.\r\n\r\n_:b8477996 ns2:created "2023-08-15"^^xsd:date;\r\n ns2:creator "Petar Hristov";\r\n ns2:title "From Insomnia";\r\n a <https://purl.org/coscine/ap/base/>.\r\n',
      type: "text/turtle",
    },
    path: "my_folder/file_of_folder.txt",
    type: "Leaf",
  },
];
export const getFileTreeResponse: FileTreeDto[] = [
  {
    directory: "",
    name: "folder_1",
    size: 0,
    creationDate: "2023-09-01T16:57:48.2525218+02:00",
    changeDate: "2023-09-01T16:57:48.2525225+02:00",
    path: "folder_1/",
    type: "Tree",
  },
  {
    directory: "",
    name: "my_folder",
    size: 0,
    creationDate: "2023-09-01T16:57:48.2525245+02:00",
    changeDate: "2023-09-01T16:57:48.2525249+02:00",
    path: "my_folder/",
    type: "Tree",
  },
  {
    directory: "",
    name: "file_0.txt",
    extension: "txt",
    size: 2513352,
    creationDate: "2023-08-23T10:27:20.062+02:00",
    changeDate: "2023-08-23T10:27:20.062+02:00",
    path: "file_0.txt",
    type: "Leaf",
  },
];
