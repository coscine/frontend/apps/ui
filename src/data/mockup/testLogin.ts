import { type LoginState } from "@/modules/login/types";
import { useLocalStorage } from "@vueuse/core";
import * as jose from "jose";

const secret = jose.base64url.decode(
  "zH4NRP1HMALxxCFnRZABFA7GOJtzU_gIj02alfL1lvI",
);

export const getTestLoginState: () => Promise<LoginState> = async () => {
  return {
    authorization: {
      bearer: ref(
        await new jose.EncryptJWT({ "urn:example:claim": true })
          .setProtectedHeader({ alg: "dir", enc: "A128CBC-HS256" })
          .setIssuedAt()
          .setIssuer("coscine")
          .setAudience("coscine")
          .setExpirationTime("2h")
          .encrypt(secret),
      ),
      expirationTimestamp: {
        token: null,
        cookie: null,
      },
    },
    currentTosVersion: { version: "1", isCurrent: true },
    loginStoredData: useLocalStorage("coscine.login.storedData", ""),
    loginUrls: {
      orcidUrl: "http://example.org",
      identityProviders: [{ id: "http://example.org" }],
    },
  };
};
