import type { SearchState } from "@/modules/search/types";
import type { SearchResultDto } from "@coscine/api-client/dist/types/Coscine.Api";

export const testSearchState: SearchState = {
  searchResults: [
    {
      uri: "https://purl.org/coscine/resources/110f8964-62f6-428d-be44-007f1039306c/file_0.txt/",
      type: "https://purl.org/coscine/terms/structure#Metadata",
      source: {
        isPublic: false,
        date_created: "2023-05-22",
        date_created_year: "2023",
        date_created_month: "May",
        date_created_day: "22",
        creator: "Petar Hristov",
        title: "files",
        belongsToProject:
          "https://purl.org/coscine/projects/48bcc699-87db-4863-be70-53272a8e3fd2",
        absolutefilename: "file_0.txt",
        fileName: "file_0.txt",
        graphName:
          "https://purl.org/coscine/resources/110f8964-62f6-428d-be44-007f1039306c/file_0.txt/",
        homepage:
          "https://coscine.rwth-aachen.de/p/sprint-20/r/110f8964-62f6-428d-be44-007f1039306c/#/file_0.txt",
        version: "1684754761",
        structureType: "https://purl.org/coscine/terms/structure#Metadata",
      },
    },
    {
      uri: "https://purl.org/coscine/resources/4103cbea-ffa3-40a5-9e5c-b99cc16f0007/1.txt/",
      type: "https://purl.org/coscine/terms/structure#Metadata",
      source: {
        isPublic: false,
        date_created: "2023-08-18",
        date_created_year: "2023",
        date_created_month: "August",
        date_created_day: "18",
        creator: "Petar Hristov",
        title: "Test",
        belongsToProject:
          "https://purl.org/coscine/projects/a93180bd-4e88-4115-8cf5-2faf6fa9063d",
        absolutefilename: "1.txt",
        fileName: "1.txt",
        graphName:
          "https://purl.org/coscine/resources/4103cbea-ffa3-40a5-9e5c-b99cc16f0007/1.txt/",
        homepage:
          "https://coscine.rwth-aachen.de/p/tree--blob/r/4103cbea-ffa3-40a5-9e5c-b99cc16f0007/#/1.txt",
        version: "1694090861",
        structureType: "https://purl.org/coscine/terms/structure#Metadata",
      },
    },
    {
      uri: "https://purl.org/coscine/resources/4103cbea-ffa3-40a5-9e5c-b99cc16f0007/file_1.txt/",
      type: "https://purl.org/coscine/terms/structure#Metadata",
      source: {
        isPublic: false,
        date_created: "2023-08-18",
        date_created_year: "2023",
        date_created_month: "August",
        date_created_day: "18",
        creator: "Petar Hristov",
        title: "file_1",
        belongsToProject:
          "https://purl.org/coscine/projects/a93180bd-4e88-4115-8cf5-2faf6fa9063d",
        absolutefilename: "file_1.txt",
        fileName: "file_1.txt",
        graphName:
          "https://purl.org/coscine/resources/4103cbea-ffa3-40a5-9e5c-b99cc16f0007/file_1.txt/",
        homepage:
          "https://coscine.rwth-aachen.de/p/tree--blob/r/4103cbea-ffa3-40a5-9e5c-b99cc16f0007/#/file_1.txt",
        version: "1692777419",
        structureType: "https://purl.org/coscine/terms/structure#Metadata",
      },
    },
    {
      uri: "https://purl.org/coscine/resources/9e01402a-571c-4867-816b-ff604817ec4f/Organizations/1.txt/",
      type: "https://purl.org/coscine/terms/structure#Metadata",
      source: {
        isPublic: false,
        date_created: "2023-09-22",
        date_created_year: "2023",
        date_created_month: "September",
        date_created_day: "22",
        creator: "Petar Hristov8",
        subject:
          "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=207",
        title: "ihihpuihphiu",
        belongsToProject:
          "https://purl.org/coscine/projects/a93180bd-4e88-4115-8cf5-2faf6fa9063d",
        absolutefilename: "Organizations/1.txt",
        fileName: "1.txt",
        graphName:
          "https://purl.org/coscine/resources/9e01402a-571c-4867-816b-ff604817ec4f/Organizations/1.txt/",
        homepage:
          "https://coscine.rwth-aachen.de/p/tree--blob/r/9e01402a-571c-4867-816b-ff604817ec4f/#/Organizations/1.txt",
        version: "1693986610",
        structureType: "https://purl.org/coscine/terms/structure#Metadata",
      },
    },
    {
      uri: "https://purl.org/coscine/resources/9e01402a-571c-4867-816b-ff604817ec4f/Organizations/Coscine remarks for APIv2.txt/",
      type: "https://purl.org/coscine/terms/structure#Metadata",
      source: {
        isPublic: false,
        date_created: "2023-09-05",
        date_created_year: "2023",
        date_created_month: "September",
        date_created_day: "5",
        creator: "Petar Hristov",
        title: "34243432243",
        belongsToProject:
          "https://purl.org/coscine/projects/a93180bd-4e88-4115-8cf5-2faf6fa9063d",
        absolutefilename: "Organizations/Coscine remarks for APIv2.txt",
        fileName: "Coscine remarks for APIv2.txt",
        graphName:
          "https://purl.org/coscine/resources/9e01402a-571c-4867-816b-ff604817ec4f/Organizations/Coscine remarks for APIv2.txt/",
        homepage:
          "https://coscine.rwth-aachen.de/p/tree--blob/r/9e01402a-571c-4867-816b-ff604817ec4f/#/Organizations/Coscine remarks for APIv2.txt",
        version: "1693986932",
        structureType: "https://purl.org/coscine/terms/structure#Metadata",
      },
    },
    {
      uri: "https://purl.org/coscine/resources/110f8964-62f6-428d-be44-007f1039306c",
      type: "https://purl.org/coscine/terms/structure#Resource",
      source: {
        deleted: false,
        archived: false,
        isPublic: false,
        date_created: "16.05.2023 12:23:33",
        date_created_year: "2023",
        date_created_month: "May",
        date_created_day: "16",
        catalog:
          "https://purl.org/coscine/resources/110f8964-62f6-428d-be44-007f1039306c/file_0.txt",
        deleted_written: "deleted false",
        alternative_title: "RDS S3",
        conforms_to: "https://purl.org/coscine/ap/base/",
        creator:
          "https://purl.org/coscine/users/0b60e2e3-5062-44b5-8f44-0e3cb5f292cb",
        description: "test",
        rights: "",
        title: "RDS S3",
        homepage:
          "https://hdl.handle.net/11148/110f8964-62f6-428d-be44-007f1039306c",
        service:
          "https://purl.org/coscine/resourcetypes/92edf4f2-0e9e-4f09-a2dd-a313de25227d",
        visibility: "https://purl.org/coscine/terms/visibility#projectMember",
        fixedvalues: "{}",
        archived_written: "archived false",
        agentgroup:
          "https://purl.org/coscine/projects/48bcc699-87db-4863-be70-53272a8e3fd2",
        accessto:
          "https://purl.org/coscine/resources/110f8964-62f6-428d-be44-007f1039306c",
        default:
          "https://purl.org/coscine/resources/110f8964-62f6-428d-be44-007f1039306c",
        mode: "http://www.w3.org/ns/auth/acl#Read",
        belongsToProject:
          "https://purl.org/coscine/projects/48bcc699-87db-4863-be70-53272a8e3fd2",
        graphName:
          "https://purl.org/coscine/resources/110f8964-62f6-428d-be44-007f1039306c",
        structureType: "https://purl.org/coscine/terms/structure#Resource",
      },
    },
    {
      uri: "https://purl.org/coscine/resources/4103cbea-ffa3-40a5-9e5c-b99cc16f0007",
      type: "https://purl.org/coscine/terms/structure#Resource",
      source: {
        deleted: false,
        archived: false,
        isPublic: false,
        date_created: "18.08.2023 06:37:07",
        date_created_year: "2023",
        date_created_month: "August",
        date_created_day: "18",
        catalog:
          "https://purl.org/coscine/resources/4103cbea-ffa3-40a5-9e5c-b99cc16f0007/1.txt",
        deleted_written: "deleted false",
        alternative_title: "RWTH RDS S3",
        conforms_to: "https://purl.org/coscine/ap/base/",
        creator:
          "https://purl.org/coscine/users/0b60e2e3-5062-44b5-8f44-0e3cb5f292cb",
        description: "S3",
        rights: "",
        title: "RWTH RDS S3",
        homepage:
          "https://hdl.handle.net/11148/4103cbea-ffa3-40a5-9e5c-b99cc16f0007",
        service:
          "https://purl.org/coscine/resourcetypes/92edf4f2-0e9e-4f09-a2dd-a313de25227d",
        visibility: "https://purl.org/coscine/terms/visibility#projectMember",
        fixedvalues:
          '{"http://purl.org/dc/terms/creator":{"https://purl.org/coscine/invisible":[{"value":"0","type":"literal","datatype":null}],"https://purl.org/coscine/defaultValue":[{"value":"{ME}","type":"literal","datatype":"http://www.w3.org/2001/XMLSchema#string"}]},"http://purl.org/dc/terms/created":{"https://purl.org/coscine/invisible":[{"value":"0","type":"literal","datatype":null}],"https://purl.org/coscine/defaultValue":[{"value":"2023-08-18","type":"literal","datatype":"http://www.w3.org/2001/XMLSchema#date"}]}}',
        archived_written: "archived false",
        agentgroup:
          "https://purl.org/coscine/projects/a93180bd-4e88-4115-8cf5-2faf6fa9063d",
        accessto:
          "https://purl.org/coscine/resources/4103cbea-ffa3-40a5-9e5c-b99cc16f0007",
        default:
          "https://purl.org/coscine/resources/4103cbea-ffa3-40a5-9e5c-b99cc16f0007",
        mode: "http://www.w3.org/ns/auth/acl#Read",
        belongsToProject:
          "https://purl.org/coscine/projects/a93180bd-4e88-4115-8cf5-2faf6fa9063d",
        graphName:
          "https://purl.org/coscine/resources/4103cbea-ffa3-40a5-9e5c-b99cc16f0007",
        structureType: "https://purl.org/coscine/terms/structure#Resource",
      },
    },
  ] as SearchResultDto[],
  pagination: {
    CurrentPage: 1,
    TotalPages: 1,
    PageSize: 10,
    TotalCount: 7,
    HasPrevious: false,
    HasNext: false,
  },
  categories: [
    { Name: "None", Count: 7 },
    { Name: "Resource", Count: 2 },
    { Name: "Project", Count: 0 },
    { Name: "Metadata", Count: 5 },
  ],
};
