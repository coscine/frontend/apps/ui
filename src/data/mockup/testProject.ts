import {
  type VisitedProjectDto,
  type ProjectState,
} from "@/modules/project/types";
import type { RoleDto } from "@coscine/api-client/dist/types/Coscine.Api";
import {
  testDiscipline,
  testOrganizationFromShibboleth,
  getTestShibbolethUser,
} from "./testUser";
import { testResourceType } from "./testResource";

export const testOwnerRole: RoleDto = {
  id: "ownerRole",
  displayName: "Owner",
  description: "owner",
};
export const testMemberRole: RoleDto = {
  id: "memberRole",
  displayName: "Member",
  description: "member",
};
export const testGuestRole: RoleDto = {
  id: "guestRole",
  displayName: "Guest",
  description: "guest",
};
export const testSlug = "testProject";

export const testProject: VisitedProjectDto = {
  id: "987654321",
  displayName: "Test Project",
  name: "Test Project Full",
  description: "Test Project Description",
  principleInvestigators: "Test PI",
  startDate: "2023-01-01",
  endDate: "2033-01-01",
  disciplines: [testDiscipline],
  organizations: [{ ...testOrganizationFromShibboleth, responsible: true }],
  visibility: { id: "1234", displayName: "Project Members" },
  slug: testSlug,
  invitations: [],
  quotas: [
    {
      allocated: {
        value: 24,
        unit: "https://qudt.org/vocab/unit/GibiBYTE",
      },
      maximum: {
        value: 30,
        unit: "https://qudt.org/vocab/unit/GibiBYTE",
      },
      projectId: "987654321",
      resourceQuotas: [
        {
          resource: {
            id: "eeb8d803-46a1-49ba-a47c-81cd4f49cd65",
          },
          reserved: {
            value: 24,
            unit: "https://qudt.org/vocab/unit/GibiBYTE",
          },
          used: {
            value: 3.6323323523,
            unit: "https://qudt.org/vocab/unit/GibiBYTE",
          },
          usedPercentage: 15.134718134,
        },
      ],
      totalReserved: {
        value: 24,
        unit: "https://qudt.org/vocab/unit/GibiBYTE",
      },
      totalUsed: {
        value: 3.6323323523,
        unit: "https://qudt.org/vocab/unit/GibiBYTE",
      },
      resourceType: {
        id: testResourceType.id,
      },
    },
  ],
  resources: [
    {
      id: "eeb8d803-46a1-49ba-a47c-81cd4f49cd65",
      displayName: "Archived Test Resource",
      type: {
        id: "123497",
      },
      archived: true,
    },
  ],
  roles: [
    {
      project: { id: "987654321" },
      role: testOwnerRole,
      user: getTestShibbolethUser(),
    },
  ], // TODO: Beware the object types!
  subProjects: [],
  creator: { id: getTestShibbolethUser().id },
};

export const testProjectGuest: VisitedProjectDto = {
  id: "987654321",
  displayName: "Test Project",
  name: "Test Project Full",
  description: "Test Project Description",
  principleInvestigators: "Test PI",
  startDate: "2023-01-01",
  endDate: "2033-01-01",
  disciplines: [testDiscipline],
  organizations: [{ ...testOrganizationFromShibboleth, responsible: true }],
  visibility: { id: "1234", displayName: "Project Members" },
  slug: testSlug,
  invitations: [],
  quotas: [
    {
      allocated: {
        value: 24,
        unit: "https://qudt.org/vocab/unit/GibiBYTE",
      },
      maximum: {
        value: 30,
        unit: "https://qudt.org/vocab/unit/GibiBYTE",
      },
      projectId: "987654321",
      resourceQuotas: [
        {
          resource: {
            id: "eeb8d803-46a1-49ba-a47c-81cd4f49cd65",
          },
          reserved: {
            value: 24,
            unit: "https://qudt.org/vocab/unit/GibiBYTE",
          },
          used: {
            value: 3.6323323523,
            unit: "https://qudt.org/vocab/unit/GibiBYTE",
          },
          usedPercentage: 15.134718134,
        },
      ],
      totalReserved: {
        value: 24,
        unit: "https://qudt.org/vocab/unit/GibiBYTE",
      },
      totalUsed: {
        value: 3.6323323523,
        unit: "https://qudt.org/vocab/unit/GibiBYTE",
      },
      resourceType: {
        id: testResourceType.id,
      },
    },
  ],
  resources: [
    {
      id: "eeb8d803-46a1-49ba-a47c-81cd4f49cd65",
      displayName: "Archived Test Resource",
      type: {
        id: "123497",
      },
      archived: true,
    },
  ],
  roles: [
    {
      project: { id: "987654321" },
      role: testGuestRole,
      user: getTestShibbolethUser(),
    },
  ], // TODO: Beware the object types!
  subProjects: [],
  creator: { id: getTestShibbolethUser().id },
};

export const testProjectState: ProjectState = {
  allProjects: [testProject],
  currentSlug: testSlug,
  disciplines: [testDiscipline],
  licenses: [{ id: "452545", displayName: "TestLicense" }],
  organizations: [{ ...testOrganizationFromShibboleth, responsible: true }],
  publicationAdvisoryServices: null,
  organizationsComplete: true,
  organizationsFilter: "",
  roles: [testOwnerRole, testMemberRole, testGuestRole],
  topLevelProjects: [testProject],
  visibilities: [
    { id: "1234", displayName: "Project Members" },
    { id: "123234324", displayName: "Public" },
  ],
  visitedProjects: {
    testProject: testProject,
  },
};
export const testProjectStateGuest: ProjectState = {
  allProjects: [testProjectGuest],
  currentSlug: testSlug,
  disciplines: [testDiscipline],
  licenses: [{ id: "452545", displayName: "TestLicense" }],
  organizations: [{ ...testOrganizationFromShibboleth, responsible: true }],
  publicationAdvisoryServices: null,
  organizationsComplete: true,
  organizationsFilter: "",
  roles: [testGuestRole],
  topLevelProjects: [testProjectGuest],
  visibilities: [
    { id: "1234", displayName: "Project Members" },
    { id: "123234324", displayName: "Public" },
  ],
  visitedProjects: {
    testProject: testProjectGuest,
  },
};
