import { type UserState } from "@/modules/user/types";
import type {
  DisciplineDto,
  UserDto,
  UserOrganizationDto,
  IdentityProviderDto,
} from "@coscine/api-client/dist/types/Coscine.Api";

export const testOrganizationFromShibboleth: UserOrganizationDto = {
  displayName: "Test SSO Organization",
  readOnly: true, // Shibboleth organizations are always read-only
  uri: "example.com",
};

export const testOrganizationFromOrcid: UserOrganizationDto = {
  displayName: "Test ORCiD Organization",
  readOnly: true, // ORCiD organizations are never read-only
  uri: "example.com",
};

export const testDiscipline: DisciplineDto = {
  id: "1",
  displayNameDe: "Test",
  displayNameEn: "Test",
  uri: "example.com",
};

export const OrcidProvider: IdentityProviderDto = {
  id: "f383f83c-26cb-47eb-80c8-639ed05bd0de",
  displayName: "ORCiD",
};

export const NFDI4Ing_AAIProvider: IdentityProviderDto = {
  id: "3a1e7185-0328-47ed-afd7-a4ffd7f03a79",
  displayName: "NFDI4Ing AAI",
};

export const testLanguage = { id: "1", displayName: "en" };

/**
 * Generates a mock Shibboleth user data object for testing purposes.
 *
 * @returns {UserDto} A mock Shibboleth user data object.
 */
export const getTestShibbolethUser: () => UserDto = (): UserDto => {
  return {
    id: "d302cb44-c934-4b54-a581-9765cab96fca",
    givenName: "Coscine",
    familyName: "Example",
    displayName: "Coscine Example",
    emails: [
      { email: "example@university.com", isConfirmed: true, isPrimary: true },
    ],
    disciplines: [testDiscipline],
    language: testLanguage,
    organizations: [testOrganizationFromShibboleth],
    areToSAccepted: true,
  };
};

/**
 * Generates a mock ORCID user data object for testing purposes.
 *
 * @returns {UserDto} A mock ORCID user data object.
 */
export const getTestOrcidUser: () => UserDto = () => {
  return {
    id: "d302cb44-c934-4b54-a581-9765cab96fca",
    givenName: "Coscine",
    familyName: "Example",
    displayName: "Coscine Example",
    emails: [
      { email: "example@orcid.com", isConfirmed: true, isPrimary: true },
    ],
    disciplines: [testDiscipline],
    language: testLanguage,
    organizations: [testOrganizationFromOrcid],
  };
};

export const getTestShibbolethUserState: () => UserState = () => {
  return {
    userProfile: {
      disciplines: [testDiscipline],
      languages: [testLanguage, { id: "2", displayName: "de" }],
      organizations: [testOrganizationFromShibboleth],
      titles: [
        {
          id: "1",
          displayName: "Prof.",
        },
        {
          id: "2",
          displayName: "Dr.",
        },
      ],
      tokens: null,
    },
    user: getTestShibbolethUser(),
    isRetrievingUser: false,
  };
};
export const getTestShibbolethUserWithoutLanguageState: () => UserState =
  () => {
    return {
      userProfile: {
        disciplines: [testDiscipline],
        languages: [],
        organizations: [testOrganizationFromShibboleth],
        titles: [
          {
            id: "1",
            displayName: "Prof.",
          },
          {
            id: "2",
            displayName: "Dr.",
          },
        ],
        tokens: null,
      },
      user: {
        ...getTestShibbolethUser(),
        language: null,
      },
    };
  };

export const getTestOrcidUserState: () => UserState = () => {
  return {
    userProfile: {
      disciplines: [testDiscipline],
      languages: [testLanguage, { id: "2", displayName: "de" }],
      organizations: [testOrganizationFromShibboleth],
      titles: [
        {
          id: "1",
          displayName: "Prof.",
        },
        {
          id: "2",
          displayName: "Dr.",
        },
      ],
      tokens: null,
    },
    user: getTestOrcidUser(),
    isRetrievingUser: false,
  };
};
