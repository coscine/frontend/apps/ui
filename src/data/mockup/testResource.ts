import type {
  VisitedResourceObject,
  ResourceState,
} from "@/modules/resource/types";
import { parseRDFDefinition } from "@/modules/resource/utils/linkedData";
import {
  baseApplicationProfile,
  baseApplicationProfileFormat,
} from "./metadata/applicationProfile";
import { radarFixedValues } from "./metadata/fixedValues";
import { testDiscipline, getTestShibbolethUser } from "./testUser";
import type {
  ResourceTypeInformationDto,
  ResourceTypeStatus,
  UserMinimalDto,
} from "@coscine/api-client/dist/types/Coscine.Api";

export const testResourceType: ResourceTypeInformationDto = {
  specificType: "rds",
  generalType: "rds",
  id: "123497",
  canCreate: true,
  canCreateLinks: true,
  canDelete: true,
  canDeleteResource: true,
  canList: true,
  canRead: true,
  canSetResourceReadonly: true,
  canUpdate: true,
  canUpdateResource: true,
  isArchived: false,
  isEnabled: true,
  isQuotaAdjustable: true,
  isQuotaAvailable: true,
  resourceContent: {
    entriesView: { columns: { always: new Set<string>() } },
    metadataView: { editableDataUrl: false, editableKey: false },
    readOnly: false,
  },
  resourceCreation: {
    components: [],
  },
  status: "active" as ResourceTypeStatus,
};

export const getTestResource: () => Promise<VisitedResourceObject> =
  async () => {
    const apUrl = "https://purl.org/coscine/ap/base/";
    const ap = await parseRDFDefinition(
      baseApplicationProfile,
      baseApplicationProfileFormat,
      apUrl,
    );
    const resourceObject: VisitedResourceObject = {
      applicationProfile: { uri: apUrl },
      archived: true,
      creator: { id: getTestShibbolethUser().id } as UserMinimalDto,
      dateCreated: null,
      description: "TestResource",
      disciplines: [testDiscipline],
      displayName: "TestResource",
      fixedValues: radarFixedValues,
      fullApplicationProfile: ap,
      id: "eeb8d803-46a1-49ba-a47c-81cd4f49cd65",
      keywords: [],
      license: undefined,
      pid: "21.11102/eeb8d803-46a1-49ba-a47c-81cd4f49cd65",
      rawApplicationProfile: ap,
      name: "TestResource",
      storedColumns: null,
      type: {
        generalType: testResourceType.specificType ?? undefined,
        specificType: testResourceType.specificType ?? undefined,
        id: testResourceType.id,
        options: {
          gitLab: {},
        },
      },
      usageRights: "",
      quota: null,
      visibility: { id: "1234", displayName: "Project Members" },
    };
    return resourceObject;
  };

export const getTestResourceState: () => Promise<ResourceState> = async () => {
  const testResource = await getTestResource();
  const resourceState: ResourceState = {
    allResources: [testResource],
    classes: {},
    currentId: "eeb8d803-46a1-49ba-a47c-81cd4f49cd65",
    enabledResourceTypes: [testResourceType],
    resourceTypes: [testResourceType],
    visitedResources: {
      "eeb8d803-46a1-49ba-a47c-81cd4f49cd65": testResource,
    },
  };
  return resourceState;
};
