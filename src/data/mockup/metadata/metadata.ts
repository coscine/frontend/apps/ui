export const radarMetadata = `<https://hdl.handle.net/21.11102/eeb8d803-46a1-49ba-a47c-81cd4f49cd65@path=%2FTest> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <https://purl.org/coscine/ap/radar/> .
<https://hdl.handle.net/21.11102/eeb8d803-46a1-49ba-a47c-81cd4f49cd65@path=%2FTest> <http://purl.org/dc/terms/created> "2021-12-09"^^<http://www.w3.org/2001/XMLSchema#date> .
<https://hdl.handle.net/21.11102/eeb8d803-46a1-49ba-a47c-81cd4f49cd65@path=%2FTest> <http://purl.org/dc/terms/creator> "Benedikt Heinrichs"^^<http://www.w3.org/2001/XMLSchema#string> .
<https://hdl.handle.net/21.11102/eeb8d803-46a1-49ba-a47c-81cd4f49cd65@path=%2FTest> <http://purl.org/dc/terms/title> "Test"^^<http://www.w3.org/2001/XMLSchema#string> .
`;

export const extractedMetadata = `@prefix dcat: <http://www.w3.org/ns/dcat#> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix ebucore: <http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#> .
@prefix exif: <http://www.w3.org/2003/12/exif/ns#> .
@prefix image: <https://purl.org/coscine/ontologies/image#> .
@prefix tika: <https://purl.org/coscine/ontologies/tika/> .
@prefix topic: <https://purl.org/coscine/ontologies/topic#> .

<https://purl.org/coscine/resources/activity_plot.png> a dcat:Catalog,
        dcat:Distribution ;
    dcterms:created "2024-06-03 13:30:17.079892" ;
    dcterms:format "image/png" ;
    dcterms:identifier "activity_plot.png" ;
    dcterms:modified "2024-06-03 13:30:17" ;
    ebucore:hasFormat "PNG" ;
    ebucore:height "500" ;
    ebucore:width "1000" ;
    exif:BitsPerSample "8 8 8 8" ;
    exif:ImageLength "500" ;
    exif:ImageWidth "1000" ;
    dcat:byteSize "70229" ;
    dcat:mediaType "image/png" ;
    image:frameNumber "1" ;
    image:isAnimated "False" ;
    image:mode "RGBA" ;
    tika:Chroma_BlackIsZero "true" ;
    tika:Chroma_ColorSpaceType "RGB" ;
    tika:Chroma_NumChannels "4" ;
    tika:Compression_CompressionTypeName "deflate" ;
    tika:Compression_Lossless "true" ;
    tika:Compression_NumProgressiveScans "1" ;
    tika:Content_Type_Parser_Override "image/ocr-png" ;
    tika:Data_BitsPerSample "8 8 8 8" ;
    tika:Data_PlanarConfiguration "PixelInterleaved" ;
    tika:Data_SampleFormat "UnsignedIntegral" ;
    tika:Dimension_HorizontalPixelSize "0.2540005" ;
    tika:Dimension_ImageOrientation "Normal" ;
    tika:Dimension_PixelAspectRatio "1.0" ;
    tika:Dimension_VerticalPixelSize "0.2540005" ;
    tika:IHDR "width=1000, height=500, bitDepth=8, colorType=RGBAlpha, compressionMethod=deflate, filterMethod=adaptive, interlaceMethod=none" ;
    tika:Text_TextEntry "keyword=Software, value=Matplotlib version3.8.1, https://matplotlib.org/, encoding=ISO-8859-1, compression=none" ;
    tika:Transparency_Alpha "nonpremultipled" ;
    tika:X_TIKA_Parsed_By "org.apache.tika.parser.DefaultParser",
        "org.apache.tika.parser.image.ImageParser",
        "org.apache.tika.parser.ocr.TesseractOCRParser" ;
    tika:X_TIKA_Parsed_By_Full_Set "org.apache.tika.parser.DefaultParser",
        "org.apache.tika.parser.image.ImageParser",
        "org.apache.tika.parser.ocr.TesseractOCRParser" ;
    tika:height "500" ;
    tika:pHYs "pixelsPerUnitXAxis=3937, pixelsPerUnitYAxis=3937, unitSpecifier=meter" ;
    tika:tEXt_tEXtEntry "keyword=Software, value=Matplotlib version3.8.1, https://matplotlib.org/" ;
    tika:width "1000" ;
    topic:about <https://purl.org/coscine/ontologies/topic/attributes#100>,
        <https://purl.org/coscine/ontologies/topic/attributes#20>,
        <https://purl.org/coscine/ontologies/topic/attributes#60>,
        <https://purl.org/coscine/ontologies/topic/attributes#80>,
        <https://purl.org/coscine/ontologies/topic/attributes#activity>,
        <https://purl.org/coscine/ontologies/topic/attributes#commit>,
        <https://purl.org/coscine/ontologies/topic/attributes#count>,
        <https://purl.org/coscine/ontologies/topic/attributes#counts>,
        <https://purl.org/coscine/ontologies/topic/attributes#date>,
        <https://purl.org/coscine/ontologies/topic/attributes#day>,
        <https://purl.org/coscine/ontologies/topic/attributes#latestactivity>,
        <https://purl.org/coscine/ontologies/topic/attributes#matching>,
        <https://purl.org/coscine/ontologies/topic/attributes#na>,
        <https://purl.org/coscine/ontologies/topic/attributes#number>,
        <https://purl.org/coscine/ontologies/topic/attributes#objects>,
        <https://purl.org/coscine/ontologies/topic/attributes#sa>,
        <https://purl.org/coscine/ontologies/topic/attributes#ss> .`;
