export const radarFixedValues = {
  "http://purl.org/dc/terms/created": {
    "https://purl.org/coscine/invisible": [
      {
        value: "0",
        type: "literal",
      },
    ],
  },
  "http://purl.org/dc/terms/creator": {
    "https://purl.org/coscine/defaultValue": [
      {
        value: "{ME}",
        type: "literal",
      },
    ],
    "https://purl.org/coscine/invisible": [
      {
        value: "0",
        type: "literal",
      },
    ],
  },
  "http://purl.org/dc/terms/rights": {
    "https://purl.org/coscine/invisible": [
      {
        value: "0",
        type: "literal",
      },
    ],
  },
  "http://purl.org/dc/terms/rightsHolder": {
    "https://purl.org/coscine/invisible": [
      {
        value: "0",
        type: "literal",
      },
    ],
  },
  "http://purl.org/dc/terms/subject": {
    "https://purl.org/coscine/invisible": [
      {
        value: "0",
        type: "literal",
      },
    ],
  },
  "http://purl.org/dc/terms/title": {
    "https://purl.org/coscine/invisible": [
      {
        value: "0",
        type: "literal",
      },
    ],
  },
  "http://purl.org/dc/terms/type": {
    "https://purl.org/coscine/invisible": [
      {
        value: "0",
        type: "literal",
      },
    ],
  },
};
