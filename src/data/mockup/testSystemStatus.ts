import moment from "moment";
import { SystemStatusState } from "@/store/types";
import type { MessageDto } from "@coscine/api-client/dist/types/Coscine.Api";

export const getNocMessagesResponse: MessageDto[] = [
  {
    id: "noc-5555",
    href: "https://example.com/noc-5555",
    type: "Disturbance",
    startDate: moment().subtract(1, "days").toISOString(), // yesterday
    endDate: moment().add(1, "days").toISOString(), // tomorrow
  },
  {
    id: "noc-5572",
    href: "https://example.com/noc-5572",
    type: "Maintenance",
    startDate: moment().subtract(1, "days").toISOString(), // yesterday
    endDate: moment().add(5, "hours").toISOString(), // in 5 hours
  },
];

export const getInternalMessagesResponse: MessageDto[] = [
  {
    id: "int-6551c5a7ba1a3870450bb960f93ba912f175bc2a31992bc776413c4cb542f5b3",
    body: {
      de: "Das ist eine Testnachricht",
      en: "This is a test message",
    },
    type: "Information",
    startDate: moment().subtract(1, "days").toISOString(), // yesterday
    endDate: moment().add(1, "days").toISOString(), // tomorrow
  },
];

export const testSystemStatusState: SystemStatusState = {
  banner: {
    noc: {
      messages: getNocMessagesResponse,
      lastFetched: new Date(),
    },
    internal: {
      messages: getInternalMessagesResponse,
      lastFetched: new Date(),
    },
    hidden: useLocalStorage("coscine.system.status.banner.hidden", []),
  },
};
