import type { MainState, Theme } from "@/store/types";
import { useLocalStorage } from "@vueuse/core";
import { v4 as uuidv4 } from "uuid";

export const getTestMainState: () => Promise<MainState> = async () => {
  return {
    coscine: {
      clientcorrelation: {
        id: useLocalStorage("coscine.clientcorrelation.id", uuidv4()),
      },
      loading: {
        counter: 0,
      },
      locale: useLocalStorage("coscine.locale", "en"),
      theme: useLocalStorage<Theme>("coscine.theme", "light"),
    },
    sidebarActive: useLocalStorage("coscine.sidebar.active", true),
  };
};
