import useMainStore from "@/store";

import type { StoreDefinition } from "pinia";

export function loadingCounterEventHandler(store: StoreDefinition) {
  const mainStore = useMainStore();

  store().$onAction(
    ({
      after, // Hook after the action returns or resolves
      onError, // Hook if the action throws or rejects
    }) => {
      // Increment the loading counter
      mainStore.coscine.loading.counter++;

      // Decrease the loading counter
      after(() => {
        mainStore.coscine.loading.counter--;
      });

      // Decrease the loading counter
      onError(() => {
        mainStore.coscine.loading.counter--;
      });
    },
  );
}
