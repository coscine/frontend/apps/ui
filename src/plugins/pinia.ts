import { createPinia } from "pinia";

let pinia = createPinia();
if (typeof process !== "undefined" && process?.env?.MOCKUP) {
  pinia = (await import("@/plugins/mockupPinia")).default;
}

export default pinia;
