import { createTestingPinia } from "@pinia/testing";
import { testProjectState } from "@/data/mockup/testProject";
import { getTestShibbolethUserState } from "@/data/mockup/testUser";
import { getTestResourceState } from "@/data/mockup/testResource";
import { getTestLoginState } from "@/data/mockup/testLogin";
import { getTestMainState } from "@/data/mockup/testMain";
import { testSearchState } from "@/data/mockup/testSearch";

const pinia = createTestingPinia({
  createSpy: () => () => false,
  initialState: {
    main: await getTestMainState(),
    login: await getTestLoginState(),
    project: testProjectState,
    resource: await getTestResourceState(),
    search: testSearchState,
    user: getTestShibbolethUserState(),
  },
});

export default pinia;
