import { createI18n } from "vue-i18n";

import de from "@/i18n/de";
import en from "@/i18n/en";

export const def = {
  de: de,
  en: en,
};

const i18n = createI18n<[(typeof def)["en"]], "en" | "de">({
  locale: localStorage.getItem("coscine.locale")?.toString(),
  fallbackLocale: "en",
  messages: def,
  globalInjection: true,
  silentFallbackWarn: true,
  legacy: false,
});

export default i18n;
