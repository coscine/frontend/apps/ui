/* vue-dompurify-html */
import VueDOMPurifyHTML from "vue-dompurify-html";

export const config = {
  namedConfigurations: {
    a: {
      USE_PROFILES: { html: true },
      ADD_ATTR: ["target"],
    },
  },
};
export default VueDOMPurifyHTML;
