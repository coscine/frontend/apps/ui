import type VueRouter from "vue-router";

declare module "pinia" {
  export interface PiniaCustomProperties {
    // set typings for custom plugins
    router: VueRouter;
  }
}
