import type { Pagination } from "@coscine/api-client/dist/types/Coscine.Api";

type PagedResponse<T> = {
  data?: Array<T> | null;
  statusCode?: number | null;
  isSuccess?: boolean;
  pagination?: Pagination;
};

type DataResponse<T> = {
  data: PagedResponse<T>;
};

type ListRequestFunction<T> = (pageNumber: number) => Promise<DataResponse<T>>;

/**
 * This method receives a paged request and queries all pages
 * @param listRequest A function that gets a pageNumber as input and returns the paged request call
 * @returns The combined result of all pages
 */
export const wrapListRequest = async <T>(
  listRequest: ListRequestFunction<T>,
): Promise<Array<T>> => {
  const returnArray: Array<T> = [];
  let hasNext: boolean | undefined = true;
  let currentPageNumber = 1;
  while (hasNext) {
    const response = await listRequest(currentPageNumber);
    const iteration = response.data;
    if (!iteration.isSuccess || !iteration.data || !iteration.pagination) {
      console.error("Error in iteration!");
      break;
    }
    hasNext = iteration.pagination.hasNext;
    currentPageNumber++;
    returnArray.push(...iteration.data);
  }
  return returnArray;
};
