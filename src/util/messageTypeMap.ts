import type { MessageType } from "@coscine/api-client/dist/types/Coscine.Api";
import type { BaseColorVariant } from "bootstrap-vue-next";

export function mapMessageTypeToBannerVariant(
  messageType: MessageType | undefined,
): keyof BaseColorVariant | null | undefined {
  switch (messageType) {
    case "Disturbance":
    case "PartialDisturbance":
    case "Interruption":
      return "danger"; // Red color for serious issues

    case "Maintenance":
    case "PartialMaintenance":
    case "LimitedOperation":
      return "warning"; // Yellow color for maintenance and partial operations

    case "Change":
      return "primary"; // Blue color for changes

    case "Hint":
    case "Information":
      return "info"; // Light blue color for hints and information

    case "Warning":
      return "warning"; // Red color for warnings

    default:
      return "light"; // Fallback color for unknown types
  }
}
