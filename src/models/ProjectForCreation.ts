import { ProjectForCreationDto } from "@coscine/api-client/dist/types/Coscine.Api";
import { OrganizationForProjectManipulation } from "@/models/OrganizationForProjectManipulation";

export interface ProjectForCreation extends ProjectForCreationDto {
  /**
   * Sets if the owners from the parent project should be copyed.
   * @type {boolean}
   * @memberof ProjectForCreation
   */
  copyOwnersFromParent?: boolean;

  /**
   * Gets or initializes the organizations associated with the project.
   * @type {Array<OrganizationForProjectManipulation>}
   * @memberof ProjectForCreation
   */
  organizations: Array<OrganizationForProjectManipulation>;
}
