import { ProjectForUpdateDto } from "@coscine/api-client/dist/types/Coscine.Api";
import { OrganizationForProjectManipulation } from "@/models/OrganizationForProjectManipulation";

export interface ProjectForUpdate extends ProjectForUpdateDto {
  /**
   * Gets or initializes the organizations associated with the project.
   * @type {Array<OrganizationForProjectManipulation>}
   * @memberof ProjectForUpdate
   */
  organizations: Array<OrganizationForProjectManipulation>;
}
