import { OrganizationForProjectManipulationDto } from "@coscine/api-client/dist/types/Coscine.Api";

export interface OrganizationForProjectManipulation
  extends OrganizationForProjectManipulationDto {
  /**
   * The displayname of the Organisation.
   * @type {string}
   * @memberof OrganizationForProjectManipulation
   */
  displayName?: string;
}
