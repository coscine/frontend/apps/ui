import {
  createRouter,
  createWebHistory,
  type RouteRecordRaw,
  type RouteLocationNormalized,
} from "vue-router";

import { AdminRoutes } from "@/modules/admin/routes";
import { LoginRoutes } from "@/modules/login/routes";
import { PidRoutes } from "@/modules/pid/routes";
import { ProjectRoutes } from "@/modules/project/routes";
import { UserRoutes } from "@/modules/user/routes";
import { SearchRoutes } from "@/modules/search/routes";
import { ErrorRoutes } from "@/modules/error/routes";

export const routes: RouteRecordRaw[] = [
  // Module Routes
  ...AdminRoutes,
  ...LoginRoutes,
  ...PidRoutes,
  ...ProjectRoutes,
  ...UserRoutes,
  ...SearchRoutes,

  ...ErrorRoutes,
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});

export default router;

// Import the relevant stores
import useUserStore from "@/modules/user/store";
import useLoginStore from "@/modules/login/store";
import useSystemStatusStore from "@/store/systemStatus";
import i18n, { def } from "@/plugins/vue-i18n";
import type { DefaultLocaleMessageSchema, LocaleMessages } from "vue-i18n";
import type { ProjectDto } from "@coscine/api-client/dist/types/Coscine.Api";

router.beforeEach(async (to, _, next) => {
  // Replace the available locale messages
  const localeMessages:
    | LocaleMessages<DefaultLocaleMessageSchema, "en" | "de">
    | undefined = to.matched
      .map((e) => e.meta.i18n)
      .filter((i) => i)
      .pop();
  if (localeMessages) {
    i18n.global.availableLocales.forEach((locale) => {
      i18n.global.setLocaleMessage(locale, def[locale]); // default locale messages
      i18n.global.mergeLocaleMessage(locale, localeMessages[locale]); // append the locale messages for the component
    });
  }
  // Define the relevant stores
  const systemStatusStore = useSystemStatusStore();
  const loginStore = useLoginStore();
  const userStore = useUserStore();

  // Handle access token from URL
  loginStore.setAccessTokenFromRoute(router, to);

  // Set the expiration times on every navigation.
  // Refer to the SessionToast.md documentation for more information.
  // NOTE: At this point the required auth cookies must be already set by the API.
  loginStore.setBearerTokenExpiration();
  loginStore.setCookieExpiration();

  // Handle contact change token from URL
  await userStore.confirmUserEmail(to);
  // Collect current NOC information
  if (
    systemStatusStore.shouldFetchMessages(
      systemStatusStore.banner.noc.lastFetched,
    )
  ) {
    systemStatusStore.retrieveCurrentNocMessages(/*silent*/ true);
  }
  // Collect current internal information
  if (
    systemStatusStore.shouldFetchMessages(
      systemStatusStore.banner.internal.lastFetched,
    )
  ) {
    systemStatusStore.retrieveCurrentInternalMessages();
  }

  // Navigation Guard - Respect route auth requirement for not logged-out users
  if (to.meta?.requiresAuth && !loginStore.isLoggedIn) {
    // Route requires auth, check if logged in; if not, redirect to login page.
    next({
      name: "login",
      // Save the location we were at to come back later
      query: { redirect: to.fullPath },
    });
  }
  // Navigation Guard - Forward to ToS page for logged-in users, when ToS not accepted
  else if (
    to.name !== "tos" &&
    loginStore.isLoggedIn &&
    userStore.user?.areToSAccepted === false
  ) {
    next({ name: "tos" });
  }
  // Navigation Guard - Forward away from ToS page for logged-in users, when ToS accepted
  else if (
    to.name === "tos" &&
    loginStore.isLoggedIn &&
    userStore.user?.areToSAccepted
  ) {
    next({ name: "home" });
  }
  // Navigation Guard - Forward away from login page for logged-in users
  else if (to.name === "login" && loginStore.isLoggedIn) {
    next({ name: "home" });
  }
  // Navigation Guard - Set directory trail for resource page when no dirTrail is provided
  else if (to.name === "resource-page" && to.params.dirTrail === undefined) {
    to.params.dirTrail = "";
    next({ name: "resource-page", params: { ...to.params } });
  }
  // Continue navigation
  else {
    next();
  }
});

// Remove /?<param>=... from the URL
export const removeQueryParameterFromUrl = function (
  route: RouteLocationNormalized,
  param: string,
) {
  // Clone the current query parameters
  const query = { ...route.query };

  // Check if the parameter exists and remove it if it does
  if (query[param] !== null && query[param] !== undefined) {
    delete query[param];

    router.push({ path: route.path, query: query });
  }
};

export const navigateToProject = function (project: ProjectDto | null) {
  if (project && project.slug) {
    // Navigate to that project-page
    router.push({
      name: "project-page",
      params: {
        slug: project.slug,
      },
    });
  } else {
    // Navigate to home
    router.push({ name: "home" });
  }
};
