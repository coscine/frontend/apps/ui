// typings.d.ts or router.ts
import "vue-router";
import type { LocaleMessages } from "vue-i18n";

declare module "vue-router" {
  interface RouteMeta {
    // is optional
    requiresAdmin?: boolean;
    requiresAuth?: boolean;
    // must be declared by every route
    breadCrumb?: string;
    default?: string;
    i18n?: LocaleMessages;
  }
}
