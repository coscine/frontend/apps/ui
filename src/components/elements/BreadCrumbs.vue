<template>
  <b-breadcrumb class="breadcrumbs">
    <!-- Home -->
    <b-breadcrumb-item :to="{ name: 'home' }" :active="crumbs.length === 0">
      <i-bi-house-fill v-if="crumbs.length === 0" aria-hidden="true" />
      <i-bi-house v-else aria-hidden="true" />
      {{ $t(`breadcrumbs.home`) }}
    </b-breadcrumb-item>

    <!-- Rest -->
    <b-breadcrumb-item
      v-for="(crumb, id) in crumbs"
      :key="id"
      :to="crumb.to"
      :active="crumb.active"
    >
      {{ crumb.text }}
    </b-breadcrumb-item>
  </b-breadcrumb>
</template>

<script lang="ts">
// import the project store
import useProjectStore from "@/modules/project/store";
// import the resource store
import useResourceStore from "@/modules/resource/store";

import type { RouteLocationMatched, RouteLocationRaw } from "vue-router";
import type {
  ProjectDto,
  ResourceDto,
} from "@coscine/api-client/dist/types/Coscine.Api";
import type { RouteRecord } from "vue-router";

interface RouteLink {
  to: RouteLocationRaw;
  text: string;
  active?: boolean;
}

export default defineComponent({
  setup() {
    const projectStore = useProjectStore();
    const resourceStore = useResourceStore();

    return { projectStore, resourceStore };
  },

  computed: {
    crumbs(): RouteLink[] {
      // Get the relevant path from the route (ignores everything after "/-")
      const splitUrl = this.$route.path.split("/-");
      const relevantPath = splitUrl[0];
      // Get the additional folders if in RCV
      const additionalFolders = splitUrl[1]
        ? decodeURIComponent(splitUrl[1]).substring(1)
        : "";
      // Filter out empty paths and the project and resource path
      let pathArray = this.filterPaths(relevantPath.split("/"));
      pathArray = this.includeRootPath(pathArray);
      pathArray.push(...this.filterPaths(additionalFolders.split("/")));
      let breadcrumbs = this.generateRouteLinks(pathArray);
      breadcrumbs = this.addParentProjects(breadcrumbs);
      return this.markLastActive(breadcrumbs);
    },

    parentProjects(): ProjectDto[] | null {
      return this.projectStore.currentParentProjects;
    },

    project(): ProjectDto | null {
      return this.projectStore.currentProject;
    },

    resource(): ResourceDto | null {
      return this.resourceStore.currentResource;
    },
  },
  watch: {
    crumbs() {
      let title = this.$t("breadcrumbs.home").toString();
      if (this.crumbs && this.crumbs.length) {
        title =
          this.crumbs.map((n) => n.text).at(-1) ??
          this.$t("breadcrumbs.home").toString();
      }
      if (
        this.resource &&
        this.resource.type &&
        this.resource.type.specificType
      ) {
        title = title.replace(
          `${this.$t(
            "resourceTypes." + this.resource.type.specificType + ".displayName",
          )}: `,
          "",
        );
      }
      this.setDocumentTitle(title);
    },
  },

  created() {
    this.setDocumentTitle();
  },

  methods: {
    /**
     * Set the document title.
     * @param {string} [title=""] - The title to set. If empty, the default title will be used.
     */
    setDocumentTitle(title: string = "") {
      if (title.trim() !== "") {
        document.title = this.$t("title.modified", {
          title: title,
        }).toString();
      } else {
        document.title = this.$t("title.default").toString();
      }
    },

    /**
     * Filters out unwanted paths from the given array.
     * @param {string[]} paths - The array of paths.
     * @returns {string[]} - The filtered array of paths.
     */
    filterPaths(paths: string[]): string[] {
      return paths.filter(
        (path) =>
          path.trim() !== "" &&
          path.trim() !== "p" &&
          path.trim() !== "r" &&
          path.trim() !== "-",
      );
    },

    /**
     * Adds a root path if the path array contains only one element.
     * @param {string[]} pathArray - The array of paths.
     * @returns {string[]} - The array of paths possibly including the root.
     */
    includeRootPath(pathArray: string[]): string[] {
      if (pathArray.length === 1) {
        pathArray.unshift("");
      }
      return pathArray;
    },

    /**
     * Generates RouteLink objects based on the path array.
     * @param {string[]} pathArray - The array of paths.
     * @returns {RouteLink[]} - The array of RouteLink objects.
     */
    generateRouteLinks(pathArray: string[]): RouteLink[] {
      let lastRouteMatched: RouteLocationMatched | null = null;
      let curAppendedDirTrail = "";
      return pathArray.reduce((breadcrumbArray: RouteLink[], path, idx) => {
        if (path === "") return breadcrumbArray;
        const routeMatched = this.$route.matched[idx];
        if (routeMatched) {
          // Only fill curAppendedDirTrail if there is a dirTrail property in the route
          if (routeMatched.path.includes(":dirTrail(.*)*")) {
            curAppendedDirTrail = path + "/";
          } else {
            curAppendedDirTrail = "";
          }
          lastRouteMatched = routeMatched;
        } else {
          curAppendedDirTrail += path + "/";
        }
        // for parameter that can occur multiple times, they occur only ones in matched, so use the last found one in this scenario
        if (lastRouteMatched) {
          breadcrumbArray.push(
            this.generateRouteLink(lastRouteMatched, path, curAppendedDirTrail),
          );
        }
        return breadcrumbArray;
      }, []);
    },

    /**
     * Generates a single RouteLink object.
     * @param {RouteRecord} routeMatched - The matched route record.
     * @param {string} path - The path segment for this breadcrumb.
     * @param {string} curAppendedDirTrail - The dirTrail to append.
     * @returns {RouteLink} - The RouteLink object.
     */
    generateRouteLink(
      routeMatched: RouteRecord,
      path: string,
      curAppendedDirTrail: string,
    ): RouteLink {
      return {
        to: {
          name: routeMatched.name
            ? routeMatched.name
            : routeMatched.meta.default,
          params: {
            dirTrail: curAppendedDirTrail ? curAppendedDirTrail : undefined,
          },
        },
        text: routeMatched?.meta?.breadCrumb
          ? this.$t(`breadcrumbs.${routeMatched.meta.breadCrumb}`, {
              path: path,
              projectName: this.project ? this.project.displayName : path,
              resourceName: this.resource ? this.resourceDisplayName() : path,
            }).toString()
          : path,
      } as RouteLink;
    },

    /**
     * Retrieves the display name of the resource if available.
     * @returns {string} - The display name of the resource or an empty string.
     */
    resourceDisplayName(): string {
      if (this.resource?.displayName) {
        if (this.resource.type?.specificType) {
          // e.g. "RWTH-RDS-S3: My Resource Name"
          return `${this.$t(
            "resourceTypes." + this.resource.type.specificType + ".displayName",
          )}: ${this.resource.displayName}`;
        }
        return this.resource.displayName;
      }
      return "";
    },

    /**
     * Adds parent projects to the breadcrumb array.
     * @param {RouteLink[]} breadcrumbs - The existing array of RouteLink objects.
     * @returns {RouteLink[]} - The updated array of RouteLink objects.
     */
    addParentProjects(breadcrumbs: RouteLink[]): RouteLink[] {
      if (this.project && this.parentProjects) {
        const parentBreadCrumbs: RouteLink[] = this.parentProjects
          .map((parentProject) => {
            return {
              to: {
                name: "project-page",
                params: { slug: parentProject.slug },
              },
              text: this.$t(`breadcrumbs.project.page`, {
                projectName: parentProject.displayName,
              }).toString(),
            } as RouteLink;
          })
          .reverse(); // Reverse the order
        breadcrumbs.unshift(...parentBreadCrumbs);
      }
      return breadcrumbs;
    },

    /**
     * Marks the last breadcrumb as active.
     * @param {RouteLink[]} breadcrumbs - The existing array of RouteLink objects.
     * @returns {RouteLink[]} - The updated array of RouteLink objects.
     */
    markLastActive(breadcrumbs: RouteLink[]): RouteLink[] {
      if (breadcrumbs.length > 0) {
        breadcrumbs[breadcrumbs.length - 1].active = true;
      }
      return breadcrumbs;
    },
  },
});
</script>

<style scoped>
.breadcrumbs {
  background-color: var(--static-element-bg);
  height: var(--breadcrumbs-height);
  margin-bottom: var(--breadcrumbs-margin);
  display: flex;
  align-items: center;
  vertical-align: middle;
}
.breadcrumbs :deep(.breadcrumb) {
  padding-left: 1rem;
  margin-bottom: 0rem;
  padding-bottom: 0rem;
}
.breadcrumbs :deep(.unplugin-icon-coscine) {
  width: 1.2em;
  height: 1.2em;
}
</style>
