/* Testing imports */
import { mount } from "@vue/test-utils";
import { createTestingPinia } from "@pinia/testing";

/* Tested Component */
import BreadCrumbs from "./BreadCrumbs.vue";

describe("BreadCrumbs", () => {
  /* Describe Pre-initialization steps */

  /* Description of the test */
  test("correctlyRendered", async () => {
    /* Mount the Component */
    const wrapper = mount(BreadCrumbs, {
      global: {
        plugins: [
          createTestingPinia({
            createSpy: vitest.fn,
          }),
        ],
        mocks: {
          $route: {
            path: "/p/testwithquota-23555242/r/69e149d2-74bb-41d6-9e77-ae0730a40f3a/-/logs%2FÄIMSHistory%2FÄIMSHistory%2FAIMSHistory%2F",
            matched: [
              {
                path: "p",
                name: "project-page",
              },
              {
                path: "r",
                name: "resource-page",
              },
              {
                path: ":dirTrail(.*)*",
                name: "resource-page",
              },
            ],
          },
        },
      },
    });

    expect(wrapper.vm.crumbs.length).toBe(6);
    expect(wrapper.vm.crumbs[5].active).toBe(true);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect((wrapper.vm.crumbs[4].to as any).params.dirTrail).toBe(
      "logs/ÄIMSHistory/ÄIMSHistory/",
    );
  });
});
