/* Testing imports */
import { mount } from "@vue/test-utils";
import { createTestingPinia } from "@pinia/testing";

/* Vue i18n */
import i18n, { def } from "@/plugins/vue-i18n";
i18n.global.availableLocales.forEach((locale) => {
  i18n.global.setLocaleMessage(locale, def[locale]); // default locale messages
});

/* Tested Component */
import Noc from "./Noc.vue";

import { getTestShibbolethUserState } from "@/data/mockup/testUser";
import {
  getNocMessagesResponse,
  testSystemStatusState,
} from "@/data/mockup/testSystemStatus";
import useSystemStatusStore from "@/store/systemStatus";
import { getTestMainState } from "@/data/mockup/testMain";
import { getTestLoginState } from "@/data/mockup/testLogin";

describe("Noc", async () => {
  // Create a mocked pinia instance with initial state
  const testingPinia = createTestingPinia({
    createSpy: vitest.fn,
    initialState: {
      main: await getTestMainState(),
      login: await getTestLoginState(),
      systemStatus: testSystemStatusState,
      user: getTestShibbolethUserState(),
    },
  });

  const createWrapper = () => {
    return mount(Noc, {
      global: {
        plugins: [testingPinia, i18n],
      },
    });
  };

  let wrapper: ReturnType<typeof createWrapper>;

  beforeEach(() => {
    wrapper = createWrapper();
  });

  const systemStatusStore = useSystemStatusStore(testingPinia);

  it("Should render NOC messages if they exist", async () => {
    await nextTick();

    const banners = wrapper.findAll(".alert");
    expect(banners.length).toBe(getNocMessagesResponse.length);
    const text = i18n.global.t("banner.noc.text.Disturbance").substring(0, 10);
    expect(banners.at(0)?.text()).toContain(text);
    expect(banners.at(0)?.attributes("class")).toContain("danger");
  });

  it("Should not render any messages if nocMessages array is empty", async () => {
    await nextTick();

    // Overwrite the nocMessages array with an empty array
    systemStatusStore.banner.noc.messages = [];

    await nextTick();

    expect(wrapper.findAll(".alert").length).toBe(0);

    // Restore the store's state, as tests are dependent on each other
    systemStatusStore.banner.noc.messages = getNocMessagesResponse;
  });

  it("Should call hideMessage when an alert is closed", async () => {
    await nextTick();

    const banner = wrapper.find(".alert");
    const closeButton = banner.findAll(".btn-close").at(0);
    closeButton?.trigger("click");

    expect(systemStatusStore.hideMessage).toHaveBeenCalledOnce();
  });

  it("Should use the correct variant for each message type", async () => {
    await nextTick();

    const alerts = wrapper.findAll(".alert");
    expect(alerts.at(0)?.attributes("class")).toContain("danger");
    expect(alerts.at(1)?.attributes("class")).toContain("warning");
  });

  it("Should render a link within the message if href is provided", async () => {
    await nextTick();

    const links = wrapper.findAll("a");
    expect(links.at(0)?.attributes("href")).toBe(
      "https://example.com/noc-5555",
    );
    expect(links.at(1)?.attributes("href")).toBe(
      "https://example.com/noc-5572",
    );
  });
});
