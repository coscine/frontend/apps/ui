/* Testing imports */
import { mount } from "@vue/test-utils";
import { createTestingPinia } from "@pinia/testing";

/* Vue i18n */
import i18n, { def } from "@/plugins/vue-i18n";
i18n.global.availableLocales.forEach((locale) => {
  i18n.global.setLocaleMessage(locale, def[locale]); // default locale messages
});

/* Tested Component */
import Internal from "./Internal.vue";

import { getTestShibbolethUserState } from "@/data/mockup/testUser";
import {
  getInternalMessagesResponse,
  testSystemStatusState,
} from "@/data/mockup/testSystemStatus";
import useSystemStatusStore from "@/store/systemStatus";
import { getTestMainState } from "@/data/mockup/testMain";
import { getTestLoginState } from "@/data/mockup/testLogin";

describe("Internal", async () => {
  // Create a mocked pinia instance with initial state
  const testingPinia = createTestingPinia({
    createSpy: vitest.fn,
    initialState: {
      main: await getTestMainState(),
      login: await getTestLoginState(),
      systemStatus: testSystemStatusState,
      user: getTestShibbolethUserState(),
    },
  });

  const createWrapper = () => {
    return mount(Internal, {
      global: {
        plugins: [testingPinia, i18n],
      },
    });
  };

  let wrapper: ReturnType<typeof createWrapper>;

  beforeEach(() => {
    // shallowMount does not work here!
    wrapper = createWrapper();
  });

  const systemStatusStore = useSystemStatusStore(testingPinia);

  vi.mock("vue-i18n", () => ({
    useI18n: () => ({
      locale: {
        value: "en",
      },
      t: (key: string) => key,
      d: (key: string) => key,
    }),
  }));

  it("Should render internal messages if they exist", async () => {
    await nextTick();

    expect(wrapper.findAll(".alert").length).toBe(1);
    expect(wrapper.text()).toEqual(getInternalMessagesResponse[0].body?.en);
  });

  it("Should not render any messages if internalMessages array is empty", async () => {
    await nextTick();

    // Overwrite the internalMessages array with an empty array
    systemStatusStore.banner.internal.messages = [];

    await nextTick();

    expect(wrapper.findAll(".alert").length).toBe(0);

    // Restore the store's state, as tests are dependent on each other
    systemStatusStore.banner.internal.messages = getInternalMessagesResponse;
  });

  it("Should use the correct variant for each message type", async () => {
    await nextTick();

    const alerts = wrapper.findAll(".alert");
    expect(alerts.at(0)?.attributes("class")).toContain("info");
  });

  it("Should call hideMessage when an alert is closed", async () => {
    await nextTick();

    const banner = wrapper.find(".alert");
    const closeButton = banner.findAll(".btn-close").at(0);
    closeButton?.trigger("click");

    expect(systemStatusStore.hideMessage).toHaveBeenCalledOnce();
  });

  it("Should not display hidden messages", async () => {
    await nextTick();

    systemStatusStore.banner.hidden = [getInternalMessagesResponse[0].id!];

    await nextTick();

    expect(wrapper.findAll(".alert").length).toBe(0);
  });
});
