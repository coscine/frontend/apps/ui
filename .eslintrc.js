module.exports = {
  root: true,
  env: {
    node: true,
    es2021: true,
  },
  ignorePatterns: ["node_modules", "build", "coverage", "components.d.ts", "Multiselect.vue"],
  plugins: ["eslint-comments", "functional"],
  extends: [
    "plugin:vue/recommended",
    "eslint:recommended",
    "@vue/typescript/recommended",
    "@vue/eslint-config-prettier",
    "@vue/eslint-config-typescript/recommended",
  ],
  parserOptions: {
    ecmaVersion: 2020,
  },
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
    "@typescript-eslint/explicit-module-boundary-types": "off",
      "eslint-comments/disable-enable-pair": [
        "error",
        { "allowWholeFile": true }
      ],
    "@typescript-eslint/no-unused-vars": ["warn", { "argsIgnorePattern": "^_" }], // will only ignore variables that start with an underscore _
    "@typescript-eslint/no-inferrable-types": "off", // this enforces the removal of the type declarations for variables that are initialized with a value (e.g. let foo: string = "foo")
    "vue/multi-word-component-names": "off",
    "vue/no-v-model-argument": "off"
  },
}
