/// <reference types="vitest/config" />
import { defineConfig, type UserConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import path from "node:path";

import { nodePolyfills } from "vite-plugin-node-polyfills";
import WindiCSS from "vite-plugin-windicss";
import Components from "unplugin-vue-components/vite";
import AutoImport from "unplugin-auto-import/vite";
import Icons from "unplugin-icons/vite";
import IconsResolver from "unplugin-icons/resolver";
import { BootstrapVueNextResolver } from "bootstrap-vue-next";

// Define the configuration with proper typing
const config: UserConfig = defineConfig({
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "src"),
    },
    dedupe: ["vue"],
  },

  define: {
    "process.env.MOCKUP": process.env.MOCKUP,
  },

  build: {
    commonjsOptions: {
      strictRequires: true,
    },
    target: "esnext",
    rollupOptions: {
      output: {
        manualChunks: {
          "@coscine/api-client": ["@coscine/api-client"],
          "@coscine/form-generator": ["@coscine/form-generator"],
          "bootstrap-vue-next": ["bootstrap-vue-next"],
          "rdf-parse": ["rdf-parse"],
        },
      },
    },
  },

  plugins: [
    nodePolyfills(),
    vue(),
    WindiCSS(),
    Components({
      dts: "src/components.d.ts",
      include: [/\.vue$/, /\.vue\?vue/],
      exclude: [
        /[\\/]node_modules[\\/]/,
        /[\\/]\.git[\\/]/,
        /[\\/]\.nuxt[\\/]/,
      ],
      resolvers: [
        BootstrapVueNextResolver(),
        IconsResolver(),
        (componentName) => {
          if (componentName == "FormGenerator") {
            return { name: "default", from: "@coscine/form-generator" };
          }
        },
      ],
    }),
    AutoImport({
      imports: ["pinia", "vue", "vue-i18n", "vue-router", "@vueuse/core"],
      dts: "src/auto-imports.d.ts",
      eslintrc: {
        enabled: true, // <-- this
      },
      resolvers: [IconsResolver()],
      dirs: ["./src"],
    }),
    Icons({
      compiler: "vue3",
      autoInstall: true,
      scale: 1,
      defaultClass: "unplugin-icon unplugin-icon-coscine",
    }),
  ],

  appType: "spa",

  server: {
    host: true,
    port: 9234,
    watch: {
      // Use polling to avoid "too many open files" (EMFILE) errors caused by hitting system file watcher limits.
      // Ignoring node_modules and .git to reduce the load and improve stability.
      usePolling: true,
      interval: 300,
      ignored: ["**/node_modules/**", "**/.git/**"],
    },
  },

  test: {
    globals: true,
    environment: "happy-dom",
    setupFiles: [path.resolve(__dirname, "test/setup.ts")],
    reporters: "dot",
  },
});

export default config;
